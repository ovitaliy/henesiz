package com.amberfog.countryflags;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.henesiz.R;
import com.henesiz.listener.SimpleTextListener;
import com.henesiz.util.ConverterUtil;
import com.henesiz.util.UiUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class PhoneView extends LinearLayout {

    protected SparseArray<ArrayList<Country>> mCountriesMap = new SparseArray<>();

    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected AutoCompleteTextView mPhoneCodeView;

    protected EditText mPhoneNumberView;
    protected CountryAdapter mAdapter;

    private EnterPhoneListener mEnterPhoneListener;

    private volatile Country __mSelectedCountry;

    public PhoneView(Context context) {
        this(context, null);
    }

    public PhoneView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(HORIZONTAL);

        View.inflate(context, R.layout.view_phone, this);

        if (isInEditMode())
            return;

        initCodes(context);

        mPhoneCodeView = (AutoCompleteTextView) findViewById(R.id.phone_code);
        mPhoneCodeView.setOnItemClickListener(mOnItemSelectedListener);
        mPhoneCodeView.addTextChangedListener(phoneCodeWatcher);
        // mPhoneCodeView.setImeActionLabel(null, R.id.phone_number);
        mPhoneCodeView.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        mPhoneCodeView.setOnEditorActionListener(mCodeOnEditorActionListener);

        mAdapter = new CountryAdapter(context);

        mPhoneCodeView.setAdapter(mAdapter);

        mPhoneNumberView = (EditText) findViewById(R.id.phone_number);

        mPhoneNumberView.addTextChangedListener(phoneNumberWatcher);
        mPhoneNumberView.setOnEditorActionListener(mCodeOnEditorActionListener);

        setHints(-1);
    }

    private View mRequireFocusView;

    private RequestFocusRunnable mRequestFocusRunnable;

    public synchronized void focus() {
        if (mPhoneCodeView.getAdapter() == null || mPhoneCodeView.getAdapter().getCount() == 0) {
            return;
        }

        mRequireFocusView = getSelectedCountry() == null ? mPhoneCodeView : mPhoneNumberView;

        if (mRequestFocusRunnable != null) {
            mRequestFocusRunnable.canceled = true;
        }
        mRequestFocusRunnable = new RequestFocusRunnable();
        postDelayed(mRequestFocusRunnable, 100);
    }

    private class RequestFocusRunnable implements Runnable {

        public boolean canceled;

        private int mFocusRequestCount = 0;

        @Override
        public void run() {
            if (canceled) return;
            View focused = ((Activity) getContext()).getCurrentFocus();

            if (focused == null || !focused.equals(mRequireFocusView)) {
                mRequireFocusView.requestFocus();
                mRequireFocusView.sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_CLICKED);

                // Show keyboard
                InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(mRequireFocusView, 0);

                if (mFocusRequestCount++ < 4) {
                    postDelayed(this, 100);
                }

            }
        }
    }

    private void phoneCodeSetData(Country country) {
        if (country != null) {
            setHints(country.getCountryCode());
            mPhoneCodeView.setText(country.getCountryCodeStr());

            setCountryIcon(country.getResId());
        } else {
            setHints(-1);
            setCountryIcon(null);
        }
    }

    private void setCountryIcon(String icon) {
        if (icon == null) {
            mPhoneCodeView.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    null,
                    null
            );
        } else {
            ImageLoader.getInstance().loadImage(icon, new DisplayImageOptions.Builder().build(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                    float w = ConverterUtil.dpToPix(getContext(), 41);
                    float h = w * 0.6f;

                    Bitmap bitmap = Bitmap.createScaledBitmap(loadedImage, (int) w, (int) h, true);

                    mPhoneCodeView.setCompoundDrawablesWithIntrinsicBounds(
                            null,
                            null,
                            new BitmapDrawable(getResources(), bitmap),
                            null
                    );
                }
            });
        }
    }

    public String getPhoneNumber() {
        return (mPhoneCodeView.getText() + "" + mPhoneNumberView.getText()).replaceAll("[^0-9.]", "");
    }

    public int getCountryId() {
        try {
            return Integer.parseInt(mPhoneCodeView.getText().toString());
        } catch (Exception ignore) {
            return 0;
        }
    }


    protected AdapterView.OnItemClickListener mOnItemSelectedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Country country = (Country) mPhoneCodeView.getAdapter().getItem(position);
            setSelectedCountry(country);
            phoneCodeSetData(country);
            focus();
        }
    };

    public boolean validate() {
        boolean isValid = false;
        String number = "+" + getPhoneNumber();

        try {
            Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(number, null);

            isValid = mPhoneNumberUtil.isValidNumber(p);
        } catch (NumberParseException ignore) {
            ignore.printStackTrace();
        }

        if (mEnterPhoneListener != null) {
            mEnterPhoneListener.onNumberIsValid(isValid);
        }

        return isValid;
    }

    private TextWatcher phoneCodeWatcher = new SimpleTextListener() {
        private boolean mItselfChange;

        @Override
        public void afterTextChanged(Editable s) {
            if (mItselfChange)
                return;

            String code = mPhoneCodeView.getEditableText().toString();
            if (mPhoneCodeView.getEditableText() instanceof Country) {
                validate();
                return;
            }

            int codeValue = -1;
            try {
                codeValue = Integer.parseInt(code.replaceAll("[^0-9.]", ""));
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }

            if (!code.startsWith("+")) {
                mItselfChange = true;
                s.insert(0, "+");
            }

            if (getSelectedCountry() == null || codeValue != getSelectedCountry().getCountryCode()) {
                setSelectedCountry(null);
                new Handler().postDelayed(new Runnable() {

                    public void run() {
                        if (mPhoneCodeView.getWindowToken() != null)
                            mPhoneCodeView.showDropDown();
                    }

                }, 100L);
                phoneCodeSetData(null);
            }

            mItselfChange = false;

            validate();
        }
    };

    private boolean validateCode(String codeString) {
        for (int i = 0; i < mAdapter.getCount(); i++) {
            if (mAdapter.getItem(i).getCountryCodeStr().equals(codeString))
                return true;
        }
        return false;
    }

    private TextWatcher phoneNumberWatcher = new SimpleTextListener() {
        private boolean mItselfChange;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mItselfChange || count == 0)
                return;

            mItselfChange = true;

            int code = 1;
            try {
                code = Integer.parseInt(mPhoneCodeView.getText().toString().replaceAll("[^0-9.]", ""));
            } catch (Exception ignore) {
            }

            StringBuilder phoneBuilder = new StringBuilder();
            String phoneNumber = getPhoneNumber();
            String hint = getHint(code);

            while (hint.length() > 0 && phoneNumber.length() > 0) {
                char hintChar = hint.charAt(0);
                if (Character.isDigit(hintChar)) {
                    phoneBuilder.append(phoneNumber.charAt(0));
                    phoneNumber = phoneNumber.substring(1);
                } else {
                    phoneBuilder.append(hintChar);
                }
                hint = hint.substring(1);
            }

            phoneNumber = phoneBuilder.toString();

            String[] phoneData = phoneNumber.split(" ");

            // mPhoneCodeView.setText(phoneData[0]);
            if (phoneData.length > 1) {
                int selection = mPhoneNumberView.getSelectionStart();
                String selectedChar = null;
                if (selection > 0) {
                    String originalPhoneNumber = mPhoneNumberView.getText().toString();
                    int index = Math.min(selection - 1, originalPhoneNumber.length() - 1);
                    selectedChar = originalPhoneNumber.substring(index, index + 1);
                }
                mPhoneNumberView.setText(phoneData[1]);
                if (selectedChar != null && mPhoneNumberView.hasFocus())
                    mPhoneNumberView.setSelection(phoneData[1].indexOf(selectedChar, selection - 1) + 1);
            } else {
                mPhoneNumberView.setText("");
            }

            mItselfChange = false;

            validate();
        }

    };

    private void setHints(int countryId) {
        String[] hints = getHint(countryId).split(" ");

        mPhoneCodeView.setHint(hints[0]);
        mPhoneNumberView.setHint(hints[1]);
    }

    private String getHint(int countryId) {
        String hint = "+7 (301)123-4567";

        if (countryId != -1) {
            String region = mPhoneNumberUtil.getRegionCodeForCountryCode(countryId);
            Phonenumber.PhoneNumber pp = mPhoneNumberUtil.getExampleNumberForType(region, PhoneNumberUtil.PhoneNumberType.MOBILE);

            if (pp != null) {
                hint = "+" + pp.getCountryCode() + " ";

                String nationalNumber = String.valueOf(pp.getNationalNumber());
                while (nationalNumber.length() < 9) {//FIXME
                    nationalNumber += "0";
                }

                StringBuilder numberBuilder = new StringBuilder(nationalNumber);
                numberBuilder.insert(numberBuilder.length() - 4, "-")
                        .insert(0, "(")
                        .insert(Math.max(3, numberBuilder.length() - 8), ")");

                hint += numberBuilder.toString();
            }
        }
        return hint;
    }

    private OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (v.equals(mPhoneCodeView)) {
                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {

                        public void run() {
                            if (mPhoneCodeView.getWindowToken() != null)
                            mPhoneCodeView.showDropDown();
                        }

                    }, 100L);
                }

                if (!hasFocus && getSelectedCountry() == null) {
                    focus();
                }
            } else if (v.equals(mPhoneNumberView)) {
                if (hasFocus) {
                    if (!validateCode(mPhoneCodeView.getText().toString())) {
                        setSelectedCountry(null);
                        focus();
                    }
                }
            }

        }
    };


    private boolean send() {
        if (validate()) {
            if (mEnterPhoneListener != null) {
                mEnterPhoneListener.onSend(getPhoneNumber());
            }
            return true;
        } else {
//            Toast.makeText(getContext(), R.string.verification_error_invalid_phone_number, Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    public void setOnEnterPhoneListener(EnterPhoneListener onEnterPhoneListener) {
        mEnterPhoneListener = onEnterPhoneListener;
    }

    private TextView.OnEditorActionListener mCodeOnEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (v.equals(mPhoneCodeView) && actionId == EditorInfo.IME_ACTION_NEXT) {
                String code = mPhoneCodeView.getText().toString();

                if (getSelectedCountry() == null || !getSelectedCountry().getCountryCodeStr().equals(code)) {
                    Country foundCountry = null;

                    int n = mPhoneCodeView.getAdapter().getCount();
                    for (int i = 0; i < n; i++) {
                        Country country = (Country) mPhoneCodeView.getAdapter().getItem(i);

                        String countryCode = country.getCountryCodeStr();
                        if (code.equals(countryCode)) {
                            foundCountry = country;
                            break;
                        }
                    }

                    if (foundCountry == null) {
                        setCountryIcon(null);
                        setSelectedCountry(null);
                    } else {
                        setSelectedCountry(foundCountry);
                        phoneCodeSetData(foundCountry);
                        focus();
                        return true;
                    }
                } else {
                    focus();
                    return true;
                }
            }

            if (v.equals(mPhoneNumberView) && actionId == EditorInfo.IME_ACTION_DONE) {
                if (send()) {
                    UiUtil.hideKeyboard(mPhoneNumberView);
                }
                return true;
            }
            return false;
        }
    };


    public interface EnterPhoneListener {
        void onSend(String phoneNumber);

        void onNumberIsValid(boolean isValid);
    }

    //----------------------------
    protected void initCodes(Context context) {
        new AsyncPhoneInitTask(context, null).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<Country>> {

        private Context mContext;
        private String mPhoneNumber;

        public AsyncPhoneInitTask(Context context, String phoneNumber) {
            mContext = context;
            mPhoneNumber = phoneNumber;
        }

        @Override
        protected ArrayList<Country> doInBackground(Void... params) {
            ArrayList<Country> data = new ArrayList<>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                // do reading, usually loop until end of file reading
                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    //process line
                    Country c = new Country(mContext, line, i);
                    data.add(c);
                    ArrayList<Country> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            if (!TextUtils.isEmpty(mPhoneNumber)) {
                return data;
            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = 7;
            try {
                code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            } catch (Exception ignore) {
            }
            ArrayList<Country> list = mCountriesMap.get(code);
            if (list != null) {
                for (Country c : list) {
                    if (c.getPriority() == 0) {
                        __mSelectedCountry = c;
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<Country> data) {
            mAdapter.addAll(data);
            if (getSelectedCountry() != null) {
                phoneCodeSetData(getSelectedCountry());
            }
            focus();

            mPhoneCodeView.setOnFocusChangeListener(onFocusChangeListener);
            mPhoneNumberView.setOnFocusChangeListener(onFocusChangeListener);
        }
    }

    //---

    private Country getSelectedCountry() {
        return __mSelectedCountry;
    }

    private void setSelectedCountry(Country selectedCountry) {
        __mSelectedCountry = selectedCountry;
    }
}
