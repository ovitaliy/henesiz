package com.henesiz;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.henesiz.rest.service.AppRetrofitSpiceService;
import com.henesiz.util.PrefHelper;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.octo.android.robospice.SpiceManager;

import io.fabric.sdk.android.Fabric;

public class App extends Application {
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static int WIDTH;
    public static int WIDTH_WITHOUT_MARGINS;
    public static int HEIGHT;
    public static int MARGIN;
    public static int IMAGE_BORDER;
    public static int IMAGE_SMALL_WIDTH;

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());
        instance = this;
        PrefHelper.init(this);

        initImageLoader();
    }

    private void initImageLoader() {
        DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPoolSize(1)
                .threadPriority(Thread.MIN_PRIORITY)
                .defaultDisplayImageOptions(displayImageOptions)
                .build();
        ImageLoader.getInstance().init(config);
    }

    private static SpiceManager sSpiceManager;

    public static SpiceManager getSpiceManager() {
        if (sSpiceManager == null || !sSpiceManager.isStarted()) {
            sSpiceManager = new SpiceManager(AppRetrofitSpiceService.class);
            sSpiceManager.start(instance);
        }
        return sSpiceManager;
    }

    public static void showNoNetworkErrorToast(Context context) {
        Toast.makeText(context, "No Internet connection", Toast.LENGTH_LONG).show();
    }
}
