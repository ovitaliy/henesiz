package com.henesiz;

import com.henesiz.model.UserInfo;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.GsonHelper;
import com.henesiz.util.PrefHelper;

/**
 * Created by Deni on 03.08.2015.
 */
public final class AppUser {
    private static final String USER_SERIALIZATION_NAME = "app_user";
    private static UserInfo mUserInfoCached;

    public static synchronized void set(UserInfo userInfo) {
        if (userInfo != null) {
            PrefHelper.setStringPref(USER_SERIALIZATION_NAME, GsonHelper.GSON.toJson(userInfo));
            AnalyticsHelper.identifyUser();
        }
        mUserInfoCached = userInfo;
    }

    public static synchronized UserInfo get() {
        if (mUserInfoCached == null) {
            String userString = PrefHelper.getStringPref(USER_SERIALIZATION_NAME);
            if (userString != null) {
                mUserInfoCached = GsonHelper.GSON.fromJson(userString, UserInfo.class);
            }
            if (mUserInfoCached == null) {
                mUserInfoCached = new UserInfo();
            }
        }
        return mUserInfoCached;
    }

    public static synchronized void clear() {
        PrefHelper.setStringPref(USER_SERIALIZATION_NAME, null);
        mUserInfoCached = null;
    }

    public static int getUid() {
        UserInfo user = get();
        int uid = 0;
        if (user != null) {
            uid = user.getId();
        }
        return uid;
    }
}
