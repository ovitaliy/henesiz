package com.henesiz.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;

import com.henesiz.R;
import com.henesiz.fragment.InfoFragment;

/**
 * Displays agreement
 */
public class AgreementActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        startFragment(InfoFragment.newInstance(), false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }
}
