package com.henesiz.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.dialogs.ErrorDialogFragment;
import com.henesiz.dialogs.ShareDialogFragment;
import com.henesiz.enums.ServerError;
import com.henesiz.enums.VideoType;
import com.henesiz.events.ServerErrorEvent;
import com.henesiz.events.ShowProgressDialogEvent;
import com.henesiz.events.UpdateFlowEvent;
import com.henesiz.events.VideoUploadedEvent;
import com.henesiz.fragment.BaseFragment;
import com.henesiz.fragment.FlowListFragment;
import com.henesiz.listener.OnUpdateVideoListener;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.listener.PayForVideoRequestListener;
import com.henesiz.rest.model.BaseCoinsResponse;
import com.henesiz.rest.request.video.MarkVideoRequest;
import com.henesiz.rest.request.video.PayForVideoRequest;
import com.henesiz.rest.service.AppRetrofitSpiceService;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.DisplayInfo;
import com.henesiz.util.NetworkUtil;
import com.henesiz.util.PrefHelper;
import com.henesiz.util.Toast;
import com.henesiz.util.VideoUploadHelper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.CachedSpiceRequest;
import com.octo.android.robospice.request.listener.SpiceServiceListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

/**
 * Activity that should be extended to have ability use base features common for all activities
 */
public abstract class BaseActivity extends ActionBarActivity implements SpiceContext, Const, OnUpdateVideoListener {
    ErrorDialogFragment mErrorDialog;

    Toolbar mToolbar;

    SpiceServiceListener mSpiceServiceListener;

    public static final int REQ_CODE_VIDEO_CREATED = 1231;
    public static final int REQ_CODE_AVATAR_CREATED = 1232;

    private static SpiceManager mSpiceManager = new SpiceManager(AppRetrofitSpiceService.class);

    /**
     * Receiver for detecting network state changes
     */
    NetworkChangeReceiver mNetworkChangeReceiver;

    public Toolbar getActionBarToolbar() {
        return mToolbar;
    }

    /**
     * Starts fragment f with tag = className
     *
     * @param f              - Fragment that should be started
     * @param addToBackStack - add to backstack
     * @param popBackStack   - clear backstack before adding fragment
     */
    public void startFragment(Fragment f, boolean addToBackStack, boolean popBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        String tag = f.getClass().getSimpleName();

        if (popBackStack) {
            manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            if (manager.findFragmentByTag(tag) != null) {
                return;
            }
        }

        FragmentTransaction ft = manager.beginTransaction();
        ft.replace(R.id.container, f, tag);
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }

        ft.commitAllowingStateLoss();
    }

    public void startFragment(Fragment f, boolean addToBackStack) {
        startFragment(f, addToBackStack, false);
    }

    /**
     * @param title - name of screen
     */
    public void setActionBarTitle(String title) {
        if (mToolbar != null) {
            TextView titleView = (TextView) mToolbar.findViewById(R.id.title);
            if (!TextUtils.isEmpty(title)) {
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(title);
            } else {
                titleView.setVisibility(View.GONE);
            }
        }
    }


    /**
     * @param offlineMode - if true, toolbar title will be updated with offline mode message
     */
    public void setActionBarOfflineMode(boolean offlineMode) {
        if (mToolbar != null) {
            TextView offlineView = (TextView) mToolbar.findViewById(R.id.offline_mode);
            if (offlineMode) {
                offlineView.setVisibility(View.VISIBLE);
            } else {
                offlineView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
        try {
            unregisterReceiver(mNetworkChangeReceiver);
            mNetworkChangeReceiver = null;
        } catch (Exception e) {
        }
        //should be removed to prevent memory leak
        mSpiceManager.removeSpiceServiceListener(mSpiceServiceListener);
        mSpiceServiceListener = null;
    }

    /*Uses for trigger showing progress dialog from any thread*/
    public void showProgressDialog(boolean show) {
        EventBus.getDefault().post(new ShowProgressDialogEvent(show));
    }

    /*Since all videos saved to database before uploading to server in order to
    * proper handling any connection issues, this method should be called to
    * check if any videos are pending and upload them*/
    void checkVideoToUpload() {
        VideoUploadHelper.uploadPendingVideos();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onVideoUploadedEvent(VideoUploadedEvent event) {
        Log.d("VUPL", "onVideoUploadedEvent()");
        checkVideoToUpload();
        EventBus.getDefault().removeStickyEvent(event);
        if (event.isSuccess()) {
            EventBus.getDefault().post(new UpdateFlowEvent());

            if (!event.isAvatar()) {
                new ShareDialogFragment.Builder()
                        .setCanBeClosed(true)
                        .setLink(event.getShortUrl())
                        .setVideoId(event.getVideoId())
                        .build()
                        .show(getSupportFragmentManager(), ShareDialogFragment.TAG);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNetworkChangeReceiver = new NetworkChangeReceiver();
        EventBus.getDefault().register(this);
        registerReceiver(mNetworkChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        // I think it's not good idea to lock screen while request is working
       /* //uses for show/hide progress dialog for server requests
        mSpiceServiceListener = new SpiceServiceListener() {
            @Override
            public void onRequestSucceeded(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(false);
            }

            @Override
            public void onRequestFailed(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(false);
            }

            @Override
            public void onRequestCancelled(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(false);
            }

            @Override
            public void onRequestProgressUpdated(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onRequestAdded(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {
                showProgressDialog(true);
            }

            @Override
            public void onRequestAggregated(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onRequestNotFound(CachedSpiceRequest<?> request, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onRequestProcessed(CachedSpiceRequest<?> cachedSpiceRequest, RequestProcessingContext requestProcessingContext) {

            }

            @Override
            public void onServiceStopped() {
                showProgressDialog(false);
            }
        };
        mSpiceManager.addSpiceServiceListener(mSpiceServiceListener);*/
    }

    public void logout() {
        mSpiceManager.cancelAllRequests();
        AppUser.clear();
        PrefHelper.setStringPref(Const.PREF_TOKEN, null);
        PrefHelper.setBooleanPref(Const.PREF_REG, false);
        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
        finish();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onServerErrorEvent(ServerErrorEvent event) {
        if (mErrorDialog != null && mErrorDialog.isVisible()) {
            mErrorDialog.dismiss();
        }
        int[] codes = event.getCodes();
        String[] messages = event.getMessages();

        if (codes != null) {
            int errorCode = -1;
            String errorMessage = "";
            for (int i = 0; i < codes.length; i++) {
                int code = codes[i];
                if (code == ServerError.WRONG_ACCESS_TOKEN.getCode()) {

                    logout();
                    return;

                } else {

                    errorCode = code;
                    if (messages != null && messages.length > i) {
                        errorMessage = messages[i];
                    }

                }
            }
            if (!TextUtils.isEmpty(errorMessage)) {
                if (getSupportFragmentManager().findFragmentByTag("error") == null) {
                    mErrorDialog = ErrorDialogFragment.newInstance(errorCode, errorMessage);
                    mErrorDialog.show(getSupportFragmentManager(), "error");
                }

            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQ_CODE_VIDEO_CREATED:
                    if (data != null) {
                        if (this instanceof MainActivity) {
                            openFlowList(data);
                        }
                    }
                    break;
            }
        }
    }

    private void openFlowList(Intent data) {
        HashMap<String, Object> params = (HashMap<String, Object>) data.getSerializableExtra(Params.PARAMS);
        VideoType videoType = (VideoType) data.getSerializableExtra(Const.Params.TYPE);
        int replyId = data.getIntExtra(Const.Params.REPLY_ID, 0);
        FlowListFragment fragment = new FlowListFragment.Builder(videoType)
                .setParams(params)
                .setReplyId(replyId)
                .build();

        startFragment(fragment, true);
    }

    /**
     * Inits device-depended constants
     */
    private void initConstants() {
        if (App.WIDTH == 0) {
            DisplayInfo displayInfo = new DisplayInfo(this);
            App.WIDTH = displayInfo.getScreenWidth();
            App.HEIGHT = displayInfo.getScreenHeight();

            App.MARGIN = getResources().getDimensionPixelSize(R.dimen.mrg_normal);
            App.WIDTH_WITHOUT_MARGINS = App.WIDTH - App.MARGIN * 2;

            App.IMAGE_BORDER = getResources().getDimensionPixelSize(R.dimen.stream_item_small_border);
            App.IMAGE_SMALL_WIDTH = (int) (App.WIDTH / 4.4 - App.IMAGE_BORDER * 4f);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initConstants();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public SpiceManager getSpiceManager() {
        if (!mSpiceManager.isStarted()) {
            mSpiceManager.start(this);
        }
        return mSpiceManager;
    }

    private class NetworkChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean offlineMode = NetworkUtil.isOfflineMode();
            setActionBarOfflineMode(offlineMode);
        }
    }

    public void navigateBack() {
        try {
            super.onBackPressed();
        } catch (Exception ex) {
            Crashlytics.log("crash on navigate back" + this.getClass().toString());
            throw ex;
        }
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.container);
        if (f != null && f instanceof BaseFragment) {
            ((BaseFragment) f).onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onVideoPay(final VideoType videoType, int videoId) {
        if (videoType != null) {
            switch (videoType) {
                case CROWD_ASK:
                case CROWD_GIVE:
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.CROWD_DONATE);
                    break;
            }
        }
        getSpiceManager().execute(
                new PayForVideoRequest(videoId),
                new PayForVideoRequestListener(videoType, videoId) {
                    @Override
                    public void onRequestSuccess(BaseCoinsResponse response) {
                        super.onRequestSuccess(response);
                        if (response != null && response.isSuccess()) {
                            showPayMessage(videoType, response.getCoinsChange());
                        }
                    }
                });
    }

    @Override
    public void onVideoMark(int videoId, int mark) {
        getSpiceManager().execute(
                new MarkVideoRequest(videoId, mark),
                new BaseRequestListener());
    }

    public void showPayMessage(VideoType type, int amount) {
        String text = "";
        switch (type) {
            case CROWD_GIVE:
                text = getString(R.string.message_crowd_share);
                break;
            case CROWD_ASK:
                text = getString(R.string.message_crowd, String.valueOf(Math.abs(amount)));
                break;
        }
        Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
    }
}