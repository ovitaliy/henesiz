package com.henesiz.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.fragment.video.AudioFragment;
import com.henesiz.fragment.video.FilterFragment;
import com.henesiz.fragment.video.FramePreviewFragment;
import com.henesiz.fragment.video.VideoPreviewFragment;
import com.henesiz.fragment.video.VideoRecordFragment;
import com.henesiz.model.Category;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.model.UploadingMedia;
import com.henesiz.model.Video;
import com.henesiz.model.VideoInfo;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.request.video.VideoUploadRequest;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.FilePathHelper;
import com.henesiz.util.NetworkUtil;
import com.henesiz.view.NewVideoView;
import com.humanet.filters.videofilter.BaseFilter;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Captures and previews video
 */
public class CaptureActivity extends BaseActivity implements
        NewVideoView,
        Const,
        VideoRecordFragment.OnVideoRecordCompleteListener,
        VideoPreviewFragment.OnVideoCommandSelected {

    public static void startNewInstance(Activity context) {
        Intent intent = new Intent(context, CaptureActivity.class);
        context.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    /**
     * @param replyForVideo - to which video response will be captured
     */
    public static void startNewInstance(Activity context, Video replyForVideo) {
        Intent intent = new Intent(context, CaptureActivity.class);
        intent.putExtra(Params.REPLY_VIDEO, replyForVideo);
        context.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewInstance(Activity activity, VideoType videoType) {
        Intent intent = new Intent(activity, CaptureActivity.class);
        intent.putExtra(Params.TYPE, videoType);
        activity.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    public static void startNewAvatarInstance(Activity activity) {
        Intent intent = new Intent(activity, CaptureActivity.class);
        intent.putExtra(Params.TYPE, VideoType.AVATAR);
        intent.putExtra(Params.AVATAR, true);
        activity.startActivityForResult(intent, REQ_CODE_VIDEO_CREATED);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_video);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }

        if (savedInstanceState == null) {
            NewVideoInfo.init(getVideoType());
        } else {
            VideoInfo video = (VideoInfo) savedInstanceState.get(Params.VIDEO);
            NewVideoInfo.set(video);
        }

        Video videoToReply = getVideoToReply();
        boolean isAvatar = isAvatar();

        if (NewVideoInfo.isVideoRecorded()) {
            openPreview();
        } else {
            NewVideoInfo.get().setVideoType(getVideoType());
            if (isAvatar) {
                NewVideoInfo.get().setIsAvatar(true);
            } else if (videoToReply != null) {
                NewVideoInfo.get().setReplyToVideoId(videoToReply.getVideoId());
                NewVideoInfo.get().setVideoType(VideoType.getById(videoToReply.getType()));
                NewVideoInfo.get().setCategoryId(videoToReply.getCategoryId());
            }
            startFragment(VideoRecordFragment.newInstance(), false);
        }
    }

    private void setResultData() {
        Intent intent = new Intent();
        Category category = NewVideoInfo.get().getCategory();
        if (category != null) {
            intent.putExtra(Params.PARAMS, category.getParams());
        }
        intent.putExtra(Params.TYPE, NewVideoInfo.get().getVideoType());
        intent.putExtra(Params.REPLY_ID, NewVideoInfo.get().getReplyToVideoId());
        setResult(RESULT_OK, intent);
    }

    private Video getVideoToReply() {
        Video video = null;
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Params.REPLY_VIDEO)) {
            video = (Video) intent.getSerializableExtra(Params.REPLY_VIDEO);
        }
        return video;
    }

    private boolean isAvatar() {
        return getIntent().getBooleanExtra(Params.AVATAR, false);
    }

    @Override
    public VideoType getVideoType() {
        VideoType videoType = null;
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(Params.TYPE)) {
            videoType = (VideoType) intent.getSerializableExtra(Params.TYPE);
        }
        return videoType;
    }

    /**
     * Save video as pending to database. It will be uploaded to server if internet connection is available
     */
    private class SaveVideoToUpload extends AsyncTask<VideoInfo, Void, Integer> {

        @Override
        protected Integer doInBackground(VideoInfo... params) {
            VideoInfo info = params[0];
            if (info.getVideoType() != null) {
                switch (info.getVideoType()) {
                    case CROWD_GIVE:
                    case CROWD_ASK:
                        if (!info.isReply()) {
                            AnalyticsHelper.trackEvent(CaptureActivity.this, AnalyticsHelper.CROWD_CREATE);
                        }
                        break;
                    default:
                        AnalyticsHelper.trackEvent(CaptureActivity.this, AnalyticsHelper.VIDEO_RECORDING);
                        break;
                }
            }

            Video video = new Video();
            video.setCreatedAt(new Date());
            video.setMedia(video.getCreatedAt() + "." + Const.VIDEO_EXTENTION);
            video.setUploadingStatus(Video.STATUS_UPLOAD_REQUIRED);
            video.setTitle(info.getTags());
            video.setAuthorId(AppUser.getUid());
            if (AppUser.get() != null) {
                video.setAuthorName(AppUser.get().getFullName());
            }
            video.setAvailable(false);

            if (info.getVideoType() != null) {
                video.setType(info.getVideoType().getId());
            }

            video.setCategoryId(info.getCategoryId());
            video.setCost(info.getCost());
            video.setReplyId(info.getReplyToVideoId());
            video.setSmileGuessed(false);

            File fileVideo = new File(info.getVideoPath());
            File fileImage = new File(info.getImagePath());

            try {
                FileUtils.copyFile(fileVideo, new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.VIDEO_EXTENTION));
                File imageOutput = new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.IMAGE_EXTENTION);
                FileUtils.copyFile(fileImage, imageOutput);
                video.setThumbUrl(imageOutput.getAbsolutePath());

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (info.isAvatar() && AppUser.get() != null) {
                //Update cached user photo until it will be uploaded and updated on server
                AppUser.get().setPhoto(video.getThumbUrl());
            }

            ContentValues contentValues = video.toContentValues();
            contentValues.put(ContentDescriptor.Videos.Cols.IS_AVATAR, info.isAvatar() ? 1 : 0);
            Uri uri = getContentResolver().insert(ContentDescriptor.Videos.URI, contentValues);

            assert uri != null && uri.getLastPathSegment() != null;
            return Integer.parseInt(uri.getLastPathSegment());
        }

        @Override
        protected void onPostExecute(Integer id) {
            super.onPostExecute(id);

            boolean offlineMode = NetworkUtil.isOfflineMode();
            if (!offlineMode) {
                uploadVideo(id);
            }
            setResultData();
            finish();
        }
    }

    private void uploadVideo(int id) {
        App.getSpiceManager().execute(new VideoUploadRequest(id), new BaseRequestListener<UploadingMedia>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                super.onRequestFailure(spiceException);
                if (spiceException instanceof NoNetworkException)
                    App.showNoNetworkErrorToast(App.getInstance());
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Params.VIDEO, NewVideoInfo.get());
    }

    private void saveToUpload() {
        VideoInfo videoInfo = NewVideoInfo.get();
        videoInfo.setIsAvatar(isAvatar());
        new SaveVideoToUpload().execute(videoInfo);
    }

    protected void onOpenDescription() {
        startFragment(VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_DESCRIPTION), true);
    }

    @Override
    public void onOpenFramePreview() {
        startFragment(FramePreviewFragment.newInstance(), true);
    }

    @Override
    public void onOpenTrack() {
        startFragment(AudioFragment.newInstance(), true);
    }

    @Override
    public void onOpenFilter() {
        startFragment(FilterFragment.newInstance(), true);
    }

    @Override
    public void onComplete(int lastAction) {
        switch (lastAction) {
            case VideoPreviewFragment.ACTION_CREATE:
                if (!NewVideoInfo.get().isAvatar()) {
                    onOpenDescription();
                    break;
                }
            default:
                saveToUpload();
        }
    }

    @Override
    public void onVideoRecordComplete(String videoPath, int cameraId) {
        NewVideoInfo.get().setAudioPath(FilePathHelper.getAudioStreamFile().getAbsolutePath());
        NewVideoInfo.get().setFilter(new BaseFilter());
        NewVideoInfo.get().setAudioApplied(NewVideoInfo.get().getAudioPath());
        NewVideoInfo.get().setImageFilterApplied(null);
        NewVideoInfo.get().setVideoFilterApplied(null);
        NewVideoInfo.get().setCameraId(cameraId);

        int framesCount = FilePathHelper.getVideoFrameFolder().listFiles().length;
        NewVideoInfo.get().setOriginalImagePath(FilePathHelper.getVideoFrameImagePath(Math.max(framesCount / 2 - 1, 0)));
        NewVideoInfo.get().setOriginalVideoPath(videoPath);
        VideoPreviewFragment.VideoProcessTask.getInstance().mPreviewBitmap = null;

        NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());
        NewVideoInfo.grabFramesPack();

        openPreview();
    }

    protected void openPreview() {
        VideoPreviewFragment mVideoPreviewFragment = VideoPreviewFragment.newInstance(VideoPreviewFragment.ACTION_CREATE);
        startFragment(mVideoPreviewFragment, true);
    }

}
