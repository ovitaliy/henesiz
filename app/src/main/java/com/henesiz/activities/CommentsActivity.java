package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.henesiz.R;
import com.henesiz.dialogs.MoreDialogFragment;
import com.henesiz.fragment.CommentsFragment;
import com.henesiz.fragment.ProfileFragment;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Video;
import com.henesiz.view.items.VideoItemView;

/**
 * Displays comments to video
 */
public class CommentsActivity extends BaseActivity implements
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnCommentListener,
        VideoItemView.OnMoreListener, OnViewProfileListener {

    public static void startNewInstance(Context context, Video video) {
        Intent intent = new Intent(context, CommentsActivity.class);
        intent.putExtra(Params.VIDEO, video);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        Video video = (Video) getIntent().getSerializableExtra(Params.VIDEO);
        startFragment(CommentsFragment.newInstance(video), false);
    }

    @Override
    public void onMore(Video video, Video videoParent) {
        MoreDialogFragment.newInstance(video, videoParent).show(getSupportFragmentManager(), MoreDialogFragment.TAG);
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }

    @Override
    public void onViewProfile(int uid) {
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    @Override
    public void onComment(Video video, String comment) {

    }

    @Override
    public void onShowComment(Video video) {
        startFragment(CommentsFragment.newInstance(video), false);
    }
}
