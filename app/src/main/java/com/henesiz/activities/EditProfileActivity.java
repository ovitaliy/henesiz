package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.events.ShowProgressDialogEvent;
import com.henesiz.fragment.registration.BaseProfileFragment;
import com.henesiz.fragment.registration.ProfileAboutFragment;
import com.henesiz.fragment.registration.ProfileInfoFragment;
import com.henesiz.model.UserInfo;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.request.user.EditUserInfoRequest;

import org.greenrobot.eventbus.EventBus;

/**
 * Uses fo edit user profile
 */
public class EditProfileActivity extends BaseActivity {

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public static class EditProfileFragment extends Fragment implements
            BaseProfileFragment.OnProfileFillListener,
            ViewPager.OnPageChangeListener,
            TabLayout.OnTabSelectedListener {

        ViewPager mViewPager;
        TabsAdapter mTabsAdapter;
        TabLayout mTabLayout;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_edit_profile, container, false);

            mTabsAdapter = new TabsAdapter(getActivity().getSupportFragmentManager());
            mViewPager = (ViewPager) v.findViewById(R.id.pager);
            mViewPager.setAdapter(mTabsAdapter);
            mViewPager.addOnPageChangeListener(this);

            mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
            mTabLayout.addTab(mTabLayout.newTab().setText(R.string.profile_edit_tab_about_you));
            mTabLayout.addTab(mTabLayout.newTab().setText(R.string.profile_edit_tab_profile));
            mTabLayout.setOnTabSelectedListener(this);

            v.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onFillProfile(AppUser.get());
                }
            });

            return v;
        }

        @Override
        public void onFillProfile(UserInfo userInfo) {
            saveProfile(userInfo);
        }

        public void saveProfile(final UserInfo userInfo) {
            AppUser.set(userInfo);
            EventBus.getDefault().post(new ShowProgressDialogEvent(true));

            ((BaseActivity) getActivity()).getSpiceManager().execute(new EditUserInfoRequest(userInfo), new BaseRequestListener() {
                @Override
                public void onRequestSuccess(BaseResponse baseResponse) {
                    super.onRequestSuccess(baseResponse);
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    intent.putExtra(Const.Params.ACTION, Const.Action.SHOW_PROFILE);
                    startActivity(intent);
                    getActivity().finish();
                }
            });
        }

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }

        public class TabsAdapter extends FragmentStatePagerAdapter {
            public TabsAdapter(FragmentManager fm) {
                super(fm);
            }

            @Override
            public Fragment getItem(int i) {
                Fragment fragment;
                switch (i) {
                    case 0:
                        fragment = ProfileAboutFragment.newInstance();
                        break;
                    default:
                        fragment = ProfileInfoFragment.newInstance();
                        break;
                }
                return fragment;
            }

            @Override
            public int getItemPosition(Object object) {
                return PagerAdapter.POSITION_NONE;
            }

            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return "OBJECT " + (position + 1);
            }
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            mTabLayout.getTabAt(position).select();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }
}
