package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.henesiz.R;
import com.henesiz.enums.FeedbackType;
import com.henesiz.fragment.FeedbackChatFragment;
import com.henesiz.fragment.FeedbackNewChatFragment;

/**
 * Uses for feedback
 */
public class FeedbackActivity extends BaseActivity {

    public static Intent newIntent(Context context, int feedbackId, FeedbackType type) {
        Intent intent = new Intent(context, FeedbackActivity.class);
        intent.putExtra(Params.FEEDBACK_ID, feedbackId);
        intent.putExtra(Params.TYPE, type);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        int id = getIntent().getIntExtra(Params.FEEDBACK_ID, -1);
        FeedbackType type = (FeedbackType) getIntent().getSerializableExtra(Params.TYPE);

        if (id != -1)
            startFragment(FeedbackChatFragment.newInstance(id, type), false);
        else
            startFragment(FeedbackNewChatFragment.newInstance(id, type), false);

    }
}
