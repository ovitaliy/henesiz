package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.appsflyer.AppsFlyerLib;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.dialogs.MoreDialogFragment;
import com.henesiz.enums.VideoType;
import com.henesiz.events.UpdateProfileEvent;
import com.henesiz.fragment.AlienLookFragment;
import com.henesiz.fragment.FlowListFragment;
import com.henesiz.fragment.MenuFragment;
import com.henesiz.fragment.NavigationDiyFragment;
import com.henesiz.fragment.NavigationInfoFragment;
import com.henesiz.fragment.NavigationMarketplaceFragment;
import com.henesiz.fragment.ProfileFragment;
import com.henesiz.fragment.VistoryFragment;
import com.henesiz.listener.OnPlayFlowListener;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Video;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.LocationHelper;
import com.henesiz.view.items.VideoItemView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Container for main menu and fragments
 */
public class MainActivity extends BaseActivity implements
        MenuFragment.OnMenuListener,
        OnPlayFlowListener,
        VideoItemView.OnCommentListener,
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnMoreListener,
        OnViewProfileListener {

    MenuFragment mMenuFragment;

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LocationHelper.init(getSpiceManager());

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        AnalyticsHelper.identifyUser();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                mToolbar,  /* nav drawer icon to replace 'Up' caret */
                R.string.app_name,  /* "open drawer" description */
                R.string.app_name  /* "close drawer" description */
        ) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                EventBus.getDefault().post(new UpdateProfileEvent());
            }
        };
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int backStack = getSupportFragmentManager().getBackStackEntryCount();
                if (backStack == 0)
                    if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mDrawerLayout.closeDrawers();
                    } else {
                        mDrawerLayout.openDrawer(GravityCompat.START);
                    }
                else
                    onBackPressed();
            }
        });
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {

                if (getSupportFragmentManager().getBackStackEntryCount() == 0) {

                    mDrawerLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            mDrawerToggle.syncState();
                        }
                    });

                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                } else {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                }
            }
        });

        mMenuFragment = (MenuFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_menu);

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mMenuFragment.setActive(com.henesiz.enums.Menu.VISTORY);
        onOpenVistory();

        checkVideoToUpload();

        //RegistrationActivity.startNewInstance(this, Constants.ACTION.FILL_PROFILE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppsFlyerLib.onActivityResume(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppsFlyerLib.onActivityPause(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void openSearch() {
        startActivity(SearchActivity.newIntent(this, false));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                break;
        }
        return false;
    }

    public void toggleDrawerMenu() {
        if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        } else {
            mDrawerLayout.closeDrawers();
        }
    }

    @Override
    public void onOpenMarketplace() {
        mDrawerLayout.closeDrawers();
        startFragment(new NavigationMarketplaceFragment(), false, true);
    }

    @Override
    public void onOpenAlienLook() {
        mDrawerLayout.closeDrawers();
        startFragment(AlienLookFragment.newInstance(), false, true);
    }

    @Override
    public void onOpenInfo() {
        mDrawerLayout.closeDrawers();
        startFragment(new NavigationInfoFragment(), false, true);
    }

    @Override
    public void onOpenDiy() {
        mDrawerLayout.closeDrawers();
        startFragment(NavigationDiyFragment.newInstance(), false, true);
    }

    @Override
    public void onOpenVistory() {
        mDrawerLayout.closeDrawers();
        startFragment(VistoryFragment.newInstance(), false, true);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent.hasExtra(Const.Params.ACTION)) {
            if (intent.getIntExtra(Const.Params.ACTION, 0) == Const.Action.PLAY_FLOW) {
                int currentVideoPosition = intent.getIntExtra(Const.Params.CURRENT_VIDEO, 0);
                HashMap<String, Object> params = (HashMap<String, Object>) intent.getSerializableExtra(Params.PARAMS);
                int replyId = intent.getIntExtra(Const.Params.REPLY_ID, 0);
                String title = intent.getStringExtra(Const.Params.TITLE);
                VideoType videoType = (VideoType) intent.getSerializableExtra(Const.Params.TYPE);
                FlowListFragment fragment = new FlowListFragment.Builder(null)
                        .setParams(params)
                        .setReplyId(replyId)
                        .setVideoType(videoType)
                        .setTitle(title)
                        .setCurrentPosition(currentVideoPosition)
                        .build();
                startFragment(fragment, true, true);
            } else if (intent.getIntExtra(Const.Params.ACTION, 0) == Const.Action.SHOW_PROFILE) {
                startFragment(ProfileFragment.newInstance(AppUser.getUid(), 1), true, true);
            }
        }
    }

    @Override
    public void onComment(Video video, String comment) {
    }

    @Override
    public void onShowComment(Video video) {
        CommentsActivity.startNewInstance(this, video);
    }

    @Override
    public void onMore(Video video, Video videoParent) {
        MoreDialogFragment.newInstance(video, videoParent).show(getSupportFragmentManager(), MoreDialogFragment.TAG);
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }

    @Override
    public void onViewProfile(int uid) {
        mDrawerLayout.closeDrawers();
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    public void onLogout() {
        logout();
    }

    @Override
    public void onPlayFlow(HashMap<String, Object> params, int replyId, String title, ArrayList<Video> mVideoList, VideoType videoType) {
        PlayFlowActivity.startNewInstance(this, params,0, replyId, title, mVideoList, 0L);
    }
}
