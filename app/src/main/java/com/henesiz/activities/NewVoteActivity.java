package com.henesiz.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.henesiz.R;
import com.henesiz.dialogs.ProgressDialog;
import com.henesiz.rest.listener.AddVoteRequestListener;
import com.henesiz.rest.model.AddVoteResponse;
import com.henesiz.rest.request.vote.AddVoteRequest;
import com.henesiz.util.AnalyticsHelper;

import java.util.ArrayList;

/**
 * Uses to create new poll
 */
public class NewVoteActivity extends BaseActivity implements View.OnClickListener {
    EditText mNewVote;
    EditText mNewOption1;
    EditText mNewOption2;
    EditText mNewOption3;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_vote);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setActionBarTitle(getString(R.string.vote_create));

        mNewVote = (EditText) findViewById(R.id.new_vote);

        mNewOption1 = (EditText) findViewById(R.id.new_option_1);
        mNewOption2 = (EditText) findViewById(R.id.new_option_2);
        mNewOption3 = (EditText) findViewById(R.id.new_option_3);

        findViewById(R.id.btn_save).setOnClickListener(this);
    }

    private boolean validate() {
        boolean success = true;
        String newVote = mNewVote.getText().toString();
        if (TextUtils.isEmpty(newVote)) {
            mNewVote.setError(getString(R.string.field_required));
            success = false;
        }

        String newOption1 = mNewOption1.getText().toString();
        if (TextUtils.isEmpty(newOption1)) {
            mNewOption1.setError(getString(R.string.field_required));
            success = false;
        }

        String newOption2 = mNewOption2.getText().toString();
        if (TextUtils.isEmpty(newOption2)) {
            mNewOption2.setError(getString(R.string.field_required));
            success = false;
        }
        return success;
    }

    @Override
    public void onClick(View view) {
        if (validate()) {
            if (mProgressDialog == null || !mProgressDialog.isShowing()) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.show();
            }

            String vote = mNewVote.getText().toString();

            ArrayList<String> optionsList = new ArrayList<>();

            String option1 = mNewOption1.getText().toString();
            if (!TextUtils.isEmpty(option1)) {
                optionsList.add(option1);
            }

            String option2 = mNewOption2.getText().toString();
            if (!TextUtils.isEmpty(option2)) {
                optionsList.add(option2);
            }

            String option3 = mNewOption3.getText().toString();
            if (!TextUtils.isEmpty(option3)) {
                optionsList.add(option3);
            }

            AnalyticsHelper.trackEvent(this, AnalyticsHelper.POLL_CREATE);

            getSpiceManager().execute(
                    new AddVoteRequest(vote, optionsList.toArray(new String[optionsList.size()])),
                    new AddVoteRequestListener(vote, optionsList) {
                        @Override
                        public void onRequestSuccess(AddVoteResponse response) {
                            super.onRequestSuccess(response);
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }
                            finish();
                        }
                    });
        }
    }
}
