package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.dialogs.DialogBuilder;
import com.henesiz.dialogs.ProgressDialog;
import com.henesiz.enums.VideoType;
import com.henesiz.fragment.registration.BaseProfileFragment;
import com.henesiz.fragment.registration.OkFragment;
import com.henesiz.fragment.registration.ProfileInfoFragment;
import com.henesiz.fragment.registration.VerificationFragment;
import com.henesiz.model.UserInfo;
import com.henesiz.rest.listener.AuthRequestListener;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.listener.CountriesRequestListener;
import com.henesiz.rest.model.AuthResponse;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.request.GetCountriesRequest;
import com.henesiz.rest.request.user.AuthRequest;
import com.henesiz.rest.request.user.EditUserInfoRequest;
import com.henesiz.util.LocationHelper;
import com.henesiz.util.PrefHelper;
import com.henesiz.view.NewVideoView;


/**
 * Uses for user registration
 */
public class RegistrationActivity extends BaseActivity implements Const,
        OkFragment.OnCongratulationListener,
        VerificationFragment.OnVerifyListener,
        NewVideoView,
        BaseProfileFragment.OnProfileFillListener {

    public static void startNewInstance(Context context) {
        Intent intent = new Intent(context, RegistrationActivity.class);
        context.startActivity(intent);
    }

    public static void startNewInstance(Context context, int action) {
        Intent intent = new Intent(context, RegistrationActivity.class);
        intent.putExtra(Params.ACTION, Action.FILL_PROFILE);
        context.startActivity(intent);
    }

    private VerificationFragment mVerificationFragment;

    private WakefulBroadcastReceiver mSmsBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        if (TextUtils.isEmpty(PrefHelper.getToken())) {
            AuthRequestListener authRequestListener = new AuthRequestListener() {
                @Override
                public void onRequestSuccess(AuthResponse response) {
                    super.onRequestSuccess(response);
                    LocationHelper.init(getSpiceManager());
                }
            };
            getSpiceManager().execute(new AuthRequest(), authRequestListener);
        } else {
            Log.i(getClass().getSimpleName(), "user token " + PrefHelper.getToken());
        }

        onOpenVerification();
    }


    @Override
    protected void onStart() {
        super.onStart();

        mSmsBroadcastReceiver = new SmsReceiver();
        registerReceiver(mSmsBroadcastReceiver, new IntentFilter(SmsReceiver.SMS_RECEIVED));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mSmsBroadcastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       /* switch (requestCode) {
            case REQUEST_CODE_VIDEO:
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        uri = data.getData();
                        Log.i("imon", uri.toString());
                    }
                }
                break;
            /*case SEND_SMS:
                if(resultCode == Activity.RESULT_OK && data != null) {
                    Bundle extras = data.getExtras();
                    Uri uri = data.getData();
                }
                break;*/
        /*}*/
    }


    public void sendVerificationCode(CharSequence code) {
        if (mVerificationFragment == null)
            return;

        mVerificationFragment.sendVerificationCode(code);
    }

    public void openCongratulationsWindow() {
        startFragment(OkFragment.newInstance(), true);
    }

    public void openSaveProfileCongratulationsWindow() {
        openMainActivity();
    }

    @Override
    public void onOpenVerification() {
        mVerificationFragment = VerificationFragment.newInstance();
        startFragment(mVerificationFragment, false);
    }

    @Override
    public void onVerify(boolean isNewUser) {
        if (isNewUser) {
            onOpenFillProfile();
        } else {
            openMainActivity();
        }
    }

    public void onOpenFillProfile() {
        startFragment(ProfileInfoFragment.newInstance(), true);
    }

    @Override
    public void onRegistrationProcedureComplete() {
        openMainActivity();
    }

    public void openMainActivity() {
        PrefHelper.setBooleanPref(PREF_REG, true);
        finish();
        MainActivity.startNewInstance(this);
    }

    @Override
    public void onFillProfile(UserInfo userInfo) {
        saveProfile(userInfo);
    }

    public void saveProfile(UserInfo userInfo) {
        AppUser.set(userInfo);
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(R.string.saving);
        progressDialog.show();
        getSpiceManager().execute(new EditUserInfoRequest(userInfo), new BaseRequestListener() {
            @Override
            public void onRequestSuccess(BaseResponse baseResponse) {
                super.onRequestSuccess(baseResponse);
                progressDialog.dismiss();
                if (baseResponse.isSuccess()) {
                    openCongratulationsWindow();
                } else {
                    new DialogBuilder(RegistrationActivity.this).setMessage(baseResponse.getErrorsMessage()).create();
                }
            }
        });
    }

    public boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    @Override
    public VideoType getVideoType() {
        return VideoType.AVATAR;
    }

    private class SmsReceiver extends WakefulBroadcastReceiver {
        private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

        private String getCodeFromSms(String body) {
            String[] words = body.split("\\s+");
            if (words.length > 0) {
                for (String word : words) {
                    if (isNumeric(word)) {
                        return word;
                    }
                }
            }
            return null;
        }

        @SuppressWarnings("ImplicitArrayToString")
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SMS_RECEIVED)) {
                SmsMessage[] msgs;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                } else {
                    Bundle data = intent.getExtras();
                    if (data != null) {
                        Object[] pdus = (Object[]) data.get("pdus");
                        msgs = new SmsMessage[pdus.length];
                        for (int i = 0; i < msgs.length; i++) {
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }
                    } else {
                        return;
                    }
                }

                String code = "";
                String body = msgs[0].getMessageBody().toLowerCase();
                boolean contains = body.contains("clickclap code") || body.contains("clikclap code") || body.contains("thehumanet code");
                if (contains) {
                    code = getCodeFromSms(body);
                    if (!TextUtils.isEmpty(code)) {
                        sendVerificationCode(code);
                    }
                }
            }
        }
    }
}
