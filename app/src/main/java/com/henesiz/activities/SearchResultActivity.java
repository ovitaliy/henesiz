package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.henesiz.R;
import com.henesiz.dialogs.MoreDialogFragment;
import com.henesiz.enums.VideoType;
import com.henesiz.fragment.ProfileFragment;
import com.henesiz.fragment.SearchResultFragment;
import com.henesiz.listener.OnUpdateVideoListener;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Video;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.listener.PayForVideoRequestListener;
import com.henesiz.rest.model.BaseCoinsResponse;
import com.henesiz.rest.request.video.MarkVideoRequest;
import com.henesiz.rest.request.video.PayForVideoRequest;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.view.items.VideoItemView;

/**
 * Uses for showing search results
 */
public class SearchResultActivity extends BaseActivity implements VideoItemView.OnCommentListener,
        VideoItemView.OnVideoChooseListener,
        VideoItemView.OnMoreListener,
        OnViewProfileListener,
        OnUpdateVideoListener {

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, SearchResultActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        startFragment(SearchResultFragment.newInstance(), false);
    }

    @Override
    public void onComment(Video video, String comment) {
    }

    @Override
    public void onShowComment(Video video) {
        CommentsActivity.startNewInstance(this, video);
    }

    @Override
    public void onMore(Video video, Video videoParent) {
        MoreDialogFragment.newInstance(video, videoParent).show(getSupportFragmentManager(), MoreDialogFragment.TAG);
    }

    @Override
    public void onVideoChosen(Video video) {
        PlayFlowActivity.startNewInstance(this, video);
    }

    @Override
    public void onViewProfile(int uid) {
        startFragment(ProfileFragment.newInstance(uid), true);
    }

    @Override
    public void onVideoPay(final VideoType videoType, int videoId) {
        if (videoType != null) {
            switch (videoType) {
                case CROWD_ASK:
                case CROWD_GIVE:
                    AnalyticsHelper.trackEvent(this, AnalyticsHelper.CROWD_DONATE);
                    break;
            }
        }
        getSpiceManager().execute(
                new PayForVideoRequest(videoId),
                new PayForVideoRequestListener(videoType, videoId) {
                    @Override
                    public void onRequestSuccess(BaseCoinsResponse response) {
                        super.onRequestSuccess(response);
                    }
                });
    }

    @Override
    public void onVideoMark(int videoId, int mark) {
        getSpiceManager().execute(
                new MarkVideoRequest(videoId, mark),
                new BaseRequestListener());
    }
}
