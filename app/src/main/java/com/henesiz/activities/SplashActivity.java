package com.henesiz.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.henesiz.App;
import com.henesiz.Const;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.FilePathHelper;
import com.henesiz.util.NetworkUtil;
import com.henesiz.util.PrefHelper;
import com.humanet.audio.MoveFilesJob;

/**
 * First launched activity
 */
public class SplashActivity extends BaseActivity implements Const {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AnalyticsHelper.init(this);

        if (PrefHelper.getBooleanPref(PREF_REG)) {
            MainActivity.startNewInstance(SplashActivity.this);
        } else {
            if (!NetworkUtil.isOfflineMode()) {
                startActivity(new Intent(SplashActivity.this, RegistrationActivity.class));
            } else {
                App.showNoNetworkErrorToast(this);
            }
        }

        AnalyticsHelper.trackEvent(this, AnalyticsHelper.ENTER_IN_APP);

        MoveFilesJob.run(this, FilePathHelper.getAudioDirectory());

        finish();
    }

}
