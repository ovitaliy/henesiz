package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.henesiz.AppUser;
import com.henesiz.R;
import com.henesiz.model.UserInfo;

/**
 * Uses to send token (invitation) to contacts
 */
public class TokenActivity extends BaseActivity implements View.OnClickListener {
    TextView mMessageRemainsView;
    TextView mMessageInfoView;
    Button mSendTokenView;
    Toolbar mToolbar;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, TokenActivity.class);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_token);

        mMessageRemainsView = (TextView) findViewById(R.id.message_remains);
        mMessageInfoView = (TextView) findViewById(R.id.message_info);

        mSendTokenView = (Button) findViewById(R.id.send_token);
        mSendTokenView.setOnClickListener(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView titleView = (TextView) mToolbar.findViewById(R.id.title);
        titleView.setText(R.string.profile_token);
        setActionBarTitle(getString(R.string.token_title));
    }

    @Override
    protected void onResume() {
        super.onResume();

        UserInfo userInfo = AppUser.get();
        mMessageRemainsView.setText(Html.fromHtml(getString(R.string.token_message_remains, userInfo.getToken())));
        mMessageInfoView.setText(Html.fromHtml(getString(R.string.token_message_info)));

        if (userInfo.getToken() == 0) {
            mSendTokenView.setEnabled(false);
        } else {
            mSendTokenView.setEnabled(true);
        }
    }

    private void sendToken() {
        startActivity(ContactsActivity.newTokenInstance(this));
    }

    @Override
    public void onClick(View v) {
        sendToken();
    }
}
