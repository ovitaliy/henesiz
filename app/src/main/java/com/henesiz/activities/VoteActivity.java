package com.henesiz.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.adapter.VoteOptionsAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.dialogs.ProgressDialog;
import com.henesiz.enums.VoteListStatus;
import com.henesiz.model.Vote;
import com.henesiz.model.VoteOption;
import com.henesiz.rest.listener.GetVoteRequestListener;
import com.henesiz.rest.listener.SendVoteRequestListener;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.request.vote.GetVoteRequest;
import com.henesiz.rest.request.vote.SendVoteRequest;
import com.henesiz.util.AnalyticsHelper;

/**
 * Uses to display votes
 */
public class VoteActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener {
    private static final int VOTE_LOADER_ID = 1311;
    private static final int OPTIONS_LOADER_ID = 3212;

    private int mVoteId;
    private VoteOptionsAdapter mAdapter;

    private ListView mListView;
    private TextView mQuestionView;

    private Vote mVote;

    private ProgressDialog mProgressDialog;

    private boolean mIsFinished;

    public static Intent newInstance(Context context, int voteId, boolean isFinished) {
        Intent intent = new Intent(context, VoteActivity.class);
        intent.putExtra("id", voteId);
        intent.putExtra("is_finished", isFinished);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vote_activity);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mListView = (ListView) findViewById(R.id.options);
        mAdapter = new VoteOptionsAdapter(this, null);
        mListView.setAdapter(mAdapter);
        mListView.setDividerHeight(App.MARGIN);

        mQuestionView = (TextView) findViewById(R.id.question);

        mVoteId = getIntent().getIntExtra("id", 0);
        mIsFinished = getIntent().getBooleanExtra("is_finished", false);

        loadVoteInfo();

        getSupportLoaderManager().initLoader(VOTE_LOADER_ID, null, this);
        getSupportLoaderManager().initLoader(OPTIONS_LOADER_ID, null, this);

        setActionBarTitle(getString(R.string.menu_poll));
    }

    private void loadVoteInfo() {
        getSpiceManager().execute(
                new GetVoteRequest(mVoteId),
                new GetVoteRequestListener(mVoteId));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case VOTE_LOADER_ID:
                return new CursorLoader(this,
                        ContentDescriptor.Votes.URI,
                        null,
                        ContentDescriptor.Votes.Cols.ID + " = " + mVoteId,
                        null,
                        null);
            case OPTIONS_LOADER_ID:
                return new CursorLoader(this,
                        ContentDescriptor.VoteOptions.URI,
                        null,
                        ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId,
                        null,
                        null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case OPTIONS_LOADER_ID:
                mAdapter.swapCursor(data);
                break;
            case VOTE_LOADER_ID:
                if (data != null && data.getCount() > 0) {
                    data.moveToFirst();
                    mVote = Vote.fromCursor(data);
                    mQuestionView.setText(mVote.getTitle());

                    if (!mVote.isVoted() && mVote.getStatus() != null && !mVote.getStatus().equals(VoteListStatus.CREATED)) {
                        mListView.setOnItemClickListener(this);
                    } else {
                        mListView.setOnItemClickListener(null);
                    }

                    mAdapter.setFinished(mIsFinished);
                    mAdapter.setVoted(mVote.getMyVote());
                    mAdapter.setVotedCount(mVote.getTotalVotes());
                    mAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case OPTIONS_LOADER_ID:
                mAdapter.swapCursor(null);
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (!mVote.isVoted()) {
            if (mProgressDialog == null || !mProgressDialog.isShowing()) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.show();
            }
            AnalyticsHelper.trackEvent(this, AnalyticsHelper.POLL_PARTICIPATE);
            VoteOption option = VoteOption.fromCursor((Cursor) mAdapter.getItem(i));
            getSpiceManager().execute(
                    new SendVoteRequest(option.getVoteId(), option.getId()),
                    new SendVoteRequestListener(option.getVoteId(), option.getId()) {
                        @Override
                        public void onRequestSuccess(BaseResponse response) {
                            super.onRequestSuccess(response);
                            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                                mProgressDialog.dismiss();
                            }

                            loadVoteInfo();
                        }
                    });
        }
    }

}
