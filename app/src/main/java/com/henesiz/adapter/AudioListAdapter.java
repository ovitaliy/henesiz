package com.henesiz.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.humanet.audio.AudioTrack;
import com.henesiz.util.OnCacheUpdateListener;
import com.henesiz.view.items.AudioItemView;

import java.util.ArrayList;

/**
 * Created by Владимир on 29.10.2014.
 */
public class AudioListAdapter extends BaseAdapter implements OnCacheUpdateListener {

    private Context mContext;
    private ArrayList<AudioTrack> mData;
    private int mSelected = -1;
    private Bitmap mAudioImage;

    public AudioListAdapter(Context context) {
        mContext = context;
        mData = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AudioItemView view = (AudioItemView) convertView;
        if (view == null) {
            view = new AudioItemView(mContext);
        }
        AudioTrack track = getAudioTrack(position);
        view.setOnCacheUpdateListener(this);
        view.setData(track);
        return view;
    }

    public AudioTrack getAudioTrack(int position) {
        return (AudioTrack) getItem(position);
    }

    public void setData(ArrayList<AudioTrack> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    public void setSelected(int selected) {
        if (mSelected == selected) {
            mSelected = -1;
        } else {
            mSelected = selected;
        }
        notifyDataSetChanged();
    }

    public int getSelected() {
        return mSelected;
    }

    @Override
    public void onCacheUpdated(Object key, Bitmap image) {
        mAudioImage = image;
    }

    @Override
    public Bitmap getCachedBitmap(Object key) {
        return mAudioImage;
    }
}
