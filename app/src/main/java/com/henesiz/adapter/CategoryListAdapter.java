package com.henesiz.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.henesiz.model.Category;
import com.henesiz.view.items.CategoryItemView;

import java.util.ArrayList;

public class CategoryListAdapter extends BaseAdapter {


    private Context mContext;
    private ArrayList<Category> mData;
    private int mSelected = 0;

    public boolean isNewDataIdentical(ArrayList<Category> data) {
        if (mData == null) {
            return false;
        }

        if (data == null){
            return false;
        }

        if (data.equals(mData)) {
            return true;
        }

        if (mData.size() != data.size()) {
            return false;
        }

        boolean identical = true;
        if (data.size() == mData.size()) {
            for (int i = 0; i < mData.size(); i++) {
                if (!data.get(i).equals(mData.get(i))) {
                    identical = false;
                }
            }
        }

        return identical;
    }

    public CategoryListAdapter(Context context) {
        mContext = context;
        mData = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Category getItem(int position) {
        if (mData != null && mData.size() > position) {
            return mData.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryItemView view;
        if (convertView == null || !(convertView instanceof CategoryItemView)) {
            view = new CategoryItemView(mContext);
        } else {
            view = (CategoryItemView) convertView;
        }

        Category frame = getItem(position);
        view.setData(frame);
        return view;
    }

    public void clear() {
        if (mData != null) {
            mData.clear();
        }
    }

    public void setData(ArrayList<Category> data) {
        mData.clear();
        if (data != null) {
            mData.addAll(data);
        }
        notifyDataSetChanged();
    }

    public void setSelected(int selected) {
        mSelected = selected;
        notifyDataSetChanged();
    }

    public int getSelected() {
        return mSelected;
    }

    public int getInitialSelectionPosition() {
        SparseArray<Integer> ids = new SparseArray<>();
        int count = getCount();
        int main = -1;
        if (mData != null) {
            for (int i = 0; i < count; i++) {
                Category item = getItem(i);
                if (item.getId() <= 0) {
                    ids.put(item.getId(), i);
                }
                if (item.isMain()) {
                    main = i;
                }
            }
        }

        if (main >= 0) {
            return main;
        } else {
            if (ids.size() == 0) {
                return Math.max(count / 2, 0);
            } else {
                int position = ids.get(Category.ALL_ID, -1);
                if (position != -1) {
                    return position;
                }
                position = ids.get(Category.FAVORITES_ID, -1);
                if (position != -1) {
                    return position;
                }
                position = ids.get(0);
                return position;
            }
        }
    }
}
