package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.model.Feedback;

/**
 * Created by Denis on 28.04.2015.
 */
public class FeedbackAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;

    public FeedbackAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return mLayoutInflater.inflate(R.layout.item_feedback, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        Feedback feedback = Feedback.fromCursor(cursor);

        TextView textView = (TextView) view.findViewById(R.id.message);
        textView.setText(String.valueOf(feedback.getLastMessage()));

//        TextView newCountView = (TextView) view.findViewById(R.id.new_count);
//        if (feedback.getNew() > 0) {
//            newCountView.setVisibility(View.VISIBLE);
//            newCountView.setText(String.valueOf(feedback.getNew()));
//        } else {
//            newCountView.setVisibility(View.GONE);
//        }

    }
}