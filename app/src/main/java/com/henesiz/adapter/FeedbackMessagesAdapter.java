package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.model.FeedbackMessage;
import com.henesiz.view.CircleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 26.11.2014.
 */
public class FeedbackMessagesAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;

    public FeedbackMessagesAdapter(Context context, Cursor c) {
        super(context, c, 0);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mLayoutInflater.inflate(R.layout.item_feedback_message, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        FeedbackMessage message = FeedbackMessage.fromCursor(cursor);
        TextView messageView = (TextView) view.findViewById(R.id.message);
        messageView.setText(message.getMessage());

        CircleImageView imageView = (CircleImageView) view.findViewById(R.id.author_image);
        TextView authorView = (TextView) view.findViewById(R.id.author_name);
        if (message.isReply()) {
            imageView.setImageResource(R.drawable.logo);
            authorView.setText(R.string.feedback_answer_author);
        } else {
            if (AppUser.get() != null) {
                ImageLoader.getInstance().displayImage(AppUser.get().getPhoto(), imageView);
                authorView.setText(AppUser.get().getFullName());
            }
        }

        TextView timeView = (TextView) view.findViewById(R.id.time);

        timeView.setText(Const.DAY_MONTH_YEAR_FORMAT.format(message.getTime()));

    }

}
