package com.henesiz.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.enums.Menu;
import com.henesiz.view.MenuView;

/**
 * Created by denisvasilenko on 08.12.15.
 */
public class MenuAdapter extends BaseAdapter {
    Menu[] mDataset = Menu.values();

    @Override
    public int getCount() {
        return mDataset.length;
    }

    @Override
    public Object getItem(int position) {
        return mDataset[position];
    }

    @Override
    public long getItemId(int position) {
        return mDataset[position].ordinal();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Menu menu = (Menu) getItem(position);
        View v = new MenuView(parent.getContext());
        ImageView mImageView = (ImageView) v.findViewById(R.id.menu_item_icon);
        TextView mTextView = (TextView) v.findViewById(R.id.menu_item_title);
        mTextView.setText(menu.getTitle());
        mImageView.setImageResource(menu.getImage());
        return v;
    }
}
