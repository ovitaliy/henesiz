package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.henesiz.model.UserInfo;
import com.henesiz.view.items.RatingItemView;

/**
 * Created by denisvasilenko on 01.03.16.
 */
public class RatingAdapter extends CursorAdapter {
    public RatingAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        UserInfo user = UserInfo.fromCursor(cursor);
        if (user.isMe()) {
            return 1;
        }
        return 0;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = new RatingItemView(context, getItemViewType(cursor.getPosition()) == 1);
        return view;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        UserInfo user = UserInfo.fromCursor(cursor);
        RatingItemView itemView = (RatingItemView) view;

        String photo = user.getPhoto100();
        if (photo == null)
            photo = user.getImageLoaderPhoto();
        itemView.setAvatar(photo);
        itemView.setName(user.getFullName());
        itemView.setRating(String.valueOf(user.getRating()));

        String location = user.getCountry();
        if (!TextUtils.isEmpty(user.getCity())) {
            if (!TextUtils.isEmpty(location)) {
                location += ", ";
            }
            location += user.getCity();
        }

        itemView.setLocation(location);
    }
}
