package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.henesiz.enums.Hobby;
import com.henesiz.enums.Interest;
import com.henesiz.enums.Language;
import com.henesiz.enums.Pet;
import com.henesiz.enums.Religion;
import com.henesiz.enums.Sport;
import com.henesiz.model.UserFinded;
import com.henesiz.model.UserInfo;
import com.henesiz.view.items.RatingItemView;

/**
 * Created by Deni on 22.06.2015.
 */
public class SearchResultAdapter extends CursorAdapter {
    LayoutInflater mLayoutInflater;
    Context mContext;

    public SearchResultAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        Cursor cursor = (Cursor) getItem(position);
        UserInfo user = UserInfo.fromCursor(cursor);
        if (user.isMe()) {
            return 1;
        }
        return 0;
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = new RatingItemView(context, getItemViewType(cursor.getPosition()) == 1);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        UserFinded user = UserFinded.fromCursor(cursor);
        RatingItemView ratingItemView = (RatingItemView) view;

        String name = "";
        if (user.getFirstName() != null && !TextUtils.isEmpty(user.getFirstName().value)) {
            if (user.getFirstName().required) {
                name = String.format("<b>%s</b>", user.getFirstName().value);
            } else {
                if (user != null) {
                    name = user.getFirstName().value;
                }
            }
            name += " ";
        }

        if (user.getLastName() != null && !TextUtils.isEmpty(user.getLastName().value)) {
            if (user.getLastName().required) {
                name += String.format("<b>%s</b>", user.getLastName().value);
            } else {
                if (user != null) {
                    name += user.getLastName().value;
                }
            }
        }
        ratingItemView.setName(name);

        StringBuilder details = new StringBuilder();
        if (user.getIdCountry() != null && !TextUtils.isEmpty(user.getIdCountry().value)) {
            if (user.getIdCountry().required) {
                details.append(String.format("<b>%s</b>", user.getCountry().getAsString()));
            } else {
                details.append(user.getCountry().getAsString());
            }
            details.append(", ");
        }

        if (user.getIdCity() != null && !TextUtils.isEmpty(user.getIdCity().value)) {
            if (user.getIdCity().required) {
                details.append(String.format("<b>%s</b>", user.getCity().getAsString()));
            } else {
                details.append(user.getCity().getAsString());
            }
        }
        details.append("<br>");

        if (user.getLang() != null && !TextUtils.isEmpty(user.getLang().value)) {
            Language lang = Language.getFromString(user.getLang().value);
            if (user.getLang().required) {
                details.append(String.format("<b>%s</b>", lang.getTitle()));
            } else {
                details.append(lang.getTitle());
            }
            details.append(", ");
        }

        if (user.getCraft() != null) {
            String craft = user.getCraft().value;
            if (!TextUtils.isEmpty(craft)) {
                if (user.getCraft().required) {
                    details.append(String.format("<b>%s</b>", craft));
                } else {
                    details.append(craft);
                }
                details.append(", ");
            }
        }

        if (user.getHobby() != null && !TextUtils.isEmpty(user.getHobby().value)) {
            String hobby = Hobby.getById(user.getHobby().value).getTitle();
            if (user.getHobby().required) {
                details.append(String.format("<b>%s</b>", hobby));
            } else {
                details.append(hobby);
            }
            details.append(", ");
        }

        if (user.getInterest() != null && !TextUtils.isEmpty(user.getInterest().value)) {
            String interest = Interest.getById(user.getInterest().value).getTitle();
            if (user.getInterest().required) {
                details.append(String.format("<b>%s</b>", interest));
            } else {
                details.append(interest);
            }
            details.append(", ");
        }

        if (user.getSport() != null && !TextUtils.isEmpty(user.getSport().value)) {
            String sport = Sport.getById(user.getSport().value).getTitle();
            if (user.getSport().required) {
                details.append(String.format("<b>%s</b>", sport));
            } else {
                details.append(sport);
            }
            details.append(", ");
        }

        if (user.getPets() != null && !TextUtils.isEmpty(user.getPets().value)) {
            String pets = Pet.getById(user.getPets().value).getTitle();
            if (user.getPets().required) {
                details.append(String.format("<b>%s</b>", pets));
            } else {
                details.append(pets);
            }
            details.append(", ");
        }

        if (user.getReligion() != null && !TextUtils.isEmpty(user.getReligion().value)) {
            String religion = Religion.getById(user.getReligion().value).getTitle();
            if (user.getReligion().required) {
                details.append(String.format("<b>%s</b>", religion));
            } else {
                details.append(religion);
            }
            details.append(", ");
        }

        ratingItemView.setLocation(details.toString());
        ratingItemView.setRating(String.valueOf(user.getRating()));
        ratingItemView.setAvatar(user.getPhoto());
    }
}
