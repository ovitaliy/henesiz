package com.henesiz.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by denisvasilenko on 09.03.16.
 */
public abstract class SelectableFragmentStatePagerAdapter extends FragmentStatePagerAdapter {
    Fragment mFragment;

    public SelectableFragmentStatePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        mFragment = (Fragment) object;
    }

    @Override
    public void setPrimaryItem(View container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        mFragment = (Fragment) object;
    }

    public Fragment getCurrentFragment() {
        return mFragment;
    }
}
