package com.henesiz.adapter;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.util.Utils;

/**
 * Created by Deni on 04.08.2015.
 */
public class TipsAdapter extends ArrayAdapter<String> {
    final int PADDING = Utils.convertDpToPixel(10, App.getInstance());

    public TipsAdapter(Context context, String[] objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = new TextView(getContext());
        String text = getItem(position);
        textView.setText(Html.fromHtml(text));
        textView.setPadding(PADDING, 0, PADDING, 0);
        textView.setTextColor(getContext().getResources().getColor(R.color.black));
        return textView;
    }
}
