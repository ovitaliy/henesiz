package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.henesiz.enums.VideoType;
import com.henesiz.listener.OnUpdateVideoListener;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Video;
import com.henesiz.view.items.VideoItemView;

import java.lang.ref.WeakReference;

/**
 * Created by Владимир on 26.11.2014.
 */
public class VideoListAdapter extends CursorAdapter {

    private Context mContext;
    private WeakReference<VideoItemView.OnVideoChooseListener> mListener;
    private WeakReference<VideoItemView.OnCommentListener> mCommentListener;
    private WeakReference<VideoItemView.OnMoreListener> mOnMore;
    private WeakReference<OnViewProfileListener> mOnViewProfileListener;
    private WeakReference<OnUpdateVideoListener> mOnUpdateVideoListener;
    private VideoType mVideoType;
    private Video mVideoParent;

    private int mCurrentUserId;

    public VideoListAdapter(Context context, Cursor cursor, int currentUserId, VideoType videoType, Video videoParent) {
        super(context, cursor, true);
        mContext = context;
        mCurrentUserId = currentUserId;
        mVideoType = videoType;
        mVideoParent = videoParent;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        VideoItemView view = new VideoItemView(mContext, mCurrentUserId, mVideoType);
        view.setOnVideoChooseListener(mListener.get());
        view.setOnCommentListener(mCommentListener.get());
        view.setOnMoreListener(mOnMore.get());
        view.setOnViewProfileListener(mOnViewProfileListener.get());
        view.setOnPayVideoListener(mOnUpdateVideoListener.get());
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ((VideoItemView) view).setData(Video.fromCursor(cursor), mVideoParent);
    }

    public void setOnVideoChooseListener(VideoItemView.OnVideoChooseListener listener) {
        mListener = new WeakReference<>(listener);
    }

    public void setOnCommentListener(VideoItemView.OnCommentListener listener) {
        mCommentListener = new WeakReference<>(listener);
    }

    public void setOnViewProfileListener(OnViewProfileListener listener) {
        mOnViewProfileListener = new WeakReference<>(listener);
    }

    public void setOnPayVideoListener(OnUpdateVideoListener listener) {
        mOnUpdateVideoListener = new WeakReference<>(listener);
    }

    public void setOnMoreListener(VideoItemView.OnMoreListener listener) {
        mOnMore = new WeakReference<>(listener);
    }

}
