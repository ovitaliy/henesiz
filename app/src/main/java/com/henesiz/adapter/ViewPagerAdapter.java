package com.henesiz.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * Created by ovi on 2/22/16.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private ViewPagerAdapterDelegator mDelegator;

    private ArrayList<View> mCachedViews = new ArrayList<>(3);

    public ViewPagerAdapter(ViewPagerAdapterDelegator delegator) {
        mDelegator = delegator;
        prepareDummyViews();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = position < mCachedViews.size() ? mCachedViews.get(position) : null;
        if (view == null) {
            view = mDelegator.getPageAt(container, position);
            mCachedViews.set(position, view);
        }
        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return mDelegator.getPagesCount();
    }

    @Override
    public void notifyDataSetChanged() {
        mCachedViews.clear();
        prepareDummyViews();
        super.notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    private void prepareDummyViews(){
        for (int i = 0; i < mDelegator.getPagesCount(); i++){
            mCachedViews.add(null);
        }
    }

    public interface ViewPagerAdapterDelegator {
        int getPagesCount();

        View getPageAt(ViewGroup container, int position);
    }
}
