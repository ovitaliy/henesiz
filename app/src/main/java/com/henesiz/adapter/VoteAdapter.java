package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.henesiz.model.Vote;
import com.henesiz.view.ItemVoteQuestionView;

/**
 * Created by Denis on 28.04.2015.
 */
public class VoteAdapter extends CursorAdapter {
    private boolean mMyVotes;

    public final void setMyVotes(final boolean myVotes) {
        mMyVotes = myVotes;
    }

    public VoteAdapter(Context context, final boolean myVotes) {
        super(context, null, 0);
        mMyVotes = myVotes;
    }

    @Override
    public final int getItemViewType(int position) {
        getCursor().moveToPosition(position);
        return Vote.fromCursor(getCursor()).getMyVote();
    }


    @Override
    public final View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = new ItemVoteQuestionView(context);
        return view;
    }

    @Override
    public final void bindView(View view, Context context, Cursor cursor) {
        Vote vote = Vote.fromCursor(cursor);
        ItemVoteQuestionView questionView = (ItemVoteQuestionView) view;

        if (mMyVotes) {
            questionView.setStatus(vote.getStatus().getTitle());
            questionView.setVoted(null);
        } else {
            questionView.setVoted(vote.isVoted());
            questionView.setStatus(null);
        }

        String title = vote.getTitle();
        if (title == null) {
            title = vote.getDescr();
        }
        questionView.setQuestion(title);
    }
}
