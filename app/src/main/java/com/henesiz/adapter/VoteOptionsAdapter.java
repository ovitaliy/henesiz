package com.henesiz.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.model.VoteOption;
import com.henesiz.view.ItemVoteOptionView;

/**
 * Created by Denis on 28.04.2015.
 */
public class VoteOptionsAdapter extends CursorAdapter {
    private LayoutInflater mLayoutInflater;
    private int mVotedCount;
    private int mVoted;
    private boolean mFinished;

    public VoteOptionsAdapter(Context context, Cursor c) {
        super(context, c, 0);
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setFinished(boolean finished) {
        mFinished = finished;
    }

    public void setVoted(int voted) {
        mVoted = voted;
    }

    public void setVotedCount(int votedCount) {
        mVotedCount = votedCount;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (mVoted != 0 || mFinished) ? 1 : 0;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        View view;
        if (mVoted != 0 || mFinished) {
            view = mLayoutInflater.inflate(R.layout.item_voted_option, viewGroup, false);
        } else {
            view = new ItemVoteOptionView(context);
        }
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        VoteOption option = VoteOption.fromCursor(cursor);

        if (mVoted != 0 || mFinished) {
            TextView titleView = (TextView) view.findViewById(R.id.title);
            String title = option.getTitle() + (mVoted == option.getId() ? " (" + context.getString(R.string.your_choice) + ")" : "");
            titleView.setText(title);

            int progress = Math.round(((float) option.getVotes() / (float) mVotedCount) * 100f);

            View progressView = view.findViewById(R.id.statistic);
            TextView countView = (TextView) view.findViewById(R.id.count);

            int maxWidth = App.WIDTH_WITHOUT_MARGINS;

            ViewGroup.LayoutParams params = progressView.getLayoutParams();
            params.width = maxWidth / 100 * progress;

            countView.setText(String.valueOf(option.getVotes()) + "(" + String.valueOf(progress) + "%)");
            //   ((LinearLayout.LayoutParams) view.findViewById(R.id.statistic).getLayoutParams())  .weight = 100 - progress;

        } else {
            ItemVoteOptionView textView = (ItemVoteOptionView) view;
            textView.setTitle(option.getTitle());
        }

    }
}
