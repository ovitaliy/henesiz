package com.henesiz.db;

import android.content.UriMatcher;
import android.net.Uri;

import com.henesiz.App;

/**
 * Created by Denis on 25.02.2015.
 */
public class ContentDescriptor {
    public static final String AUTHORITY = App.getInstance().getPackageName() + ".db.provider";
    public static final UriMatcher URI_MATCHER = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, Videos.TABLE_NAME, Videos.CODE_ALL);
        matcher.addURI(AUTHORITY, Videos.TABLE_NAME + "/#", Videos.CODE_ID);
        matcher.addURI(AUTHORITY, Smiles.TABLE_NAME, Smiles.CODE_ALL);
        matcher.addURI(AUTHORITY, Smiles.TABLE_NAME + "/#", Smiles.CODE_ID);
        matcher.addURI(AUTHORITY, Categories.TABLE_NAME, Categories.CODE_ALL);
        matcher.addURI(AUTHORITY, Categories.TABLE_NAME + "/#", Categories.CODE_ID);
        matcher.addURI(AUTHORITY, Tags.TABLE_NAME, Tags.CODE_ALL);
        matcher.addURI(AUTHORITY, Tags.TABLE_NAME + "/#", Tags.CODE_ID);
        matcher.addURI(AUTHORITY, Balances.TABLE_NAME, Balances.CODE_ALL);
        matcher.addURI(AUTHORITY, Balances.TABLE_NAME + "/#", Balances.CODE_ID);
        matcher.addURI(AUTHORITY, Votes.TABLE_NAME, Votes.CODE_ALL);
        matcher.addURI(AUTHORITY, Votes.TABLE_NAME + "/#", Votes.CODE_ID);
        matcher.addURI(AUTHORITY, VoteOptions.TABLE_NAME, VoteOptions.CODE_ALL);
        matcher.addURI(AUTHORITY, VoteOptions.TABLE_NAME + "/#", VoteOptions.CODE_ID);
        matcher.addURI(AUTHORITY, Cities.TABLE_NAME, Cities.CODE_ALL);
        matcher.addURI(AUTHORITY, Cities.TABLE_NAME + "/#", Cities.CODE_ID);
        matcher.addURI(AUTHORITY, Countries.TABLE_NAME, Countries.CODE_ALL);
        matcher.addURI(AUTHORITY, Countries.TABLE_NAME + "/#", Countries.CODE_ID);
        matcher.addURI(AUTHORITY, Info.TABLE_NAME, Info.CODE_ALL);
        matcher.addURI(AUTHORITY, Info.TABLE_NAME + "/#", Info.CODE_ID);
        matcher.addURI(AUTHORITY, Users.TABLE_NAME, Users.CODE_ALL);
        matcher.addURI(AUTHORITY, Users.TABLE_NAME + "/#", Users.CODE_ID);
        matcher.addURI(AUTHORITY, Feedback.TABLE_NAME, Feedback.CODE_ALL);
        matcher.addURI(AUTHORITY, Feedback.TABLE_NAME + "/#", Feedback.CODE_ID);
        matcher.addURI(AUTHORITY, FeedbackMessages.TABLE_NAME, FeedbackMessages.CODE_ALL);
        matcher.addURI(AUTHORITY, FeedbackMessages.TABLE_NAME + "/#", FeedbackMessages.CODE_ID);
        matcher.addURI(AUTHORITY, AudioTracks.TABLE_NAME, AudioTracks.CODE_ALL);
        matcher.addURI(AUTHORITY, AudioTracks.TABLE_NAME + "/#", AudioTracks.CODE_ID);
        return matcher;
    }

    public static class Videos {
        public static final String TABLE_NAME = "videos";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 100;
        public static final int CODE_ID = 200;

        public static class Cols {
            public static final String ID = "video_id";
            public static final String VIDEO_URL = "video_url";
            public static final String VIDEO_SHORT_URL = "video_short_url";
            public static final String THUMB_URL = "thumb_url";
            public static final String CREATED_AT = "created_at";
            public static final String AUTHOR_ID = "author_id";
            public static final String AUTHOR_NAME = "author_name";
            public static final String VIEWS = "views";
            public static final String LIKES = "likes";
            public static final String REPLIES = "replies";
            public static final String COMMENTS_COUNT = "comments_count";
            public static final String SMILE_ID = "smile_id";
            public static final String TAGS = "tags";
            public static final String LENGTH = "length";
            public static final String MY_MARK = "my_mark";
            public static final String GUESSED = "guessed";
            public static final String CITY_ID = "id_city";
            public static final String CITY_ENG = "city_eng";
            public static final String CITY_ESP = "city_esp";
            public static final String CITY_RUS = "city_rus";
            public static final String COUNTRY_ID = "id_country";
            public static final String COUNTRY_ENG = "country_eng";
            public static final String COUNTRY_ESP = "country_esp";
            public static final String COUNTRY_RUS = "country_rus";
            public static final String COST = "cost";
            public static final String AVAILABLE = "paid";
            public static final String GAME_ID = "game_id";
            public static final String REPLAY_ID = "replay_id";
            public static final String EARN = "earn";
            public static final String PRODUCT_TYPE = "product_type";
            public static final String TYPE = "type";
            public static final String CATEGORY_ID = "category_id";
            public static final String SCREEN = "screen";

            public static final String IS_AVATAR = "is_avatar";

            public static final String UPLOADING_STATUS = "to_upload";
            public static final String UPLOADED_VIDEO = "uploaded_video";
            public static final String UPLOADED_IMAGE = "uploaded_image";
        }
    }

    @Deprecated
    public static class Smiles {
        public static final String TABLE_NAME = "smiles";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 300;
        public static final int CODE_ID = 400;

        public static class Cols {
            public static final String ID = "id";
            public static final String PATH = "path";
            public static final String NAME = "name";
        }
    }

    public static class Categories {
        public static final String TABLE_NAME = "categories";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 500;
        public static final int CODE_ID = 600;

        public static class Cols {
            public static final String ID = "id";
            public static final String NAME_RUS = "name_rus";
            public static final String NAME_ENG = "name_eng";
            public static final String NAME_ESP = "name_esp";
            public static final String VIDEO_TYPE = "video_type";
            public static final String MAIN = "main";
            public static final String TECH = "tech";
            public static final String PARAMS = "params";
            public static final String THUMBNAIL = "thumb";
            public static final String VIDEO_COUNT = "video_count";
        }
    }

    @Deprecated
    public static class Tags {
        public static final String TABLE_NAME = "tags";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 700;
        public static final int CODE_ID = 800;

        public static class Cols {
            public static final String ID = "id";
            public static final String NAME = "name";
        }
    }

    public static class Balances {
        public static final String TABLE_NAME = "balance";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 900;
        public static final int CODE_ID = 1000;

        public static class Cols {
            public static final String ID = "id";
            public static final String TYPE = "type";
            public static final String CHANGE = "change";
            public static final String OBJECT_ID = "video_id";
            public static final String OBJECT_IMG = "video_img";
            public static final String CREATED_AT = "created_at";
        }
    }

    public static class Countries {
        public static final String TABLE_NAME = "countries";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1100;
        public static final int CODE_ID = 1200;

        public static class Cols {
            public static final String ID = "id";
            public static final String NAME_ENG = "name_eng";
            public static final String NAME_ESP = "name_esp";
            public static final String NAME_RUS = "name_rus";
            public static final String PHONE_CODE = "phone_code";
            public static final String PHONE_LENGHT = "phone_lenght";
        }
    }

    public static class Cities {
        public static final String TABLE_NAME = "cities";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1300;
        public static final int CODE_ID = 1400;

        public static class Cols {
            public static final String ID = "id";
            public static final String COUNTRY_ID = "country_id";
            public static final String NAME_ENG = "name_eng";
            public static final String NAME_ESP = "name_esp";
            public static final String NAME_RUS = "name_rus";
        }
    }

    public static class Votes {
        public static final String TABLE_NAME = "votes";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1700;
        public static final int CODE_ID = 1800;

        public static class Cols {
            public static final String ID = "id";
            public static final String TITLE_ENG = "title_eng";
            public static final String TITLE_ESP = "title_esp";
            public static final String TITLE_RUS = "title_rus";
            public static final String STATUS = "status";
            public static final String TYPE = "type";
            public static final String MY_VOTE = "my_vote";
            public static final String DESCR_ENG = "descr_eng";
            public static final String DESCR_ESP = "descr_esp";
            public static final String DESCR_RUS = "descr_rus";
            public static final String VOTES_TOTAL = "votes";
        }
    }

    public static class VoteOptions {
        public static final String TABLE_NAME = "options";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 1900;
        public static final int CODE_ID = 2000;

        public static class Cols {
            public static final String OPTION_ID = "id";
            public static final String VOTE_ID = "vote_id";
            public static final String VOTES = "votes";
            public static final String TITLE_ENG = "title_eng";
            public static final String TITLE_ESP = "title_esp";
            public static final String TITLE_RUS = "title_rus";
        }
    }

    @Deprecated
    public static class Info {
        public static final String TABLE_NAME = "info";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2100;
        public static final int CODE_ID = 2200;

        public static class Cols {
            public static final String TYPE = "type";
            public static final String TEXT = "_text";
        }
    }

    public static class Users {
        public static final String TABLE_NAME = "users";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2500;
        public static final int CODE_ID = 2600;

        public static final int RECORD_TYPE_SEARCH = 1;
        public static final int RECORD_TYPE_RATING_ALL = 2;
        public static final int RECORD_TYPE_RATING_APP = 3;

        public static class Cols {
            public static final String ID = "id";
            public static final String FIRST_NAME = "first_name";
            public static final String LAST_NAME = "last_name";
            public static final String PHOTO = "photo";
            public static final String ID_CITY = "id_city";
            public static final String ID_COUNTRY = "id_country";
            public static final String LANG = "lang";
            public static final String HOBBY = "hobby";
            public static final String SPORT = "sport";
            public static final String PETS = "pets";
            public static final String INTEREST = "interest";
            public static final String RELIGION = "religion";
            public static final String CRAFT = "craft";
            public static final String TAGS = "tags";
            public static final String RATING = "rating";
            public static final String COUNTRY_ENG = "country_eng";
            public static final String COUNTRY_ESP = "country_esp";
            public static final String COUNTRY_RUS = "country_rus";
            public static final String CITY_ENG = "city_eng";
            public static final String CITY_ESP = "city_esp";
            public static final String CITY_RUS = "city_rus";
            public static final String RECORD_TYPE = "type";
        }
    }

    public static class Feedback {
        public static final String TABLE_NAME = "feedback";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2700;
        public static final int CODE_ID = 2800;

        public static class Cols {
            public static final String ID = "id";
            public static final String TYPE = "type";
            public static final String LAST_MESSAGE = "last_message";
            public static final String NEW = "_new";
        }
    }

    public static class FeedbackMessages {
        public static final String TABLE_NAME = "feedback_messages";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 2900;
        public static final int CODE_ID = 3000;

        public static class Cols {
            public static final String ID_USER = "id_user";
            public static final String MESSAGE = "message";
            public static final String ID_FEEDBACK = "id_feedback";
            public static final String TIME = "time";
        }
    }

    @Deprecated
    public static class AudioTracks {
        public static final String TABLE_NAME = "audio_tracks";
        public static final Uri URI = Uri.parse("content://" + AUTHORITY + "/" + TABLE_NAME);
        public static final int CODE_ALL = 3100;
        public static final int CODE_ID = 3200;

        public static class Cols {
            public static final String TITLE = "title";
            public static final String PATH = "path";
            public static final String ARTIST = "artis";
        }
    }

}