package com.henesiz.db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by ovitali on 26.01.2015.
 * Changed by Denis on 25.02.2015
 */
public class DataProvider extends ContentProvider {
    private DBHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = DBHelper.getInstance(getContext());
        return true;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Cursor cursor = db.query(tableName, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public Uri insert(@NonNull Uri uri, @NonNull ContentValues values) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        long id = db.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_IGNORE);

        if (id != -1) {
            getContext().getContentResolver().notifyChange(uri, null);
            return Uri.withAppendedPath(uri, Long.toString(id));
        }

        return uri;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int delete(@NonNull Uri uri, @NonNull String selection, String[] selectionArgs) {
        String tableName = getTableName(uri);
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int count = db.delete(tableName, selection, selectionArgs);
        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int update(@NonNull Uri uri, @NonNull ContentValues values, String selection, String[] selectionArgs) {
        String tableName = getTableName(uri);

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int count = db.update(tableName, values, selection, selectionArgs);

        if (count > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return count;
    }

    @SuppressWarnings("ConstantConditions")
    public int bulkInsert(Uri uri, @NonNull ContentValues[] values) {
        int numInserted = 0;

        String tableName = getTableName(uri);

        SQLiteDatabase sqlDB = mDbHelper.getWritableDatabase();
        sqlDB.beginTransaction();
        try {
            for (ContentValues cv : values) {
                long newID = sqlDB.insertOrThrow(tableName, null, cv);
                if (newID <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            sqlDB.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);

            numInserted = values.length;
        } catch (Exception e) {
            Log.e("", e.getMessage());
        } finally {
            sqlDB.endTransaction();
        }
        return numInserted;
    }

    private String getTableName(Uri uri) {
        String tableName;
        switch (ContentDescriptor.URI_MATCHER.match(uri)) {
            case ContentDescriptor.Videos.CODE_ALL:
            case ContentDescriptor.Videos.CODE_ID:
                tableName = ContentDescriptor.Videos.TABLE_NAME;
                break;
            case ContentDescriptor.Smiles.CODE_ALL:
            case ContentDescriptor.Smiles.CODE_ID:
                tableName = ContentDescriptor.Smiles.TABLE_NAME;
                break;
            case ContentDescriptor.Categories.CODE_ALL:
            case ContentDescriptor.Categories.CODE_ID:
                tableName = ContentDescriptor.Categories.TABLE_NAME;
                break;
            case ContentDescriptor.Tags.CODE_ALL:
            case ContentDescriptor.Tags.CODE_ID:
                tableName = ContentDescriptor.Tags.TABLE_NAME;
                break;
            case ContentDescriptor.Balances.CODE_ALL:
            case ContentDescriptor.Balances.CODE_ID:
                tableName = ContentDescriptor.Balances.TABLE_NAME;
                break;
            case ContentDescriptor.Cities.CODE_ALL:
            case ContentDescriptor.Cities.CODE_ID:
                tableName = ContentDescriptor.Cities.TABLE_NAME;
                break;
            case ContentDescriptor.Countries.CODE_ALL:
            case ContentDescriptor.Countries.CODE_ID:
                tableName = ContentDescriptor.Countries.TABLE_NAME;
                break;
            case ContentDescriptor.Votes.CODE_ALL:
            case ContentDescriptor.Votes.CODE_ID:
                tableName = ContentDescriptor.Votes.TABLE_NAME;
                break;
            case ContentDescriptor.VoteOptions.CODE_ALL:
            case ContentDescriptor.VoteOptions.CODE_ID:
                tableName = ContentDescriptor.VoteOptions.TABLE_NAME;
                break;
            case ContentDescriptor.Info.CODE_ALL:
            case ContentDescriptor.Info.CODE_ID:
                tableName = ContentDescriptor.Info.TABLE_NAME;
                break;
            case ContentDescriptor.Users.CODE_ALL:
            case ContentDescriptor.Users.CODE_ID:
                tableName = ContentDescriptor.Users.TABLE_NAME;
                break;
            case ContentDescriptor.Feedback.CODE_ID:
            case ContentDescriptor.Feedback.CODE_ALL:
                tableName = ContentDescriptor.Feedback.TABLE_NAME;
                break;
            case ContentDescriptor.FeedbackMessages.CODE_ID:
            case ContentDescriptor.FeedbackMessages.CODE_ALL:
                tableName = ContentDescriptor.FeedbackMessages.TABLE_NAME;
                break;
            case ContentDescriptor.AudioTracks.CODE_ID:
            case ContentDescriptor.AudioTracks.CODE_ALL:
                tableName = ContentDescriptor.AudioTracks.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Can't recognize uri!");
        }

        return tableName;
    }
}
