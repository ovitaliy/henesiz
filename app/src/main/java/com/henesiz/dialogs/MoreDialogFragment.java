package com.henesiz.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.henesiz.App;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.MainActivity;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.fragment.FlowListFragment;
import com.henesiz.model.Video;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.request.video.ComplaintVideoRequest;
import com.henesiz.view.dialogViews.MoreDialogView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by michael on 25.11.14.
 */
public class MoreDialogFragment extends DialogFragment implements Const {
    public static final String TAG = "MoreDialog";

    private MoreDialogView mView;
    private Video mVideo;
    private Video mVideoParent;
    private int mAction = -1;
    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args.containsKey("video")) {
            mVideo = (Video) args.getSerializable("video");
        }

        if (args.containsKey("video_parent")) {
            mVideoParent = (Video) args.getSerializable("video_parent");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mView = new MoreDialogView(getActivity());

        RadioButton responsesButton = (RadioButton) mView.findViewById(R.id.rb_watch_responses);
        if (mVideo.getReplays() > 0) {
            responsesButton.setVisibility(View.VISIBLE);
            responsesButton.setText(App.getInstance().getString(R.string.record_answer_watch, mVideo.getReplays()));
        } else {
            responsesButton.setVisibility(View.GONE);
        }

        RadioGroup rg = (RadioGroup) mView.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mAction = checkedId;
                switch (mAction) {
                    case R.id.rb_share:
                        new ShareDialogFragment.Builder()
                                .setLink(mVideo.getShortUrl())
                                .setVideoId(mVideo.getVideoId())
                                .setCanBeClosed(true)
                                .build()
                                .show(getFragmentManager(), ShareDialogFragment.TAG);
                        dismiss();
                        break;
                    case R.id.rb_complaint:
                        mSpiceManager.execute(new ComplaintVideoRequest(mVideo.getVideoId()), new BaseRequestListener());
                        dismiss();
                        break;
                    case R.id.rb_answer:
                        recordAnswer();
                        dismiss();
                        break;
                    case R.id.rb_watch_responses:
                        watchResponses();
                        break;
                    default:
                        dismiss();
                        break;
                }
            }
        });

        Dialog dialog = new AlertDialog.Builder(getActivity())
                .setView(mView)
                .show();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.CENTER);
        return dialog;
    }

    public static MoreDialogFragment newInstance(Video video, Video videoParent) {
        MoreDialogFragment fragment = new MoreDialogFragment();
        Bundle args = new Bundle(2);
        args.putSerializable("video", video);
        args.putSerializable("video_parent", videoParent);
        fragment.setArguments(args);
        return fragment;
    }

    private void recordAnswer() {
        CaptureActivity.startNewInstance(getActivity(), mVideo);
    }

    private void watchResponses() {
        FlowListFragment fragment = new FlowListFragment.Builder(null)
                .setReplyId(mVideo.getVideoId())
                .setTitle(getString(R.string.dialog_responses_watch))
                .build();

        ((MainActivity) getActivity()).startFragment(fragment, true);
        dismiss();
    }

}
