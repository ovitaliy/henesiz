package com.henesiz.dialogs;

import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.ContactsActivity;
import com.henesiz.util.Utils;

import java.util.ArrayList;

public class SelectContactsTypeDialog extends DialogFragment implements Const {
    private AsyncTask<Integer, Void, ArrayList<ContactsActivity.Group>> mTask;

    private int mSelectedGroup;

    public SelectContactsTypeDialog setCallback(AsyncTask<Integer, Void, ArrayList<ContactsActivity.Group>> task) {
        mTask = task;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_select_contact_group, container, false);
        RadioGroup rg = (RadioGroup) view.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedGroup = checkedId;
            }
        });
        view.findViewById(R.id.fab).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mTask.execute(mSelectedGroup);
                dismiss();
            }
        });
        rg.check(R.id.rb_fav);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(Utils.convertDpToPixel(300, getActivity()), ActionBar.LayoutParams.WRAP_CONTENT);
    }
}
