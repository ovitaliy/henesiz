package com.henesiz.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.henesiz.App;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.ContactsActivity;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.request.video.ShareVideoRequest;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.view.dialogViews.ShareDialogView;
import com.octo.android.robospice.SpiceManager;

public class ShareDialogFragment extends DialogFragment implements Const, View.OnClickListener {

    public static final String TAG = "ShareDialog";

    private String mLink;
    private int mVideoId;
    private boolean mCanBeClosed;

    SpiceManager mSpiceManager;

    View mCloseButton;

    private int mSelectedShareMethod;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args.containsKey("video_id")) {
            mVideoId = args.getInt("video_id");
        }

        if (args.containsKey("closeable")) {
            mCanBeClosed = getArguments().getBoolean("closeable");
        }

        if (args.containsKey("link")) {
            mLink = getArguments().getString("link");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = new ShareDialogView(getActivity());

        RadioGroup rg = (RadioGroup) view.findViewById(R.id.group_share);
        rg.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mSelectedShareMethod = checkedId;
            }
        });

        view.findViewById(R.id.btn_nav).setOnClickListener(this);
        mCloseButton = view.findViewById(R.id.btn_close);
        mCloseButton.setOnClickListener(this);
        if (isCanBeClosed()) {
            mCloseButton.setVisibility(View.VISIBLE);
        } else {
            mCloseButton.setVisibility(View.GONE);
        }

        AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.Dialog_No_Border)
                .setView(view)
                .setCancelable(false)
                .show();
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (getActivity() instanceof CaptureActivity) {
            getActivity().finish();
        }
    }

    private String getText(String url) {
        return App.getInstance().getString(R.string.share_other, url);
    }

    private void share(int shareMethod) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.SHARE);
        String text = "Henesiz";
        switch (shareMethod) {
            case R.id.rb_sms:
                getActivity().startActivity(ContactsActivity.newShareInstance(getActivity(), text + " " + mLink, mVideoId));
                break;
            case R.id.rb_msg:
                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.setType("text/plain");
                msg.putExtra(Intent.EXTRA_SUBJECT, text);
                msg.putExtra(Intent.EXTRA_TEXT, getText(mLink));
                getActivity().startActivity(Intent.createChooser(msg, App.getInstance().getString(R.string.ch_msg)));
                mSpiceManager.execute(new ShareVideoRequest(mVideoId, "messenger"), new BaseRequestListener());
                break;
            default:
                break;
        }

        //we should make close button available immediatly after sharing
        if (mCloseButton.getVisibility() != View.VISIBLE) {
            mCloseButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_nav:
                share(mSelectedShareMethod);
                break;
            case R.id.btn_close:
                dismiss();
                break;
        }
    }

    public boolean isCanBeClosed() {
        return mCanBeClosed;
    }

    public static class Builder {
        Bundle mArgs;

        public Builder() {
            mArgs = new Bundle(4);
        }

        public ShareDialogFragment build() {
            ShareDialogFragment fragment = new ShareDialogFragment();
            fragment.setArguments(mArgs);
            return fragment;
        }

        public Builder setLink(String link) {
            mArgs.putString("link", link);
            return this;
        }

        public Builder setVideoId(int videoId) {
            mArgs.putInt("video_id", videoId);
            return this;
        }

        public Builder setCanBeClosed(boolean canBeClosed) {
            mArgs.putBoolean("closeable", canBeClosed);
            return this;
        }
    }
}
