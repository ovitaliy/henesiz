package com.henesiz.enums;

import com.henesiz.App;
import com.henesiz.R;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public enum BalanceChangeType {

    REGISTRATION {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_registration);
        }

        @Override
        public int getId() {
            return 200;
        }
    },
    FIRST_VIDEO {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_first_video);
        }

        @Override
        public int getId() {
            return 101;
        }
    },
    FIRST_SHARE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_first_share);
        }

        @Override
        public int getId() {
            return 102;
        }
    },
    FILL_EXTENDED_PROFILE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_fill_profile);
        }

        @Override
        public int getId() {
            return 201;
        }
    },
    INVITE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_invite_friend);
        }

        @Override
        public int getId() {
            return 202;
        }
    },
    ADD_VIDEO {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_add_video);
        }

        @Override
        public int getId() {
            return 203;
        }
    },
    VIDEO_SHARE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_share_video);
        }

        @Override
        public int getId() {
            return 204;
        }
    },
    CREATE_VLOG {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_create_vlog);
        }

        @Override
        public int getId() {
            return 205;
        }
    },
    FIRST_100 {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_first_100);
        }

        @Override
        public int getId() {
            return 108;
        }
    },
    LEFT_LIKE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_left_like);
        }

        @Override
        public int getId() {
            return 206;
        }
    },
    RECEIVE_LIKE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_receive_like);
        }

        @Override
        public int getId() {
            return 207;
        }
    },
    TRY_GUESS_SMILE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_trying_guess_smile);
        }

        @Override
        public int getId() {
            return 111;
        }
    },
    GUESS_SMILE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_guess_smile);
        }

        @Override
        public int getId() {
            return 112;
        }
    },
    BUY_WIDGET {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_buy_widget);
        }

        @Override
        public int getId() {
            return 212;
        }
    },
    BUY_GRIMACE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_buy_grimace);
        }

        @Override
        public int getId() {
            return 217;
        }
    },
    ADD_GRIMACE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_add_grimace);
        }

        @Override
        public int getId() {
            return 214;
        }
    },
    SHARE_GRIMACE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_share_grimace);
        }

        @Override
        public int getId() {
            return 215;
        }
    },
    SELL_GRIMACE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_sell_grimace);
        }

        @Override
        public int getId() {
            return 216;
        }
    },
    FILL_PORTFOLIO {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_fill_portfolio);
        }

        @Override
        public int getId() {
            return 118;
        }
    },
    SUBSCRIBE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_subscribe);
        }

        @Override
        public int getId() {
            return 210;
        }
    },
    NEW_SUBSCRIBER {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_new_subscriber);
        }

        @Override
        public int getId() {
            return 211;
        }
    },
    VOTE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_vote);
        }

        @Override
        public int getId() {
            return 209;
        }
    },
    CREATE_POLL {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_create_poll);
        }

        @Override
        public int getId() {
            return 208;
        }
    },
    CROWD_RECEIVE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_crowd_receive);
        }

        @Override
        public int getId() {
            return 226;
        }
    },
    CROWD_GIVE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_crowd_give);
        }

        @Override
        public int getId() {
            return 227;
        }
    },
    DIY_SHARE_SKILL {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.video_type_diy_give);
        }

        @Override
        public int getId() {
            return 218;
        }
    },
    DIY_SHARE_OBJECT {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.video_type_diy_give);
        }

        @Override
        public int getId() {
            return 219;
        }
    },
    DIY_GET_SKILL {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.video_type_diy_ask);
        }

        @Override
        public int getId() {
            return 220;
        }
    },
    DIY_GET_OBJECT {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.video_type_diy_ask);
        }

        @Override
        public int getId() {
            return 221;
        }
    },
    ALIEN_LOOK_CREATE_TASK {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_alien_look_create_task);
        }

        @Override
        public int getId() {
            return 222;
        }
    },
    ALIEN_LOOK_PERFORM_TASK {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_alien_look_perform_task);
        }

        @Override
        public int getId() {
            return 223;
        }
    },
    ALIEN_LOOK_CREATE_CHALLENGE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_alien_look_create_challenge);
        }

        @Override
        public int getId() {
            return 224;
        }
    },
    ALIEN_LOOK_MEET_CHALLENGE {
        @Override
        public String getTitle() {
            return App.getInstance().getString(R.string.moncoins_history_alien_look_meet_challenge);
        }

        @Override
        public int getId() {
            return 225;
        }
    }
    ;

    public abstract String getTitle();

    public abstract int getId();

    public static BalanceChangeType getById(int id) {
        for (BalanceChangeType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public static class Deserializer implements JsonDeserializer<BalanceChangeType> {
        @Override
        public BalanceChangeType deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
            return getById(element.getAsInt());
        }
    }

}
