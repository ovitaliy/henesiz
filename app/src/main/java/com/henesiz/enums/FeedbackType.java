package com.henesiz.enums;

import com.henesiz.rest.ServerRequestParams;

/**
 * Created by Deni on 06.07.2015.
 */
public enum FeedbackType implements ServerRequestParams {
    SUGGESTION, ERROR, COMPLAIN;

    public int getId() {
        return ordinal();
    }

    @Override
    public String getRequestParam() {
        return String.valueOf(ordinal());
    }

    public static FeedbackType getById(int id) {
        return values()[id];
    }
}