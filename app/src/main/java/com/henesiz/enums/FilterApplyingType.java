package com.henesiz.enums;

/**
 * Created by Denis on 13.05.2015.
 */
public enum FilterApplyingType {
    ONLY_PREVIEW, ONLY_VIDEO, ALL
}
