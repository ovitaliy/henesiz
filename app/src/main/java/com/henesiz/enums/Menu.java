package com.henesiz.enums;

import com.henesiz.R;

public enum Menu {
    VISTORY {
        public int getTitle() {
            return R.string.menu_vistory;
        }

        @Override
        public int getImage() {
            return R.drawable.menu_vistory_selector;
        }
    }, DIY {
        public int getTitle() {
            return R.string.menu_diy;
        }

        @Override
        public int getImage() {
            return R.drawable.menu_diy_selector;
        }

    }, ALIEN_LOOK {
        public int getTitle() {
            return R.string.menu_alien_look;
        }

        @Override
        public int getImage() {
            return R.drawable.menu_alien_look_selector;
        }
    }, MARKETPLACE {
        public int getTitle() {
            return R.string.menu_marketplace;
        }

        @Override
        public int getImage() {
            return R.drawable.menu_marketplace_selector;
        }
    }, INFO {
        public int getTitle() {
            return R.string.menu_info;
        }

        @Override
        public int getImage() {
            return R.drawable.menu_info_selector;
        }
    };

    public abstract int getTitle();

    public abstract int getImage();
}
