package com.henesiz.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.view.CirclePickerItem;
import com.henesiz.rest.ServerRequestParams;

/**
 * Created by Deni on 21.07.2015.
 */
public enum ProductType implements CirclePickerItem, ServerRequestParams {
    PRODUCT, SERVICE;

    @Override
    public String getRequestParam() {
        return String.valueOf(ordinal());
    }

    public int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String emotionName = "product_" + name().toLowerCase();
        return context.getResources().getIdentifier(emotionName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Drawable drawable;
        int resId = getResId(App.getInstance(), "drawable");
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        } else {
            drawable = ContextCompat.getDrawable(App.getInstance(), R.drawable.beach);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal() + 1;
    }

    @Override
    public String getTitle() {
        String title = null;
        Context context = App.getInstance();
        int resId = getResId(context, "string");
        if (resId != 0) {
            title = context.getString(resId);
        }
        return title;
    }

    public static ProductType getById(int id) {
        if (id < 1) {
            id = 1;
        }
        for (ProductType productType : values()) {
            if (productType.getId() == id) {
                return productType;
            }
        }
        return null;
    }
}
