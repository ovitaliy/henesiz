package com.henesiz.enums;

import com.henesiz.App;
import com.henesiz.R;

/**
 * Created by Deni on 17.08.2015.
 */
public enum ServerError {
    WRONG_SMS_CODE(300, R.string.error_sms_wrongcode),
    WRONG_ACCESS_TOKEN(4, -1),
    NO_NETWORK(1, R.string.video_displaying_no_internet);

    private int mMessageResId;
    private int mCode;

    ServerError(int code, int messageResId) {
        mMessageResId = messageResId;
        mCode = code;
    }

    public String getMessage() {
        if (mMessageResId != -1) {
            return App.getInstance().getString(mMessageResId);
        } else {
            return "";
        }
    }

    public int getCode() {
        return mCode;
    }

    public static ServerError getByCode(int code) {
        for (ServerError error : values()) {
            if (error.getCode() == code) {
                return error;
            }
        }

        return null;
    }

}
