package com.henesiz.enums;

/**
 * Created by Denis on 22.04.2015.
 */
public enum Side {
    LEFT, RIGHT;

    public int getId() {
        return ordinal() + 1;
    }

    public static Side getById(int id) {
        for (Side side : values()) {
            if (side.getId() == id) {
                return side;
            }
        }
        return null;
    }
}
