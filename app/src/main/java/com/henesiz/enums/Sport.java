package com.henesiz.enums;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;

import com.henesiz.App;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.view.EditableCirclePickerItem;
import com.henesiz.rest.ServerRequestParams;
import com.henesiz.util.Utils;

/**
 * Created by Denis on 05.03.2015.
 */
public enum Sport implements EditableCirclePickerItem, ServerRequestParams {
    FOOTBALL,
    BASKETBALL,
    HOCKEY,
    VOLLEYBALL,
    BASEBALL,
    FITNESS,
    YOGA,
    CYCLING,
    SWIMMING,
    MARTIAL,
    WINTER,
    TENNIS,
    EXTREME,
    RUGBY,
    NATIONAL,
    OTHER_OPTION;

    private String mCustomTitle;

    private int getResId(Context context, String type) {
        String packageName = context.getPackageName();
        String resName = "";
        if (!isOtherOption()) {
            resName += "sport_";
        }
        resName += name().toLowerCase();
        return context.getResources().getIdentifier(resName, type, packageName);
    }

    @Override
    public Drawable getDrawable() {
        Context context = App.getInstance();
        Drawable drawable;
        int resId = getResId(context, Const.RES_TYPE_DRAWABLE);
        if (resId != 0) {
            drawable = ContextCompat.getDrawable(App.getInstance(), resId);
        } else {
            drawable = ContextCompat.getDrawable(App.getInstance(), R.drawable.beach);
        }
        return drawable;
    }

    @Override
    public int getId() {
        return ordinal() + 1;
    }

    @Override
    public String getTitle() {
        if (!TextUtils.isEmpty(mCustomTitle) || isOtherOption()) {
            return mCustomTitle;
        } else {
            String title = null;
            Context context = App.getInstance();
            int resId = getResId(context, Const.RES_TYPE_STRING);
            if (resId != 0) {
                title = context.getString(resId);
            }
            return title;
        }
    }

    @Override
    public boolean isOtherOption() {
        return this.equals(OTHER_OPTION);
    }

    @Override
    public void setTitle(String title) {
        mCustomTitle = title;
    }

    public static Sport getById(String id) {
        Sport result = null;
        if (Utils.isInteger(id)) {
            for (Sport item : values()) {
                if (item.getId() == Integer.parseInt(id)) {
                    result = item;
                }
            }
        }
        if (result == null) {
            result = OTHER_OPTION;
            result.setTitle(id);
        }
        return result;
    }

    @Override
    public String getRequestParam() {
        if (isOtherOption()) {
            return getTitle();
        }
        return String.valueOf(getId());
    }
}
