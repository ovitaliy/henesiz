package com.henesiz.enums;

import com.henesiz.R;
import com.henesiz.rest.ServerRequestParams;

/**
 * Created by Denis on 06.04.2015.
 */
public enum VideoType implements ServerRequestParams {
    AVATAR {
        public int getCode() {
            return 1;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "all";
        }

        public int getTitle() {
            return -1;
        }
    },

    FLOW_ALL {
        public int getCode() {
            return 0;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "all";
        }

        public int getTitle() {
            return -1;
        }
    },

    FLOW_BEST {
        public int getCode() {
            return 0;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "best";
        }

        public int getTitle() {
            return -1;//shouldn't be showed
        }
    },

    FLOW_MY_CHOICE {
        public int getCode() {
            return 0;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "my";
        }

        public int getTitle() {
            return -1;//shouldn't be showed
        }
    },

    VLOG {
        public int getCode() {
            return 2;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return null;
        }

        public int getTitle() {
            return R.string.video_type_vlog;
        }
    },

    CROWD_ASK {
        public int getCode() {
            return 6;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "please";
        }

        public int getTitle() {
            return R.string.video_type_crowd;
        }
    },

    CROWD_GIVE {
        public int getCode() {
            return 6;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "give";
        }

        public int getTitle() {
            return -1;
        }

    },

    NEWS {
        public int getCode() {
            return 10;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return null;
        }

        public int getTitle() {
            return R.string.video_type_news;
        }
    },

    DIY_GIVE_SKILL {
        public int getCode() {
            return 14;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "skill";
        }

        public int getTitle() {
            return R.string.video_type_diy;
        }
    },

    DIY_GIVE_ITEM {
        public int getCode() {
            return 14;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "item";
        }

        public int getTitle() {
            return -1;
        }
    },

    DIY_GET_SKILL {
        public int getCode() {
            return 15;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "skill";
        }

        public int getTitle() {
            return -1;
        }
    },

    DIY_GET_ITEM {
        public int getCode() {
            return 15;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "item";
        }

        public int getTitle() {
            return -1;
        }
    },

    ALIEN_LOOK_TASK {
        public int getCode() {
            return 16;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "look";
        }

        public int getTitle() {
            return R.string.video_type_look;
        }
    },

    ALIEN_LOOK_CHALLENGE {
        public int getCode() {
            return 16;
        }

        @Override
        public String getRequestParam() {
            return String.valueOf(getCode());
        }

        public String getAdditionParam() {
            return "challenge";
        }

        public int getTitle() {
            return -1;
        }
    };

    public static VideoType getById(int id) {
        for (VideoType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }
        return null;
    }

    public static VideoType getByCode(int code) {
        for (VideoType type : values()) {
            if (type.getCode() == code) {
                return type;
            }
        }
        return null;
    }

    public int getId() {
        return ordinal() + 1;
    }

    public boolean isFlowType() {
        return this.equals(FLOW_ALL) || this.equals(FLOW_BEST) || this.equals(FLOW_MY_CHOICE);
    }

    public int getCode() {
        return this.getCode();
    }

    @Override
    public String getRequestParam() {
        return this.getRequestParam();
    }

    public String getAdditionParam() {
        return this.getAdditionParam();
    }

    public int getTitle() {
        return this.getTitle();
    }
}
