package com.henesiz.enums;

import com.henesiz.App;
import com.henesiz.R;

/**
 * Created by Denis on 23.04.2015.
 */
public enum VoteListStatus {
    FINISHED, ACTIVE, CREATED, DECLINED;

    public int getId() {
        switch (this) {
            case FINISHED:
                return 0;
            case ACTIVE:
                return 1;
            case CREATED:
                return 3;
            default:
                return 4;
        }
    }

    public String getTitle() {
        switch (this) {
            case CREATED:
                return App.getInstance().getString(R.string.vote_status_wait);
            case DECLINED:
                return App.getInstance().getString(R.string.vote_status_declined);
            default:
                return App.getInstance().getString(R.string.vote_status_approoved);
        }
    }

    public static VoteListStatus getById(int id) {
        switch (id) {
            case 0:
                return FINISHED;
            case 1:
                return ACTIVE;
            case 3:
                return CREATED;
            default:
                return DECLINED;
        }
    }
}
