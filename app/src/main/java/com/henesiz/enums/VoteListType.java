package com.henesiz.enums;

import com.henesiz.rest.ServerRequestParams;

/**
 * Created by Denis on 28.04.2015.
 */
public enum VoteListType implements ServerRequestParams {
    NOW {
        @Override
        public String getRequestParam() {
            return "now";
        }
    }, PAST {
        @Override
        public String getRequestParam() {
            return "past";
        }
    },
    MY{
        @Override
        public String getRequestParam() {
            return null;
        }
    }
}
