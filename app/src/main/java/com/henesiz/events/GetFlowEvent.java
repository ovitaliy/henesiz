package com.henesiz.events;

import com.henesiz.model.Video;

import java.util.ArrayList;

/**
 * New response with videos received
 */
public class GetFlowEvent {
    ArrayList<Video> mVideos = new ArrayList<>();

    public GetFlowEvent(ArrayList<Video> list) {
        mVideos.clear();
        mVideos.addAll(list);
    }

    public ArrayList<Video> getVideos() {
        return mVideos;
    }
}