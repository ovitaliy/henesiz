package com.henesiz.events;

/**
 * New file downloaded to path
 */
public class LoadedFileEvent {
    String mPath;

    public LoadedFileEvent(String path) {
        mPath = path;
    }

    public String getPath() {
        return mPath;
    }
}
