package com.henesiz.events;

/**
 * Server error received
 */
public class ServerErrorEvent {
    int[] mCodes;
    String[] mMessages;

    public ServerErrorEvent(int codes[], String[] messages) {
        mCodes = codes;
        mMessages = messages;
    }

    public int[] getCodes() {
        return mCodes;
    }

    public String[] getMessages() {
        return mMessages;
    }
}
