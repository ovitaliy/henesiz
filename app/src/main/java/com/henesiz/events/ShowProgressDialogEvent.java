package com.henesiz.events;

/**
 * Force show progress dialog
 */
public class ShowProgressDialogEvent {
    private boolean mShow;

    public ShowProgressDialogEvent(boolean show) {
        mShow = show;
    }

    public boolean isShow() {
        return mShow;
    }
}
