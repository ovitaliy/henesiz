package com.henesiz.events;

/**
 * Video was captured and processed
 */
public class VideoProcessedEvent {
    private boolean mSuccess;

    public VideoProcessedEvent(boolean success) {
        mSuccess = success;
    }

    public boolean isSuccess() {
        return mSuccess;
    }
}
