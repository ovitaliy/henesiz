package com.henesiz.events;

/**
 * Video uploaded to server
 */
public class VideoUploadedEvent {
    private boolean mSuccess;
    private int mVideoId;
    private String mShortUrl;
    private boolean mAvatar;

    public VideoUploadedEvent(boolean success, int videoId, String shortUrl, boolean avatar) {
        mSuccess = success;
        mVideoId = videoId;
        mShortUrl = shortUrl;
        mAvatar = avatar;
    }

    public int getVideoId() {
        return mVideoId;
    }

    public String getShortUrl() {
        return mShortUrl;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    public boolean isAvatar() {
        return mAvatar;
    }
}
