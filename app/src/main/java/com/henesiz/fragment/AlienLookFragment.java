package com.henesiz.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.henesiz.R;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.adapter.SelectableFragmentStatePagerAdapter;
import com.henesiz.enums.VideoType;

public class AlienLookFragment extends BaseFragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    TabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    TabLayout mTabLayout;

    private Tab mCurrentTab = Tab.TASK;

    public enum Tab {
        TASK, CHALLENGE;
    }

    public static AlienLookFragment newInstance() {
        AlienLookFragment f = new AlienLookFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_alien_look);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentTab = (Tab) savedInstanceState.getSerializable("tab");
//            mCurrentItem = (SimpleListItem) savedInstanceState.getSerializable("item");
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.new_video_shot:
                VideoType type;
                switch (mTabLayout.getSelectedTabPosition()) {
                    case 0:
                        type = VideoType.ALIEN_LOOK_TASK;
                        break;
                    default:
                        type = VideoType.ALIEN_LOOK_CHALLENGE;
                        break;
                }
                CaptureActivity.startNewInstance(getActivity(), type);
                break;
            case R.id.open_flow:
                openFlow();
                break;
        }
    }

    private void openFlow() {
        ((CategoriesPreviewFragment) mTabsAdapter.getCurrentFragment()).openFlowList();
    }

    @Override
    protected void initUI(final View v, Bundle savedInstanceState) {
        mTabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabsAdapter);

        mViewPager.addOnPageChangeListener(this);

        mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.alien_look_tasks));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.alien_look_challenge));

        mTabsAdapter.notifyDataSetChanged();
        mTabLayout.setOnTabSelectedListener(this);

        v.findViewById(R.id.new_video_shot).setOnClickListener(this);
        v.findViewById(R.id.open_flow).setOnClickListener(this);
    }

    @Override
    public void unregisterForContextMenu(View view) {
        super.unregisterForContextMenu(view);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("item", mCurrentItem);
        outState.putSerializable("tab", mCurrentTab);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_alien_look);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class TabsAdapter extends SelectableFragmentStatePagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = TaskFragment.newInstance();
                    break;
                default:
                    fragment = ChallengeFragment.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    public static final class ChallengeFragment extends CategoriesPreviewFragment {
        public static ChallengeFragment newInstance() {
            ChallengeFragment fragment = new ChallengeFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.alien_look_challenge);
        }

        @Override
        public VideoType getVideoType() {
            return VideoType.ALIEN_LOOK_CHALLENGE;
        }
    }

    public static final class TaskFragment extends CategoriesPreviewFragment {
        public static TaskFragment newInstance() {
            TaskFragment fragment = new TaskFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.alien_look_tasks);
        }

        @Override
        public VideoType getVideoType() {
            return VideoType.ALIEN_LOOK_TASK;
        }
    }
}