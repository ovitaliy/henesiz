package com.henesiz.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.R;
import com.henesiz.adapter.BalanceListAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.Balance;
import com.henesiz.model.UserInfo;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.model.CoinsHistoryListResponse;
import com.henesiz.rest.request.user.GetCoinsHistoryRequest;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.List;

/**
 * Created by Denis on 03.03.2015.
 */
public class BalanceFragment extends BaseFragment implements
        LoaderManager.LoaderCallbacks<Cursor>,
        AbsListView.OnScrollListener {

    private static final int BALANCE_LOADER_ID = 2;
    private ListView mListView;

    private BalanceListAdapter mAdapterHistory;
    SpiceManager mSpiceManager;

    public static BalanceFragment newInstance() {
        BalanceFragment fragment = new BalanceFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_balance);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        UserInfo user = AppUser.get();

        if (user.getFirstName() != null)
            ((TextView) view.findViewById(R.id.name)).setText(user.getFullName());

        if (!TextUtils.isEmpty(user.getPhoto())) {
            ImageLoader.getInstance().displayImage(user.getImageLoaderPhoto(), (ImageView) view.findViewById(R.id.avatar));
        }

        ((TextView) view.findViewById(R.id.balance_mon_coin)).setText(String.valueOf(user.getBalance()));

        mListView = (ListView) view.findViewById(R.id.transactions);
        mAdapterHistory = new BalanceListAdapter(getActivity(), null);
        mListView.setAdapter(mAdapterHistory);

        getActivity().getSupportLoaderManager().initLoader(BALANCE_LOADER_ID, null, this);

        mListView.setOnScrollListener(this);
        loadBalance();
    }

    private synchronized void loadBalance() {
        Log.i("LOAD BALANCE", "last id:" + mLastId);
        mSpiceManager.execute(new GetCoinsHistoryRequest(mLastId), new RequestListener<CoinsHistoryListResponse>() {
            @Override
            public void onRequestFailure(SpiceException spiceException) {
                mLoading = false;
            }

            @Override
            public void onRequestSuccess(final CoinsHistoryListResponse response) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (response != null) {
                            List<Balance> history = response.getHistory();
                            if (history != null) {
                                int count = history.size();
                                if (count != 0) {
                                    if (mLastId == 0) {
                                        App.getInstance().getContentResolver().delete(ContentDescriptor.Balances.URI, null, null);
                                    }
                                    mLastId = history.get(count - 1).getId();

                                    ContentValues[] contentValueses = new ContentValues[count];
                                    for (int i = 0; i < count; i++) {
                                        Balance balance = history.get(i);
                                        contentValueses[i] = balance.toContentValues();
                                    }

                                    App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Balances.URI, contentValueses);
                                }

                                if (count < 25) {
                                    mIsLastPage = true;
                                }
                            }
                        }
                        mLoading = false;
                    }
                }).start();
                if (isAdded()) {
                    if (response != null && response.isSuccess()) {
                        int balance = response.getBalance();
                        UserInfo userInfo = AppUser.get();
                        userInfo.setBalance(balance);
                        AppUser.set(userInfo);
                        ((TextView) getView().findViewById(R.id.balance_mon_coin)).setText(String.valueOf(balance));
                    }
                }
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                ContentDescriptor.Balances.URI,
                null,
                null,
                null,
                ContentDescriptor.Balances.Cols.CREATED_AT + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == BALANCE_LOADER_ID) {
            mAdapterHistory.swapCursor(data);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (loader.getId() == BALANCE_LOADER_ID) {
            mAdapterHistory.swapCursor(null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mListView.scrollTo(0, mLastVisiblePosition + 1);
    }

    @Override
    public String getTitle() {
        return getString(R.string.wow);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    private boolean mIsLastPage;
    private int mLastVisiblePosition;
    private int mLastId;
    private boolean mLoading;

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        mLastVisiblePosition = firstVisibleItem;
        if (!mIsLastPage && !mLoading && totalItemCount >= 25 && firstVisibleItem + visibleItemCount > totalItemCount - 5) {
            mLoading = true;
            loadBalance();
        }
    }
}
