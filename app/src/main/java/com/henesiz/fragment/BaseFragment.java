package com.henesiz.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.henesiz.Const;
import com.henesiz.activities.BaseActivity;
import com.henesiz.activities.MainActivity;
import com.henesiz.dialogs.ProgressDialog;
import com.henesiz.events.ShowProgressDialogEvent;
import com.henesiz.util.NetworkUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public abstract class BaseFragment extends Fragment implements Const, View.OnClickListener {

    private volatile ProgressDialog mProgressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutRes = getArguments().getInt(EXTRA_LAYOUT_ID);
        final View view = inflater.inflate(layoutRes, container, false);
        initUI(view, savedInstanceState);
        return view;
    }

    @Override
    public void onClick(View v) {

    }

    /*--------------------------------------------------------------------------------------------*/
    /*Custom methods*/
    /*--------------------------------------------------------------------------------------------*/

    protected void initUI(View view, Bundle savedInstanceState) {
    }

    private void updateTitle() {
        ((BaseActivity) getActivity()).setActionBarTitle(getTitle());
        ((BaseActivity) getActivity()).setActionBarOfflineMode(NetworkUtil.isOfflineMode());
    }

    private void initActionBarLeftButton() {
//        ActionBar actionBar = ((BaseActivity) getActivity()).getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            setHasOptionsMenu(true);
//            if (hasSideMenu()) {
//                actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.btn_menu));
//            } else {
//                actionBar.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.btn_back));
//            }
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTitle();
        EventBus.getDefault().register(this);
        initActionBarLeftButton();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (hasSideMenu()) {
                    ((MainActivity) getActivity()).toggleDrawerMenu();
                    return true;
                } else {
                    onBackPressed();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowProgressDialogEvent(ShowProgressDialogEvent event) {
        if (getProgressDialog() != null) {
            if (event.isShow()) {
                getProgressDialog().show();
            } else {
                getProgressDialog().dismiss();
            }
        }
    }

    public abstract String getTitle();

    public abstract boolean hasSideMenu();

    public void onBackPressed() {
        ((BaseActivity) getActivity()).navigateBack();
    }

    public ProgressDialog getProgressDialog() {
        if (isVisible() && mProgressDialog == null){
            mProgressDialog = new ProgressDialog(getActivity());
        }
        return mProgressDialog;
    }
}
