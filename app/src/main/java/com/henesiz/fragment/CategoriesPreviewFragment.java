package com.henesiz.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.MainActivity;
import com.henesiz.adapter.CategoryListAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.model.Category;
import com.henesiz.model.SimpleListItem;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.CategoriesRequestListener;
import com.henesiz.rest.request.GetCategoriesRequest;
import com.henesiz.view.CategorizedFlowsView;
import com.henesiz.view.CircleLayout;
import com.henesiz.view.VideoPreviewView;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;

/**
 * Uses to display categories preview and to navigate to categories flow
 */
public abstract class CategoriesPreviewFragment extends Fragment implements
        CircleLayout.OnItemSelectedListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        View.OnClickListener, Const, CategorizedFlowsView {

    private VideoPreviewView mPreviewView;
    private CircleLayout mCircleLayout;
    private SimpleListItem mCurrentItem;
    private CategoryListAdapter mCategoriesAdapter;
    private ImageView mImageView;
    private SpiceManager mSpiceManager;
    private int mLoaderId;

    public abstract String getTitle();

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        int type = args.getInt(Params.TYPE);
        String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + type
                + " AND " + ContentDescriptor.Categories.Cols.VIDEO_COUNT + " > 0";
        return new CursorLoader(getActivity(),
                ContentDescriptor.Categories.URI,
                null,
                selection,
                null,
                null);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoaderId = getVideoType().getId() + 82;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (data != null) {
            ArrayList<Category> categories = new ArrayList<>();
            if (data.moveToFirst()) {
                do {
                    categories.add(Category.fromCursor(data));
                } while (data.moveToNext());
            }

            categories = Category.reorder(categories);

            if (isAdded()) {
                if (!mCategoriesAdapter.isNewDataIdentical(categories)) {
                    mCategoriesAdapter.setData(categories);
                    mCircleLayout.setAdapter(mCategoriesAdapter);
                    mCircleLayout.setSelectedItem(mCategoriesAdapter.getInitialSelectionPosition());
                    mCircleLayout.invalidateAll();
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().getSupportLoaderManager().destroyLoader(mLoaderId);
    }

    @Override
    public void openFlowList() {
        if (mCurrentItem != null) {
            FlowListFragment.Builder builder = new FlowListFragment.Builder(getVideoType());
            builder.setParams(((Category) mCurrentItem).getParams());
            builder.setTitle(getTitle());
            ((MainActivity) getActivity()).startFragment(builder.build(), true);
        }
    }

    @Override
    public Category getCurrentCategory() {
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        fill();
    }

    private void fill() {
        if (mCategoriesAdapter != null) {
            mCircleLayout.clear();
            mCategoriesAdapter.setData(null);
        }
        VideoType type = getVideoType();
        Bundle args = new Bundle(1);
        args.putInt(Params.TYPE, type.getId());
        getActivity().getSupportLoaderManager().restartLoader(mLoaderId, args, this);
        mPreviewView.hide();
        loadCategories(type);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.preview_image:
                openFlowList();
                break;
        }
    }

    @Override
    public void onItemSelected(Object data) {
        mCurrentItem = (SimpleListItem) data;
        if (data != null) {
            mPreviewView.show(((SimpleListItem) data).getImage());
        } else {
            mPreviewView.hide();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_categories_preview, container, false);
        mImageView = (ImageView) view.findViewById(R.id.preview_image);
        mImageView.setOnClickListener(this);

        mPreviewView = (VideoPreviewView) view.findViewById(R.id.video_preview);
        mCircleLayout = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mCircleLayout.setOnItemSelectedListener(this);

        mCategoriesAdapter = new CategoryListAdapter(getActivity());
        mCircleLayout.setAdapter(mCategoriesAdapter);
        return view;
    }

    @Override
    public void loadCategories(VideoType type) {
        mSpiceManager.execute(
                new GetCategoriesRequest(type),
                new CategoriesRequestListener(type));
    }

    public abstract VideoType getVideoType();
}
