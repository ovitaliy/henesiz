package com.henesiz.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.henesiz.R;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.adapter.SelectableFragmentStatePagerAdapter;
import com.henesiz.enums.VideoType;

public class CrowdFragment extends BaseFragment implements
        ViewPager.OnPageChangeListener,
        TabLayout.OnTabSelectedListener, View.OnClickListener {
    TabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    TabLayout mTabLayout;

    public static CrowdFragment newInstance() {
        CrowdFragment f = new CrowdFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_crowd);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.new_video_shot:
                VideoType type;
                switch (mTabLayout.getSelectedTabPosition()) {
                    case 0:
                        type = VideoType.CROWD_ASK;
                        break;
                    default:
                        type = VideoType.CROWD_GIVE;
                        break;
                }
                CaptureActivity.startNewInstance(getActivity(), type);
                break;
            case R.id.open_flow:
                openFlow();
                break;
        }
    }

    private void openFlow() {
        ((CategoriesPreviewFragment) mTabsAdapter.getCurrentFragment()).openFlowList();
    }

    @Override
    protected void initUI(final View v, Bundle savedInstanceState) {
        mTabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabsAdapter);

        mViewPager.addOnPageChangeListener(this);

        mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.crowd_ask));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.crowd_give));

        mTabsAdapter.notifyDataSetChanged();

        mTabLayout.setOnTabSelectedListener(this);

        v.findViewById(R.id.open_flow).setOnClickListener(this);
        v.findViewById(R.id.new_video_shot).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
//        checkTips();
    }

    @Override
    public void unregisterForContextMenu(View view) {
        super.unregisterForContextMenu(view);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_crowd);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class TabsAdapter extends SelectableFragmentStatePagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = CrowdAskFragment.newInstance();
                    break;
                default:
                    fragment = CrowdGiveFragment.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    public static final class CrowdAskFragment extends CategoriesPreviewFragment {
        public static CrowdAskFragment newInstance() {
            CrowdAskFragment fragment = new CrowdAskFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.crowd_ask);
        }

        @Override
        public VideoType getVideoType() {
            return VideoType.CROWD_ASK;
        }
    }

    public static final class CrowdGiveFragment extends CategoriesPreviewFragment {
        public static CrowdGiveFragment newInstance() {
            CrowdGiveFragment fragment = new CrowdGiveFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.crowd_give);
        }

        @Override
        public VideoType getVideoType() {
            return VideoType.CROWD_GIVE;
        }
    }
}