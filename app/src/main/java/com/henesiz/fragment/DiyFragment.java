package com.henesiz.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.henesiz.R;
import com.henesiz.activities.BaseActivity;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.enums.VideoType;
import com.henesiz.util.Utils;

public class DiyFragment extends CategoriesPreviewFragment implements View.OnClickListener {
    VideoType mVideoType;

    public static DiyFragment newInstance(VideoType type) {
        Bundle args = new Bundle(1);
        args.putSerializable(Params.VIDEO_TYPE, type);
        DiyFragment fragment = new DiyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getTitle() {
        String title = null;
        String skill = getString(R.string.diy_skill);
        String object = getString(R.string.diy_object);
        String give = getString(R.string.diy_tab_share);
        String get = getString(R.string.diy_tab_get);
        switch (mVideoType) {
            case DIY_GET_ITEM:
                title = object + ": " + get;
                break;
            case DIY_GET_SKILL:
                title = skill + ": " + get;
                break;
            case DIY_GIVE_ITEM:
                title = object + ": " + give;
                break;
            case DIY_GIVE_SKILL:
                title = skill + ": " + give;
                break;
        }
        if (!TextUtils.isEmpty(title)) {
            title = title.split("/")[0];
        }
        return title;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        mVideoType = (VideoType) getArguments().getSerializable(Params.VIDEO_TYPE);
        ((BaseActivity) getActivity()).setActionBarTitle(getTitle());
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) super.onCreateView(inflater, container, savedInstanceState);
        FloatingActionButton fab = new FloatingActionButton(getActivity());
        fab.setImageResource(R.drawable.btn_plus_icon);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.RIGHT | Gravity.BOTTOM;
        params.bottomMargin = Utils.convertDpToPixel(10, getActivity());
        params.rightMargin = Utils.convertDpToPixel(10, getActivity());
        fab.setLayoutParams(params);
        ViewCompat.setElevation(fab, Utils.convertDpToPixel(5, getActivity()));
        fab.setId(R.id.add_button);
        view.addView(fab);
        fab.setOnClickListener(this);

        FloatingActionButton fabOpen = new FloatingActionButton(getActivity());
        fabOpen.setImageResource(R.drawable.next_double);
        params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.LEFT | Gravity.BOTTOM;
        params.bottomMargin = Utils.convertDpToPixel(10, getActivity());
        params.leftMargin = Utils.convertDpToPixel(10, getActivity());
        fabOpen.setLayoutParams(params);
        ViewCompat.setElevation(fabOpen, Utils.convertDpToPixel(5, getActivity()));
        fabOpen.setId(R.id.open_flow);
        view.addView(fabOpen);
        fabOpen.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.add_button:
                CaptureActivity.startNewInstance(getActivity(), getVideoType());
                break;
            case R.id.open_flow:
                openFlowList();
                break;
        }
    }

    @Override
    public VideoType getVideoType() {
        return mVideoType;
    }
}