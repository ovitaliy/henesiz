package com.henesiz.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.henesiz.R;
import com.henesiz.activities.FeedbackActivity;
import com.henesiz.adapter.FeedbackAdapter;
import com.henesiz.adapter.ViewPagerAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.FeedbackType;
import com.henesiz.listener.SimpleOnTabSelectedListener;
import com.henesiz.model.Feedback;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.MessageListsRequestListener;
import com.henesiz.rest.request.message.GetMessageListsRequest;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Denis on 03.03.2015.
 */
public class FeedbackFragment extends BaseFragment implements
        ViewPagerAdapter.ViewPagerAdapterDelegator,
        AdapterView.OnItemClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    int mCurrentTab;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private FeedbackAdapter[] mAdapters = new FeedbackAdapter[3];


    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static FeedbackFragment newInstance() {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_feedback);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);

        FloatingActionButton button = (FloatingActionButton) view.findViewById(R.id.done);
        button.setOnClickListener(this);

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.feedback_motion));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.feedback_complains));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.feedback_problems));
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);


        for (int i = 0; i < 3; i++) {
            mAdapters[i] = new FeedbackAdapter(getActivity(), null, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mSpiceManager.execute(
                new GetMessageListsRequest(),
                new MessageListsRequestListener());
        loadFromDb(mCurrentTab);
    }

    private void loadFromDb(int position) {
        Bundle args = new Bundle(1);
        args.putInt("type", getType().ordinal());
        getLoaderManager().restartLoader(position, args, this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Feedback feedback = Feedback.fromCursor((Cursor) parent.getAdapter().getItem(position));
        openFeedback(feedback.getId());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String selection = ContentDescriptor.Feedback.Cols.TYPE + " = " + args.getInt("type");
        return new CursorLoader(getActivity(), ContentDescriptor.Feedback.URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapters[loader.getId()].swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapters[loader.getId()].swapCursor(null);
    }

    private void openFeedback(int id) {
        FeedbackType type = getType();
        getActivity().startActivity(FeedbackActivity.newIntent(getActivity(), id, type));
    }

    private FeedbackType getType() {
        FeedbackType type;
        switch (mCurrentTab) {
            case 0:
                type = FeedbackType.SUGGESTION;
                break;
            case 1:
                type = FeedbackType.COMPLAIN;
                break;
            default:
                type = FeedbackType.ERROR;
                break;
        }
        return type;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                openFeedback(-1);
                break;
        }
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {
        ListView listView = new ListView(getActivity());
        listView.setDivider(null);
        listView.setAdapter(mAdapters[position]);
        listView.setOnItemClickListener(this);
        return listView;
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_feedback);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }


    private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

            mCurrentTab = position;
            loadFromDb(position);
        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
        }
    };


}
