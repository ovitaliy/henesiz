package com.henesiz.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.henesiz.R;
import com.henesiz.enums.FeedbackType;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.model.AddMessageResponse;
import com.henesiz.rest.request.message.AddMessageRequest;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.NetworkUtil;
import com.henesiz.view.EditWithCounterView;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Владимир on 06.12.2014.
 */
public class FeedbackNewChatFragment extends BaseFragment {

    private int mFeedbackId;
    private FeedbackType mFeedbackType;
    SpiceManager mSpiceManager;

    private EditWithCounterView mNewMessageView;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    public static FeedbackNewChatFragment newInstance(int feedbackId, FeedbackType type) {
        FeedbackNewChatFragment fragment = new FeedbackNewChatFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_feedback_new_message);
        args.putInt("feedback_id", feedbackId);
        args.putInt("type", type.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if (args.containsKey("feedback_id")) {
            mFeedbackId = args.getInt("feedback_id");
        }

        mFeedbackType = FeedbackType.getById(args.getInt("type"));
    }


    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mNewMessageView = (EditWithCounterView) view.findViewById(R.id.new_message);

        view.findViewById(R.id.btn_save).setOnClickListener(this);

    }


    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mNewMessageView.getWindowToken(), 0);
    }

    private void sendComment(String message) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.FEEDBACK_SEND);

        if (!NetworkUtil.isOfflineMode()) {
            if (getProgressDialog() == null || !getProgressDialog().isShowing()) {
                getProgressDialog().show();
            }

            hideKeyboard();
            mNewMessageView.setText("");

            mSpiceManager.execute(
                    new AddMessageRequest(mFeedbackId, message, mFeedbackType.getId()),
                    new BaseRequestListener<AddMessageResponse>() {
                        @Override
                        public void onRequestSuccess(AddMessageResponse response) {
                            super.onRequestSuccess(response);
                            if (isAdded()) {
                                if (response != null && response.isSuccess()) {
                                    mFeedbackId = response.getListId();
                                    getActivity().onBackPressed();
                                }
                            }
                        }

                        @Override
                        public void onComplete() {
                            if (getProgressDialog() != null && getProgressDialog().isShowing()) {
                                getProgressDialog().dismiss();
                            }
                        }
                    }
            );
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                String comment = mNewMessageView.getText();
                if (!comment.isEmpty()) {
                    sendComment(comment);
                }
                break;
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.feedback_new_message);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
