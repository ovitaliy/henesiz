package com.henesiz.fragment;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.PlayFlowActivity;
import com.henesiz.adapter.VideoListAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.Screen;
import com.henesiz.enums.VideoType;
import com.henesiz.events.GetFlowEvent;
import com.henesiz.events.LoadedFileEvent;
import com.henesiz.events.UpdateFlowEvent;
import com.henesiz.jobs.VideoPreloadRunnable;
import com.henesiz.listener.OnUpdateVideoListener;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Video;
import com.henesiz.presenters.video.VideoCacherPresenter;
import com.henesiz.presenters.video.VideoListLoaderPresenter;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.listener.FlowRequestListener;
import com.henesiz.rest.listener.SimpleRequestListener;
import com.henesiz.rest.request.video.GetFlowRequest;
import com.henesiz.rest.request.video.VideoPreloadRequest;
import com.henesiz.util.NetworkUtil;
import com.henesiz.view.items.VideoItemView;
import com.henesiz.views.IVideoListView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Владимир on 26.11.2014.
 */
public final class FlowListFragment extends BaseFragment implements
        AbsListView.OnScrollListener,
        IVideoListView,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final int LOADER = 1000;
    private static final int PRELOAD_COUNT = 3;
    private VideoListAdapter mVideoListAdapter;

    private Video mVideoParent;
    private String mTitle;
    private int mLoadingCount;

    private SpiceManager mSpiceManager;

    private int mReplyId;
    private int mUserId;
    private VideoType mVideoType;
    private HashMap<String, Object> mParams;
    private int mPosition;


    private VideoCacherPresenter mVideoCacherPresenter;
    private VideoListLoaderPresenter mVideoListLoaderPresenter;

    public static class Builder {
        Bundle mArgs = new Bundle();

        public Builder(VideoType videoType) {
            setVideoType(videoType);
        }

        public Builder setReplyId(int replyId) {
            mArgs.putInt(Params.REPLY_ID, replyId);
            return this;
        }

        public Builder setParams(HashMap<String, Object> params) {
            mArgs.putSerializable(Params.PARAMS, params);
            return this;
        }

        public Builder setVideoType(VideoType videoType) {
            mArgs.putSerializable(Params.TYPE, videoType);
            return this;
        }

        public Builder setTitle(String title) {
            mArgs.putString(Params.TITLE, title);
            return this;
        }

        public Builder setCurrentPosition(int position) {
            mArgs.putInt(Params.POSITION, position);
            return this;
        }

        public Builder setUserId(int userId) {
            mArgs.putSerializable(Params.USER_ID, userId);
            return this;
        }

        public FlowListFragment build() {
            FlowListFragment fragment = new FlowListFragment();
            mArgs.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_video_list);
            fragment.setArguments(mArgs);
            return fragment;
        }
    }

    private ListView mVideoListView;

    private VideoItemView.OnCommentListener mOnCommentListener;
    private VideoItemView.OnVideoChooseListener mOnVideoChooseListener;
    private VideoItemView.OnMoreListener mOnMoreListener;
    private OnViewProfileListener mOnViewProfileListener;
    private OnUpdateVideoListener mOnUpdateVideoListener;

    private int mLastVisiblePosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mVideoCacherPresenter = new VideoCacherPresenter();
        mVideoListLoaderPresenter = new VideoListLoaderPresenter();
        mVideoListLoaderPresenter.setVideoListView(this);

        mSpiceManager = ((SpiceContext) getActivity()).getSpiceManager();
        mVideoCacherPresenter.setSpiceManager(mSpiceManager);
        mVideoListLoaderPresenter.setSpiceManager(mSpiceManager);

        Bundle args = getArguments();

        if (args.containsKey(Params.TYPE)) {
            mVideoType = (VideoType) args.getSerializable(Params.TYPE);
        }

        if (args.containsKey(Params.REPLY_ID)) {
            mReplyId = args.getInt(Params.REPLY_ID);
            mVideoListLoaderPresenter.setReplayId(mReplyId);
        }

        if (args.containsKey(Params.USER_ID)) {
            mUserId = args.getInt(Params.USER_ID);
            mVideoListLoaderPresenter.setUserId(mUserId);
        }

        if (args.containsKey(Params.PARAMS)) {
            mParams = (HashMap<String, Object>) args.getSerializable(Params.PARAMS);
            mVideoListLoaderPresenter.setFlowRequestParams(mParams);
        }

        if (args.containsKey("parent")) {
            mVideoParent = (Video) args.getSerializable("parent");
        }

        if (args.containsKey(Params.POSITION)) {
            mPosition = args.getInt(Params.POSITION);
        }

        mTitle = args.getString(Params.TITLE);


    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mVideoListView = (ListView) view.findViewById(R.id.video_list_view);

        mVideoListAdapter = new VideoListAdapter(getActivity(), null, AppUser.getUid(), mVideoType, mVideoParent);
        mVideoListView.setAdapter(mVideoListAdapter);
        mVideoListView.setOnScrollListener(this);
        mVideoListAdapter.setOnCommentListener(mOnCommentListener);
        mVideoListAdapter.setOnMoreListener(mOnMoreListener);
        mVideoListAdapter.setOnVideoChooseListener(mOnVideoChooseListener);
        mVideoListAdapter.setOnViewProfileListener(mOnViewProfileListener);
        mVideoListAdapter.setOnPayVideoListener(mOnUpdateVideoListener);

        Bundle bundle = new Bundle(1);
        bundle.putLong("screen", getScreenId());

        getActivity().getLoaderManager().restartLoader(LOADER, bundle, this);

        loadList();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getSupportLoaderManager().destroyLoader(LOADER);
    }

    private void loadList() {
        if (!NetworkUtil.isOfflineMode()) {
            mVideoListLoaderPresenter.loadPage();
        }
    }

    @Override
    public void addVideos(List<Video> videoList) {
        int count = videoList.size();
        mIsLastPage = count < Const.LIMIT;
    }

    @Override
    public long getLastVideoId() {
        if (mVideoListAdapter.getCount() > 0) {
            Video lastVideo = Video.fromCursor((Cursor) mVideoListAdapter.getItem(mVideoListView.getCount() - 1));
            return lastVideo.getVideoId();
        }
        return 0;
    }

    private Screen getScreen() {
        Screen screen;
        if (getArguments().getBoolean("isMyStream")) {
            screen = Screen.MY_PROFILE;
        } else {
            screen = Screen.LIST_FLOW;
        }
        return screen;
    }

    @Override
    public void onResume() {
        super.onResume();

        mVideoListView.scrollTo(0, mLastVisiblePosition + 1);
        mVideoListView.setOnScrollListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mVideoListView.setOnScrollListener(null);

        mVideoCacherPresenter.stopCache();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


        mOnCommentListener = (VideoItemView.OnCommentListener) activity;
        mOnMoreListener = (VideoItemView.OnMoreListener) activity;
        mOnVideoChooseListener = new VideoItemView.OnVideoChooseListener() {
            @Override
            public void onVideoChosen(Video video) {
                ArrayList<Video> videosToWatch = new ArrayList<>();
                int currentVideoId = 0;
                for (int i = 0; i < mVideoListAdapter.getCount(); i++) {
                    Video v = Video.fromCursor((Cursor) mVideoListAdapter.getItem(i));
                    if (video.getMedia().equals(v.getMedia())) {
                        currentVideoId = i;
                    }
                    videosToWatch.add(v);
                }
                if (videosToWatch.size() > 0)
                    PlayFlowActivity.startNewInstance(getActivity(), mParams, currentVideoId, mReplyId, mTitle, videosToWatch, getScreenId());
            }
        };
        mOnViewProfileListener = (OnViewProfileListener) activity;
        mOnUpdateVideoListener = (OnUpdateVideoListener) activity;
    }

    private boolean mIsLastPage;

    private void runPreloader() {
        int preloadCount = Math.min(PRELOAD_COUNT, mVideoListAdapter.getCount()) - mLoadingCount;
        mLoadingCount += preloadCount;
        String[] urls = new String[preloadCount];
        for (int i = 0; i < preloadCount; i++) {
            try {
                Video lastVideo = Video.fromCursor((Cursor) mVideoListAdapter.getItem(i));
                urls[i] = lastVideo.getMedia();
            } catch (Exception ignore) {
                return;
            }
        }
        if (preloadCount > 0)
            mVideoCacherPresenter.runCacher(urls);
    }


    public void loadNextPage() {
        mVideoListView.setOnScrollListener(null);

        if (mVideoListView.getCount() > 0) {
            loadList();
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        mLastVisiblePosition = firstVisibleItem;
        if (!mIsLastPage && totalItemCount >= Const.LIMIT && firstVisibleItem + visibleItemCount > totalItemCount - 5) {
            loadNextPage();
        }
    }

    public long getScreenId() {
        long screenId = getScreen().ordinal();
        if (mUserId != 0) {
            screenId += mUserId << 24;
        }
        return screenId;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        long screen = args.getLong("screen");

        String where = ContentDescriptor.Videos.Cols.SCREEN + "=" + screen;
        return new CursorLoader(getActivity(), ContentDescriptor.Videos.URI, null, where, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mVideoListAdapter.changeCursor(data);
        if (mPosition > 0) {
            mVideoListView.setSelection(mPosition);
            mPosition = -1;
        }

        if (data != null && data.getCount() > 0) {
            runPreloader();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            if (mVideoType != null) {
                switch (mVideoType) {
                    case FLOW_BEST:
                    case FLOW_MY_CHOICE:
                    case FLOW_ALL:
                        mTitle = getString(R.string.menu_vistory);
                        break;
                    case CROWD_ASK:
                    case CROWD_GIVE:
                        mTitle = getString(R.string.menu_crowd);
                        break;
                    case NEWS:
                        mTitle = getString(R.string.vistory_tab_news);
                        break;
                    case VLOG:
                        mTitle = getString(R.string.vistory_tab_vlog);
                        break;
                    case ALIEN_LOOK_CHALLENGE:
                    case ALIEN_LOOK_TASK:
                        mTitle = getString(R.string.menu_alien_look);
                        break;
                    case DIY_GET_ITEM:
                    case DIY_GET_SKILL:
                        mTitle = getString(R.string.video_type_diy_ask);
                        break;
                    case DIY_GIVE_ITEM:
                    case DIY_GIVE_SKILL:
                        mTitle = getString(R.string.video_type_diy_give);
                        break;
                }
            } else if (mReplyId > 0) {
                mTitle = getString(R.string.dialog_responses_watch);
            }
        }
        return mTitle;
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onUpdateFlowEvent(UpdateFlowEvent event) {
        if (mVideoListLoaderPresenter != null)
            mVideoListLoaderPresenter.loadPage(0);
    }
}
