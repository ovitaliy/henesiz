package com.henesiz.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.henesiz.R;
import com.henesiz.activities.BaseActivity;
import com.henesiz.adapter.InfoAdapter;
import com.henesiz.adapter.ViewPagerAdapter;
import com.henesiz.listener.SimpleOnTabSelectedListener;

/**
 * Created by Denis on 03.03.2015.
 */
public class InfoFragment extends BaseFragment implements
        ViewPagerAdapter.ViewPagerAdapterDelegator {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_info);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info_tab_1));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info_tab_2));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.info_tab_3));
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        ((BaseActivity) getActivity()).startFragment(new NavigationInfoFragment(), true, true);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_info);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    class TextProcessorTask extends AsyncTask<String, Void, String[]> {

        InfoAdapter mInfoAdapter;

        public TextProcessorTask(InfoAdapter infoAdapter) {
            mInfoAdapter = infoAdapter;
        }

        @Override
        protected String[] doInBackground(String... strings) {
            String text = strings[0];
            return text.split("<br/>");
        }

        @Override
        protected void onPostExecute(String[] result) {
            super.onPostExecute(result);
            mInfoAdapter.setData(result);
            mInfoAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {
        RecyclerView recyclerView = new RecyclerView(getActivity());
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        InfoAdapter mAdapter = new InfoAdapter(new String[]{});
        recyclerView.setAdapter(mAdapter);

        String text;
        switch (position) {
            case 0:
                text = getString(R.string.terms_info);
                break;
            case 1:
                text = getString(R.string.privacy_info);
                break;
            default:
                text = getString(R.string.about_info).replaceAll("&", "");
                break;
        }

        new TextProcessorTask(mAdapter).execute(text);

        return recyclerView;
    }


    private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);
        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
        }
    };

}
