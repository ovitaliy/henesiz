package com.henesiz.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.MainActivity;
import com.henesiz.activities.RegistrationActivity;
import com.henesiz.adapter.MenuAdapter;
import com.henesiz.enums.Menu;
import com.henesiz.events.UpdateProfileEvent;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.UserInfo;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.UserInfoRequestListener;
import com.henesiz.rest.request.user.GetUserInfoRequest;
import com.henesiz.util.NetworkUtil;
import com.henesiz.util.Toast;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

/**
 * Created by Inteza23 on 06.11.2014.
 */
public class MenuFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    private OnMenuListener mOnMenuListener;
    private MenuAdapter mAdapter;
    private OnViewProfileListener mOnViewProfileListener;
    private SpiceManager mSpiceManager;
    TextView mCopyrightView;
    ListView mListView;
    private ImageView mAvatarView;
    private ImageButton mLogoutView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, true);
        mCopyrightView = (TextView) view.findViewById(R.id.copyright);
        mListView = (ListView) view.findViewById(R.id.list);
        view.findViewById(R.id.header).setOnClickListener(this);
        mAdapter = new MenuAdapter();
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mLogoutView = (ImageButton) view.findViewById(R.id.logout);
        mLogoutView.setOnClickListener(this);

        setCopyright();
        return view;
    }

    private void setCopyright() {
        int curYear = Calendar.getInstance().get(Calendar.YEAR);
        String param = "";
        if (curYear > 2016) {
            param = " - " + curYear;
        }
        mCopyrightView.setText(String.format(getString(R.string.copyright), param));
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        onProfileUpdatedEvent(null);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onProfileUpdatedEvent(UpdateProfileEvent event) {
        if (AppUser.getUid() != 0) {
            fillUserInfo(AppUser.get());
        }
        initialLoad();
    }

    private void initialLoad() {
        if (!NetworkUtil.isOfflineMode()) {
            mSpiceManager.execute(new GetUserInfoRequest(), "menu_user_update", 15000,
                    new UserInfoRequestListener(new UserInfoRequestListener.OnGetUserInfoListener() {
                        @Override
                        public void onGetUserInfo(UserInfo userInfo) {
                            if (userInfo == null || TextUtils.isEmpty(userInfo.getFullName())) {
                                RegistrationActivity.startNewInstance(getActivity(), Const.Action.FILL_PROFILE);
                                return;
                            }
                            fillUserInfo(userInfo);
                        }
                    }));
        }
    }

    private void fillUserInfo(UserInfo userInfo) {
        View v = getView();
        if (v == null) {
            return;
        }

        mAvatarView = (ImageView) v.findViewById(R.id.avatar);
        TextView textView;

        textView = (TextView) v.findViewById(R.id.name);
        textView.setText(userInfo.getFullName());

        textView = (TextView) v.findViewById(R.id.location);
        textView.setText(String.format("%s, %s", userInfo.getCountry(), userInfo.getCity()));

        textView = (TextView) v.findViewById(R.id.balance);
        textView.setText(String.valueOf(userInfo.getRating()));

        if (!TextUtils.isEmpty(userInfo.getPhoto())) {
            ImageLoader.getInstance().displayImage(userInfo.getImageLoaderPhoto(), mAvatarView);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnMenuListener = (OnMenuListener) activity;
        mOnViewProfileListener = (OnViewProfileListener) activity;
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header:
                mOnViewProfileListener.onViewProfile(AppUser.getUid());
                break;
            case R.id.logout:
                ((MainActivity) getActivity()).onLogout();
                break;
        }
    }

    public void setActive(final Menu menu) {
        if (getView() != null) {
            getView().post(new Runnable() {
                @Override
                public void run() {
                    if (mAdapter != null && mAdapter.getCount() > 0) {
                        for (int i = 0; i < mAdapter.getCount(); i++) {
                            Menu item = (Menu) mAdapter.getItem(i);
                            if (item.equals(menu)) {
                                mListView.setItemChecked(i, true);
                            }
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Menu item = (Menu) mAdapter.getItem(position);
        switch (item) {
            case VISTORY:
                mOnMenuListener.onOpenVistory();
                break;
            case DIY:
                mOnMenuListener.onOpenDiy();
                break;
            case ALIEN_LOOK:
                mOnMenuListener.onOpenAlienLook();
                break;
            case MARKETPLACE:
                mOnMenuListener.onOpenMarketplace();
                break;
            case INFO:
                mOnMenuListener.onOpenInfo();
                break;
            default:
                Toast.makeText(getActivity(), "UNDER CONSTRUCTION", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public interface OnMenuListener {
        void onOpenVistory();

        void onOpenInfo();

        void onOpenDiy();

        void onOpenMarketplace();

        void onOpenAlienLook();
    }

}
