package com.henesiz.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.activities.BaseActivity;
import com.henesiz.enums.VideoType;
import com.henesiz.view.CirclePickerItem;
import com.henesiz.view.CirclePickerView;
import com.henesiz.view.NavigateView;

import java.util.ArrayList;
import java.util.Arrays;

public class NavigationDiyFragment extends BaseFragment implements
        NavigateView<NavigationDiyFragment.SubItem>,
        TabLayout.OnTabSelectedListener {
    CirclePickerView mPicker;
    SubItem mSubItem;
    FloatingActionButton mNext;
    TabLayout mTabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        mPicker = (CirclePickerView) view.findViewById(R.id.picker);
        mNext = (FloatingActionButton) view.findViewById(R.id.fab);
        mNext.setOnClickListener(this);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.diy_tab_get));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.diy_tab_share));
        mTabLayout.setOnTabSelectedListener(this);

        mPicker.fill(getSubMenuItems(),
                -1,
                new CirclePickerView.OnPickListener() {
                    @Override
                    public void OnPick(CirclePickerItem element) {
                        setSelectedSubMenuItem((SubItem) element);
                    }
                });

        updateNextButtonEnabledStatus();
        return view;
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.fab:
                navigateToSubMenuItem(getSelectedSubMenuItem());
                break;
        }
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_diy);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }

    private void openDiy(VideoType type) {
        ((BaseActivity) getActivity()).startFragment(DiyFragment.newInstance(type), true);
    }

    @Override
    public void navigateToSubMenuItem(SubItem item) {
        int tab = mTabLayout.getSelectedTabPosition();
        if (item != null) {
            switch (item) {
                case SKILL:
                    openDiy(tab == 1 ? VideoType.DIY_GIVE_SKILL : VideoType.DIY_GET_SKILL);
                    break;
                case OBJECT:
                    openDiy(tab == 1 ? VideoType.DIY_GIVE_ITEM : VideoType.DIY_GET_ITEM);
                    break;
            }
        }
    }

    public static NavigationDiyFragment newInstance() {
        NavigationDiyFragment fragment = new NavigationDiyFragment();
        return fragment;
    }

    @Override
    public void navigateBack() {
        //unused
    }

    @Override
    public void setSelectedSubMenuItem(SubItem item) {
        mSubItem = item;
        updateNextButtonEnabledStatus();
    }

    @Override
    public void updateNextButtonEnabledStatus() {
        mNext.post(new Runnable() {
            @Override
            public void run() {
                if (getSelectedSubMenuItem() == null) {
                    mNext.setEnabled(false);
                } else {
                    mNext.setEnabled(true);
                }
            }
        });
    }

    @Override
    public SubItem getSelectedSubMenuItem() {
        return mSubItem;
    }

    @Override
    public ArrayList<CirclePickerItem> getSubMenuItems() {
        return new ArrayList<CirclePickerItem>(Arrays.asList(SubItem.values()));
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public enum SubItem implements CirclePickerItem {
        SKILL {
            @Override
            public Drawable getDrawable() {
                return ContextCompat.getDrawable(App.getInstance(), R.drawable.skills);
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.diy_skill);
            }
        },
        OBJECT {
            @Override
            public Drawable getDrawable() {
                return ContextCompat.getDrawable(App.getInstance(), R.drawable.product);
            }

            @Override
            public String getTitle() {
                return App.getInstance().getString(R.string.diy_object);
            }

        };

        @Override
        public int getId() {
            return ordinal() + 1;
        }
    }
}
