package com.henesiz.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.NewVoteActivity;
import com.henesiz.activities.VoteActivity;
import com.henesiz.adapter.ViewPagerAdapter;
import com.henesiz.adapter.VoteAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VoteListType;
import com.henesiz.listener.SimpleOnTabSelectedListener;
import com.henesiz.model.Vote;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.GetVoteListRequestListener;
import com.henesiz.rest.model.VoteListsResponse;
import com.henesiz.rest.request.vote.GetVotesListRequest;
import com.octo.android.robospice.SpiceManager;

/**
 * Created by Denis on 02.12.2015.
 */
public class PollListFragment extends BaseFragment implements
        View.OnClickListener,
        ViewPagerAdapter.ViewPagerAdapterDelegator,
        AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>,
        Const {


    private static final int POLL_LOADER_ID = 1;

    public static PollListFragment newInstance() {
        Bundle args;
        PollListFragment f = new PollListFragment();
        args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_poll_list);
        f.setArguments(args);
        return f;
    }

    private int mCurrentTabPosition = 0;
    private FloatingActionButton mAddButton;

    private SpiceManager mSpiceManager;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private VoteAdapter[] mAdapters = new VoteAdapter[3];

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        mAddButton = (FloatingActionButton) view.findViewById(R.id.done);
        mAddButton.setOnClickListener(this);

        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setAdapter(new ViewPagerAdapter(this));
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.poll_tab_active));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.poll_tab_history));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.poll_tab_my));
        mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);


        for (int i = 0; i < 3; i++) {
            mAdapters[i] = new VoteAdapter(getActivity(), i == VoteListType.MY.ordinal());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadVoteList(mCurrentTabPosition);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_poll);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    private void loadVoteList(int position) {
        mCurrentTabPosition = position;

        final VoteListType type = VoteListType.values()[position];

        if (type.equals(VoteListType.MY))
            mAddButton.setVisibility(View.VISIBLE);
        else
            mAddButton.setVisibility(View.GONE);

        mSpiceManager.execute(
                new GetVotesListRequest(type),
                new GetVoteListRequestListener(type) {
                    @Override
                    public void onRequestSuccess(VoteListsResponse response) {
                        super.onRequestSuccess(response);
                        Bundle args = new Bundle(1);
                        args.putInt(Params.TYPE, type.ordinal());
                        getActivity().getSupportLoaderManager().restartLoader(type.ordinal(), args, PollListFragment.this);
                    }
                });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                getActivity().startActivity(new Intent(getActivity(), NewVoteActivity.class));
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Vote vote = Vote.fromCursor((Cursor) mAdapters[mCurrentTabPosition].getItem(position));
//        if (vote.getStatus() == null || vote.getStatus().equals(VoteListStatus.ACTIVE) || vote.getStatus().equals(VoteListStatus.FINISHED)) {
        getActivity().startActivity(VoteActivity.newInstance(getActivity(), vote.getId(), mCurrentTabPosition == VoteListType.PAST.ordinal()));
//        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(),
                ContentDescriptor.Votes.URI,
                null,
                ContentDescriptor.Votes.Cols.TYPE + " = " + args.getInt(Params.TYPE, 0),
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        int loaderId = loader.getId();
        mAdapters[loaderId].swapCursor(data);
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @Override
    public View getPageAt(ViewGroup container, int position) {
        ListView listView = new ListView(getActivity());
        listView.setDivider(null);
        listView.setAdapter(mAdapters[position]);
        listView.setOnItemClickListener(this);
        return listView;
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        int loaderId = loader.getId();
        mAdapters[loaderId].swapCursor(null);
    }


    private ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            mTabLayout.setOnTabSelectedListener(null);
            mTabLayout.getTabAt(position).select();
            mTabLayout.setOnTabSelectedListener(mOnTabSelectedListener);

            loadVoteList(position);

        }
    };

    private SimpleOnTabSelectedListener mOnTabSelectedListener = new SimpleOnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition(), false);
        }
    };
}
