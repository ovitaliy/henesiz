package com.henesiz.fragment;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.LongSparseArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.R;
import com.henesiz.activities.BaseActivity;
import com.henesiz.activities.EditProfileActivity;
import com.henesiz.activities.TokenActivity;
import com.henesiz.enums.Education;
import com.henesiz.enums.Hobby;
import com.henesiz.enums.Interest;
import com.henesiz.enums.Pet;
import com.henesiz.enums.Religion;
import com.henesiz.enums.Screen;
import com.henesiz.enums.Sport;
import com.henesiz.model.UserInfo;
import com.henesiz.model.VideoStatistic;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.listener.UserInfoRequestListener;
import com.henesiz.rest.request.user.FollowUnfollowUserRequest;
import com.henesiz.rest.request.user.GetUserInfoRequest;
import com.henesiz.util.Utils;
import com.henesiz.view.CheckableImageView;
import com.henesiz.view.CirclePickerItem;
import com.henesiz.view.CirclePickerView;
import com.henesiz.view.items.StatisticItem;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.octo.android.robospice.SpiceManager;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends BaseFragment implements ViewPager.OnPageChangeListener, TabLayout.OnTabSelectedListener {
    private int mDefTab = 0;
    TabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    private TextView mHeaderButton;
    SpiceManager mSpiceManager;
    TabLayout mTabLayout;

    public static ProfileFragment newInstance(int userId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Params.USER_ID, userId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ProfileFragment newInstance(int userId, int defTab) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(Params.USER_ID, userId);
        args.putInt("def_tab", defTab);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mUserId = getArguments().getInt(Params.USER_ID);
            if (getArguments().containsKey("def_tab")) {
                mDefTab = getArguments().getInt("def_tab");
            }
        } else {
            mUserId = AppUser.getUid();
        }
    }

    private int mUserId;

    private UserInfo mUserInfo;

    private TextView mRatingView;

    private CheckableImageView mActionButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_profile, container, false);

        if (mUserId == AppUser.getUid()) {
            mUserInfo = AppUser.get();
        }

        mActionButton = (CheckableImageView) v.findViewById(R.id.action);
        mActionButton.setOnClickListener(this);

        if (mUserInfo != null && mUserInfo.isMe()) {
            mActionButton.setImageResource(R.drawable.btn_edit);
        } else {
            mActionButton.setImageResource(R.drawable.friend_button);
        }

        mRatingView = (TextView) v.findViewById(R.id.rating);

        mHeaderButton = (TextView) ((BaseActivity) getActivity()).getActionBarToolbar().findViewById(R.id.header_button);
        mHeaderButton.setOnClickListener(this);

        mTabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabsAdapter);

        mViewPager.addOnPageChangeListener(this);

        mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.profile_tab_content));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.profile_tab_profile));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.profile_tab_statistics));

        mTabLayout.setOnTabSelectedListener(this);
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mUserId == AppUser.getUid()) {
            fillProfile(AppUser.get());
        }

        mHeaderButton.setVisibility(View.VISIBLE);

        loadUserInfo();
    }

    @Override
    public void onPause() {
        super.onPause();
        mHeaderButton.setVisibility(View.GONE);
    }

    private void fillProfile(UserInfo userInfo) {
        fillProfile(getView(), userInfo);
    }

    private void fillProfile(View v, UserInfo userInfo) {
        if (isAdded()) {
            TextView textView;
            mUserInfo = userInfo;

            textView = (TextView) v.findViewById(R.id.name);
            if (textView != null) {
                textView.setText(userInfo.getFullName());
            }

            textView = (TextView) v.findViewById(R.id.location);
            if (textView != null) {
                textView.setText(userInfo.getCountry() + ", " + userInfo.getCity() + ", " + userInfo.getLanguage().getTitle());
            }

            if (userInfo.getPhoto() != null) {
                ImageView avatar = (ImageView) v.findViewById(R.id.avatar);
                if (avatar != null) {
                    ImageLoader.getInstance().displayImage(userInfo.getImageLoaderPhoto(), avatar);
                }
            }

            if (mActionButton != null) {
                if (mUserInfo.getRelationship() == 1) {
                    mActionButton.setChecked(true);
                }
            }

            if (mRatingView != null) {
                mRatingView.setText(String.valueOf(userInfo.getRating()));
            }

            if (mHeaderButton != null) {
                if (!mUserInfo.isMe() && !TextUtils.isEmpty(mUserInfo.getPhone())) {
                    mHeaderButton.setText(null);
                    mHeaderButton.setBackground(App.getInstance().getResources().getDrawable(R.drawable.phone));
                } else if (mUserInfo.isMe()) {
                    mHeaderButton.setBackground(App.getInstance().getResources().getDrawable(R.drawable.btn_token));
//                    mHeaderButton.setText(String.valueOf(mUserInfo.getToken()));
                }
            }
        }

        mTabsAdapter.reload(userInfo);
    }

    private Screen getScreen() {
        Screen screen;
        if (mUserId == AppUser.getUid()) {
            screen = Screen.MY_PROFILE;
        } else {
            screen = Screen.USER_PROFILE;
        }
        return screen;
    }

    private void loadUserInfo() {
        mSpiceManager.execute(new GetUserInfoRequest(mUserId), new UserInfoRequestListener(mUserId, new UserInfoRequestListener.OnGetUserInfoListener() {
            @Override
            public void onGetUserInfo(UserInfo userInfo) {
                fillProfile(userInfo);
            }
        }));
    }

    private void onActionButtonClick() {
        if (mUserInfo != null) {
            if (mUserInfo.isMe()) {
                EditProfileActivity.startNewInstance(getActivity());
            } else {
                mUserInfo.setRelationship(mUserInfo.getRelationship() == 1 ? 0 : 1);
                mActionButton.setChecked(mUserInfo.getRelationship() == 1);
                mSpiceManager.execute(new FollowUnfollowUserRequest(mUserInfo.getId(), mUserInfo.getRelationship() == 1),
                        new BaseRequestListener());
            }
        }
    }

    private String normalizePhone(String phone) {
        if (!phone.contains("+")) {
            phone = "+" + phone;
        }
        return phone;
    }

    private void makeCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + normalizePhone(mUserInfo.getPhone())));
        startActivity(intent);
    }

    private void openTokenScreen() {
        startActivity(TokenActivity.newIntent(getActivity()));
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.action:
                onActionButtonClick();
                break;
            case R.id.header_button:
                if (mUserInfo.isMe()) {
                    openTokenScreen();
                } else {
                    makeCall();
                }
                break;
        }
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class TabsAdapter extends FragmentStatePagerAdapter implements Reloadable {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        private Fragment[] mFragments = new Fragment[3];

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = new FlowListFragment.Builder(null).setUserId(mUserId).build();
                    break;
                case 1:
                    fragment = QuestionarieFragment.newInstance(mUserInfo);
                    break;
                default:
                    fragment = StatisticsFragment.newInstance(mUserInfo);
                    break;
            }
            mFragments[i] = fragment;
            return fragment;
        }

        @Override
        public void reload(UserInfo userInfo) {
            for (Fragment fragment : mFragments) {
                if (fragment != null && fragment instanceof Reloadable)
                    ((Reloadable) fragment).reload(userInfo);
            }
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    public static class QuestionarieFragment extends Fragment implements Reloadable {
        UserInfo mUserInfo;
        CirclePickerView mCirclePickerView;

        public static QuestionarieFragment newInstance(UserInfo userInfo) {
            Bundle args = new Bundle(1);
            args.putSerializable("user", userInfo);
            QuestionarieFragment fragment = new QuestionarieFragment();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mUserInfo = (UserInfo) getArguments().getSerializable("user");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            FrameLayout view = new FrameLayout(getActivity());
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mCirclePickerView = new CirclePickerView(getActivity());
            mCirclePickerView.setBackgroundResource(R.drawable.circle_picker_bg);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            params.gravity = Gravity.CENTER;
            view.addView(mCirclePickerView, params);
            return view;
        }

        @Override
        public void onResume() {
            super.onResume();
            fill();
        }

        private void fill() {
            ArrayList<CirclePickerItem> list = new ArrayList<>(5);

            int i = 1;
            if (mUserInfo != null) {
                Sport sport = mUserInfo.getSport();
                if (sport != null && !(sport.isOtherOption() && TextUtils.isEmpty(sport.getTitle()))) {
                    list.add(new StatisticItem(i++, mUserInfo.getSport()));
                }
                Pet pet = mUserInfo.getPet();
                if (pet != null && !(pet.isOtherOption() && TextUtils.isEmpty(pet.getTitle()))) {
                    list.add(new StatisticItem(i++, mUserInfo.getPet()));
                }
                Hobby hobby = mUserInfo.getHobbie();
                if (hobby != null && !(hobby.isOtherOption() && TextUtils.isEmpty(hobby.getTitle()))) {
                    list.add(new StatisticItem(i++, mUserInfo.getHobbie()));
                }
                Religion religion = mUserInfo.getReligion();
                if (religion != null && !(religion.isOtherOption() && TextUtils.isEmpty(religion.getTitle()))) {
                    list.add(new StatisticItem(i++, mUserInfo.getReligion()));
                }
                Interest interest = mUserInfo.getInterest();
                if (interest != null && !(interest.isOtherOption() && TextUtils.isEmpty(interest.getTitle()))) {
                    list.add(new StatisticItem(i++, mUserInfo.getInterest()));
                }
                Education education = mUserInfo.getDegree();
                if (education != null && !TextUtils.isEmpty(education.getTitle())) {
                    list.add(new StatisticItem(i++, mUserInfo.getDegree()));
                }
                if (mUserInfo.getJobType() != null) {
                    list.add(new StatisticItem(i++, mUserInfo.getJobType()));
                }
            }

            mCirclePickerView.fill(list, -1, null);
        }

        @Override
        public void reload(UserInfo userInfo) {
            mUserInfo = userInfo;
            fill();
        }
    }

    public static class StatisticsFragment extends Fragment implements Reloadable {
        UserInfo mUserInfo;
        CirclePickerView mCirclePickerView;

        public static StatisticsFragment newInstance(UserInfo userInfo) {
            Bundle args = new Bundle(1);
            args.putSerializable("user", userInfo);
            StatisticsFragment fragment = new StatisticsFragment();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mUserInfo = (UserInfo) getArguments().getSerializable("user");
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            FrameLayout view = new FrameLayout(getActivity());
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mCirclePickerView = new CirclePickerView(getActivity());
            mCirclePickerView.setBackgroundResource(R.drawable.circle_picker_bg);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            params.gravity = Gravity.CENTER;
            view.addView(mCirclePickerView, params);
            return view;
        }

        @Override
        public void onResume() {
            super.onResume();
            fill();
        }

        void fill() {
            ArrayList<CirclePickerItem> list = new ArrayList<>(11);
            if (mUserInfo != null && isAdded()) {
                List<VideoStatistic> categories = mUserInfo.getCategoryStatisticList();
                if (categories != null) {
                    int count = categories.size();
                    LongSparseArray<VideoStatistic> map = new LongSparseArray();
                    for (int i = 0; i < count; i++) {
                        VideoStatistic categoryStatistic = mUserInfo.getCategoryStatisticList().get(i);
                        if (categoryStatistic.getDuration() > 0 && categoryStatistic.getTitleResId() != 0 && categoryStatistic.getImageResId() != 0) {
                            VideoStatistic item = map.get(categoryStatistic.getTitleResId());
                            if (item == null) {
                                item = categoryStatistic;
                            } else {
                                item.setDuration(item.getDuration() + categoryStatistic.getDuration());
                            }

                            map.put(categoryStatistic.getTitleResId(), item);
                        }
                    }

                    for (int i = 0; i < map.size(); i++) {
                        long key = map.keyAt(i);
                        VideoStatistic val = map.get(key);
                        list.add(new StatisticItem(i,
                                getString(val.getTitleResId()) + "\n" + Utils.formatMillisToTime((long) (val.getDuration() * 1000)),
                                val.getImageResId()));
                    }
                }
            }
            mCirclePickerView.fill(list, -1, null);
        }

        @Override
        public void reload(UserInfo userInfo) {
            mUserInfo = userInfo;
            fill();
        }
    }

    private interface Reloadable {
        void reload(UserInfo userInfo);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}