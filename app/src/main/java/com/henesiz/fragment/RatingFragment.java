package com.henesiz.fragment;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.activities.MainActivity;
import com.henesiz.adapter.RatingAdapter;
import com.henesiz.adapter.SelectableFragmentStatePagerAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.UserInfo;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.model.UserRatingResponse;
import com.henesiz.rest.request.user.RatingRequest;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;

public class RatingFragment extends BaseFragment implements ViewPager.OnPageChangeListener,
        TabLayout.OnTabSelectedListener {
    TabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    TabLayout mTabLayout;

    @Override
    protected void initUI(final View v, Bundle savedInstanceState) {
        mTabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabsAdapter);

        mViewPager.addOnPageChangeListener(this);

        mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.rating_tab_last));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.rating_tab_all));

        mTabsAdapter.notifyDataSetChanged();

        mTabLayout.setOnTabSelectedListener(this);
    }


    public static RatingFragment newInstance() {
        RatingFragment ratingFragment = new RatingFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_rating);
        ratingFragment.setArguments(args);
        return ratingFragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.rating);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    public class TabsAdapter extends SelectableFragmentStatePagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = RatingPageFragment.newInstance(false);
                    break;
                default:
                    fragment = RatingPageFragment.newInstance(true);
                    break;
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public static class RatingPageFragment extends Fragment implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {
        private SpiceManager mSpiceManager;
        private ListView mListView;
        private RatingAdapter mRatingAdapter;
        private TextView mRateView;
        private TextView mRatePositionView;
        private boolean mAll;
        private static final int LOADER_ID = 1;

        public static RatingPageFragment newInstance(boolean all) {
            RatingPageFragment fragment = new RatingPageFragment();
            Bundle args = new Bundle(1);
            args.putBoolean("all", all);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            mSpiceManager = ((SpiceContext) activity).getSpiceManager();
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mRatingAdapter = new RatingAdapter(getActivity(), null, 0);
            mAll = getArguments().getBoolean("all");
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_rating_page, container, false);
            mListView = (ListView) view.findViewById(R.id.tops_list);
            mListView.setOnItemClickListener(this);
            mListView.setAdapter(mRatingAdapter);

            mRateView = (TextView) view.findViewById(R.id.rate);
            mRatePositionView = (TextView) view.findViewById(R.id.rate_position);
            return view;
        }

        private void setRating(int rating, int position) {
            mRateView.setText(getString(R.string.rating_rate, rating));
            if (position != -1) {
                mRatePositionView.setText(getString(R.string.rating_rate_position, String.valueOf(position) + "%"));
            }
        }

        @Override
        public void onResume() {
            super.onResume();
            getLoaderManager().restartLoader(LOADER_ID, null, this);
            mSpiceManager.execute(new RatingRequest(mAll), new BaseRequestListener<UserRatingResponse>() {
                @Override
                public void onRequestFailure(SpiceException spiceException) {
                    super.onRequestFailure(spiceException);
                }

                @Override
                public void onRequestSuccess(final UserRatingResponse response) {
                    super.onRequestSuccess(response);
                    if (!isVisible()) {
                        return;
                    }
                    setRating(response.getRating(), response.getPosition());

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            UserInfo[] list = response.getTopUsers();
                            ContentValues[] contentValues = new ContentValues[list.length];
                            int recordType;
                            if (mAll) {
                                recordType = ContentDescriptor.Users.RECORD_TYPE_RATING_ALL;
                            } else {
                                recordType = ContentDescriptor.Users.RECORD_TYPE_RATING_APP;
                            }
                            for (int i = 0; i < list.length; i++) {
                                UserInfo user = list[i];
                                ContentValues values = user.toContentValues();
                                values.put(ContentDescriptor.Users.Cols.RECORD_TYPE, recordType);
                                contentValues[i] = values;
                            }
                            if (isAdded()) {
                                String selection = ContentDescriptor.Users.Cols.RECORD_TYPE + " = " + recordType;
                                getActivity().getContentResolver().delete(ContentDescriptor.Users.URI, selection, null);
                                getActivity().getContentResolver().bulkInsert(ContentDescriptor.Users.URI, contentValues);
                            }
                        }
                    }).start();
                }
            });
        }

        @Override
        public void onPause() {
            super.onPause();
            getActivity().getSupportLoaderManager().destroyLoader(LOADER_ID);
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Cursor cursor = (Cursor) mRatingAdapter.getItem(position);
            if (cursor != null) {
                UserInfo userInfo = UserInfo.fromCursor(cursor);
                if (userInfo != null) {
                    ((MainActivity) getActivity()).onViewProfile(userInfo.getId());
                }
            }
        }


        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            int recordType;
            if (mAll) {
                recordType = ContentDescriptor.Users.RECORD_TYPE_RATING_ALL;
            } else {
                recordType = ContentDescriptor.Users.RECORD_TYPE_RATING_APP;
            }
            String selection = ContentDescriptor.Users.Cols.RECORD_TYPE +
                    " = " + recordType;
            return new CursorLoader(getActivity(), ContentDescriptor.Users.URI, null, selection, null, null);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mRatingAdapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            mRatingAdapter.swapCursor(null);
        }
    }
}
