package com.henesiz.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.BaseActivity;
import com.henesiz.activities.SearchResultActivity;
import com.henesiz.adapter.SelectableFragmentStatePagerAdapter;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.Education;
import com.henesiz.enums.Hobby;
import com.henesiz.enums.Interest;
import com.henesiz.enums.Job;
import com.henesiz.enums.Language;
import com.henesiz.enums.Pet;
import com.henesiz.enums.Religion;
import com.henesiz.enums.Sport;
import com.henesiz.enums.VideoType;
import com.henesiz.listener.SimpleTextListener;
import com.henesiz.model.Category;
import com.henesiz.model.Country;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.CategoriesRequestListener;
import com.henesiz.rest.listener.UserSearchRequestListener;
import com.henesiz.rest.model.UserSearchResponse;
import com.henesiz.rest.request.GetCategoriesRequest;
import com.henesiz.rest.request.user.SearchUserRequest;
import com.henesiz.util.DisplayInfo;
import com.henesiz.util.LocationHelper;
import com.henesiz.util.Utils;
import com.henesiz.view.CirclePickerItem;
import com.henesiz.view.CirclePickerView;
import com.henesiz.view.CityAutoCompleteTextView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Deni on 23.06.2015.
 */
public class SearchFragment extends BaseFragment implements ViewPager.OnPageChangeListener,
        TabLayout.OnTabSelectedListener,
        View.OnClickListener {

    TabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    TabLayout mTabLayout;

    SearchUserRequest searchUserRequest = new SearchUserRequest();

    FloatingActionButton mSearchButton;

    SpiceManager mSpiceManager;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        mSearchButton = (FloatingActionButton) view.findViewById(R.id.search);
        mSearchButton.setOnClickListener(this);

        mTabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) view.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabsAdapter);

        mViewPager.addOnPageChangeListener(this);

        mTabLayout = (TabLayout) view.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_profile));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_questions));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.search_tab_content));

        mTabsAdapter.notifyDataSetChanged();

        mTabLayout.setOnTabSelectedListener(this);
        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onClick(View v) {
        search();
    }

    private void search() {
        getProgressDialog().show();
        getProgressDialog().setCancelable(true);

        mSearchButton.setEnabled(false);
        mSpiceManager.execute(searchUserRequest, "search", DurationInMillis.ONE_SECOND, new UserSearchRequestListener(searchUserRequest.getParams(), true) {
            @Override
            public void onRequestSuccess(UserSearchResponse response) {
                super.onRequestSuccess(response);
                if (isAdded()) {
                    startActivity(SearchResultActivity.newIntent(getActivity()));

                }
            }

            @Override
            public void onRequestFailure(SpiceException spiceException) {
                super.onRequestFailure(spiceException);
                if (isVisible() && spiceException instanceof NoNetworkException)
                    App.showNoNetworkErrorToast(getActivity());
            }

            @Override
            public void onComplete() {
                super.onComplete();
                if (isVisible()) {
                    getProgressDialog().dismiss();
                    mSearchButton.setEnabled(true);
                    getProgressDialog().setOnCancelListener(null);
                }
            }
        });

        getProgressDialog().setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mSpiceManager.cancel(searchUserRequest);
                mSearchButton.setEnabled(true);
                getProgressDialog().setOnCancelListener(null);

                ArgsMap argsMap = searchUserRequest.getParams();

                searchUserRequest = new SearchUserRequest();
                searchUserRequest.setParams(argsMap);
            }
        });
    }

    public class TabsAdapter extends SelectableFragmentStatePagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = ProfileFragment.newInstance();
                    break;
                case 1:
                    fragment = QuestionarieFragment.newInstance();
                    break;
                default:
                    fragment = ContentFragment.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    public static final class QuestionarieFragment extends Fragment {
        int mInterest = -1;
        int mHobby = -1;
        int mReligion = -1;
        int mSport = -1;
        int mPet = -1;

        public static QuestionarieFragment newInstance() {
            QuestionarieFragment fragment = new QuestionarieFragment();
            return fragment;
        }

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_search_questionarie, container, false);
            return view;
        }

        @Override
        public void onStart() {
            super.onStart();
            initPickerFromResource(R.id.hobby_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Hobby.values())), mHobby);
            initPickerFromResource(R.id.sport_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Sport.values())), mSport);
            initPickerFromResource(R.id.pet_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Pet.values())), mPet);
            initPickerFromResource(R.id.religion_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Religion.values())), mReligion);
            initPickerFromResource(R.id.interest_picker, new ArrayList<CirclePickerItem>(Arrays.asList(Interest.values())), mInterest);
        }

        public void initPickerFromResource(final int pickerId, ArrayList<CirclePickerItem> source, int initElemId) {
            CirclePickerView picker = (CirclePickerView) getView().findViewById(pickerId);
            int margin = Utils.convertDpToPixel(10, getActivity());
            int widht = DisplayInfo.getScreenWidth(getActivity()) - (margin * 2);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(widht, widht);
            params.gravity = Gravity.CENTER_HORIZONTAL;
            params.setMargins(margin, margin, margin, margin);
            picker.setLayoutParams(params);

            picker.fill(source,
                    initElemId,
                    new CirclePickerView.OnPickListener() {
                        @Override
                        public void OnPick(CirclePickerItem element) {
                            SearchUserRequest request = ((SearchFragment) getParentFragment()).searchUserRequest;
                            switch (pickerId) {
                                case R.id.hobby_picker:
                                    mHobby = element.getId();
                                    request.setHobbie(mHobby);
                                    break;
                                case R.id.sport_picker:
                                    mSport = element.getId();
                                    request.setSport(mSport);
                                    break;
                                case R.id.pet_picker:
                                    mPet = element.getId();
                                    request.setPets(mPet);
                                    break;
                                case R.id.religion_picker:
                                    mReligion = element.getId();
                                    request.setReligion(mReligion);
                                    break;
                                case R.id.interest_picker:
                                    mInterest = element.getId();
                                    request.setInterest(mInterest);
                                    break;
                            }
                        }
                    });
        }
    }

    public static class ProfileFragment extends Fragment implements AdapterView.OnItemSelectedListener {
        private int cnId = -1, ctId = -1;
        private Spinner mProfileLangs;
        private Spinner mCountriesView;
        private Spinner mProfileOccupation;
        private Spinner mProfileEducation;
        private CityAutoCompleteTextView mProfileCities;
        private List<Integer> mCountriesId = new ArrayList<>();
        private List<Integer> mCitiesId = new ArrayList<>();
        ArrayAdapter<CharSequence> mJobAdapter;
        private EditText mFirstNameField;
        private EditText mLastNameField;

        private SearchUserRequest mSearchUserRequest;

        public static ProfileFragment newInstance() {
            ProfileFragment fragment = new ProfileFragment();
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mSearchUserRequest = ((SearchFragment) getParentFragment()).searchUserRequest;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_search_profile, container, false);
            mProfileLangs = (Spinner) view.findViewById(R.id.sp_lang);

            mFirstNameField = (EditText) view.findViewById(R.id.name);
            mFirstNameField.addTextChangedListener(new SimpleTextListener() {

                @Override
                public void afterTextChanged(Editable s) {
                    mSearchUserRequest.setFirstName(s.toString());
                }
            });
            mLastNameField = (EditText) view.findViewById(R.id.last_name);
            mLastNameField.addTextChangedListener(new SimpleTextListener() {

                @Override
                public void afterTextChanged(Editable s) {
                    mSearchUserRequest.setLastName(s.toString());
                }
            });

            ArrayAdapter<CharSequence> langProfileAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.langs, R.layout.spinner_item);

            Language lang;
            if (AppUser.get() != null) {
                lang = AppUser.get().getLanguage();
                cnId = AppUser.get().getCountryId();
                ctId = AppUser.get().getCityId();
            } else {
                lang = Language.getSystem();
                cnId = -1;
                ctId = -1;
            }

            langProfileAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            mProfileLangs.setAdapter(langProfileAdapter);
            mProfileLangs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    mSearchUserRequest.setLang(Language.getById((int) mProfileLangs.getSelectedItemId()).getRequestParam());
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            if (lang != null) {
                mProfileLangs.setSelection(lang.getId());
            }

            mCountriesView = (Spinner) view.findViewById(R.id.sp_country);
            mCountriesView.setOnItemSelectedListener(this);
            mProfileCities = (CityAutoCompleteTextView) view.findViewById(R.id.profile_city);
            mProfileCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mProfileCities.setSelectedPosition(position);
                    mSearchUserRequest.setCityId(String.valueOf(mProfileCities.getSelectedItemId()));
                }
            });

            CharSequence[] edus = new CharSequence[Education.values().length + 1];
            edus[0] = getString(R.string.search_education_choose);
            int i = 1;
            for (Education education : Education.values()) {
                edus[i++] = education.getTitle();
            }
            ArrayAdapter mEducationAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, edus);
            mProfileEducation = (Spinner) view.findViewById(R.id.sp_edu);
            mEducationAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            mProfileEducation.setAdapter(mEducationAdapter);
            mProfileEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mProfileEducation.getSelectedItemPosition() > 0) {
                        Education education = Education.values()[mProfileEducation.getSelectedItemPosition() - 1];
                        mSearchUserRequest.setEducation(education.getRequestParam());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            CharSequence[] jobs = new CharSequence[Job.values().length + 1];
            jobs[0] = getString(R.string.search_occupation_choose);
            i = 1;
            for (Job job : Job.values()) {
                jobs[i++] = job.getTitle();
            }
            mJobAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, jobs);
            mProfileOccupation = (Spinner) view.findViewById(R.id.sp_job);
            mJobAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            mProfileOccupation.setAdapter(mJobAdapter);
            mProfileOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (mProfileOccupation.getSelectedItemPosition() > 0) {
                        Job job = Job.values()[mProfileOccupation.getSelectedItemPosition() - 1];
                        mSearchUserRequest.setJob(job.getRequestParam());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            loadCountries();
            return view;
        }

        private void loadCountries() {

            List<Country> countries = LocationHelper.getCountriesList();

            mCountriesId.clear();
            List<CharSequence> c_names = new ArrayList<>();
            for (int j = 0; j < countries.size(); j++) {
                Country item = countries.get(j);
                mCountriesId.add(item.getId());
                c_names.add(item.getTitle());
            }

            c_names.add(0, getString(R.string.choose_country));
            ArrayAdapter<CharSequence> countryProfileAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, c_names);
            countryProfileAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            mCountriesView.setAdapter(countryProfileAdapter);
            mCountriesView.setOnItemSelectedListener(this);

            if (cnId >= 0) {
                for (int i = 0; i < mCountriesId.size(); i++) {
                    int id = mCountriesId.get(i);
                    if (id == cnId) {
                        mCountriesView.setSelection(i + 1);
                        break;
                    }
                }
                cnId = -1;
            }
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()) {
                case R.id.sp_country:
                    mProfileCities.clear();
                    if (position > 0) {
                        int countryId = mCountriesId.get(position - 1);
                        mProfileCities.setCountryId(countryId);
                        mSearchUserRequest.setCountryId(countryId);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public static class ContentFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,
            AdapterView.OnItemSelectedListener,
            View.OnClickListener {
        private int cnId = -1, ctId = -1;
        private int LOADER_CAT_ID = 1;
        //temp vars to store current ids
        private VideoType mVideoType;

        private List<Integer> mCategoriesIds = new ArrayList<>();
        private List<Integer> mContentTypeIds = new ArrayList<>();

        private Spinner mCountriesView;
        private CityAutoCompleteTextView mContentCities;

        private List<Integer> mCountriesId = new ArrayList<>();
        private List<Integer> mCitiesId = new ArrayList<>();

        private Button mDateFrom;
        private Button mDateTo;
        private Spinner mCategoryView;
        private Spinner mTypeView;

        private SearchUserRequest mSearchUserRequest;

        public static ContentFragment newInstance() {
            ContentFragment fragment = new ContentFragment();
            return fragment;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mSearchUserRequest = ((SearchFragment) getParentFragment()).searchUserRequest;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.fragment_search_content, container, false);
            mCountriesView = (Spinner) view.findViewById(R.id.sp_video_country);
            mCountriesView.setOnItemSelectedListener(this);

            mContentCities = (CityAutoCompleteTextView) view.findViewById(R.id.content_city);

            mContentCities.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    mContentCities.setSelectedPosition(i);
                    mSearchUserRequest.setVideoIdCity(String.valueOf(mContentCities.getSelectedItemId()));
                }
            });

            mCategoryView = (Spinner) view.findViewById(R.id.sp_category);
            mCategoryView.setOnItemSelectedListener(this);

            mTypeView = (Spinner) view.findViewById(R.id.sp_video_type);
            mTypeView.setOnItemSelectedListener(this);

            if (AppUser.get() != null) {
                cnId = AppUser.get().getCountryId();
                ctId = AppUser.get().getCityId();
            } else {
                cnId = -1;
                ctId = -1;
            }

            mDateFrom = (Button) view.findViewById(R.id.date_start);
            mDateFrom.setOnClickListener(this);

            mDateTo = (Button) view.findViewById(R.id.date_end);
            mDateTo.setOnClickListener(this);

            loadCountries();
            loadVideoTypes();
            return view;
        }

        private void loadCountries() {

            List<Country> countries = LocationHelper.getCountriesList();

            mCountriesId.clear();
            List<CharSequence> c_names = new ArrayList<>();
            for (int j = 0; j < countries.size(); j++) {
                Country item = countries.get(j);
                mCountriesId.add(item.getId());
                c_names.add(item.getTitle());
            }

            c_names.add(0, getString(R.string.choose_country));
            ArrayAdapter<CharSequence> countryProfileAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, c_names);
            countryProfileAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            mCountriesView.setAdapter(countryProfileAdapter);
            mCountriesView.setOnItemSelectedListener(this);

            if (cnId >= 0) {
                for (int i = 0; i < mCountriesId.size(); i++) {
                    int id = mCountriesId.get(i);
                    if (id == cnId) {
                        mCountriesView.setSelection(i + 1);
                        break;
                    }
                }
                cnId = -1;
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.date_start:
                    pickDate(mDateFrom);
                    break;
                case R.id.date_end:
                    pickDate(mDateTo);
                    break;
            }
        }

        private void pickDate(final Button dateView) {
            final Calendar c = Calendar.getInstance(Locale.getDefault());
            Date curDate;
            try {
                curDate = Const.DAY_MONTH_YEAR_FORMAT.parse(dateView.getText().toString());
                c.setTimeInMillis(curDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DatePickerDialog d = new DatePickerDialog(getActivity(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            c.set(year, monthOfYear, dayOfMonth);
                            dateView.setText(Const.DAY_MONTH_YEAR_FORMAT.format(c.getTime()));

                            try {
                                String time = null;
                                if (!TextUtils.isEmpty(dateView.getText())) {
                                    time = String.valueOf(Const.DAY_MONTH_YEAR_FORMAT.parse(dateView.getText().toString()).getTime());
                                }
                                if (!TextUtils.isEmpty(time)) {
                                    switch (dateView.getId()) {
                                        case R.id.date_start:
                                            mSearchUserRequest.setVideoCreatedAtFrom(time);
                                            break;
                                        case R.id.date_end:
                                            mSearchUserRequest.setVideoCreatedAtTo(time);
                                            break;
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                    }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
            d.getDatePicker().setMaxDate(new Date().getTime());
            d.show();
        }

        private void loadCategories(final VideoType videoType) {
            if (videoType != null) {
                new AsyncTask<Void, Void, ArrayList<Category>>() {

                    @Override
                    protected ArrayList<Category> doInBackground(Void... voids) {
                        String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + videoType.getId();
                        Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Categories.URI, null, selection, null, null);
                        ArrayList<Category> list = new ArrayList<>();
                        if (cursor.moveToFirst()) {
                            do {
                                Category category = Category.fromCursor(cursor);
                                list.add(category);
                            } while (cursor.moveToNext());
                        }
                        return list;
                    }

                    @Override
                    protected void onPostExecute(ArrayList<Category> categories) {
                        super.onPostExecute(categories);
                        if (isAdded()) {
                            List<CharSequence> catTitles = new ArrayList<>();
                            for (int j = 0; j < categories.size(); j++) {
                                Category item = categories.get(j);
                                mCategoriesIds.add(item.getId());
                                catTitles.add(item.getTitle());
                            }
                            catTitles.add(0, getString(R.string.search_select_category));
                            ArrayAdapter<CharSequence> c_adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, catTitles);
                            c_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                            mCategoryView.setAdapter(c_adapter);
                            mCategoryView.setOnItemSelectedListener(ContentFragment.this);
                            for (int i = 0; i < mCategoriesIds.size(); i++) {
                                int id = mCategoriesIds.get(i);
                                if (id == ctId) {
                                    mCategoryView.setSelection(i + 1);
                                    break;
                                }
                            }
                        }
                    }
                }.execute();
            }
        }

        private void loadVideoTypes() {
            List<CharSequence> typeTitles = new ArrayList<>();
            for (VideoType type : VideoType.values()) {
                if (type.getTitle() != -1) {
                    mContentTypeIds.add(type.getCode());
                    typeTitles.add(getResources().getString(type.getTitle()));
                }
            }

            typeTitles.add(0, getString(R.string.new_video_title));
            ArrayAdapter<CharSequence> c_adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, typeTitles);
            c_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
            mTypeView.setAdapter(c_adapter);
            mTypeView.setOnItemSelectedListener(ContentFragment.this);
            mTypeView.setSelection(0);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            if (id == LOADER_CAT_ID) {
                if (mVideoType != null) {
                    String selection = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + mVideoType.getId();
                    return new CursorLoader(getActivity(), ContentDescriptor.Categories.URI, null, selection, null, null);
                }
            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            if (loader.getId() == LOADER_CAT_ID) {
                loadCategories(mVideoType);
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {

        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()) {
                case R.id.sp_video_country:
                    mContentCities.clear();
                    if (position > 0) {
                        int countryId = mCountriesId.get(position - 1);
                        mContentCities.setCountryId(countryId);
                        mSearchUserRequest.setVideoIdCountry(countryId);
                    }
                    break;
                case R.id.sp_video_type:
                    mVideoType = null;
                    if (position == 0) {
                        mCategoryView.setSelection(0);
                        mCategoriesIds.clear();
                    } else {
                        if (mContentTypeIds != null && mContentTypeIds.size() > 0) {
                            mVideoType = VideoType.getByCode(mContentTypeIds.get(position - 1));
                            ((BaseActivity) getActivity()).getSpiceManager().execute(new GetCategoriesRequest(mVideoType), new CategoriesRequestListener(mVideoType));
                            getActivity().getSupportLoaderManager().restartLoader(LOADER_CAT_ID, null, this);
                        }
                    }
                    break;
                case R.id.sp_category:
                    if (mCategoryView.getSelectedItemPosition() != 0) {
                        int catId = mCategoriesIds.get(mCategoryView.getSelectedItemPosition() - 1);
                        mSearchUserRequest.setCategory(catId);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }


    @Override
    public String getTitle() {
        return getString(R.string.action_search);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }
}
