package com.henesiz.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.henesiz.R;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.adapter.SelectableFragmentStatePagerAdapter;
import com.henesiz.dialogs.TipsDialogFragment;
import com.henesiz.enums.VideoType;
import com.henesiz.util.PrefHelper;

public class VistoryFragment extends BaseFragment implements
        ViewPager.OnPageChangeListener,
        TabLayout.OnTabSelectedListener, View.OnClickListener {
    TabsAdapter mTabsAdapter;
    ViewPager mViewPager;
    TabLayout mTabLayout;

    private Tab mCurrentTab = Tab.VLOG;

    public enum Tab {
        NEWS, VLOG, MY_CHOICE;
    }

    public static VistoryFragment newInstance() {
        VistoryFragment f = new VistoryFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_vistory);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentTab = (Tab) savedInstanceState.getSerializable("tab");
//            mCurrentItem = (SimpleListItem) savedInstanceState.getSerializable("item");
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.new_video_shot:
                VideoType type;
                switch (mTabLayout.getSelectedTabPosition()) {
                    case 0:
                        type = VideoType.NEWS;
                        break;
                    case 1:
                        type = VideoType.VLOG;
                        break;
                    default:
                        type = VideoType.FLOW_ALL;
                        break;
                }
                CaptureActivity.startNewInstance(getActivity(), type);
                break;
            case R.id.open_flow:
                openFlow();
                break;
        }
    }

    private void openFlow() {
        ((CategoriesPreviewFragment) mTabsAdapter.getCurrentFragment()).openFlowList();
    }

    @Override
    protected void initUI(final View v, Bundle savedInstanceState) {
        mTabsAdapter = new TabsAdapter(getChildFragmentManager());
        mViewPager = (ViewPager) v.findViewById(R.id.pager);
        mViewPager.setAdapter(mTabsAdapter);

        mViewPager.addOnPageChangeListener(this);

        mTabLayout = (TabLayout) v.findViewById(R.id.tabhost);
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.vistory_tab_news));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.vistory_tab_vlog));
        mTabLayout.addTab(mTabLayout.newTab().setText(R.string.vistory_tab_mychoice));

        mTabsAdapter.notifyDataSetChanged();

        mTabLayout.setOnTabSelectedListener(this);

        v.findViewById(R.id.open_flow).setOnClickListener(this);
        v.findViewById(R.id.new_video_shot).setOnClickListener(this);
    }

    private synchronized void checkTips() {
        if (!PrefHelper.getBooleanPref("tips")) {
            PrefHelper.setBooleanPref("tips", true);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    new TipsDialogFragment().show(getFragmentManager(), "tips");
                }
            }, 1000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkTips();
    }

    @Override
    public void unregisterForContextMenu(View view) {
        super.unregisterForContextMenu(view);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        outState.putSerializable("item", mCurrentItem);
        outState.putSerializable("tab", mCurrentTab);
    }

    @Override
    public String getTitle() {
        return getString(R.string.menu_vistory);
    }

    @Override
    public boolean hasSideMenu() {
        return true;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        mTabLayout.getTabAt(position).select();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class TabsAdapter extends SelectableFragmentStatePagerAdapter {
        public TabsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment;
            switch (i) {
                case 0:
                    fragment = NewsFragment.newInstance();
                    break;
                case 1:
                    fragment = VlogFragment.newInstance();
                    break;
                default:
                    fragment = MyChoiceFragment.newInstance();
                    break;
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "OBJECT " + (position + 1);
        }
    }

    public static final class NewsFragment extends CategoriesPreviewFragment {
        public static NewsFragment newInstance() {
            NewsFragment fragment = new NewsFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.vistory_tab_news);
        }

        @Override
        public VideoType getVideoType() {
            return VideoType.NEWS;
        }
    }

    public static final class VlogFragment extends CategoriesPreviewFragment {
        public static VlogFragment newInstance() {
            VlogFragment fragment = new VlogFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.vistory_tab_vlog);
        }

        @Override
        public VideoType getVideoType() {
            return VideoType.VLOG;
        }
    }

    public static final class MyChoiceFragment extends CategoriesPreviewFragment {
        public static MyChoiceFragment newInstance() {
            MyChoiceFragment fragment = new MyChoiceFragment();
            return fragment;
        }

        @Override
        public String getTitle() {
            return getString(R.string.vistory_tab_mychoice);
        }


        @Override
        public VideoType getVideoType() {
            return VideoType.FLOW_MY_CHOICE;
        }
    }
}