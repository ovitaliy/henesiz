package com.henesiz.fragment.registration;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.henesiz.R;
import com.henesiz.fragment.BaseFragment;

public class OkFragment extends BaseFragment {
    public static OkFragment newInstance() {
        OkFragment fragment = new OkFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_ok);
        fragment.setArguments(args);
        return fragment;
    }

    private OnCongratulationListener mOpenVerificationListener;

    public OkFragment() {
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        view.findViewById(R.id.fab).setOnClickListener(this);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                nextScreen();
                break;
        }
    }

    private void nextScreen() {
        mOpenVerificationListener.onRegistrationProcedureComplete();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOpenVerificationListener = (OnCongratulationListener) activity;
    }

    public interface OnCongratulationListener {
        void onOpenVerification();

        void onOpenFillProfile();

        void onRegistrationProcedureComplete();
    }

}
