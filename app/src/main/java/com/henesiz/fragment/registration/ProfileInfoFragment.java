package com.henesiz.fragment.registration;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.activities.CaptureActivity;
import com.henesiz.activities.RegistrationActivity;
import com.henesiz.enums.Education;
import com.henesiz.enums.Job;
import com.henesiz.enums.Language;
import com.henesiz.model.Country;
import com.henesiz.model.UserInfo;
import com.henesiz.util.LocationHelper;
import com.henesiz.util.Utils;
import com.henesiz.view.CircleImageView;
import com.henesiz.view.CityAutoCompleteTextView;
import com.henesiz.view.SwitchButton;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by michael on 30.10.14.
 */
public class ProfileInfoFragment extends BaseProfileFragment implements OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    private EditText mFirstNameField;
    private EditText mLastNameField;
    private Spinner mCountries;
    private Spinner mEducationSpinner;
    private Spinner mOccupationSpinner;
    private CityAutoCompleteTextView mCitiesView;
    private TextView mBirth;
    private List<Integer> mCountryIds = new ArrayList<>();
    private List<Integer> mCityIds = new ArrayList<>();
    //temp vars to store current ids
    private int cnId = -1, ctId = -1;
    FloatingActionButton mFab;
    private Spinner mLangView;

    private SwitchButton mGenderSwitchButton;
    private CheckBox mMaleCheckbox;
    private CheckBox mFemaleCheckbox;

    private Activity mActivity;

    private Date mSelectedBirthDay;

    private ImageView mAvatarView;

    ArrayAdapter<CharSequence> mEducationAdapter;
    ArrayAdapter<CharSequence> mJobAdapter;

    public static ProfileInfoFragment newInstance() {
        ProfileInfoFragment f = new ProfileInfoFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_profile_info);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {

        if (!(getActivity() instanceof RegistrationActivity)) {
            view.findViewById(R.id.title).setVisibility(View.GONE);
        }

        // init views
        mLangView = (Spinner) view.findViewById(R.id.sp_lang);
        mFirstNameField = (EditText) view.findViewById(R.id.field_first_name);
        mLastNameField = (EditText) view.findViewById(R.id.field_last_name);
        mCountries = (Spinner) view.findViewById(R.id.sp_country);
        mCountries.setOnItemSelectedListener(this);
        mCitiesView = (CityAutoCompleteTextView) view.findViewById(R.id.sp_city);
        mCitiesView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mCitiesView.setSelectedPosition(i);
                if (mCitiesView.getSelectedItem() != null) {
                    AppUser.get().setCity(mCitiesView.getSelectedItem().getTitle());
                    AppUser.get().setCityId(mCitiesView.getSelectedItemId());
                }
            }
        });

        mBirth = (TextView) view.findViewById(R.id.txt_birth_field);
        mBirth.setOnClickListener(this);

        mFab = (FloatingActionButton) view.findViewById(R.id.fab);
        if (getActivity() instanceof RegistrationActivity) {
            mFab.setVisibility(View.VISIBLE);
        } else {
            mFab.setVisibility(View.GONE);
        }

        ArrayAdapter<CharSequence> langAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.langs, R.layout.spinner_item);
        Language lang = AppUser.get().getLanguage();
        langAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mLangView.setAdapter(langAdapter);
        mLangView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppUser.get().setLanguage(Language.getById(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (lang != null) {
            mLangView.setSelection(lang.getId());
        }

        CharSequence[] edus = new CharSequence[Education.values().length + 1];
        int i = 1;
        for (Education education : Education.values()) {
            edus[i++] = education.getTitle();
        }
        edus[0] = mActivity.getString(R.string.profile_pick_education);

        mEducationAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, edus);
        mEducationSpinner = (Spinner) view.findViewById(R.id.sp_edu);
        mEducationAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mEducationSpinner.setAdapter(mEducationAdapter);
        mEducationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppUser.get().setDegree(Education.getById(String.valueOf(position - 1)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Job

        CharSequence[] jobs = new CharSequence[Job.values().length + 1];
        i = 1;
        for (Job job : Job.values()) {
            jobs[i++] = job.getTitle();
        }
        jobs[0] = mActivity.getString(R.string.profile_pick_occupation);

        mJobAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item, android.R.id.text1, jobs);
        mOccupationSpinner = (Spinner) view.findViewById(R.id.sp_job);
        mJobAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mOccupationSpinner.setAdapter(mJobAdapter);
        mOccupationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppUser.get().setJobType(Job.getById(String.valueOf(position - 1)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (AppUser.get().getJobType() != null) {
            mOccupationSpinner.setSelection(Math.min(AppUser.get().getJobType().getId() + 1, mJobAdapter.getCount() - 1));
        }

        if (AppUser.get().getDegree() != null) {
            mEducationSpinner.setSelection(Math.min(AppUser.get().getDegree().getId() + 1, mEducationAdapter.getCount() - 1));
        }

        TextView textView;

        textView = (TextView) view.findViewById(R.id.profession);
        textView.setText(AppUser.get().getCraft());
        textView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AppUser.get().setCraft(s.toString());
            }
        });

        mFab.setOnClickListener(this);
        //load avatar if available
        mAvatarView = (CircleImageView) view.findViewById(R.id.avatar);
        mAvatarView.setOnClickListener(this);

        boolean maleSelected = true;
        if (AppUser.get() != null) {
            mAvatarView.setOnClickListener(this);
            mFirstNameField.setText(AppUser.get().getFirstName());
            mLastNameField.setText(AppUser.get().getLastName());
            maleSelected = AppUser.get().getGender() == 0;

            cnId = AppUser.get().getCountryId();
            ctId = AppUser.get().getCityId();

            mCitiesView.setText(AppUser.get().getCity());

            long birthday = AppUser.get().getBirthDate();
            if (birthday != 0) {
                mBirth.setText(Utils.convertFromUnixTimestamp(sdf, birthday));
                mBirth.setTextColor(Color.BLACK);
            }
        }

        mMaleCheckbox = (CheckBox) view.findViewById(R.id.male);
        mFemaleCheckbox = (CheckBox) view.findViewById(R.id.female);
        mGenderSwitchButton = (SwitchButton) view.findViewById(R.id.gender);

        mMaleCheckbox.setChecked(maleSelected);
        mFemaleCheckbox.setChecked(!maleSelected);
        mGenderSwitchButton.setChecked(!maleSelected);
        mGenderSwitchButton.setOnCheckedChangeListener(this);
        mMaleCheckbox.setOnCheckedChangeListener(this);
        mFemaleCheckbox.setOnCheckedChangeListener(this);

        if (AppUser.get().baseDataAdded) {
            Calendar c = Calendar.getInstance(Locale.getDefault());
            mBirth.setText(sdf.format(c.getTime()));
            mBirth.setTextColor(Color.BLACK);
        } else {
            checkUserData();
        }
        loadCountries();

        mFirstNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AppUser.get().setFirstName(s.toString());
            }
        });
        mLastNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                AppUser.get().setLastName(s.toString());
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        ImageLoader.getInstance().displayImage(AppUser.get().getImageLoaderPhoto(), mAvatarView);
    }

    @Override
    public String getTitle() {
        return getString(R.string.account);
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

//    String lang = getLang(mLangs.getSelectedItemPosition());
//    mUserInfo.setLanguage(lang);

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_birth_field:
                final Calendar c = Calendar.getInstance(Locale.getDefault());
                Date curDate;
                try {
                    curDate = sdf.parse(mBirth.getText().toString());
                    c.setTimeInMillis(curDate.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DatePickerDialog d = new DatePickerDialog(mActivity,
                        new OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                c.set(year, monthOfYear, dayOfMonth);
                                mSelectedBirthDay = c.getTime();
                                mBirth.setText(sdf.format(c.getTime()));
                                mBirth.setTextColor(Color.BLACK);
                                try {
                                    long birth_stamp;
                                    birth_stamp = sdf.parse(mBirth.getText().toString()).getTime();
                                    AppUser.get().setBirthDate(birth_stamp / 1000);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.YEAR, -1 * Const.USER_MIN_AGE);
                d.getDatePicker().setMaxDate(calendar.getTime().getTime());
                d.show();
                break;
            case R.id.fab:
                checkMandatoryFields(AppUser.get());
                break;
            case R.id.avatar:
                updateAvatar();
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        boolean maleSelected = false;
        switch (buttonView.getId()) {
            case R.id.male:
                maleSelected = isChecked;
                break;
            case R.id.gender:
            case R.id.female:
                maleSelected = !isChecked;
                break;
        }

        mMaleCheckbox.setOnCheckedChangeListener(null);
        mFemaleCheckbox.setOnCheckedChangeListener(null);
        mGenderSwitchButton.setOnCheckedChangeListener(null);

        mMaleCheckbox.setChecked(maleSelected);

        mGenderSwitchButton.setChecked(!maleSelected);
        mFemaleCheckbox.setChecked(!maleSelected);

        mMaleCheckbox.setOnCheckedChangeListener(this);
        mFemaleCheckbox.setOnCheckedChangeListener(this);
        mGenderSwitchButton.setOnCheckedChangeListener(this);

        AppUser.get().setGender(maleSelected ? 0 : 1);
    }

    private void updateAvatar() {
        CaptureActivity.startNewAvatarInstance(getActivity());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sp_country: {
                if (position == 0) {
                    mCitiesView.setVisibility(View.GONE);
                } else {
                    if (mCountries.getSelectedItemPosition() > 0) {
                        mCitiesView.setVisibility(View.VISIBLE);
                        int countryId = mCountryIds.get(mCountries.getSelectedItemPosition() - 1);
                        if (countryId != AppUser.get().getCountryId()) {
                            mCitiesView.setText("");
                        }
                        AppUser.get().setCountry(mCountries.getItemAtPosition(mCountries.getSelectedItemPosition()).toString());
                        AppUser.get().setCountryId(countryId);
                        mCitiesView.setCountryId(countryId);
                    }
                }
                break;
            }
            case R.id.sp_city:
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    /*private boolean allDataIsSet() {
        boolean nameIsSet = !TextUtils.isEmpty(mNameField.getText());
        return nameIsSet && isLangSelected && isCitySelected && isGenderSelected && isBirthSelected;
    }*/

    private void loadCountries() {

        List<Country> countries = LocationHelper.getCountriesList();
        mCountryIds.clear();
        List<CharSequence> c_names = new ArrayList<>();
        for (int j = 0; j < countries.size(); j++) {
            Country item = countries.get(j);
            mCountryIds.add(item.getId());
            c_names.add(item.getTitle());
        }

        c_names.add(0, getString(R.string.choose_your_country));
        ArrayAdapter<CharSequence> c_adapter = new ArrayAdapter<>(mActivity, R.layout.spinner_item, c_names);
        c_adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        mCountries.setAdapter(c_adapter);
        mCountries.setOnItemSelectedListener(ProfileInfoFragment.this);

        if (cnId >= 0) {
            for (int i = 0; i < mCountryIds.size(); i++) {
                int id = mCountryIds.get(i);
                if (id == cnId) {
                    mCountries.setSelection(i + 1);
                    break;
                }
            }
        }

    }

    private void checkUserData() {
        if (AppUser.get().baseDataAdded) {
            cnId = AppUser.get().getCountryId();
            ctId = AppUser.get().getCityId();
            Date d = new Date(AppUser.get().getBirthDate());
            mBirth.setText(sdf.format(d));
            mBirth.setTextColor(Color.BLACK);
        }
    }

    private void checkMandatoryFields(UserInfo info) {
        Calendar todayCalendar = Calendar.getInstance();
        todayCalendar.set(Calendar.HOUR_OF_DAY, 0);
        todayCalendar.set(Calendar.MINUTE, 0);
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setPositiveButton(R.string.ok, null);
        if (TextUtils.isEmpty(info.getPhoto())) {
            builder.setMessage(R.string.auth_no_avatar)
                    .create()
                    .show();
        } else if (TextUtils.isEmpty(mFirstNameField.getText()) || TextUtils.isEmpty(mLastNameField.getText())) {
            builder.setMessage(R.string.alert_name)
                    .create()
                    .show();
        } else if (mCountries.getSelectedItemPosition() <= 0) {
            builder.setMessage(R.string.alert_cn)
                    .create()
                    .show();
        } else if (mCitiesView.getSelectedItem() == null) {
            builder.setMessage(R.string.alert_ct)
                    .create()
                    .show();
        } else if (mSelectedBirthDay == null || mSelectedBirthDay != null && mSelectedBirthDay.after(todayCalendar.getTime())) {
            builder.setMessage(R.string.alert_birth)
                    .create()
                    .show();
        } else if (mEducationSpinner.getSelectedItemPosition() <= 0) {
            builder.setMessage(R.string.alert_ed)
                    .create()
                    .show();
        } else if (mOccupationSpinner.getSelectedItemPosition() <= 0) {
            builder.setMessage(R.string.alert_oc)
                    .create()
                    .show();
        } else {
            info.baseDataAdded = true;
            mOnProfileFillListener.onFillProfile(info);
        }
    }
}