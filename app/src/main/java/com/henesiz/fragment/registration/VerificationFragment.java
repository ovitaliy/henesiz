package com.henesiz.fragment.registration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.amberfog.countryflags.PhoneView;
import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.activities.AgreementActivity;
import com.henesiz.dialogs.ProgressDialog;
import com.henesiz.fragment.BaseFragment;
import com.henesiz.rest.SpiceContext;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.model.VerifyCodeResponse;
import com.henesiz.rest.request.user.GetSmsCodeRequest;
import com.henesiz.rest.request.user.VerifyCodeRequest;
import com.henesiz.util.AnalyticsHelper;
import com.henesiz.util.PrefHelper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;

import static android.view.View.VISIBLE;

/**
 * Created by michael on 28.10.14.
 */
public class VerificationFragment extends BaseFragment implements TextView.OnEditorActionListener, PhoneView.EnterPhoneListener {
    private static final int SMS_TIMEOUT = 20000;
    private static final int SMS_NOT_RECEIVED = 1;

    private PhoneView mPhoneField;
    private int mError = -1;
    private boolean isSmsReceived = false;
    private boolean isTimeoutExpired = false;
    FloatingActionButton mFab;

    private ViewGroup mCodeContainerView;
    private EditText mCodeDigitView1;
    private EditText mCodeDigitView2;
    private EditText mCodeDigitView3;
    private EditText mCodeDigitView4;
    private TextView mChangeNumberView;
    private TextView mPhoneHintView;
    private TextView mActionLabel;
    private TextView mAgreementView;

    private OnVerifyListener mOnVerifyListener;

    private SpiceManager mSpiceManager;

    private ProgressDialog mSmsWaitingProgressDialog;

    public static VerificationFragment newInstance() {
        VerificationFragment f = new VerificationFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_verification);
        f.setArguments(args);
        return f;
    }

    private TextWatcher mCodeWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mCodeDigitView1.isFocused()) {
                mCodeDigitView2.requestFocus();
            } else if (mCodeDigitView2.isFocused()) {
                mCodeDigitView3.requestFocus();
            } else if (mCodeDigitView3.isFocused()) {
                mCodeDigitView4.requestFocus();
            } else if (mCodeDigitView4.isFocused()) {
                if (mError > 0) {
                    if (getRequestCode().length() == 4) {
                        mFab.setEnabled(true);
                    } else {
                        mFab.setEnabled(false);
                    }
                }
            }
        }
    };

    private String getRequestCode() {
        String d1 = mCodeDigitView1.getText().toString();
        String d2 = mCodeDigitView2.getText().toString();
        String d3 = mCodeDigitView3.getText().toString();
        String d4 = mCodeDigitView4.getText().toString();
        return new StringBuilder(4).append(d1).append(d2).append(d3).append(d4).toString();
    }

    @Override
    protected void initUI(View v, Bundle savedInstanceState) {
        //title
        Spanned info = Html.fromHtml(getString(R.string.auth_phone_enter));
        mActionLabel = (TextView) v.findViewById(R.id.action);

        //phone number field
        mPhoneField = (PhoneView) v.findViewById(R.id.field_phone);
        mPhoneField.setOnEnterPhoneListener(this);
        mFab = (FloatingActionButton) v.findViewById(R.id.fab);
        mFab.setEnabled(false);
        mFab.setOnClickListener(this);

        mCodeDigitView1 = (EditText) v.findViewById(R.id.digit1);
        mCodeDigitView2 = (EditText) v.findViewById(R.id.digit2);
        mCodeDigitView3 = (EditText) v.findViewById(R.id.digit3);
        mCodeDigitView4 = (EditText) v.findViewById(R.id.digit4);

        mCodeDigitView1.addTextChangedListener(mCodeWatcher);
        mCodeDigitView2.addTextChangedListener(mCodeWatcher);
        mCodeDigitView3.addTextChangedListener(mCodeWatcher);
        mCodeDigitView4.addTextChangedListener(mCodeWatcher);

        mCodeContainerView = (ViewGroup) v.findViewById(R.id.code_container);
        mPhoneHintView = (TextView) v.findViewById(R.id.phone_number_hint);
        mChangeNumberView = (TextView) v.findViewById(R.id.change_number);
        mChangeNumberView.setOnClickListener(this);

        mAgreementView = (TextView) v.findViewById(R.id.agreement);
        mAgreementView.setOnClickListener(this);

        setViewMode(false);
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnVerifyListener = (OnVerifyListener) activity;
        mSpiceManager = ((SpiceContext) activity).getSpiceManager();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:
                handleInput();
                break;
            case R.id.change_number:
                changeNumber();
                break;
            case R.id.agreement:
                openAgreement();
                break;
        }
    }

    private void openAgreement() {
        //Toast.makeText(getActivity(), "UNDER CONSTRUCTION", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), AgreementActivity.class));
    }

    private void changeNumber() {
        setViewMode(false);
    }

    private void setViewMode(boolean isCodeMode) {
        if (!isCodeMode) {
            mCodeContainerView.setVisibility(View.GONE);
            mAgreementView.setVisibility(View.VISIBLE);
            mPhoneField.setVisibility(VISIBLE);
            mCodeDigitView1.setText("");
            mCodeDigitView2.setText("");
            mCodeDigitView3.setText("");
            mCodeDigitView4.setText("");
            mActionLabel.setText(R.string.auth_phone_enter);
        } else {
            mCodeContainerView.setVisibility(View.VISIBLE);
            mAgreementView.setVisibility(View.GONE);
            mPhoneField.setVisibility(View.GONE);
            mActionLabel.setText(R.string.auth_phone_code);
            mCodeDigitView1.setText("");
            mCodeDigitView2.setText("");
            mCodeDigitView3.setText("");
            mCodeDigitView4.setText("");
            mPhoneHintView.setText("+" + mPhoneField.getPhoneNumber());
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mCodeDigitView1, InputMethodManager.SHOW_IMPLICIT);
            mCodeDigitView1.requestFocus();
        }
    }

    private void handleInput() {

        View view = getActivity().getCurrentFocus();

        if (view instanceof EditText) {
            if (view.getId() == R.id.digit4) {
                if (getRequestCode().length() < 4) {
                    return;
                }
            } else if (view.getId() == R.id.field_phone) {
                if (((EditText) view).getText().length() < 10) {
                    return;
                }
            }
        }

        if (TextUtils.isEmpty(getRequestCode())) {
            onSend(mPhoneField.getPhoneNumber());
        } else {
            isTimeoutExpired = false;
            sendVerificationCode(getRequestCode());
            mCodeDigitView1.setText("");
            mCodeDigitView2.setText("");
            mCodeDigitView3.setText("");
            mCodeDigitView4.setText("");
        }
    }

    private void showResend() {
        dismissSmsWaitingProgressDialog();

        if (isAdded()) {
            setViewMode(true);

            mFab.setEnabled(false);
        }
    }

    public void requestCode(String phone) {
        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.CODE_REQUEST);
        mSpiceManager.execute(
                new GetSmsCodeRequest(phone),
                new BaseRequestListener<BaseResponse>() {
                    @Override
                    public void onRequestSuccess(BaseResponse baseResponse) {
                        super.onRequestSuccess(baseResponse);
                        if (baseResponse != null && baseResponse.isSuccess()) {
                            mError = -1;
                        } else {
                            mError = SMS_NOT_RECEIVED;
                            showResend();
                        }
                    }

                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        super.onRequestFailure(spiceException);
                        mError = SMS_NOT_RECEIVED;
                        showResend();

                        if (spiceException instanceof NoNetworkException && getActivity() != null)
                            App.showNoNetworkErrorToast(getActivity());

                        dismissSmsWaitingProgressDialog();
                    }
                });

//            isCodeResponseOk = false;
        isSmsReceived = false;
        isTimeoutExpired = false;
        mSmsWaitingProgressDialog = new ProgressDialog(getActivity());
        mSmsWaitingProgressDialog.setTitle(getString(R.string.getting_code));
        mSmsWaitingProgressDialog.show();
        waitForSms(System.currentTimeMillis());
    }

    public void sendVerificationCode(CharSequence code) {
        Log.e("IMON", "Sms received: " + System.currentTimeMillis());
        dismissSmsWaitingProgressDialog();
        if (isTimeoutExpired) {
            return;
        }
        isSmsReceived = true;

        AnalyticsHelper.trackEvent(getActivity(), AnalyticsHelper.NUMBER_VERIFICATION);
        mSpiceManager.execute(
                new VerifyCodeRequest(code.toString()),
                new BaseRequestListener<VerifyCodeResponse>() {
                    @Override
                    public void onRequestSuccess(VerifyCodeResponse response) {
                        super.onRequestSuccess(response);
                        if (response != null && response.isSuccess()) {
                            if (response.getUserId() != 0) {
                                mError = -1;
                                PrefHelper.setStringPref(PREF_PHONE, mPhoneField.getPhoneNumber());
                                mOnVerifyListener.onVerify(response.getStatus() == 0);
                            }
                        } else {
                            mError = SMS_NOT_RECEIVED;
                        }


                    }

                    @Override
                    public void onRequestFailure(SpiceException spiceException) {
                        super.onRequestFailure(spiceException);
                        if (spiceException instanceof NoNetworkException && getActivity() != null)
                            App.showNoNetworkErrorToast(getActivity());
                    }

                    @Override
                    public void onComplete() {
                        if (getProgressDialog() != null && getProgressDialog().isShowing()) {
                            getProgressDialog().dismiss();
                        }
                    }
                });

        getProgressDialog().show();
    }

    private void waitForSms(final long startTime) {
        new AsyncTask<Void, Void, Integer>() {
            @Override
            protected Integer doInBackground(Void... params) {
                while (true) {
                    if (System.currentTimeMillis() - startTime > SMS_TIMEOUT) {
                        isTimeoutExpired = true;
                        Log.e("IMON", "Timeout expired: " + System.currentTimeMillis());
                        return SMS_NOT_RECEIVED;
                    } else {
                        if (isSmsReceived) {
                            return -1;
                        }
                    }
                }
            }

            @Override
            protected void onPostExecute(Integer res) {
                mError = res;
                if (res > 0) {
                    //timeout expired, show message
                    showResend();
                }
                dismissSmsWaitingProgressDialog();
            }
        }.execute();
    }

    @Override
    public void onSend(String phoneNumber) {
        requestCode(phoneNumber);
        //mNextBtn.setEnabled(mPinEntryView.getText().length() == 4);
    }

    @Override
    public void onNumberIsValid(boolean isValid) {
        mFab.setEnabled(isValid);
    }

    public interface OnVerifyListener {
        void onVerify(boolean isNewUser);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        boolean handled = false;
        switch (actionId) {
            case EditorInfo.IME_ACTION_DONE:
            case EditorInfo.IME_ACTION_SEND:
                handleInput();
                handled = true;
                break;
        }
        return handled;
    }


    private void dismissSmsWaitingProgressDialog() {
        if (mSmsWaitingProgressDialog != null)
            mSmsWaitingProgressDialog.dismiss();
    }
}
