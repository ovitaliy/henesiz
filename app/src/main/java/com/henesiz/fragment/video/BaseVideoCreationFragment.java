package com.henesiz.fragment.video;

import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpegSync;
import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.enums.FilterApplyingType;
import com.henesiz.events.VideoProcessingFinishedEvent;
import com.henesiz.fragment.BaseFragment;
import com.henesiz.listener.VideoProcessListener;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.util.BitmapDecoder;
import com.henesiz.util.FfmpegHelper;
import com.henesiz.util.FilePathHelper;
import com.henesiz.util.ImageCroper;
import com.henesiz.view.CircleLayout;
import com.henesiz.view.CircleView;
import com.henesiz.view.GreenProgressBar;
import com.henesiz.view.square.SquareRelativeLayout;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ovitali on 12.01.2015.
 */
public abstract class BaseVideoCreationFragment extends BaseVideoFragment implements
        View.OnClickListener,
        VideoProcessListener {

    protected ImageView mImageView;
    protected CircleLayout mListView;
    protected FloatingActionButton mNextButton;

    protected Bitmap mImage;

    private ImageButton mStartVideoButton;

    protected MediaPlayer mediaPlayer;
    protected boolean isVideoPlaying;

    private SurfaceView mSurfaceView;

    protected GreenProgressBar mProgressBar;

    protected SquareRelativeLayout mSquareRelativeLayout;

    private static Thread mHandlingThread;

    private static Status mCurrentStatus = Status.NONE;

    private static File mOutputFile = FilePathHelper.getVideoTmpFile2();

    public enum Status {
        NONE, RUNNING, PREVIEW_COMPLETE, VIDEO_COMPLETE, AUDIO_COMPLETE, CANCELLED, DONE
    }

    public boolean isCanPlayVideo() {
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {

        CircleView circleView = (CircleView) view.findViewById(R.id.video_view);
        mImageView = circleView.getPreviewView();

        mProgressBar = circleView.getProgressBar();

        circleView.setPlayButtonClickListener(this);

        mStartVideoButton = circleView.getPlayButton();
        mStartVideoButton.setOnClickListener(this);

        mSurfaceView = circleView.getSurfaceView();
        mSurfaceView.setOnClickListener(this);


        mListView = (CircleLayout) view.findViewById(R.id.horizontal_list_view);
        mNextButton = (FloatingActionButton) view.findViewById(R.id.button_done);
    }


    private void checkVideoAndPlay() {
        if (!NewVideoInfo.get().isVideoFilterApplied() || !NewVideoInfo.get().isAudioApplied()) {
            VideoProcessTask.getInstance().obtainFilter(FilterApplyingType.ONLY_VIDEO, true);
        } else {
            play(NewVideoInfo.get().getVideoPath(), true);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.play_button: {
                checkVideoAndPlay();
                break;
            }
            case R.id.surface_view:
                pause();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVideoProcessingFinishedEvent(VideoProcessingFinishedEvent event) {
        checkVideoAndPlay();
    }

    @Override
    public void onVideoProcessStatus(Status status) {
        View view = getView();
        if (view == null) return;

        view.post(new Runnable() {
            @Override
            public void run() {
                mImage = VideoProcessTask.getInstance().getPreview();
                mImageView.setImageBitmap(mImage);
            }
        });

        if (status.equals(Status.RUNNING) || status.equals(Status.NONE)) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    mStartVideoButton.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                    mNextButton.setEnabled(false);
                }
            });
        }

        if (status.equals(Status.PREVIEW_COMPLETE) || status.equals(Status.DONE)) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    mImageView.setVisibility(View.VISIBLE);
                    mStartVideoButton.setVisibility(View.VISIBLE);
                }
            });
        }
        if (status.equals(Status.DONE)) {
            view.post(new Runnable() {
                @Override
                public void run() {
                    if (!isCanPlayVideo()) {
                        //play button shouldn't be visible on frame preview screen
                        mStartVideoButton.setVisibility(View.GONE);
                    } else {
                        mStartVideoButton.setVisibility(View.VISIBLE);
                    }
                    setNextButtonEnabled();
                    mProgressBar.setVisibility(View.GONE);
                }
            });
        }

    }

    public void play(String path, final boolean isVideo) {
        if (isAdded()) {
            if (!TextUtils.isEmpty(path)) {
                releaseMediaPlayer();

                isVideoPlaying = isVideo;

                mediaPlayer = new MediaPlayer();
                mSurfaceView.getHolder().setFormat(PixelFormat.TRANSPARENT);
                mSurfaceView.getHolder().setFormat(PixelFormat.OPAQUE);

                try {
                    mediaPlayer.setDataSource(path);
                    mediaPlayer.prepare();
                    mediaPlayer.setDisplay(mSurfaceView.getHolder());
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            releaseMediaPlayer();
                            mStartVideoButton.setVisibility(View.VISIBLE);
                            mImageView.setVisibility(View.VISIBLE);
                            isVideoPlaying = false;
                        }
                    });

                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                        @Override
                        public void onPrepared(MediaPlayer mp) {
                            if (isVideo) {
                                mStartVideoButton.setVisibility(View.INVISIBLE);
                                mImageView.setVisibility(View.INVISIBLE);
                            }
                        }
                    });
                    mediaPlayer.start();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void pause() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            mStartVideoButton.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.VISIBLE);
            isVideoPlaying = false;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseMediaPlayer();
        VideoProcessTask.getInstance().setListener(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        VideoProcessTask.getInstance().setListener(this);
        mStartVideoButton.setVisibility(View.GONE);
        onVideoProcessStatus(VideoProcessTask.getInstance().getStatus());
    }

    @Override
    public String getTitle() {
        return null;
    }

    protected void releaseMediaPlayer() {
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying())
                mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public enum VideoProcessTask {
        INSTANCE;

        VideoProcessTask() {
            mTaskList = new LinkedList<>();
        }

        public static VideoProcessTask getInstance() {
            return INSTANCE;
        }

        private VideoProcessListener mVideoProcessListener;

        public Bitmap mPreviewBitmap = null;

        private volatile LinkedList<Runnable> mTaskList;

        public Bitmap getPreview() {
            return mPreviewBitmap;
        }

        public void setPreviewBitmap(Bitmap bitmap) {
            mPreviewBitmap = bitmap;
        }

        public Status setListener(VideoProcessListener videoProcessListener) {
            mVideoProcessListener = videoProcessListener;
            return mCurrentStatus;
        }

        public void changeStatus(Status newStatus) {
            Log.d("status", newStatus.name());
            if (!newStatus.equals(mCurrentStatus)) {
                mCurrentStatus = newStatus;
                if (mVideoProcessListener != null) {
                    mVideoProcessListener.onVideoProcessStatus(mCurrentStatus);
                }
            }
        }

        public Status getStatus() {
            return mCurrentStatus;
        }

        private void stopTasks() {
            mTaskList.clear();
        }

        public void applyPreviewFilter() {
            applyPreviewFilter(false);
        }

        public void applyPreviewFilter(boolean ignoreCheckApplying) {
            if (!NewVideoInfo.get().isImageFilterApplied() || mPreviewBitmap == null || ignoreCheckApplying) {
                obtainFilter(FilterApplyingType.ONLY_PREVIEW);
            }
        }

        public void applyVideoFilter() {
            if (!NewVideoInfo.get().isVideoFilterApplied()) {
                obtainFilter(FilterApplyingType.ONLY_VIDEO);
            }
        }

        public void applyAudio() {
            if (!NewVideoInfo.get().isAudioApplied()) {
                obtainFilter(FilterApplyingType.ONLY_VIDEO);
            }
        }

        private void obtainFilter(FilterApplyingType filterApplyingType) {
            obtainFilter(filterApplyingType, false);
        }

        private void obtainFilter(final FilterApplyingType filterApplyingType, final boolean playAfterFinish) {

            IFilter filter = NewVideoInfo.get().getFilter();
            String originalImagePath = NewVideoInfo.get().getOriginalImagePath();
            String originalVideoPath = NewVideoInfo.get().getOriginalVideoPath();

            String audio = NewVideoInfo.get().getAudioPath();

            if (mHandlingThread != null && mHandlingThread.isAlive()) {
                mHandlingThread.interrupt();
            }

            stopTasks();

            if (filterApplyingType.equals(FilterApplyingType.ONLY_PREVIEW)
                    || filterApplyingType.equals(FilterApplyingType.ALL)
                    || !NewVideoInfo.get().isImageFilterApplied()
                    ) {

                filterImage(originalImagePath, filter);
            }

            if (filter == null && TextUtils.isEmpty(audio)) {
                //Let's just copy original file, since no filter applying
                copyFile();
                NewVideoInfo.get().setVideoFilterApplied(null);
                NewVideoInfo.get().setAudioApplied(null);
            } else {
                if (filterApplyingType.equals(FilterApplyingType.ONLY_VIDEO) || filterApplyingType.equals(FilterApplyingType.ALL)) {
                    filterVideo(originalVideoPath, filter, audio);
                }
            }

            if (mTaskList.size() == 0) {
                changeStatus(Status.DONE);
                return;
            }


            mHandlingThread = new Thread() {
                @Override
                public void run() {
                    long started = System.currentTimeMillis();
                    Log.d("ThreadFilter", "started " + Thread.currentThread().getName());

                    changeStatus(Status.RUNNING);

                    while (!mTaskList.isEmpty() && !isInterrupted()) {
                        Runnable cancelableCallable = mTaskList.poll();

                        if (cancelableCallable != null) {
                            try {
                                cancelableCallable.run();
                            } catch (Exception ex) {
                                Log.e("VideoProcessTask", "HandlingThread", ex);
                            }
                        }
                    }

                    if (!isInterrupted()) {
                        changeStatus(Status.DONE);
                        if (playAfterFinish) {
                            EventBus.getDefault().post(new VideoProcessingFinishedEvent());
                        }
                        Log.d("ThreadFilter", "finished at" + (System.currentTimeMillis() - started));
                    } else {
                        Log.d("ThreadFilter", "isInterrupted at" + (System.currentTimeMillis() - started));
                    }
                }
            };
            mHandlingThread.start();
        }

        private Status filterImage(final String input, final IFilter filter) {
            final String output = FilePathHelper.getImageTmpFile().getAbsolutePath();

            if (filter == null) {
                mTaskList.add(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = BitmapDecoder.createSquareBitmap(input, App.WIDTH_WITHOUT_MARGINS, false);
                        mPreviewBitmap = ImageCroper.getCircularBitmap(bitmap, App.IMAGE_BORDER, App.getInstance().getResources().getColor(R.color.green_main));
                        NewVideoInfo.get().setImagePath(null);
                        NewVideoInfo.get().setImageFilterApplied(null);
                    }
                });
                return Status.PREVIEW_COMPLETE;
            }

            List<Runnable> taskList = FilterController.getFilterToImageTasks(
                    FilePathHelper.getVideoCacheDirectory().getAbsolutePath(),
                    input,
                    output,
                    filter,
                    NewVideoInfo.get().getWidth(),
                    NewVideoInfo.get().getHeight(),
                    new FilterController.OperationCallback() {
                        @Override
                        public void onOperationComplete() {
                            Bitmap bitmap = BitmapDecoder.createSquareBitmap(output, App.WIDTH_WITHOUT_MARGINS, false);
                            if (bitmap != null) {
                                mPreviewBitmap = ImageCroper.getCircularBitmap(bitmap, App.IMAGE_BORDER, App.getInstance().getResources().getColor(R.color.green_main));
                                NewVideoInfo.get().setImagePath(output);
                                NewVideoInfo.get().setImageFilterApplied(filter);
                            }
                        }
                    }
            );

            mTaskList.addAll(taskList);


            return Status.PREVIEW_COMPLETE;
        }


        private Status filterVideo(final String input, final IFilter filter, final String audio) {

            final String output = FilePathHelper.getVideoTmpFile3().getAbsolutePath();

            List<Runnable> taskList = new ArrayList<>();

            if (filter != null) {
                taskList.addAll(FilterController.getFilterToVideoTasks(
                        FilePathHelper.getVideoCacheDirectory().getAbsolutePath(),
                        input,
                        output,
                        filter,
                        NewVideoInfo.get().getWidth(),
                        NewVideoInfo.get().getHeight(),
                        new FilterController.OperationCallback() {
                            @Override
                            public void onOperationComplete() {
                                NewVideoInfo.get().setVideoFilterApplied(filter);
                            }
                        }
                ));
            } else {
                copyFile(new File(input), new File(output));
                NewVideoInfo.get().setVideoFilterApplied(null);
            }

            taskList.add(new MergeVideoRunnable(
                    output,
                    audio,
                    FilePathHelper.getVideoTmpFile2().getAbsolutePath()
            ));

            mTaskList.addAll(taskList);

            return Status.VIDEO_COMPLETE;
        }


        public void copyFile() {
            copyFile(new File(NewVideoInfo.get().getOriginalVideoPath()), mOutputFile);
        }

        public void copyFile(File srcFile, File dstFile) {
            try {
                FileUtils.copyFile(
                        srcFile,
                        dstFile
                );
            } catch (Exception ignore) {
            }
        }
    }


    public abstract void setNextButtonEnabled();

    private static final class MergeVideoRunnable implements Runnable {

        private String mVideoFile;
        private String mAudioFile;
        private String mOutputVideoFile;

        public MergeVideoRunnable(String videoFile, String audioFile, String outputVideoFile) {
            mVideoFile = videoFile;
            mAudioFile = audioFile;
            mOutputVideoFile = outputVideoFile;
        }

        @Override
        public void run() {
            String audioParams = mAudioFile.contains(".aac")
                    ? "-acodec copy -bsf:a aac_adtstoasc"
                    : "";

            String command = new FfmpegHelper.CmdBuilder(FfmpegHelper.Type.IMAGE)
                    .addInput(mVideoFile)
                    .addInput(String.format("%s", mAudioFile))
                    .addOutput(mOutputVideoFile)
                    .setAdditionalParams("-vcodec copy")
                    .setAdditionalParams(audioParams)
                    .applyAudio(true)
                    .build(false);

            FFmpegSync.getInstance(App.getInstance()).execute(
                    command,
                    new ExecuteBinaryResponseHandler()
            );

            NewVideoInfo.get().setVideoPath(mOutputVideoFile);
            NewVideoInfo.get().setAudioApplied(mAudioFile);
        }
    }


}

