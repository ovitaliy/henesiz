package com.henesiz.fragment.video;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.henesiz.fragment.BaseFragment;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.model.VideoInfo;
import com.humanet.filters.FilterController;

/**
 * Created by ovi on 4/27/16.
 */
public abstract class BaseVideoFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            VideoInfo video = (VideoInfo) savedInstanceState.get(Params.VIDEO);
            NewVideoInfo.set(video);
            FilterController.init(getActivity().getApplication(), NewVideoInfo.get().getCameraId());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(Params.VIDEO, NewVideoInfo.get());
    }
}
