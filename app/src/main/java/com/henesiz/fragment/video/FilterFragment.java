package com.henesiz.fragment.video;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.adapter.VideoFilterListAdapter;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.util.FilePathHelper;
import com.henesiz.view.CircleLayout;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Владимир on 30.10.2014.
 */
public class FilterFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {


    private VideoFilterListAdapter mAdapter;
    private ArrayList<IFilter> mList;

    private IFilter mSelectedFilter;

    private static int EMPTY;

    public static FilterFragment newInstance() {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_preview_base);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.video_edit);
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.filter);

        mAdapter = new VideoFilterListAdapter(getActivity());

        mListView.setOnItemSelectedListener(this);
        mNextButton.setOnClickListener(this);

        initFilterList();
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void setNextButtonEnabled() {
        mNextButton.setEnabled(true);
    }

    private void initFilterList() {

        new ScaleImageTask() {
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mList = new ArrayList<>();
                Collections.addAll(mList, FilterController.getFilters());

                EMPTY = (int) Math.ceil(mList.size() / 2);

                mAdapter.setImagePath(FilePathHelper.getVideoPreviewImageSmallPath());
                mAdapter.setData(mList);
                mListView.setAdapter(mAdapter);

                int selectedIndex = EMPTY;
                if (NewVideoInfo.get().getFilter() != null) {
                    for (int i = 0; i < mList.size(); i++) {
                        IFilter f = mList.get(i);
                        if (filtersAreSame(NewVideoInfo.get().getFilter(), f)) {
                            selectedIndex = i;
                            break;
                        }
                    }
                }

                mListView.setSelectedItem(selectedIndex);

                onItemSelected(mList.get(selectedIndex));
            }
        }.executeOnExecutor(ScaleImageTask.THREAD_POOL_EXECUTOR);

    }

    public boolean filtersAreSame(IFilter original, IFilter filter) {
        if (original == null) {
            return filter == null;
        }

        if (filter == null)
            return false;

        return original.getTitle().equals(filter.getTitle());
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.button_done:
                VideoProcessTask.getInstance().applyVideoFilter();
                getActivity().onBackPressed();
                break;
        }
    }


    @Override
    public void onItemSelected(Object data) {
        mSelectedFilter = (IFilter) data;
        releaseMediaPlayer();

        if (mSelectedFilter == null) {
            NewVideoInfo.get().setVideoPath(null);
            NewVideoInfo.get().setImagePath(null);
        }

        NewVideoInfo.get().setFilter(mSelectedFilter);
        VideoProcessTask.getInstance().applyPreviewFilter();
    }

    private class ScaleImageTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {

                while (NewVideoInfo.isIsFilterBuilding())
                    Thread.sleep(50);

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }
    }


}