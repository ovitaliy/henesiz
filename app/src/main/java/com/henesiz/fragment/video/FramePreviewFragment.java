package com.henesiz.fragment.video;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.adapter.FrameListAdapter;
import com.henesiz.model.Frame;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.util.FilePathHelper;
import com.henesiz.view.CircleLayout;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Владимир on 29.10.2014.
 */
public class FramePreviewFragment extends BaseVideoCreationFragment implements CircleLayout.OnItemSelectedListener {

    private ArrayList<Frame> mFrames;
    private FrameListAdapter mAdapter;

    private Frame mSelectedFrame;

    public static FramePreviewFragment newInstance() {
        FramePreviewFragment fragment = new FramePreviewFragment();
        Bundle args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_preview_base);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.video_edit);
    }

    @Override
    protected void initUI(View view, Bundle savedInstanceState) {
        super.initUI(view, savedInstanceState);
        mNextButton.setOnClickListener(this);

        ((TextView) view.findViewById(R.id.title)).setText(R.string.preview);

        if (!NewVideoInfo.isFrameGrabbing())
            initAdapter();
        else
            waitUntilGrabbingWillComplete();
    }

    private void initAdapter() {
        mFrames = new ArrayList<>();
        mListView.setOnItemSelectedListener(this);

        File[] frames = FilePathHelper.getVideoFrameFolder().listFiles();

        mAdapter = new FrameListAdapter(getActivity());
        makeFramesList(frames);
        int selected = 0;
        for (int i = 0; i < mFrames.size(); i++) {
            String videoPreviewPath = NewVideoInfo.get().getOriginalImagePath();
            String framePreviewPath = mFrames.get(i).getImagePath();
            if (videoPreviewPath.equals(framePreviewPath)) {
                selected = i;
                break;
            }
        }

        mListView.setAdapter(mAdapter);
        mListView.setSelectedItem(selected);
    }

    private void waitUntilGrabbingWillComplete() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                if (NewVideoInfo.isFrameGrabbing() && isAdded()) {
                    try {
                        Thread.sleep(200);
                    } catch (Exception ignore) {
                    }
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (isAdded()) {
                    initAdapter();
                }
            }
        }.execute();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mAdapter != null) {
            mAdapter.clearCache();
        }
    }

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public void setNextButtonEnabled() {
        mNextButton.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.button_done:
                if (mSelectedFrame != null) {
                    getActivity().onBackPressed();
                    NewVideoInfo.rebuildFilterPack(NewVideoInfo.get().getOriginalImagePath());
                }
                break;
        }
    }

    private void makeFramesList(File[] paths) {
        mFrames.clear();
        for (File path : paths) {
            Frame frame = new Frame();
            frame.setImagePath(path.getAbsolutePath());
            mFrames.add(frame);
        }
        mAdapter.setData(mFrames);
    }

    public void onItemSelected(Object data) {
        mSelectedFrame = (Frame) data;
        NewVideoInfo.get().setOriginalImagePath(mSelectedFrame.getImagePath());
        NewVideoInfo.get().setImageFilterApplied(null);
        VideoProcessTask.getInstance().applyPreviewFilter(true);
    }

    @Override
    public boolean isCanPlayVideo() {
        return false;
    }
}
