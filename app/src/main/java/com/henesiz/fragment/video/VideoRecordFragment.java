package com.henesiz.fragment.video;

import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.henesiz.App;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.dialogs.DialogBuilder;
import com.henesiz.enums.VideoType;
import com.henesiz.events.VideoProcessedEvent;
import com.henesiz.model.NewVideoInfo;
import com.henesiz.util.FilePathHelper;
import com.henesiz.view.CircleView;
import com.henesiz.view.VideoTextureView;
import com.humanet.camera.CameraApi;
import com.humanet.camera.CameraApi16Impl;
import com.humanet.camera.CameraApi21Impl;
import com.humanet.filters.FilterController;
import com.humanet.video.ProcessCapturedVideoRunnable;

import org.apache.commons.io.FileUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Inteza23 on 28.10.2014.
 */
public final class VideoRecordFragment extends BaseVideoFragment implements Const {

    private static final String LOG_TAG = "VideoRecordFragment";

    // <--- recorder params
    private long mTimeStartRecording;

    private boolean mIsRecording;
    // <--- recorder params


    private TextureView mTextureView;

    private TextView mTitleText;

    private UpdateTimeTask mTimeTask;

    private ProgressBar mProgressBar;

    private static final int VIDEO_MAX_LENGTH = 7;
    private static final int PROGRESS_MAX = VIDEO_MAX_LENGTH * 1000;

    private boolean mIsFlashOn = false;
    private boolean mIsFacingCamera;

    private ImageButton mStartRecordingButton, mCameraFlashButton, mChangeCameraButton;

    private HolderCallback mHolderCallback;

    private OnVideoRecordCompleteListener mListener = null;

    private CameraApi mCameraApi;

    private CircleView mCircleView;


    private boolean mIsFragmentPaused;

    @Override
    public boolean hasSideMenu() {
        return false;
    }

    @Override
    public String getTitle() {
        return getString(R.string.video_recording);
    }

    public static VideoRecordFragment newInstance() {
        Bundle args;
        VideoRecordFragment f = new VideoRecordFragment();
        args = new Bundle();
        args.putInt(EXTRA_LAYOUT_ID, R.layout.fragment_video_record);
        f.setArguments(args);
        f.setRetainInstance(true);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT < 21)
            mCameraApi = new CameraApi16Impl();
        else
            mCameraApi = new CameraApi21Impl(App.getInstance());

        if (mCameraApi.getCamerasCount() > 1) {
            VideoType videoType = NewVideoInfo.get().getVideoType();

            if (videoType != null
                    && (videoType.equals(VideoType.VLOG)
                    || videoType.equals(VideoType.AVATAR))) {
                mIsFacingCamera = true;
            }
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCircleView = (CircleView) view.findViewById(R.id.surface_view);

        initProgressBar(view);
        initSurfaceView(view);

        mTitleText = (TextView) view.findViewById(R.id.video_recording_title);

        mStartRecordingButton = (ImageButton) view.findViewById(R.id.btn_rec);
        mStartRecordingButton.setOnClickListener(mRecordOnClickListener);

        mHolderCallback = new HolderCallback();
        mTextureView.setSurfaceTextureListener(mHolderCallback);

        initChangeCameraButton(view);
    }

    @Override
    public void onResume() {
        super.onResume();

        mIsFragmentPaused = false;

        mListener = (OnVideoRecordCompleteListener) getActivity();

        if (mHolderCallback.isAvailable()) {
            initCamera();
        }

    }

    @Override
    public void onPause() {
        super.onPause();

        mIsFragmentPaused = true;

        EventBus.getDefault().unregister(this);

        if (mIsRecording) {
            stopRecording();
        }

        mCameraApi.releaseCamera();
    }

    private void initChangeCameraButton(View view) {
        mChangeCameraButton = (ImageButton) view.findViewById(R.id.btn_change_camera);
        if (mCameraApi.getCamerasCount() > 1) {
            mChangeCameraButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mIsFlashOn) {
                        mCameraApi.turnFlash(false);
                        mIsFlashOn = false;
                    }
                    mIsFacingCamera = !mIsFacingCamera;

                    initCamera();
                }
            });
        } else {
            mChangeCameraButton.setEnabled(false);
        }
    }

    public void initProgressBar(View view) {
        mProgressBar = (ProgressBar) view.findViewById(R.id.video_recording_progress);
        mProgressBar.setMax(PROGRESS_MAX);
        mProgressBar.setProgress(0);
    }


    private void initSurfaceView(View view) {
        mTextureView = mCircleView.getTextureView();

        mTextureView.setOnTouchListener(mOnTouchListener);
    }

    private void initCamera() {
        try {
            if (mHolderCallback != null && mHolderCallback.isAvailable()) {
                mCameraApi.setCameraFacing(mIsFacingCamera);

                mCameraApi.setSurfaceHolder(mTextureView);

                initFlashButton(getView());
            }
        } catch (CameraApi.CameraException ex) {
            ex.printStackTrace();
            Toast.makeText(getActivity(), R.string.camera_failure, Toast.LENGTH_SHORT).show();
            getActivity().finish();

            Crashlytics.logException(ex);
        }
    }

    private void initFlashButton(View view) {
        mCameraFlashButton = (ImageButton) view.findViewById(R.id.btn_flash);
        if (mCameraApi.hasFlash()) {
            mCameraFlashButton.setEnabled(true);
            mCameraFlashButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mIsFlashOn = !mIsFlashOn;
                    mCameraApi.turnFlash(!mIsFlashOn);
                }
            });

        } else {
            mCameraFlashButton.setEnabled(false);
        }
    }

    private void initSurfaceViewSize() {
        View parent = (View) mTextureView.getParent();

        VideoTextureView view = (VideoTextureView) mTextureView;

        float width = mCameraApi.getCapturedImageHeight();
        float height = mCameraApi.getCapturedImageWidth();

        float parentWidth = parent.getMeasuredWidth();
        float parentHeight = parent.getMeasuredHeight();
        float scale = Math.max(parentWidth / width, parentHeight / height);
        float scaledWidth = (width * scale);
        float scaledHeight = (height * scale);

        if (view.setCalculatedSizes((int) scaledWidth, (int) scaledHeight)) {
            view.setCalculatedX((int) ((parentWidth - scaledWidth) / 2));
            view.setCalculatedY((int) ((parentHeight - scaledHeight) / 2));

            view.requestLayout();
        }
    }

    private class HolderCallback implements TextureView.SurfaceTextureListener {

        private boolean mAvailable;

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            mAvailable = true;

            initCamera();
            initSurfaceViewSize();

            //  waitForFrame();

        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            mAvailable = false;
            //   mCameraApi.releaseCamera();
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }


        public boolean isAvailable() {
            return mAvailable;
        }
    }


    private View.OnClickListener mRecordOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (!mIsRecording) {
                startRecording();
            } else {
                stopRecording();
            }

        }
    };

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void startRecording() {

        File f = FilePathHelper.getVideoTmpFile();
        if (f.exists() && f.delete()) {
            try {
                f.createNewFile();
            } catch (Exception ignore) {
            }
        }

        cleanUpDirectories();

        mTimeTask = new UpdateTimeTask();

        mProgressBar.post(new Runnable() {
            @Override
            public void run() {
                mTimeStartRecording = System.currentTimeMillis();
                new Timer().schedule(mTimeTask, 30, 30);
                mIsRecording = true;

                invalidateViewsVisibilitiesForRecordingStatus();
            }
        });

        mCameraApi.startRecord(f);
    }

    private void invalidateViewsVisibilitiesForRecordingStatus() {
        if (mIsRecording) {
            mChangeCameraButton.setVisibility(View.INVISIBLE);
            mCameraFlashButton.setVisibility(View.INVISIBLE);

            mProgressBar.setVisibility(View.VISIBLE);
        } else {

            mChangeCameraButton.setVisibility(View.VISIBLE);
            mCameraFlashButton.setVisibility(View.VISIBLE);

            mProgressBar.setVisibility(View.INVISIBLE);

            mTitleText.setText(R.string.video_record_title);
        }

        mProgressBar.setProgress(0);

    }

    public void stopRecording() {
        long recordLength = System.currentTimeMillis() - mTimeStartRecording;

        if (mTimeTask != null) {
            mTimeTask.cancel();
        }
        mTimeTask = null;
        //  btnRecStart.setImageResource(R.drawable.ic_record_start);
        mStartRecordingButton.setEnabled(true);

        if (mIsRecording) {
            Log.v(LOG_TAG, "Finishing recording, calling stop and release on recorder");
            try {
                mCameraApi.stopRecord();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!mIsFragmentPaused) {
            if (recordLength > 1500) {
                getProgressDialog().setTitle(R.string.processing);
                getProgressDialog().show();

                int size = Math.min(mCameraApi.getCapturedImageWidth(), mCameraApi.getCapturedImageHeight());
                NewVideoInfo.get().setSizes(size, size);

                Thread thread = new Thread(new ProcessCapturedVideoRunnable.Builder(App.getInstance())
                        .setCameraId(mIsFacingCamera ? 1 : 0)
                        .setCameraSideSize(size)
                        .setFramesFolder(FilePathHelper.getVideoFrameFolder())
                        .setSourceVideoFilePath(FilePathHelper.getVideoTmpFile())
                        .setTmpVideoFilePath(FilePathHelper.getVideoTmpFile2())
                        .setAudioStreamPath(FilePathHelper.getAudioStreamFile())
                        .setListener(new ProcessCapturedVideoRunnable.OnVideoProcessedListener() {
                            @Override
                            public void onVideoProcessed(boolean success) {
                                EventBus.getDefault().postSticky(new VideoProcessedEvent(success));
                            }
                        })
                        .build());
                thread.setPriority(Thread.MAX_PRIORITY);
                thread.start();
            } else {
                showErrorAboutNoVideo();
                //initCamera();
            }
        }
        mIsRecording = false;

        invalidateViewsVisibilitiesForRecordingStatus();
    }

    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onVideoProcessedEvent(VideoProcessedEvent event) {
        EventBus.getDefault().removeAllStickyEvents();
        if (getProgressDialog() != null && !getProgressDialog().isDismissed()) {
            getProgressDialog().dismiss();
        }

        if (event.isSuccess()) {
            FilterController.init(App.getInstance(), mIsFacingCamera ? 1 : 0);

            mListener.onVideoRecordComplete(FilePathHelper.getVideoTmpFile().getAbsolutePath(), mIsFacingCamera ? 1 : 0);
        } else {
            new DialogBuilder(getActivity())
                    .setMessage(R.string.frame_error_text)
                    .setCancelable(true)
                    .create()
                    .show();
        }
    }


    private class UpdateTimeTask extends TimerTask {
        @Override
        public void run() {
            mTitleText.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (mIsRecording) {
                            long d = System.currentTimeMillis() - mTimeStartRecording;
                            Date date = new Date(d);
                            mTitleText.setText(new SimpleDateFormat("mm:ss", Locale.getDefault()).format(date));
                            int videoRecordingLength = (int) date.getTime();
                            if (videoRecordingLength > VIDEO_MAX_LENGTH * 1000) {
                                stopRecording();
                            }
                            mProgressBar.setProgress(videoRecordingLength);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        stopRecording();
                    }
                }

            });
        }
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (MotionEvent.ACTION_DOWN == event.getAction()) {
                mCameraApi.focus();
            }
            return false;
        }
    };

    private void showErrorAboutNoVideo() {
        new DialogBuilder(getActivity())
                .setMessage(R.string.no_recorded_video)
                .create()
                .show();
    }


    private void cleanUpDirectories() {
        new Thread() {
            @Override
            public void run() {
                try {
                    FileUtils.cleanDirectory(FilePathHelper.getVideoFrameFolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    public interface OnVideoRecordCompleteListener {
        void onVideoRecordComplete(String videoPath, int cameraId);
    }
}
