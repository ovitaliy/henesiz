package com.henesiz.jobs;

import android.text.TextUtils;

import com.henesiz.events.LoadedFileEvent;
import com.henesiz.util.FilePathHelper;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;

import okio.BufferedSink;
import okio.Okio;

/**
 * Created by ovitali on 22.01.2015.
 */
public class VideoPreloadRunnable implements Runnable {
    String mUrl;

    public VideoPreloadRunnable(String url) {
        mUrl = url;
    }

    @Override
    public void run() {

        if (TextUtils.isEmpty(mUrl)) {
            return;
        }

        String videoName = mUrl.substring(mUrl.lastIndexOf("/") + 1);

        File destinationFile = new File(FilePathHelper.getVideoCacheDirectory(), videoName);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(mUrl)
                .addHeader("Content-Type", "application/json")
                .build();

        try {
            Response response = client.newCall(request).execute();

            if (!destinationFile.exists() || destinationFile.length() != response.body().contentLength()) {
                destinationFile.delete();

                BufferedSink sink = Okio.buffer(Okio.sink(destinationFile));
                sink.writeAll(response.body().source());
                sink.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        EventBus.getDefault().post(new LoadedFileEvent(destinationFile.getAbsolutePath()));
    }
}

