package com.henesiz.listener;

/**
 * Phone number selection listener
 */
public interface NumberSelectedListener {
    void onNumberSelected(String number);
}