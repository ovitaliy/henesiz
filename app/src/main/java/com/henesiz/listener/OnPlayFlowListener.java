package com.henesiz.listener;

import com.henesiz.enums.VideoType;
import com.henesiz.model.Video;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Deni on 13.07.2015.
 */
public interface OnPlayFlowListener {
    void onPlayFlow(HashMap<String, Object> params, int replyId, String title, ArrayList<Video> mVideoList, VideoType videoType);
}
