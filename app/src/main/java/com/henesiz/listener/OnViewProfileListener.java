package com.henesiz.listener;

/**
 * Created by ovitali on 29.01.2015.
 */
public interface OnViewProfileListener {
    void onViewProfile(int uid);
}
