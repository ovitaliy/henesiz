package com.henesiz.listener;

import com.henesiz.fragment.video.BaseVideoCreationFragment;

/**
 * Created by ovitali on 17.02.2015.
 */
public interface VideoProcessListener {
    void onVideoProcessStatus(BaseVideoCreationFragment.Status status);
}
