package com.henesiz.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.BalanceChangeType;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Владимир on 13.11.2014.
 */
public class Balance {

    @SerializedName("id")
    int id;

    @SerializedName("type")
    BalanceChangeType balanceChangeType;

    @SerializedName("change")
    int change;

    @SerializedName("id_object")
    int mObjectId;

    @SerializedName("id_user")
    int mUserId;

    @SerializedName("img_object")
    String mObjectImg;

    @SerializedName("created_at")
    Date createdAt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BalanceChangeType getBalanceChangeType() {
        return balanceChangeType;
    }

    public void setBalanceChangeType(BalanceChangeType balanceChangeType) {
        this.balanceChangeType = balanceChangeType;
    }

    public int getChange() {
        return change;
    }

    public void setChange(int change) {
        this.change = change;
    }

    public int getObjectId() {
        return mObjectId;
    }

    public void setObjectId(int objectId) {
        this.mObjectId = objectId;
    }

    public String getObjectImg() {
        return mObjectImg;
    }

    public void setObjectImg(String objectImg) {
        this.mObjectImg = objectImg;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Balance() {
    }

    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues(6);
        contentValues.put(ContentDescriptor.Balances.Cols.ID, getId());
        if (getBalanceChangeType() != null) {
            contentValues.put(ContentDescriptor.Balances.Cols.TYPE, getBalanceChangeType().getId());
        }
        contentValues.put(ContentDescriptor.Balances.Cols.CHANGE, getChange());
        contentValues.put(ContentDescriptor.Balances.Cols.OBJECT_ID, getObjectId());
        contentValues.put(ContentDescriptor.Balances.Cols.OBJECT_IMG, getObjectImg());
        contentValues.put(ContentDescriptor.Balances.Cols.CREATED_AT, getCreatedAt().getTime());
        return contentValues;
    }

    public static Balance fromCursor(Cursor cursor) {
        Balance balance = new Balance();
        try {
            balance.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.ID)));
            balance.setBalanceChangeType(BalanceChangeType.getById(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.TYPE))));
            balance.setChange(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.CHANGE)));
            balance.setObjectId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.OBJECT_ID)));
            if (balance.getObjectId() > 0)
                balance.setObjectImg(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.OBJECT_IMG)));

            balance.setCreatedAt(new Date(cursor.getLong(cursor.getColumnIndex(ContentDescriptor.Balances.Cols.CREATED_AT))));
        } catch (IllegalStateException e) {
            Log.e("Balance", e.getMessage());
        }

        return balance;
    }
}
