package com.henesiz.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.FeedbackType;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis on 28.04.2015.
 */
public class Feedback {
    @SerializedName("id")
    private int mId;

    @SerializedName("type")
    private FeedbackType mType;

    @SerializedName("last_msg")
    private String mLastMessage;

    @SerializedName("n")
    private int mNew;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(4);
        values.put(ContentDescriptor.Feedback.Cols.ID, mId);
        values.put(ContentDescriptor.Feedback.Cols.TYPE, mType.getId());
        values.put(ContentDescriptor.Feedback.Cols.LAST_MESSAGE, mLastMessage);
        values.put(ContentDescriptor.Feedback.Cols.NEW, mNew);
        return values;
    }

    public static Feedback fromCursor(Cursor cursor) {
        Feedback feeback = new Feedback();
        feeback.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.ID)));
        int status = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.TYPE));
        if (status != 0) {
            feeback.setType(FeedbackType.getById(status));
        }
        feeback.setNew(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.NEW)));
        feeback.setLastMessage(cursor.getString(cursor.getColumnIndex(ContentDescriptor.Feedback.Cols.LAST_MESSAGE)));
        return feeback;
    }

    public FeedbackType getType() {
        return mType;
    }

    public void setType(FeedbackType type) {
        mType = type;
    }

    public String getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
    }

    public int getNew() {
        return mNew;
    }

    public void setNew(int aNew) {
        mNew = aNew;
    }
}
