package com.henesiz.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.henesiz.AppUser;
import com.henesiz.db.ContentDescriptor;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Deni on 06.07.2015.
 */
public class FeedbackMessage {
    @SerializedName("id_user")
    private int mUserId;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("created_at")
    private Date mTime;

    public Boolean isReply() {
        return mUserId != AppUser.getUid();
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public Date getTime() {
        return mTime;
    }

    public void setTime(Date time) {
        mTime = time;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(4);
        values.put(ContentDescriptor.FeedbackMessages.Cols.ID_USER, mUserId);
        values.put(ContentDescriptor.FeedbackMessages.Cols.MESSAGE, mMessage);
        values.put(ContentDescriptor.FeedbackMessages.Cols.TIME, mTime.getTime());
        return values;
    }

    public static FeedbackMessage fromCursor(Cursor cursor) {
        FeedbackMessage message = new FeedbackMessage();
        message.setUserId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.FeedbackMessages.Cols.ID_USER)));
        message.setMessage(cursor.getString(cursor.getColumnIndex(ContentDescriptor.FeedbackMessages.Cols.MESSAGE)));
        message.setTime(new Date(cursor.getLong(cursor.getColumnIndex(ContentDescriptor.FeedbackMessages.Cols.TIME))));
        return message;
    }
}
