package com.henesiz.model;

/**
 * Created by Владимир on 29.10.2014.
 */
public class Frame {
    private String mImagePath;

    public Frame() {
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }
}
