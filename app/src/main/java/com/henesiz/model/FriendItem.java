package com.henesiz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Inteza23 on 16.12.2014.
 */
public class FriendItem implements Parcelable {
    @SerializedName("name")
    private String mName;

    @SerializedName("phones")
    private ArrayList<String> mNumbers = new ArrayList<>();

    public FriendItem() {
    }

    public ArrayList<String> getPhoneNumbers() {
        return mNumbers;
    }

    public void addPhoneNumber(String phoneNumber) {
        mNumbers.add(phoneNumber);
    }

    public ArrayList<String> getTrimmedPhones() {
        ArrayList<String> list = new ArrayList<>();
        for (String item : mNumbers) {
            String phone = item.replaceAll("[^\\d]", "");
            if (phone.startsWith("8")) {
                phone = phone.substring(1, phone.length());
                phone = "7" + phone;
            }
            list.add(phone);
        }
        return list;
    }

    public void setPhoneNumbers(ArrayList<String> phoneNumber) {
        this.mNumbers = phoneNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeStringList(mNumbers);
    }

    public static final Parcelable.Creator<FriendItem> CREATOR = new Parcelable.Creator<FriendItem>() {
        public FriendItem createFromParcel(Parcel in) {
            return new FriendItem(in);
        }

        public FriendItem[] newArray(int size) {
            return new FriendItem[size];
        }
    };

    private FriendItem(Parcel parcel) {
        mName = parcel.readString();
        parcel.readStringList(mNumbers);
    }
}
