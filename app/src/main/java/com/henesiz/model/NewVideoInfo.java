package com.henesiz.model;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.henesiz.App;
import com.henesiz.enums.VideoType;
import com.henesiz.util.BitmapDecoder;
import com.henesiz.util.FilePathHelper;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;
import com.humanet.video.ProcessFrameGrabbing;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Singletone that uses to keep info about video that was just recorded
 */
public class NewVideoInfo {
    private static VideoInfo sVideoInfo;

    public static void init(VideoType videoType) {
        sVideoInfo = new VideoInfo(videoType);
    }

    public static VideoInfo get() {
        return sVideoInfo;
    }

    public static void set(VideoInfo info) {
        sVideoInfo = info;
    }

    public static boolean isVideoRecorded() {
        return sVideoInfo != null && !TextUtils.isEmpty(get().getOriginalVideoPath()) && new File(get().getOriginalVideoPath()).exists();
    }

    private static boolean sIsFilterBuilding = false;
    private volatile static boolean sIsFrameGrabbing = false;

    public static boolean isIsFilterBuilding() {
        return sIsFilterBuilding;
    }

    public static boolean isFrameGrabbing() {
        return sIsFrameGrabbing;
    }

    private static ScheduledExecutorService sFilterExecutor;

    public synchronized static void rebuildFilterPack(final String newImagePath) {
        sIsFilterBuilding = true;

        if (sFilterExecutor != null) {
            sFilterExecutor.shutdownNow();
        }
        sFilterExecutor = Executors.newSingleThreadScheduledExecutor();

        final File filtersDir = FilePathHelper.getVideoFilteredImagePreviewFolder();

        List<Runnable> tasks = new ArrayList<>();

        Runnable prepareRunnable = new Runnable() {
            @Override
            public void run() {

                Bitmap bitmap = BitmapDecoder.createSquareBitmap(newImagePath, App.IMAGE_SMALL_WIDTH, 0, false);
                FileOutputStream fileOutputStream = null;
                try {
                    fileOutputStream = new FileOutputStream(FilePathHelper.getVideoPreviewImageSmallPath());

                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);

                    for (File f : filtersDir.listFiles()) {
                        FileUtils.forceDelete(f);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (Exception ignore) {
                        }
                    }
                }

            }
        };

        tasks.add(prepareRunnable);

        IFilter[] filters = FilterController.getFilters();


        for (IFilter filter : filters) {
            tasks.addAll(FilterController.getFilterToImageTasks(
                    filtersDir.getAbsolutePath(),
                    FilePathHelper.getVideoPreviewImageSmallPath(),
                    FilePathHelper.getFilteredImagePreview(filter.getTitle()).getAbsolutePath(),
                    filter,
                    100, 100,
                    null
                    )
            );
        }

        Runnable complete = new Runnable() {
            @Override
            public void run() {
                sIsFilterBuilding = false;
            }
        };
        tasks.add(complete);

        for (Runnable runnable : tasks)
            sFilterExecutor.execute(runnable);

    }

    public synchronized static void grabFramesPack() {
        sIsFrameGrabbing = true;

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.execute(ProcessFrameGrabbing.getRunnable(FilePathHelper.getVideoFrameFolder(), FilePathHelper.getVideoTmpFile(), ProcessFrameGrabbing.MODE_ALL_EXCLUDING_FIRST_FRAME));
        executor.execute(new Runnable() {
            @Override
            public void run() {
                sIsFrameGrabbing = false;
            }
        });

    }
}
