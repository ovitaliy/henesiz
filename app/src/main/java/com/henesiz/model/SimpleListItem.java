package com.henesiz.model;

import java.io.Serializable;

public interface SimpleListItem extends Serializable {
    String getTitle();

    String getImage();
}
