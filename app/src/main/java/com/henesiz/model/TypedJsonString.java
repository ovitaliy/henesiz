package com.henesiz.model;

import retrofit.mime.TypedString;

/**
 * Created by denisvasilenko on 11.01.16.
 */
public class TypedJsonString extends TypedString {
    public TypedJsonString(String string) {
        super(string);
    }

    @Override
    public String mimeType() {
        return "application/json";
    }
}
