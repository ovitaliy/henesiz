package com.henesiz.model;

import com.google.gson.annotations.SerializedName;
import com.henesiz.rest.model.BaseResponse;

/**
 * Created by ovi on 12/22/15.
 */
public class UploadingMedia extends BaseResponse {

    @SerializedName("media")
    private String mMedia;
    @SerializedName("thumb")
    private String mThumb;

    public String getMedia() {
        return mMedia;
    }

    public String getThumb() {
        return mThumb;
    }

    public void setMedia(String media) {
        mMedia = media;
    }

    public void setThumb(String thumb) {
        mThumb = thumb;
    }
}
