package com.henesiz.model;

import android.media.MediaPlayer;
import android.text.TextUtils;

import com.henesiz.Const;
import com.henesiz.enums.VideoType;
import com.henesiz.util.FilePathHelper;
import com.humanet.filters.FilterController;
import com.humanet.filters.videofilter.IFilter;

import java.io.File;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Contains info about captured video
 */
public class VideoInfo implements Serializable {
    private int mLocalId;
    private String mVideoPath;
    private String mImagePath;
    private String mAudioPath;
    private IFilter mFilter;
    private int mCameraId;
    private int mReplyToVideoId;
    private int mCategoryId;
    private Category mCategory;
    private String mTags;

    private int mCost;

    private VideoType mVideoType;

    private String mOriginalVideoPath;
    private String mOriginalImagePath;

    private IFilter mVideoFilterApplied;
    private IFilter mImageFilterApplied;
    private String mAudioAppliedPath;

    private boolean mIsAvatar;

    private BigDecimal mDuration;

    private int mAudioId;

    private int mWidth;
    private int mHeight;

    private String mUploadedVideoPath;
    private String mUploadedImagePath;

    public String getOriginalVideoPath() {
        return mOriginalVideoPath;
    }

    public void setOriginalVideoPath(String originalVideoPath) {
        mOriginalVideoPath = originalVideoPath;
        mVideoPath = mOriginalVideoPath;
    }

    public String getOriginalImagePath() {
        return mOriginalImagePath;
    }

    public void setOriginalImagePath(String originalImagePath) {
        mOriginalImagePath = originalImagePath;
        setImagePath(originalImagePath);
    }

    public void setVideoFilterApplied(IFilter videoFilterApplied) {
        mVideoFilterApplied = videoFilterApplied;
    }


    public void setImageFilterApplied(IFilter videoFilterApplied) {
        mImageFilterApplied = videoFilterApplied;
    }

    public void setAudioApplied(String audioApplied) {
        mAudioAppliedPath = audioApplied;
    }

    public boolean isVideoFilterApplied() {
        return FilterController.areFiltersSame(mFilter, mVideoFilterApplied);
    }

    public boolean isImageFilterApplied() {
        return FilterController.areFiltersSame(mFilter, mImageFilterApplied);
    }

    public boolean isAudioApplied() {
        return mAudioPath.equals(mAudioAppliedPath);
    }

    public int getCameraId() {
        return mCameraId;
    }

    public void setCameraId(int mCameraId) {
        this.mCameraId = mCameraId;
    }

    public IFilter getFilter() {
        return mFilter;
    }

    public void setFilter(IFilter filter) {
        mFilter = filter;
    }

    public VideoInfo(VideoType videoType) {
        mVideoType = videoType;
    }

    public String getAudioPath() {
        return mAudioPath;
    }

    public void setAudioPath(String audioPath) {
        mAudioPath = audioPath;
    }

    public String getVideoPath() {
        if (TextUtils.isEmpty(mVideoPath)) {
            mVideoPath = mOriginalVideoPath;
        }
        return mVideoPath;
    }

    public void setVideoPath(String videoPath) {
        mVideoPath = videoPath;
    }

    public String getImagePath() {
        if (TextUtils.isEmpty(mImagePath)) {
            mImagePath = mOriginalImagePath;
        }
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    public int getReplyToVideoId() {
        return mReplyToVideoId;
    }

    public void setReplyToVideoId(int replyToVideoId) {
        mReplyToVideoId = replyToVideoId;
    }

    public int getCategoryId() {
        return mCategoryId;
    }

    public void setCategoryId(int categoryId) {
        mCategoryId = categoryId;
    }

    public String getTags() {
        return mTags;
    }

    public void setTags(String tags) {
        mTags = tags;
    }

    public VideoType getVideoType() {
        return mVideoType;
    }

    public int getCost() {
        return mCost;
    }

    public void setCost(int cost) {
        mCost = cost;
    }

    public void setVideoType(VideoType videoType) {
        mVideoType = videoType;
    }

    public int getLocalId() {
        return mLocalId;
    }

    public void setIsAvatar(boolean isAvatar) {
        mIsAvatar = isAvatar;
    }

    public boolean isAvatar() {
        return mIsAvatar;
    }

    public void setLocalId(int localId) {
        mLocalId = localId;
    }

    public BigDecimal getDuration() {
        return mDuration;
    }

    public void setDuration(double duration) {
        mDuration = new BigDecimal(duration);
        mDuration = mDuration.setScale(1, BigDecimal.ROUND_HALF_UP);
    }

    public int getAudioId() {
        return mAudioId;
    }

    public void setAudioId(int audioId) {
        mAudioId = audioId;
    }

    public void detectAudioId() {
        if (getAudioPath() != null) {
            String[] audioFiles = FilePathHelper.getAudioDirectory().list();
            for (int i = 0; i < audioFiles.length; i++) {
                if (audioFiles[i].contains(getAudioPath())) {
                    setAudioId(i);
                    break;
                }
            }
        }
    }

    /**
     * Call this method to define duration of video
     */
    public void detectVideoDuration() {
        try {
            MediaPlayer mp = new MediaPlayer();
            mp.setDataSource(getVideoPath());
            mp.prepare();
            setDuration((float) mp.getDuration() / 1000f);
            mp.release();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public Category getCategory() {
        return mCategory;
    }

    public void setCategory(Category category) {
        mCategory = category;
    }

    public boolean isReply() {
        return getReplyToVideoId() > 0;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public String getUploadedVideoPath() {
        return mUploadedVideoPath;
    }

    public void setUploadedVideoPath(String uploadedVideoPath) {
        mUploadedVideoPath = uploadedVideoPath;
    }

    public String getUploadedImagePath() {
        return mUploadedImagePath;
    }

    public void setUploadedImagePath(String uploadedImagePath) {
        mUploadedImagePath = uploadedImagePath;
    }

    public void setSizes(int width, int height) {
        mWidth = width;
        mHeight = height;
    }

    public static VideoInfo create(Video video) {
        VideoType videoType = null;
        if (video.getType() > 0) {
            videoType = VideoType.getById(video.getType());
        }

        final VideoInfo info = new VideoInfo(videoType);
        File videoPath = new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.VIDEO_EXTENTION);
        info.setVideoPath(videoPath.getPath());
        info.setImagePath(video.getThumbUrl());

        info.setCategoryId(video.getCategoryId());
        info.setCost(video.getCost());
        info.setReplyToVideoId(video.getReplyId());
        info.setTags(video.getTitle());

        info.detectAudioId();
        info.detectVideoDuration();
        return info;
    }
}
