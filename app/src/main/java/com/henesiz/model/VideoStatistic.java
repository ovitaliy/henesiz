package com.henesiz.model;

import com.henesiz.R;
import com.henesiz.enums.VideoType;

public class VideoStatistic {
    private int mImageResId;
    private int mTitleResId;
    private int mId;
    private float mDuration;

    public VideoStatistic(int videoTypeCode, float duration) {
        mDuration = duration;
        mId = videoTypeCode;
        VideoType type = VideoType.getByCode(videoTypeCode);
        if (type != null) {
            switch (type) {
                case VLOG:
                    mImageResId = R.drawable.activity;
                    mTitleResId = R.string.imon_tab_vlog;
                    break;
                case CROWD_ASK:
                case CROWD_GIVE:
                    mImageResId = R.drawable.crowd;
                    mTitleResId = R.string.menu_crowd;
                    break;
                case NEWS:
                    mImageResId = R.drawable.interest_news;
                    mTitleResId = R.string.interest_news;
                    break;
                case DIY_GET_ITEM:
                case DIY_GET_SKILL:
                case DIY_GIVE_ITEM:
                case DIY_GIVE_SKILL:
                    mImageResId = R.drawable.diy;
                    mTitleResId = R.string.video_type_diy;
                    break;
                case ALIEN_LOOK_TASK:
                case ALIEN_LOOK_CHALLENGE:
                    mImageResId = R.drawable.alien_look;
                    mTitleResId = R.string.menu_alien_look;
                    break;
            }
        }
    }

    public int getImageResId() {
        return mImageResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getId() {
        return mId;
    }

    public float getDuration() {
        return mDuration;
    }

    public void setDuration(float duration) {
        mDuration = duration;
    }
}
