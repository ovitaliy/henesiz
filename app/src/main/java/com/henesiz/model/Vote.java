package com.henesiz.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;

import com.henesiz.AppUser;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.Language;
import com.henesiz.enums.VoteListStatus;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Denis on 28.04.2015.
 */
public class Vote {
    @SerializedName("id")
    private int mId;

    @SerializedName("title_rus")
    private String mTitleRus;

    @SerializedName("title_esp")
    private String mTitleEsp;

    @SerializedName("title_eng")
    private String mTitleEng;

    @SerializedName("description_rus")
    private String mDescrRus;

    @SerializedName("description_esp")
    private String mDescrEsp;

    @SerializedName("description_eng")
    private String mDescrEng;

    private int mTotalVotes;

    @SerializedName("status")
    private VoteListStatus mStatus;

    @SerializedName("my_vote")
    private Integer mMyVote;

    private String mTitle;

    @SerializedName("description")
    private String mDescr;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues(12);
        if (TextUtils.isEmpty(mDescrEng) && TextUtils.isEmpty(mDescrEsp) && TextUtils.isEmpty(mDescrEsp)) {
            mDescrEng = mDescr;
            mDescrEsp = mDescr;
            mDescrRus = mDescr;
        }
        values.put(ContentDescriptor.Votes.Cols.ID, mId);
        values.put(ContentDescriptor.Votes.Cols.TITLE_RUS, mTitleRus);
        values.put(ContentDescriptor.Votes.Cols.TITLE_ENG, mTitleEng);
        values.put(ContentDescriptor.Votes.Cols.TITLE_ESP, mTitleEsp);
        values.put(ContentDescriptor.Votes.Cols.DESCR_ENG, mDescrEng);
        values.put(ContentDescriptor.Votes.Cols.DESCR_ESP, mDescrEsp);
        values.put(ContentDescriptor.Votes.Cols.DESCR_RUS, mDescrRus);
        values.put(ContentDescriptor.Votes.Cols.STATUS, mStatus == null ? -1 : mStatus.getId());
        values.put(ContentDescriptor.Votes.Cols.MY_VOTE, mMyVote);
        values.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, mTotalVotes);
        return values;
    }

    public static Vote fromCursor(Cursor cursor) {
        Vote vote = new Vote();
        vote.setId(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.ID)));
        int status = cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.STATUS));
        if (status != -1) {
            vote.setStatus(VoteListStatus.getById(status));
        }
        vote.mTitleEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_ENG));
        vote.mDescrEng = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_ENG));

        vote.mTitleRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_RUS));
        vote.mDescrRus = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_RUS));

        vote.mTitleEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.TITLE_ESP));
        vote.mDescrEsp = cursor.getString(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.DESCR_ESP));

        vote.setMyVote(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.MY_VOTE)));
        vote.setTotalVotes(cursor.getInt(cursor.getColumnIndex(ContentDescriptor.Votes.Cols.VOTES_TOTAL)));
        return vote;
    }

    public void setStatus(VoteListStatus status) {
        mStatus = status;
    }

    public String getTitle() {
        if (TextUtils.isEmpty(mTitle)) {
            switch (Language.getSystem()) {
                case ENG:
                    mTitle = mTitleEng;
                    break;
                case ESP:
                    mTitle = mTitleEsp;
                    break;
                case RUS:
                    mTitle = mTitleRus;
                    break;
            }
            if (TextUtils.isEmpty(mTitle)) {
                if (!TextUtils.isEmpty(mTitleEng)) {
                    mTitle = mTitleEng;
                } else if (!TextUtils.isEmpty(mTitleEsp)) {
                    mTitle = mTitleEsp;
                } else if (!TextUtils.isEmpty(mTitleRus)) {
                    mTitle = mTitleRus;
                }
            }
        }
        return mTitle;
    }

    public String getDescr() {
        if (TextUtils.isEmpty(mDescr)) {
            if (!TextUtils.isEmpty(mDescrEsp))
                mDescr = mDescrEsp;
            else if (!TextUtils.isEmpty(mDescrRus))
                mDescr = mDescrRus;
            else
                mDescr = mDescrEng;
        }
        return mDescr;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Boolean isVoted() {
        return mMyVote != null && mMyVote != 0;
    }

    public VoteListStatus getStatus() {
        return mStatus;
    }

    public void setDescr(String descr) {
        mDescr = descr;
    }

    public int getMyVote() {
        return mMyVote;
    }

    public void setMyVote(int myVote) {
        mMyVote = myVote;
    }

    public void setTotalVotes(int totalVotes) {
        mTotalVotes = totalVotes;
    }

    public int getTotalVotes() {
        return mTotalVotes;
    }
}
