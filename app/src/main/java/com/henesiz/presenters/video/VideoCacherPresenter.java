package com.henesiz.presenters.video;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.henesiz.Const;
import com.henesiz.rest.listener.SimpleRequestListener;
import com.henesiz.rest.model.FlowResponse;
import com.henesiz.rest.request.video.VideoPreloadRequest;
import com.henesiz.views.IVideoCacheView;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;

import org.apache.commons.io.FilenameUtils;

/**
 * Created by ovi on 16.05.2016.
 */
public class VideoCacherPresenter {

    private SpiceManager mSpiceManager;

    private VideoPreloadRequest mVideoPreloadTask;

    private IVideoCacheView mVideoCacheView;

    private String mCurrentCachingUrls[];

    public void setSpiceManager(SpiceManager spiceManager) {
        mSpiceManager = spiceManager;
    }

    public void setVideoCacheView(IVideoCacheView videoCacheView) {
        mVideoCacheView = videoCacheView;
    }

    public void runCacher(String... urls) {
        if (!urlsAreEqual(mCurrentCachingUrls, urls)) {
            stopCache();
            mVideoPreloadTask = new VideoPreloadRequest(urls);
            mSpiceManager.execute(mVideoPreloadTask, urlsToKey(urls), DurationInMillis.ONE_SECOND, new PreloadRequestListener(mVideoCacheView));
            mCurrentCachingUrls = urls;
        }
    }

    public void stopCache() {
        if (mVideoPreloadTask != null) {
            mVideoPreloadTask.cancel();
            mVideoPreloadTask = null;
        }
    }

    private static class PreloadRequestListener extends SimpleRequestListener<String[]> implements RequestProgressListener {
        private IVideoCacheView mVideoCacheView;

        public PreloadRequestListener(IVideoCacheView videoCacheView) {
            mVideoCacheView = videoCacheView;
        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            if (mVideoCacheView != null)
                mVideoCacheView.publishCachingProgress(progress.getProgress());
        }

        @Override
        public void onRequestSuccess(String[] strings) {
            if (mVideoCacheView != null)
                mVideoCacheView.publishCachingComplete(true);
        }
    }


    private boolean urlsAreEqual(@Nullable String[] oldUrls, @Nullable String[] newNewUrls) {
        if (oldUrls == null)
            return false;
        if (newNewUrls == null)
            return false;

        if (oldUrls.length != newNewUrls.length)
            return false;

        for (int i = 0; i < oldUrls.length; i++) {
            if (!oldUrls[i].equals(newNewUrls[i]))
                return false;
        }
        return true;
    }

    private static String urlsToKey(String[] urls) {
        String fileNames[] = new String[urls.length];
        for (int i = 0; i< urls.length; i++){
            fileNames[i] = FilenameUtils.getBaseName(urls[i]);
        }
        return TextUtils.join("_", fileNames);
    }

    @Override
    protected void finalize() throws Throwable {
        stopCache();
        super.finalize();
    }
}
