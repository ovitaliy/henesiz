package com.henesiz.presenters.video;

import com.henesiz.Const;
import com.henesiz.enums.VideoType;
import com.henesiz.rest.listener.SimpleRequestListener;
import com.henesiz.rest.model.FlowResponse;
import com.henesiz.rest.request.video.GetFlowRequest;
import com.henesiz.rest.request.video.VideoPreloadRequest;
import com.henesiz.views.IVideoListView;
import com.octo.android.robospice.SpiceManager;

import java.util.HashMap;

/**
 * Created by ovi on 16.05.2016.
 */
public class VideoListLoaderPresenter {

    private SpiceManager mSpiceManager;

    private IVideoListView mVideoListView;

    private boolean mIsLastPage = false;

    private VideoType mVideoType;
    private int mReplayId;
    private int mUserId;
    private HashMap<String, Object> mFlowRequestParams;


    public void setSpiceManager(SpiceManager spiceManager) {
        mSpiceManager = spiceManager;
        spiceManager.cancel(String[].class, "VideoPreloadRequest");
    }

    public void setVideoListView(IVideoListView videoListView) {
        mVideoListView = videoListView;
    }

    public void setVideoType(VideoType videoType) {
        mVideoType = videoType;
    }

    public void setReplayId(int replayId) {
        mReplayId = replayId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public void setFlowRequestParams(HashMap<String, Object> flowRequestParams) {
        mFlowRequestParams = flowRequestParams;
    }


    public void loadPage() {
        loadPage(mVideoListView.getLastVideoId());
    }
    public void loadPage(long lastId) {
        GetFlowRequest request = new GetFlowRequest(mFlowRequestParams);
        request.saveData(mVideoListView.getScreenId(), lastId > 0);
        request.setLast(lastId);
        request.setReplyId(mReplayId);
        request.setUserId(mUserId);

        mSpiceManager.execute(request, new GetFlowRequestListener());
    }

    private class GetFlowRequestListener extends SimpleRequestListener<FlowResponse> {
        @Override
        public void onRequestSuccess(FlowResponse response) {
            int count = response.getFlow() == null ? 0 : response.getFlow().size();

            mIsLastPage = count < Const.LIMIT;

            if (response.getFlow() != null && mVideoListView != null)
                mVideoListView.addVideos(response.getFlow());
        }
    }


}
