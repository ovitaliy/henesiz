package com.henesiz.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.henesiz.util.NetworkUtil;
import com.henesiz.util.VideoUploadHelper;

public class InternetReceiver extends BroadcastReceiver {
    public InternetReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean offlineMode = NetworkUtil.isOfflineMode();

        if (!offlineMode) {
            VideoUploadHelper.uploadPendingVideos();
        }
    }
}
