package com.henesiz.rest;

import com.google.gson.Gson;
import com.henesiz.Const;
import com.henesiz.enums.Language;
import com.henesiz.util.GsonHelper;
import com.henesiz.util.PrefHelper;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Uses as parameters container for server requests
 */
public class ArgsMap extends HashMap<String, Object> {

    public ArgsMap() {
        this(true);
    }

    public ArgsMap(boolean withAuthToken) {
        put("lang", Language.getSystem().getRequestParam());
        if (withAuthToken) {
            put("auth_token", PrefHelper.getStringPref(Const.PREF_TOKEN));
        }
    }

    public String put(String key, int value) {
        return (String) super.put(key, String.valueOf(value));
    }

    public String put(String key, long value) {
        return (String)super.put(key, String.valueOf(value));
    }


    public <T>T get(String key){
        return (T)super.get(key);
    }




    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
