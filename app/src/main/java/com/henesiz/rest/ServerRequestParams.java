package com.henesiz.rest;

import java.io.Serializable;

/**
 * Created by Denis on 28.04.2015.
 * <p/>
 * Indicates that object can be used as server request param
 */
public interface ServerRequestParams extends Serializable{
    String getRequestParam();
}
