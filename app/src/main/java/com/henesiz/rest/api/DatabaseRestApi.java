package com.henesiz.rest.api;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.model.CitiesListResponse;
import com.henesiz.rest.model.CountriesListResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface DatabaseRestApi {
    @POST("/database.getCountries")
    CountriesListResponse getCountries(@Body ArgsMap options);

    @POST("/database.getCities")
    CitiesListResponse getCities(@Body ArgsMap options);
}
