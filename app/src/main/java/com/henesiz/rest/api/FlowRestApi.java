package com.henesiz.rest.api;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.model.CategoriesListResponse;
import com.henesiz.rest.model.FlowResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface FlowRestApi {

    @POST("/flow.get")
    FlowResponse getFlow(@Body ArgsMap options);

    @POST("/flow.getCategories")
    CategoriesListResponse getCategories(@Body ArgsMap options);
}