package com.henesiz.rest.api;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.model.AddMessageResponse;
import com.henesiz.rest.model.MessageListsResponse;
import com.henesiz.rest.model.MessagesResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public interface MessageRestApi {

    @POST("/message.get")
    MessagesResponse getMessages(@Body ArgsMap options);

    @POST("/message.getList")
    MessageListsResponse getMessageLists(@Body ArgsMap options);

    @POST("/message.add")
    AddMessageResponse addMessage(@Body ArgsMap options);
}