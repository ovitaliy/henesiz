package com.henesiz.rest.api;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.model.AuthResponse;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.model.CoinsHistoryListResponse;
import com.henesiz.rest.model.UserInfoResponse;
import com.henesiz.rest.model.UserRatingResponse;
import com.henesiz.rest.model.UserSearchResponse;
import com.henesiz.rest.model.VerifyCodeResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface UserRestApi {

    @POST("/user.add")
    AuthResponse auth(@Body ArgsMap options);

    @POST("/user.get")
    UserInfoResponse getUserInfo(@Body ArgsMap options);

    @POST("/user.edit")
    BaseResponse editUserInfo(@Body ArgsMap options);

    @POST("/user.token")
    BaseResponse sendToken(@Body ArgsMap options);

    @POST("/user.follow")
    BaseResponse followUser(@Body ArgsMap options);

    @POST("/user.unfollow")
    BaseResponse unfollowUser(@Body ArgsMap options);

    @POST("/user.coinsHistory")
    CoinsHistoryListResponse getCoinsHistory(@Body ArgsMap options);

    @POST("/user.search")
    UserSearchResponse searchUser(@Body ArgsMap options);

    @POST("/user.getCode")
    BaseResponse getSmsCode(@Body ArgsMap options);

    @POST("/user.verify")
    VerifyCodeResponse verifySmsCode(@Body ArgsMap options);

    @POST("/user.rating")
    UserRatingResponse getRating(@Body Map options);
}
