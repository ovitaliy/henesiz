package com.henesiz.rest.api;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.model.AddVideoResponse;
import com.henesiz.rest.model.BaseCoinsResponse;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.model.CommentsResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 14.09.15.
 */
public interface VideoRestApi {

    @POST("/video.add")
    AddVideoResponse addVideo(@Body ArgsMap options);

    @POST("/video.share")
    BaseResponse shareVideo(@Body ArgsMap options);

    @POST("/video.complaint")
    BaseResponse complaintVideo(@Body ArgsMap options);

    @POST("/video.guess")
    BaseResponse guessVideoSmile(@Body ArgsMap options);

    @POST("/video.pay")
    BaseCoinsResponse payForVideo(@Body ArgsMap options);

    @POST("/video.mark")
    BaseResponse markVideo(@Body ArgsMap options);

    @POST("/video.update")
    BaseResponse updateVideo(@Body ArgsMap options);

    @POST("/video.getComments")
    CommentsResponse getComments(@Body ArgsMap options);

    @POST("/video.comment")
    BaseResponse commentVideo(@Body ArgsMap options);
}