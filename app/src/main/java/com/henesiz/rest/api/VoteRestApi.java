package com.henesiz.rest.api;

import com.henesiz.model.TypedJsonString;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.model.AddVoteResponse;
import com.henesiz.rest.model.BaseResponse;
import com.henesiz.rest.model.VoteListsResponse;
import com.henesiz.rest.model.VoteResponse;

import java.util.Map;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public interface VoteRestApi {

    @POST("/vote.add")
    AddVoteResponse addVote(@Body TypedJsonString options);

    @POST("/vote.vote")
    BaseResponse sendVote(@Body ArgsMap options);

    @POST("/vote.getList")
    VoteListsResponse getVoteList(@Body ArgsMap options);

    @POST("/vote.my")
    VoteListsResponse getMyList(@Body ArgsMap options);

    @POST("/vote.get")
    VoteResponse getVote(@Body ArgsMap options);
}