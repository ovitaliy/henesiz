package com.henesiz.rest.listener;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.VideoInfo;
import com.henesiz.rest.model.AddVideoResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddVideoRequestListener extends BaseRequestListener<AddVideoResponse> {
    public static final String TAG = "AddVideoListener";
    OnAddedVideoListener mListener;
    VideoInfo mVideoInfo;
    Boolean mClearCache;

    public AddVideoRequestListener(VideoInfo videoInfo, OnAddedVideoListener listener) {
        mListener = listener;
        mVideoInfo = videoInfo;
    }

    public AddVideoRequestListener(VideoInfo videoInfo, OnAddedVideoListener listener, boolean clearCache) {
        mListener = listener;
        mVideoInfo = videoInfo;
        mClearCache = clearCache;
    }

    public AddVideoRequestListener(VideoInfo videoInfo) {
        mVideoInfo = videoInfo;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final AddVideoResponse response) {
        super.onRequestSuccess(response);
        if (response != null) {
            if (response.isSuccess()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Context context = App.getInstance();
                        int videoId = response.getVideoId();
                        int localId = mVideoInfo.getLocalId();

                        context.getContentResolver().delete(ContentDescriptor.Videos.URI,
                                "_id =" + localId, null);

                        if (mVideoInfo.getReplyToVideoId() > 0) {
                            Cursor cursor = context.getContentResolver().query(ContentDescriptor.Videos.URI,
                                    new String[]{ContentDescriptor.Videos.Cols.REPLIES},
                                    ContentDescriptor.Videos.Cols.ID + " = " + mVideoInfo.getReplyToVideoId(),
                                    null,
                                    null);

                            int repliesCount = 1;//new reply we just added
                            try {
                                if (cursor.moveToFirst()) {
                                    repliesCount += cursor.getInt(0);
                                }
                            } finally {
                                cursor.close();
                            }

                            ContentValues values = new ContentValues(1);
                            values.put(ContentDescriptor.Videos.Cols.REPLIES, repliesCount);
                            context.getContentResolver().update(ContentDescriptor.Videos.URI, values, ContentDescriptor.Videos.Cols.ID + " = " + mVideoInfo.getReplyToVideoId(), null);
                        }

                        if (mListener != null) {
                            mListener.onAddedVideo(videoId, response.getShortUrl());
                        }
                    }
                }).start();
            }
        }
    }

    public interface OnAddedVideoListener {
        void onAddedVideo(int videoId, String shortUrl);
    }
}
