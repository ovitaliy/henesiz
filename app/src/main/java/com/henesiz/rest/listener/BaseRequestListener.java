package com.henesiz.rest.listener;

import android.support.annotation.CallSuper;
import android.util.Log;

import com.henesiz.enums.ServerError;
import com.henesiz.events.ServerErrorEvent;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.exception.NetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import org.greenrobot.eventbus.EventBus;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by denisvasilenko on 17.09.15.
 */
public class BaseRequestListener<T extends BaseResponse> implements RequestListener<T> {
    @Override
    @CallSuper
    public void onRequestFailure(SpiceException spiceException) {
        String message;
        if (spiceException != null) {
            message = spiceException.getMessage();
        } else {
            message = "";
        }
        if (spiceException instanceof NetworkException) {
            NetworkException exception = (NetworkException) spiceException;
            if (exception.getCause() instanceof RetrofitError) {
                RetrofitError error = (RetrofitError) exception.getCause();
                Response response = error.getResponse();
                if (response != null) {
                    int httpErrorCode = response.getStatus();
                    if (httpErrorCode == 401) {
                        EventBus.getDefault().post(new ServerErrorEvent(new int[]{ServerError.WRONG_ACCESS_TOKEN.getCode()}, null));
                    }
                }
                return;
            }
        }
        /*if (spiceException instanceof NoNetworkException) {
            EventBus.getDefault().post(new ServerErrorEvent(new int[]{ServerError.NO_NETWORK.getCode()}, new String[]{ServerError.NO_NETWORK.getMessage()}));
        }*/
        Log.e("SERVER", "Request failure: " + message);
        onComplete();
    }

    @Override
    @CallSuper
    public void onRequestSuccess(T response) {
        Log.v("SERVER", "Success");
        if (response != null && response.isError()) {
            EventBus.getDefault().postSticky(new ServerErrorEvent(response.getErrorCodes(), response.getErrorMessages()));
        }
        onComplete();
    }

    public void onComplete(){}

}
