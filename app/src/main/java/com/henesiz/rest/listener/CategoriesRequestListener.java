package com.henesiz.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.model.Category;
import com.henesiz.rest.model.CategoriesListResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class CategoriesRequestListener extends BaseRequestListener<CategoriesListResponse> {
    public static final String TAG = "CategoriesListener";

    private VideoType mVideoType;

    public CategoriesRequestListener(VideoType videoType) {
        mVideoType = videoType;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final CategoriesListResponse response) {
        super.onRequestSuccess(response);
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<Category> categoryList = response.getCategories();

                ArrayList<ContentValues> contentValueses = new ArrayList<>();

                if (categoryList != null) {
                    for (Category category : categoryList) {
                        ContentValues values = category.toContentValues();
                        values.put(ContentDescriptor.Categories.Cols.VIDEO_TYPE, mVideoType.getId());
                        contentValueses.add(values);
                    }

                    ContentValues[] contentValuesToInsert = new ContentValues[contentValueses.size()];
                    contentValueses.toArray(contentValuesToInsert);

                    String deleteSelect = ContentDescriptor.Categories.Cols.VIDEO_TYPE + " = " + mVideoType.getId();

                    App.getInstance().getContentResolver().delete(ContentDescriptor.Categories.URI, deleteSelect, null);
                    App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Categories.URI, contentValuesToInsert);
                }
            }
        }).start();
    }
}
