package com.henesiz.rest.listener;

import android.content.ContentValues;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.Country;
import com.henesiz.rest.model.CountriesListResponse;
import com.henesiz.util.LocationHelper;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class CountriesRequestListener extends BaseRequestListener<CountriesListResponse> {
    public static final String TAG = "CountriesListener";

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final CountriesListResponse response) {
        super.onRequestSuccess(response);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (response != null) {
                    List<Country> countries = response.getCountries();
                    LinkedHashMap<Integer, Country> countriesMap = new LinkedHashMap<>();
                    if (countries != null) {
                        int count = countries.size();
                        ContentValues[] contentValues = new ContentValues[count];
                        for (int i = 0; i < count; i++) {
                            Country country = countries.get(i);
                            countriesMap.put(country.getId(), country);
                            contentValues[i] = country.toContentValues();
                        }

                        App.getInstance().getContentResolver().delete(ContentDescriptor.Countries.URI,
                                null,
                                null);

                        App.getInstance().getContentResolver().bulkInsert(ContentDescriptor.Countries.URI,
                                contentValues);

                        LocationHelper.update(countriesMap);
                    }
                }
            }
        }).start();
    }
}
