package com.henesiz.rest.listener;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.Video;
import com.henesiz.rest.model.FlowResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

import java.util.ArrayList;

/**
 * Created by denisvasilenko on 16.09.15.
 */
@Deprecated
public class FlowRequestListener extends BaseRequestListener<FlowResponse> {
    public static final String TAG = "FlowListener";
    private long mScreen;
    private boolean mPagination;
    private OnGetFlowListener mListener;

    public FlowRequestListener(long screen, boolean isPagination, OnGetFlowListener listener) {
        mScreen = screen;
        mPagination = isPagination;
        mListener = listener;
    }

    public FlowRequestListener(int screen, boolean isPagination) {
        mScreen = screen;
        mPagination = isPagination;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(final FlowResponse response) {
        super.onRequestSuccess(response);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (response != null) {
                    Context context = App.getInstance();
                    if (!mPagination) {
                        context.getContentResolver().delete(
                                ContentDescriptor.Videos.URI,
                                ContentDescriptor.Videos.Cols.SCREEN + " = " + mScreen,
                                null);
                    }

                    ArrayList<Video> list = response.getFlow();

                    if (list != null) {
                        int count = list.size();

                        final ContentResolver contentResolver = context.getContentResolver();

                        ContentValues[] contentValueses = new ContentValues[count];
                        for (int i = 0; i < count; i++) {
                            Video video = list.get(i);
                            ContentValues values = video.toContentValues();
                            values.put(ContentDescriptor.Videos.Cols.SCREEN, mScreen);
                            contentValueses[i] = values;
                        }
                        contentResolver.bulkInsert(ContentDescriptor.Videos.URI, contentValueses);

                        if (mListener != null) {
                            mListener.onGetFlow(list);
                        }
                    }
                }
            }
        }).start();
    }

    public interface OnGetFlowListener {
        void onGetFlow(ArrayList<Video> list);
    }
}
