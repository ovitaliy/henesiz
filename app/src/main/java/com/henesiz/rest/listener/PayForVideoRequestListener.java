package com.henesiz.rest.listener;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.rest.model.BaseCoinsResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class PayForVideoRequestListener extends BaseRequestListener<BaseCoinsResponse> {
    public static final String TAG = "PayVideoListener";
    private int mVideoId;
    private VideoType mVideoType;

    public PayForVideoRequestListener(VideoType videoType, int videoId) {
        mVideoId = videoId;
        mVideoType = videoType;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(BaseCoinsResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {

            ContentResolver resolver = App.getInstance().getContentResolver();
            int earn = 0;//already earn sum
            Cursor cursor = resolver.query(ContentDescriptor.Videos.URI, new String[]{ContentDescriptor.Videos.Cols.EARN}, ContentDescriptor.Videos.Cols.ID + "=" + mVideoId, null, null);
            try {
                if (cursor.moveToFirst()) {
                    earn += cursor.getInt(0);
                }
            } finally {
                cursor.close();
            }

            int amount = response.getCoinsChange();
            earn += Math.abs(amount);

            ContentValues values = new ContentValues(2);
            values.put(ContentDescriptor.Videos.Cols.EARN, earn);
            values.put(ContentDescriptor.Videos.Cols.AVAILABLE, 0);
            resolver.update(ContentDescriptor.Videos.URI,
                    values,
                    ContentDescriptor.Videos.Cols.ID + " = " + mVideoId,
                    null);
        }
    }
}
