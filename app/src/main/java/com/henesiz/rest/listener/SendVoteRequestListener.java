package com.henesiz.rest.listener;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class SendVoteRequestListener extends BaseRequestListener<BaseResponse> {
    public static final String TAG = "SendVoteListener";
    int mVoteId;
    int mOptionId;

    public SendVoteRequestListener(int voteId, int optionId) {
        mVoteId = voteId;
        mOptionId = optionId;
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(BaseResponse response) {
        super.onRequestSuccess(response);
        if (response != null && response.isSuccess()) {

            Cursor cursor = App.getInstance().getContentResolver().query(ContentDescriptor.Votes.URI,
                    new String[]{ContentDescriptor.Votes.Cols.VOTES_TOTAL},
                    ContentDescriptor.Votes.Cols.ID + " = " + mVoteId,
                    null,
                    null);

            int curTotalVotes = 0;
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    curTotalVotes = cursor.getInt(0);
                }
            } finally {
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }

            ContentValues values = new ContentValues(2);
            values.put(ContentDescriptor.Votes.Cols.MY_VOTE, mOptionId);
            values.put(ContentDescriptor.Votes.Cols.VOTES_TOTAL, curTotalVotes);

            //Update vote info
            App.getInstance().getContentResolver().update(ContentDescriptor.Votes.URI,
                    values,
                    ContentDescriptor.Votes.Cols.ID + " = " + mVoteId,
                    null);

            cursor = App.getInstance().getContentResolver().query(ContentDescriptor.VoteOptions.URI,
                    new String[]{ContentDescriptor.VoteOptions.Cols.VOTES},
                    ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId + " AND " + ContentDescriptor.VoteOptions.Cols.OPTION_ID + " = " + mOptionId,
                    null,
                    null);

            int curVotes = 0;
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    curVotes = cursor.getInt(0);
                }
            } finally {
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }

            values.clear();
            values.put(ContentDescriptor.VoteOptions.Cols.VOTES, ++curVotes);

            //Update vote option info
            App.getInstance().getContentResolver().update(ContentDescriptor.VoteOptions.URI,
                    values,
                    ContentDescriptor.VoteOptions.Cols.VOTE_ID + " = " + mVoteId + " AND " + ContentDescriptor.VoteOptions.Cols.OPTION_ID + " = " + mOptionId,
                    null);

        }
    }
}
