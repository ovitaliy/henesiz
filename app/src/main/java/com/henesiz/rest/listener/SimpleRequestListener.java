package com.henesiz.rest.listener;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

/**
 * Created by ovitali on 13.10.2015.
 */
public class SimpleRequestListener<RESULT> implements RequestListener<RESULT> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        if (spiceException != null)
            Log.e("SimpleRequestListener", "spiceException", spiceException);
    }

    @Override
    public void onRequestSuccess(RESULT result) {
        if (result != null)
            Log.e("SimpleRequestListener", result.toString());
    }
}
