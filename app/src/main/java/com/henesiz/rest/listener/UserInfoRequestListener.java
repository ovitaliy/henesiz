package com.henesiz.rest.listener;

import android.util.Log;

import com.henesiz.AppUser;
import com.henesiz.model.UserInfo;
import com.henesiz.rest.model.UserInfoResponse;
import com.octo.android.robospice.persistence.exception.SpiceException;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class UserInfoRequestListener extends BaseRequestListener<UserInfoResponse> {
    public static final String TAG = "UserInfoListener";
    OnGetUserInfoListener mListener;
    private int mUserId;

    public UserInfoRequestListener(int userId, OnGetUserInfoListener listener) {
        mListener = listener;
        mUserId = userId;
    }

    public UserInfoRequestListener(OnGetUserInfoListener listener) {
        mListener = listener;
        mUserId = 0;
    }

    public UserInfoRequestListener() {
    }

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        super.onRequestFailure(spiceException);
        Log.e(TAG, spiceException.toString());
    }

    @Override
    public void onRequestSuccess(UserInfoResponse response) {
        super.onRequestSuccess(response);
        if (response != null) {
            UserInfo userInfo = response.getUser();

            if (userInfo != null) {
                if (mUserId == 0 || AppUser.get() == null || AppUser.getUid() == userInfo.getId()) {
                    AppUser.set(userInfo);
                }

                if (mListener != null) {
                    mListener.onGetUserInfo(userInfo);
                }
            }
        }
    }

    public interface OnGetUserInfoListener {
        void onGetUserInfo(UserInfo userInfo);
    }
}
