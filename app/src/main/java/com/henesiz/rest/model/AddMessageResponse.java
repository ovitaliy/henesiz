package com.henesiz.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddMessageResponse extends BaseResponse {
    @SerializedName("id_list")
    int mListId;

    public int getListId() {
        return mListId;
    }
}