package com.henesiz.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddVideoResponse extends BaseResponse {
    @SerializedName("id_video")
    int mVideoId;

    @SerializedName("short_url")
    String mShortUrl;

    public String getShortUrl() {
        return mShortUrl;
    }

    public int getVideoId() {
        return mVideoId;
    }
}