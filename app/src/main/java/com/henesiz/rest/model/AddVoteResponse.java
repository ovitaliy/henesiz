package com.henesiz.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddVoteResponse extends BaseResponse {
    @SerializedName("id")
    int mId;

    public int getId() {
        return mId;
    }
}