package com.henesiz.rest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class BaseResponse {

    @SerializedName("error")
    int[] mErrorCodes;

    @SerializedName("err_msg")
    String[] mErrorMessages;

    public boolean isError() {
        return mErrorCodes != null && mErrorCodes.length > 0 && mErrorCodes[0] != 0;
    }

    public int[] getErrorCodes() {
        return mErrorCodes;
    }

    public String[] getErrorMessages() {
        return mErrorMessages;
    }

    public boolean isSuccess() {
        return (mErrorCodes == null || mErrorCodes.length == 0 || mErrorCodes[0] == 0);
    }

    public String getErrorsMessage() {
        StringBuilder builder = new StringBuilder();
        for (String msg : mErrorMessages) {
            builder.append(msg).append(" ");
        }
        return builder.toString();
    }
}