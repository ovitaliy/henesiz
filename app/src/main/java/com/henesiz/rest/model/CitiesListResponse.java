package com.henesiz.rest.model;

import com.henesiz.model.City;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class CitiesListResponse extends BaseResponse {
    @SerializedName("cities")
    List<City> mCities;

    @SerializedName("count")
    int mCount;

    public List<City> getCities() {
        return mCities;
    }

    public int getCount() {
        return mCount;
    }
}