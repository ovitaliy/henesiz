package com.henesiz.rest.model;

import com.henesiz.model.Comment;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class CommentsResponse extends BaseResponse {
    @SerializedName("comment")
    List<Comment> mComments;

    public List<Comment> getComments() {
        return mComments;
    }
}