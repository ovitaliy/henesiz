package com.henesiz.rest.model;

import com.henesiz.model.Country;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class CountriesListResponse extends BaseResponse {
    @SerializedName("countries")
    List<Country> mCountries;

    @SerializedName("count")
    int mCount;

    public List<Country> getCountries() {
        return mCountries;
    }

    public int getCount() {
        return mCount;
    }
}