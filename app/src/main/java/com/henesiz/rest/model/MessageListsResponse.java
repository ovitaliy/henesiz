package com.henesiz.rest.model;

import com.henesiz.model.Feedback;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class MessageListsResponse extends BaseResponse {
    @SerializedName("lists")
    List<Feedback> mLists;

    public List<Feedback> getLists() {
        return mLists;
    }
}