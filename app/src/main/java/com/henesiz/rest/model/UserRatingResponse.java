package com.henesiz.rest.model;

import com.google.gson.annotations.SerializedName;
import com.henesiz.model.UserInfo;

public class UserRatingResponse extends BaseResponse {
    @SerializedName("rating")
    int mRate;

    @SerializedName("my_percent")
    int mPosition;

    @SerializedName("users")
    UserInfo[] mTopUsers;

    public int getRating() {
        return mRate;
    }

    public int getPosition() {
        return mPosition;
    }

    public UserInfo[] getTopUsers() {
        return mTopUsers;
    }
}