package com.henesiz.rest.model;

import com.henesiz.model.UserFinded;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class UserSearchResponse extends BaseResponse {
    @SerializedName("users")
    List<UserFinded> mUsers;

    public List<UserFinded> getUsers() {
        return mUsers;
    }
}