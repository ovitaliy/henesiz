package com.henesiz.rest.model;

import com.henesiz.model.Vote;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class VoteListsResponse extends BaseResponse {
    @SerializedName("votes")
    List<Vote> mVotes;

    public List<Vote> getVotes() {
        return mVotes;
    }
}