package com.henesiz.rest.model;

import com.henesiz.model.VoteOption;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class VoteResponse extends BaseResponse {
    @SerializedName("vote")
    Vote mVote;

    public Vote getVote() {
        return mVote;
    }

    public class Vote {
        @SerializedName("description_rus")
        String mDescriptionRus;

        @SerializedName("description_eng")
        String mDescriptionEng;

        @SerializedName("description_esp")
        String mDescriptionEsp;

        @SerializedName("options")
        List<VoteOption> mOptions;

        @SerializedName("my_vote")
        int mMyVote;

        public List<VoteOption> getOptions() {
            return mOptions;
        }

        public String getDescriptionEng() {
            return mDescriptionEng;
        }

        public String getDescriptionEsp() {
            return mDescriptionEsp;
        }

        public String getDescriptionRus() {
            return mDescriptionRus;
        }

        public int getMyVote() {
            return mMyVote;
        }
    }
}