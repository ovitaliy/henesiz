package com.henesiz.rest.request;

import com.henesiz.rest.ServerRequestParams;

/**
 * Created by denisvasilenko on 01.10.15.
 */
public enum CategoriesRequestType implements ServerRequestParams {
    SHORT {
        @Override
        public String getRequestParam() {
            return "short";
        }
    }, FULL {
        @Override
        public String getRequestParam() {
            return "full";
        }
    };
}
