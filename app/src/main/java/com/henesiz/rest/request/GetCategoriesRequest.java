package com.henesiz.rest.request;

import android.text.TextUtils;

import com.henesiz.enums.VideoType;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.FlowRestApi;
import com.henesiz.rest.model.CategoriesListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class GetCategoriesRequest extends RetrofitSpiceRequest<CategoriesListResponse, FlowRestApi> {
    CategoriesRequestType mRequestType;
    VideoType mVideoType;

    public GetCategoriesRequest(VideoType videoType, CategoriesRequestType requestType) {
        super(CategoriesListResponse.class, FlowRestApi.class);
        mRequestType = requestType;
        mVideoType = videoType;
    }

    public GetCategoriesRequest(VideoType videoType) {
        super(CategoriesListResponse.class, FlowRestApi.class);
        mRequestType = CategoriesRequestType.FULL;
        mVideoType = videoType;
    }

    @Override
    public CategoriesListResponse loadDataFromNetwork() throws Exception {
        ArgsMap args = new ArgsMap(true);
        args.put("type", mRequestType.getRequestParam());
        args.put("video_type", mVideoType.getRequestParam());
        if (!TextUtils.isEmpty(mVideoType.getAdditionParam())) {
            args.put("params", mVideoType.getAdditionParam());
        }
        return getService().getCategories(args);
    }
}
