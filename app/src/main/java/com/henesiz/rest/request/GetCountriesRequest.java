package com.henesiz.rest.request;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.DatabaseRestApi;
import com.henesiz.rest.model.CountriesListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 15.09.15.
 */
public class GetCountriesRequest extends RetrofitSpiceRequest<CountriesListResponse, DatabaseRestApi> {

    public GetCountriesRequest() {
        super(CountriesListResponse.class, DatabaseRestApi.class);
    }

    @Override
    public CountriesListResponse loadDataFromNetwork() throws Exception {
        ArgsMap args = new ArgsMap(true);
        return getService().getCountries(args);
    }
}
