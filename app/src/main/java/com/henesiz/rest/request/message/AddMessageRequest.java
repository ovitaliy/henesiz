package com.henesiz.rest.request.message;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.MessageRestApi;
import com.henesiz.rest.model.AddMessageResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddMessageRequest extends RetrofitSpiceRequest<AddMessageResponse, MessageRestApi> {
    int mListId;
    String mMessage;
    int mType;

    public AddMessageRequest(int listId, String message, int type) {
        super(AddMessageResponse.class, MessageRestApi.class);
        mListId = listId;
        mMessage = message;
        mType = type;
    }

    @Override
    public AddMessageResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("type", mType);
        map.put("message", mMessage);
        if (mListId > 0) {
            map.put("id_list", mListId);
        }
        return getService().addMessage(map);
    }
}
