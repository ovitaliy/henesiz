package com.henesiz.rest.request.message;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.MessageRestApi;
import com.henesiz.rest.model.MessageListsResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMessageListsRequest extends RetrofitSpiceRequest<MessageListsResponse, MessageRestApi> {

    public GetMessageListsRequest() {
        super(MessageListsResponse.class, MessageRestApi.class);
    }

    @Override
    public MessageListsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        return getService().getMessageLists(map);
    }
}
