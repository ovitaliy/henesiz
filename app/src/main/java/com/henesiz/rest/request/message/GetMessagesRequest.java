package com.henesiz.rest.request.message;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.MessageRestApi;
import com.henesiz.rest.model.MessagesResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetMessagesRequest extends RetrofitSpiceRequest<MessagesResponse, MessageRestApi> {
    int mListId;

    public GetMessagesRequest(int listId) {
        super(MessagesResponse.class, MessageRestApi.class);
        mListId = listId;
    }

    @Override
    public MessagesResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_list", mListId);
        return getService().getMessages(map);
    }
}
