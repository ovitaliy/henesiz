package com.henesiz.rest.request.user;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.AuthResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class AuthRequest extends RetrofitSpiceRequest<AuthResponse, UserRestApi> {
    String mGcmToken;

    public AuthRequest(String gcmToken) {
        super(AuthResponse.class, UserRestApi.class);
        mGcmToken = gcmToken;
    }

    public AuthRequest() {
        super(AuthResponse.class, UserRestApi.class);
        mGcmToken = "";
    }

    @Override
    public AuthResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(false);
        map.put("token", mGcmToken);
        map.put("os", "android");
        return getService().auth(map);
    }
}
