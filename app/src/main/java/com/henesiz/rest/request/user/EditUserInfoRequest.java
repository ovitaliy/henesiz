package com.henesiz.rest.request.user;

import android.text.TextUtils;

import com.henesiz.model.UserInfo;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 17.09.15.
 */
public class EditUserInfoRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    UserInfo mUserInfo;

    public EditUserInfoRequest(UserInfo user) {
        super(BaseResponse.class, UserRestApi.class);
        this.mUserInfo = user;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);

        String photo = mUserInfo.getPhoto();
        if (!TextUtils.isEmpty(photo) && photo.contains("http")) {
            map.put("photo", mUserInfo.getPhoto());
        }

        map.put("first_name", mUserInfo.getFirstName());

        if (!TextUtils.isEmpty(mUserInfo.getPatronymic())) {
            map.put("patronymic", mUserInfo.getPatronymic());
        }

        if (!TextUtils.isEmpty(mUserInfo.getLastName())) {
            map.put("last_name", mUserInfo.getLastName());
        }

        map.put("lang", mUserInfo.getLanguage().getRequestParam());
        map.put("id_country", mUserInfo.getCountryId());
        map.put("id_city", mUserInfo.getCityId());
        map.put("gender", mUserInfo.getGender());
        map.put("birth_date", mUserInfo.getBirthDate());


        if (mUserInfo.getDegree() != null) {
            map.put("degree", mUserInfo.getDegree().getRequestParam());
        }

        if (mUserInfo.getJobType() != null) {
            map.put("job", mUserInfo.getJobType().getRequestParam());
        }

        if (mUserInfo.getHobbie() != null) {
            map.put("hobbie", mUserInfo.getHobbie().getRequestParam());
        }

        if (mUserInfo.getSport() != null) {
            map.put("sport", mUserInfo.getSport().getRequestParam());
        }

        if (mUserInfo.getPet() != null) {
            map.put("pets", mUserInfo.getPet().getRequestParam());
        }

        if (mUserInfo.getInterest() != null) {
            map.put("interest", mUserInfo.getInterest().getRequestParam());
        }

        if (mUserInfo.getReligion() != null) {
            map.put("religion", mUserInfo.getReligion().getRequestParam());
        }

        if (mUserInfo.getCraft() != null) {
            map.put("craft", mUserInfo.getCraft());
        }

        map.put("widget", mUserInfo.isWidget() ? 1 : 0);

        return getService().editUserInfo(map);
    }
}
