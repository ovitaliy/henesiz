package com.henesiz.rest.request.user;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class FollowUnfollowUserRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    boolean mFollow;
    int mUserId;

    public FollowUnfollowUserRequest(int userId, boolean follow) {
        super(BaseResponse.class, UserRestApi.class);
        mUserId = userId;
        mFollow = follow;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_user", mUserId);
        if (mFollow) {
            return getService().followUser(map);
        } else {
            return getService().unfollowUser(map);
        }
    }
}
