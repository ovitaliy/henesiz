package com.henesiz.rest.request.user;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.CoinsHistoryListResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class GetCoinsHistoryRequest extends RetrofitSpiceRequest<CoinsHistoryListResponse, UserRestApi> {
    private int mLastId;

    public GetCoinsHistoryRequest(int lastId) {
        super(CoinsHistoryListResponse.class, UserRestApi.class);
        mLastId = lastId;
    }

    public GetCoinsHistoryRequest() {
        super(CoinsHistoryListResponse.class, UserRestApi.class);
        mLastId = 0;
    }

    @Override
    public CoinsHistoryListResponse loadDataFromNetwork() throws Exception {
        ArgsMap args = new ArgsMap(true);
        if (mLastId != 0) {
            args.put("last", mLastId);
        }
        return getService().getCoinsHistory(args);
    }
}
