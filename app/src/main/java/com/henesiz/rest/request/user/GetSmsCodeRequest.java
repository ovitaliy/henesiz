package com.henesiz.rest.request.user;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetSmsCodeRequest extends RetrofitSpiceRequest<BaseResponse, UserRestApi> {
    String mPhone;

    public GetSmsCodeRequest(String phone) {
        super(BaseResponse.class, UserRestApi.class);
        mPhone = phone;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("phone", mPhone);
        return getService().getSmsCode(map);
    }
}
