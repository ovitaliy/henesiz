package com.henesiz.rest.request.user;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.UserInfoResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class GetUserInfoRequest extends RetrofitSpiceRequest<UserInfoResponse, UserRestApi> {
    int mUserId;

    public GetUserInfoRequest(int userId) {
        super(UserInfoResponse.class, UserRestApi.class);
        mUserId = userId;
    }

    public GetUserInfoRequest() {
        super(UserInfoResponse.class, UserRestApi.class);
    }

    @Override
    public UserInfoResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        if (mUserId != 0) {
            map.put("id_user", String.valueOf(mUserId));
        }
        return getService().getUserInfo(map);
    }
}
