package com.henesiz.rest.request.user;


import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.UserRatingResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by ovitali on 13.10.2015.
 */
public class RatingRequest extends RetrofitSpiceRequest<UserRatingResponse, UserRestApi> {
    private ArgsMap argsMap;

    public RatingRequest(boolean all) {
        super(UserRatingResponse.class, UserRestApi.class);
        argsMap = new ArgsMap(true);
        if (!all) {
            argsMap.put("type", "local");
        }
    }

    @Override
    public UserRatingResponse loadDataFromNetwork() throws Exception {
        return getService().getRating(argsMap);
    }
}
