package com.henesiz.rest.request.user;

import android.text.TextUtils;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.UserSearchResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class SearchUserRequest extends RetrofitSpiceRequest<UserSearchResponse, UserRestApi> {
    private ArgsMap mParams;

    public ArgsMap getParams() {
        return mParams;
    }

    public SearchUserRequest() {
        super(UserSearchResponse.class, UserRestApi.class);
        mParams = new ArgsMap(true);
    }

    public void setParams(ArgsMap params) {
        mParams = params;
    }

    public void setSmileId(int smileId) {
        mParams.put("id_smile", smileId);
    }

    public void setFilterId(int filterId) {
        mParams.put("id_filter", filterId);
    }

    public void setFirstName(String firstName) {
        if (!TextUtils.isEmpty(firstName)) {
            mParams.put("first_name", firstName);
        } else {
            mParams.remove("first_name");
        }
    }

    public void setLastName(String lastName) {
        if (!TextUtils.isEmpty(lastName)) {
            mParams.put("last_name", lastName);
        } else {
            mParams.remove("last_name");
        }
    }

    public void setCountryId(int id) {
        if (id > 0) {
            mParams.put("id_country", String.valueOf(id));
        } else {
            mParams.remove("id_country");
        }
    }

    public void setJob(String id) {
        mParams.put("job", id);
    }

    public void setEducation(String id) {
        mParams.put("degree", id);
    }

    public void setCityId(String id) {
        mParams.put("id_city", id);
    }

    public void setLang(String lang) {
        mParams.put("lang", lang);
    }

    public void setPhones(String phones[]) {
        StringBuilder toPhones = new StringBuilder();
        for (String item : phones) {
            toPhones.append(item);
            toPhones.append(",");
        }
        String result = "";
        if (toPhones.length() > 1) {
            result = toPhones.substring(0, toPhones.length() - 1);
        }
        mParams.put("phones", result);
    }

    public void setHobbie(int hobbie) {
        if (hobbie != -1) {
            mParams.put("hobbie", String.valueOf(hobbie));
        } else {
            mParams.remove("hobbie");
        }
    }

    public void setInterest(int interest) {
        if (interest != -1) {
            mParams.put("interest", String.valueOf(interest));
        } else {
            mParams.remove("interest");
        }
    }

    public void setSport(int sport) {
        if (sport != -1) {
            mParams.put("sport", String.valueOf(sport));
        } else {
            mParams.remove("sport");
        }
    }

    public void setPets(int pets) {
        if (pets != -1) {
            mParams.put("pets", String.valueOf(pets));
        } else {
            mParams.remove("pets");
        }
    }

    public void setReligion(int religion) {
        if (religion != -1) {
            mParams.put("religion", String.valueOf(religion));
        } else {
            mParams.remove("religion");
        }
    }

    public void setVideoIdCountry(int videoIdCountry) {
        if (videoIdCountry > 0) {
            mParams.put("video_id_country", String.valueOf(videoIdCountry));
        } else {
            mParams.remove("video_id_country");
        }
    }

    public void setCategory(int category) {
        mParams.put("id_category", String.valueOf(category));
    }

    public void setVideoIdCity(String videoIdCity) {
        mParams.put("video_id_city", videoIdCity);
    }

    public void setVideoCreatedAtFrom(String createdAtFrom) {
        mParams.put("video_created_at_from", createdAtFrom);
    }

    public void setVideoCreatedAtTo(String createdAtTo) {
        mParams.put("video_created_at_to", createdAtTo);
    }

    @Override
    public UserSearchResponse loadDataFromNetwork() throws Exception {
        return getService().searchUser(mParams);
    }
}
