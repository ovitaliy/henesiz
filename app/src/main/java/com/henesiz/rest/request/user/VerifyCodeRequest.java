package com.henesiz.rest.request.user;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.model.VerifyCodeResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class VerifyCodeRequest extends RetrofitSpiceRequest<VerifyCodeResponse, UserRestApi> {
    String mCode;

    public VerifyCodeRequest(String code) {
        super(VerifyCodeResponse.class, UserRestApi.class);
        mCode = code;
    }

    @Override
    public VerifyCodeResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("code", mCode);
        return getService().verifySmsCode(map);
    }
}
