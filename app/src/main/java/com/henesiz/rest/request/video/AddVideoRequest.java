package com.henesiz.rest.request.video;

import android.text.TextUtils;

import com.henesiz.enums.VideoType;
import com.henesiz.model.VideoInfo;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VideoRestApi;
import com.henesiz.rest.model.AddVideoResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class AddVideoRequest extends RetrofitSpiceRequest<AddVideoResponse, VideoRestApi> {
    private VideoInfo mVideoInfo;
    private String mVideoUrl;
    private String mThumbUrl;

    public AddVideoRequest(VideoInfo videoInfo, String videoUrl, String thumbUrl) {
        super(AddVideoResponse.class, VideoRestApi.class);
        mVideoInfo = videoInfo;
        mVideoUrl = videoUrl;
        mThumbUrl = thumbUrl;
    }

    @Override
    public AddVideoResponse loadDataFromNetwork() throws Exception {

        ArgsMap params = new ArgsMap(true);
        params.put("id_audio", mVideoInfo.getAudioId());
        params.put("id_effect", 0);
        params.put("id_color", 0);
        params.put("media", mVideoUrl);
        params.put("thumb", mThumbUrl);
        float duration = 0;
        if (mVideoInfo.getDuration() == null) {
            mVideoInfo.detectVideoDuration();
        }
        if (mVideoInfo.getDuration() != null) {
            duration = mVideoInfo.getDuration().floatValue();
        }
        params.put("length", String.valueOf(duration));
        params.put("id_reply", mVideoInfo.getReplyToVideoId());
        if (mVideoInfo.getCategoryId() > 0) {
            params.put("id_category", mVideoInfo.getCategoryId());
        }

        if (!TextUtils.isEmpty(mVideoInfo.getTags())) {
            params.put("title", mVideoInfo.getTags());
        }

        VideoType videoType = mVideoInfo.getVideoType();
        if (videoType == null) {
            videoType = VideoType.FLOW_ALL;
        }

        if (videoType != null) {
            params.put("type", videoType.getRequestParam());
        }

        if (!TextUtils.isEmpty(videoType.getAdditionParam())) {
            if (!videoType.isFlowType()) {
                params.put("params", videoType.getAdditionParam());
            }
        }

        if (mVideoInfo.getCost() != 0) {
            params.put("cost", mVideoInfo.getCost());
        }

        return getService().addVideo(params);
    }
}
