package com.henesiz.rest.request.video;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.Video;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.FlowRestApi;
import com.henesiz.rest.model.FlowResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by denisvasilenko on 16.09.15.
 */
public class GetFlowRequest extends RetrofitSpiceRequest<FlowResponse, FlowRestApi> {
    HashMap<String, Object> mParams;
    int mReplyId;
    long mLast;
    int mUserId;

    //
    private long mScreen;
    private boolean mPagination;

    public GetFlowRequest(HashMap<String, Object> params) {
        super(FlowResponse.class, FlowRestApi.class);
        mParams = params;
    }

    public GetFlowRequest() {
        super(FlowResponse.class, FlowRestApi.class);
    }

    public void setReplyId(int replyId) {
        mReplyId = replyId;
    }

    public void setLast(long last) {
        mLast = last;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public void saveData(long screen, boolean isPagination) {
        mScreen = screen;
        mPagination = isPagination;
    }


    @Override
    public FlowResponse loadDataFromNetwork() throws Exception {
        ArgsMap argsMap = new ArgsMap(true);

        if (mUserId != 0) {
            argsMap.put("id_user", mUserId);
        }

        if (mReplyId != 0) {
            argsMap.put("id_reply", mReplyId);
        } else {
            if (mParams != null) {
                argsMap.putAll(mParams);
            }
        }

        if (mLast > 0) {
            argsMap.put("last", mLast);
        }

        Log.i("API: GetFlowRequest", argsMap.toString());
        FlowResponse flowResponse = getService().getFlow(argsMap);

        Context context = App.getInstance();
        ArrayList<Video> videos = new ArrayList<>();
        if (!mPagination) {
            context.getContentResolver().delete(
                    ContentDescriptor.Videos.URI,
                    ContentDescriptor.Videos.Cols.SCREEN + " = " + mScreen,
                    null);
        }

        ArrayList<Video> list = flowResponse.getFlow();

        if (list != null) {
            int count = list.size();

            final ContentResolver contentResolver = context.getContentResolver();

            ContentValues[] contentValueses = new ContentValues[count];
            for (int i = 0; i < count; i++) {
                Video video = list.get(i);
                videos.add(video);
                ContentValues values = video.toContentValues();
                values.put(ContentDescriptor.Videos.Cols.SCREEN, mScreen);
                contentValueses[i] = values;
            }
            contentResolver.bulkInsert(ContentDescriptor.Videos.URI, contentValueses);
        }

        return flowResponse;
    }
}
