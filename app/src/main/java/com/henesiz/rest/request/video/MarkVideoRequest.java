package com.henesiz.rest.request.video;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VideoRestApi;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class MarkVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    int mMark;

    public MarkVideoRequest(int videoId, int mark) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mMark = mark;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put("mark", mMark);

        return getService().markVideo(map);
    }
}
