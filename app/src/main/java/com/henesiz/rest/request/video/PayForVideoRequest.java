package com.henesiz.rest.request.video;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VideoRestApi;
import com.henesiz.rest.model.BaseCoinsResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 18.09.15.
 */
public class PayForVideoRequest extends RetrofitSpiceRequest<BaseCoinsResponse, VideoRestApi> {
    private int mVideoId;

    public PayForVideoRequest(int videoId) {
        super(BaseCoinsResponse.class, VideoRestApi.class);
        mVideoId = videoId;
    }

    @Override
    public BaseCoinsResponse loadDataFromNetwork() throws Exception {
        ArgsMap params = new ArgsMap(true);
        params.put("id_video", mVideoId);
        return getService().payForVideo(params);
    }
}
