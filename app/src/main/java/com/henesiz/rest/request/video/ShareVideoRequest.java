package com.henesiz.rest.request.video;

import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VideoRestApi;
import com.henesiz.rest.model.BaseResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 21.09.15.
 */
public class ShareVideoRequest extends RetrofitSpiceRequest<BaseResponse, VideoRestApi> {
    int mVideoId;
    String mPhones;

    public ShareVideoRequest(int videoId, String phones) {
        super(BaseResponse.class, VideoRestApi.class);
        mVideoId = videoId;
        mPhones = phones;
    }

    @Override
    public BaseResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_video", mVideoId);
        map.put("numbers", mPhones);
        return getService().shareVideo(map);
    }
}
