package com.henesiz.rest.request.video;

import android.os.Build;

import com.henesiz.App;
import com.henesiz.BuildConfig;
import com.henesiz.rest.CountingFileRequestBody;
import com.henesiz.util.FilePathHelper;
import com.henesiz.util.NetworkUtil;
import com.octo.android.robospice.request.SpiceRequest;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;


/**
 * Created by ovitali on 16.10.2015.
 */
public class VideoPreloadRequest extends SpiceRequest<String[]> {

    private static final int BUFFER_SIZE = 4056;

    private String[] mVideoUrls;

    public VideoPreloadRequest(String... videoUrls) {
        super(String[].class);
        mVideoUrls = videoUrls;
    }

    @Override
    public int getPriority() {
        return PRIORITY_LOW;
    }

    @Override
    public String[] loadDataFromNetwork() throws Exception {
        String[] localFilesName = new String[mVideoUrls.length];

        boolean isWiFi = BuildConfig.DEBUG && NetworkUtil.getConnectionStatus(App.getInstance()) == NetworkUtil.WIFI;

        for (int i = 0; i < mVideoUrls.length; i++) {
            String videoUrl = mVideoUrls[i];
            String videoName = FilenameUtils.getName(videoUrl);

            File destinationFile = new File(FilePathHelper.getVideoCacheDirectory(), videoName);

            if (destinationFile.exists())
                continue;

            File tmpDestinationFile = new File(FilePathHelper.getVideoTmpCacheDirectory(), videoName);

            localFilesName[i] = tmpDestinationFile.getAbsolutePath();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(videoUrl)
                    .addHeader("Content-Type", "application/json")
                    .build();

            Response response = client.newCall(request).execute();

            if (!tmpDestinationFile.exists() || tmpDestinationFile.length() < response.body().contentLength()) {
                long contentLength = response.body().contentLength();

                long writtenBytes = tmpDestinationFile.length();

                Listener listener = new Listener(mVideoUrls.length, i);

                BufferedSink sink = Okio.buffer(Okio.appendingSink(tmpDestinationFile));
                BufferedSource bufferedSource = response.body().source();
                bufferedSource.skip(writtenBytes);

                while (writtenBytes < contentLength) {
                    sink.write(bufferedSource, Math.min(BUFFER_SIZE, contentLength - writtenBytes));
                    listener.onRequestProgress(writtenBytes, contentLength);

                    // slowdown downloading speed in debug mode
                    if (isWiFi) Thread.sleep(25);

                    writtenBytes += BUFFER_SIZE;
                }
                bufferedSource.close();
                sink.close();
            }

            FileUtils.copyFile(tmpDestinationFile, destinationFile);
            FileUtils.forceDelete(tmpDestinationFile);
        }
        return localFilesName;
    }

    private class Listener implements CountingFileRequestBody.Listener {

        private long mLastProgress = -1;

        private final double mFilesCount;
        private final double mCurrentFile;

        public Listener(final int filesCount, final int currentFile) {
            mFilesCount = filesCount;
            mCurrentFile = currentFile;
        }

        @Override
        public void onRequestProgress(long bytesWritten, long contentLength) {
            long progress = (long) (bytesWritten * 100d / contentLength / mFilesCount);

            long partProgress = (long) (100d / mFilesCount * (mCurrentFile));

            if (mLastProgress != progress) {
                publishProgress(progress + partProgress);
                mLastProgress = progress + partProgress;
            }
        }
    }
}
