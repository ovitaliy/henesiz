package com.henesiz.rest.request.video;

import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.events.VideoUploadedEvent;
import com.henesiz.model.UploadingMedia;
import com.henesiz.model.Video;
import com.henesiz.model.VideoInfo;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.CountingFileRequestBody;
import com.henesiz.rest.api.VideoRestApi;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.model.AddVideoResponse;
import com.henesiz.rest.request.user.EditUserInfoRequest;
import com.henesiz.rest.service.AppRetrofitSpiceService;
import com.henesiz.util.FilePathHelper;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by ovitali on 19.10.2015.
 */
public final class VideoUploadRequest extends RetrofitSpiceRequest<UploadingMedia, VideoRestApi> {

    private static final String TAG = "VideoUploadRequest";

    private boolean mIsNewAvatar;
    private VideoInfo mVideoInfo;

    private int mVideoId;

    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    private SpiceManager mSpiceManager;

    public VideoUploadRequest(int videoId) {
        super(UploadingMedia.class, VideoRestApi.class);

        mVideoId = videoId;
    }

    @Override
    public UploadingMedia loadDataFromNetwork() throws Exception {
        try {
            mVideoInfo = getVideoInfo();
            if (mVideoInfo == null)
                return null;

            Context context = App.getInstance();

            mNotifyManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            mBuilder = new NotificationCompat.Builder(context);
            mBuilder.setContentTitle("Video Upload").setSmallIcon(android.R.drawable.stat_sys_upload);
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.logo);
            mBuilder.setLargeIcon(bm);

            mIsNewAvatar = mVideoInfo.isAvatar();

            changeVideoPostingStatus(Video.STATUS_UPLOADING);

            UploadingMedia uploadingMedia = uploadVideo();

            if (mIsNewAvatar && AppUser.get() != null) {
                AppUser.get().setPhoto(uploadingMedia.getThumb());
                getSpiceManager().execute(new EditUserInfoRequest(AppUser.get()), new BaseRequestListener<>());
            }

            AddVideoResponse addVideoResponse = sendRequest(uploadingMedia);
            // post event with video uploaded data.  this event will be caught in current running activity (based on BaseVideoManageActivity)
            if (!mIsNewAvatar) {
                EventBus.getDefault().post(addVideoResponse);
            }

            if (mVideoInfo.getLocalId() > 0) {
                App.getInstance().getContentResolver().delete(ContentDescriptor.Videos.URI, "_id = " + mVideoInfo.getLocalId(), null);
            }

            stopSpiceManager();

            EventBus.getDefault().post(uploadingMedia);

            EventBus.getDefault().postSticky(new VideoUploadedEvent(true, addVideoResponse.getVideoId(), addVideoResponse.getShortUrl(), mIsNewAvatar));

            return uploadingMedia;

        } catch (Exception ex) {

            stopSpiceManager();

            changeVideoPostingStatus(Video.STATUS_UPLOAD_REQUIRED);

            if (mNotifyManager != null) {
                mNotifyManager.cancel(mVideoId);
            }

            throw ex;
        }
    }


    private void changeVideoPostingStatus(@Video.UploadingStatus int status) {
        ContentValues contentValues = new ContentValues(1);
        contentValues.put(ContentDescriptor.Videos.Cols.UPLOADING_STATUS, status);
        saveVideoData(contentValues);
    }

    private void saveVideoUploadingData(String videoUrl, String imageUrl) {
        ContentValues contentValues = new ContentValues(2);
        contentValues.put(ContentDescriptor.Videos.Cols.UPLOADED_VIDEO, videoUrl);
        contentValues.put(ContentDescriptor.Videos.Cols.UPLOADED_IMAGE, imageUrl);
        saveVideoData(contentValues);
    }

    private void saveVideoData(ContentValues contentValues) {
        App.getInstance().getContentResolver().update(ContentDescriptor.Videos.URI, contentValues, "_id = " + mVideoInfo.getLocalId(), null);
    }

    private AddVideoResponse sendRequest(UploadingMedia uploadingMedia) {
        ArgsMap params = new ArgsMap(true);

        VideoInfo videoInfo = mVideoInfo;
        int audioId = 0;

        if (videoInfo.getAudioPath() != null) {
            String[] audioFiles = FilePathHelper.getAudioDirectory().list();
            for (int i = 0; i < audioFiles.length; i++) {
                if (audioFiles[i].contains(videoInfo.getAudioPath())) {
                    audioId = i;
                    break;
                }
            }
        }

        float duration = 0;
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(videoInfo.getVideoPath());
            duration = Integer.parseInt(mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION));
            duration /= 1000.f;
            duration = duration == 0 ? 1 : duration;
            mediaMetadataRetriever.release();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (mIsNewAvatar) {
            params.put("avatar", 1);
        }

        params.put("media", uploadingMedia.getMedia());
        params.put("thumb", uploadingMedia.getThumb());

        params.put("id_audio", String.valueOf(audioId));
        params.put("id_effect", 0 + "");
        params.put("id_color", 0 + "");
        params.put("length", duration + "");
        params.put("id_smile", "0");
        params.put("id_reply", videoInfo.getReplyToVideoId());

        if (!mIsNewAvatar) {
            if (videoInfo.getCategory() != null) {
                params.put("id_category", videoInfo.getCategory().getId());
            }
        } else {
            params.put("id_category", 1);
        }

        if (!TextUtils.isEmpty(videoInfo.getTags())) {
            params.put("title", videoInfo.getTags());
        }

        if (videoInfo.getVideoType() != null) {
            params.put("type", videoInfo.getVideoType().getRequestParam());
            if (videoInfo.getVideoType().getAdditionParam() != null)
                params.put("params", videoInfo.getVideoType().getAdditionParam());
        }

        if (videoInfo.getCost() != 0) {
            params.put("cost", videoInfo.getCost());
        }

        try {
            FileUtils.forceDelete(new File(mVideoInfo.getVideoPath()));
            FileUtils.forceDelete(new File(mVideoInfo.getImagePath()));
        } catch (IOException ignore) {
            // ignore IOException
        }

        Log.d(TAG, "params:" + params.toString());
        return getService().addVideo(params);
    }

    private UploadingMedia uploadVideo() throws Exception {
        UploadingMedia uploadingMedia;
        if (mVideoInfo.getUploadedVideoPath() != null || mVideoInfo.getUploadedImagePath() != null) {
            uploadingMedia = new UploadingMedia();
            uploadingMedia.setMedia(mVideoInfo.getUploadedVideoPath());
            uploadingMedia.setThumb(mVideoInfo.getUploadedImagePath());
        } else {
            File videoFile = new File(mVideoInfo.getVideoPath());
            File imgFile = new File(mVideoInfo.getImagePath());

            Log.i(TAG, "start uploading:" + new Date());

            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
            final MediaType MEDIA_TYPE_MP4 = MediaType.parse("video/mp4");

            final OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(0, TimeUnit.SECONDS);
            client.setReadTimeout(0, TimeUnit.SECONDS);
            client.setWriteTimeout(0, TimeUnit.SECONDS);


            MultipartBuilder multipartBuilder = new MultipartBuilder();
            multipartBuilder.type(MultipartBuilder.FORM);
            multipartBuilder.addPart(Headers.of("Content-Disposition", "form-data; name=\"thumb\"; filename=\"" + imgFile.getName() + "\""),
                    RequestBody.create(MEDIA_TYPE_PNG, imgFile));

            multipartBuilder.addPart(
                    Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"" + videoFile.getName() + "\""),
                    new CountingFileRequestBody(RequestBody.create(MEDIA_TYPE_MP4, videoFile), new CountingFileRequestBody.Listener() {
                        int mLastProgress = -1;

                        @Override
                        public void onRequestProgress(long bytesWritten, long contentLength) {
                            int progress = (int) ((bytesWritten / (float) contentLength) * 100);
                            if (progress != mLastProgress && progress > mLastProgress) {
                                publishProgress(progress);
                                Log.d(TAG, "uploaded:" + progress + "%");
                                mLastProgress = progress;
                            }
                        }
                    })
            );

            RequestBody requestBody = multipartBuilder.build();

            Request request = new Request.Builder()
                    .url(Const.MEDIA_URL + "/media/")
                    .post(requestBody)
                    .build();

            Response requestResponse = client.newCall(request).execute();

            String responseString = IOUtils.toString(requestResponse.body().byteStream());

            uploadingMedia = new Gson().fromJson(responseString, UploadingMedia.class);

            saveVideoUploadingData(uploadingMedia.getMedia(), uploadingMedia.getThumb());


            Log.d(TAG, "response:" + responseString);
        }
        return uploadingMedia;
    }

    @Override
    protected void publishProgress(float progress) {
        super.publishProgress(progress);

        if (progress < 100) {
            mBuilder.setProgress(100, (int) progress, false);
            mBuilder.setContentText("Progress: " + progress + "%");
            mNotifyManager.notify(mVideoId, mBuilder.build());
        } else {
            mNotifyManager.cancel(mVideoId);
        }
    }


    private VideoInfo getVideoInfo() {
        Cursor videoCursor = App.getInstance().getContentResolver().query(ContentDescriptor.Videos.URI,
                null,
                ContentDescriptor.Videos.Cols.UPLOADING_STATUS + " = " + Video.STATUS_UPLOAD_REQUIRED + " AND _id=" + mVideoId,
                null,
                null);
        VideoInfo info = null;
        if (videoCursor != null && videoCursor.moveToFirst()) {
            final Video video = Video.fromCursor(videoCursor);
            boolean isAvatar = videoCursor.getInt(videoCursor.getColumnIndex(ContentDescriptor.Videos.Cols.IS_AVATAR)) == 1;

            VideoType videoType = null;
            if (video.getType() > 0) {
                videoType = VideoType.getById(video.getType());
            }

            info = new VideoInfo(videoType);
            info.setVideoType(videoType);

            File videoPath = new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.VIDEO_EXTENTION);
            info.setVideoPath(videoPath.getAbsolutePath());

            File imagePath = new File(FilePathHelper.getUploadDirectory(), video.getCreatedAt() + "." + Const.IMAGE_EXTENTION);
            info.setImagePath(imagePath.getAbsolutePath());

            info.setCategoryId(video.getCategoryId());
            info.setCost(video.getCost());
            info.setReplyToVideoId(video.getReplyId());
            info.setIsAvatar(isAvatar);
            info.setTags(video.getTitle());

            info.setUploadedVideoPath(video.getUploadedVideoPath());
            info.setUploadedImagePath(video.getUploadedImagePath());

            info.setLocalId(videoCursor.getInt(video.getVideoId()));

            videoCursor.close();
        }

        return info;
    }

    private SpiceManager getSpiceManager() {
        if (mSpiceManager == null) {
            mSpiceManager = new SpiceManager(AppRetrofitSpiceService.class);
            mSpiceManager.start(App.getInstance());
        }
        return mSpiceManager;
    }

    private void stopSpiceManager() {
        if (mSpiceManager != null) {
            mSpiceManager.shouldStop();
            mSpiceManager = null;
        }
    }
}
