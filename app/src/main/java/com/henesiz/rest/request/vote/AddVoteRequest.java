package com.henesiz.rest.request.vote;

import com.henesiz.enums.Language;
import com.henesiz.model.TypedJsonString;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VoteRestApi;
import com.henesiz.rest.model.AddVoteResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class AddVoteRequest extends RetrofitSpiceRequest<AddVoteResponse, VoteRestApi> {
    String[] mOptions;
    String mDescription;

    public AddVoteRequest(String description, String[] options) {
        super(AddVoteResponse.class, VoteRestApi.class);
        mDescription = description;
        mOptions = options;
    }

    @Override
    public AddVoteResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("title", mDescription);
        map.put("lang", Language.getSystem().getRequestParam());

        JSONObject object = new JSONObject(map);
        object.put("options", new JSONArray(Arrays.asList(mOptions)));
        return getService().addVote(new TypedJsonString(object.toString()));
    }
}
