package com.henesiz.rest.request.vote;

import com.henesiz.enums.Language;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VoteRestApi;
import com.henesiz.rest.model.VoteResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVoteRequest extends RetrofitSpiceRequest<VoteResponse, VoteRestApi> {
    int mVoteId;

    public GetVoteRequest(int voteId) {
        super(VoteResponse.class, VoteRestApi.class);
        mVoteId = voteId;
    }

    @Override
    public VoteResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("id_vote", mVoteId);
        map.put("lang", Language.getSystem().getRequestParam());
        return getService().getVote(map);
    }
}
