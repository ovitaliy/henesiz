package com.henesiz.rest.request.vote;

import com.henesiz.enums.Language;
import com.henesiz.enums.VoteListType;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.api.VoteRestApi;
import com.henesiz.rest.model.VoteListsResponse;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

/**
 * Created by denisvasilenko on 22.09.15.
 */
public class GetVotesListRequest extends RetrofitSpiceRequest<VoteListsResponse, VoteRestApi> {
    VoteListType mType;

    public GetVotesListRequest(VoteListType type) {
        super(VoteListsResponse.class, VoteRestApi.class);
        mType = type;
    }

    @Override
    public VoteListsResponse loadDataFromNetwork() throws Exception {
        ArgsMap map = new ArgsMap(true);
        map.put("lang", Language.getSystem().getRequestParam());
        if (!mType.equals(VoteListType.MY)) {
            map.put("type", mType.getRequestParam());
            return getService().getVoteList(map);
        } else {
            return getService().getMyList(map);
        }
    }
}
