package com.henesiz.rest.service;

import android.app.Application;

import com.henesiz.Const;
import com.henesiz.rest.api.DatabaseRestApi;
import com.henesiz.rest.api.FlowRestApi;
import com.henesiz.rest.api.MessageRestApi;
import com.henesiz.rest.api.UserRestApi;
import com.henesiz.rest.api.VideoRestApi;
import com.henesiz.rest.api.VoteRestApi;
import com.henesiz.util.GsonHelper;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.retrofit.GsonRetrofitObjectPersisterFactory;

import java.io.File;

import retrofit.converter.Converter;
import retrofit.converter.GsonConverter;

/**
 * A pre-set, easy to use, retrofit service. It will use retrofit for network
 * requests and both networking and caching will use Gson. To use it, just add
 * to your manifest :
 * <p/>
 * <pre>
 * &lt;service
 *   android:name="com.octo.android.robospice.retrofit.AppRetrofitSpiceService"
 *   android:exported="false" /&gt;
 * </pre>
 *
 * @author SNI
 */
public class AppRetrofitSpiceService extends RetrofitSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(DatabaseRestApi.class);
        addRetrofitInterface(UserRestApi.class);
        addRetrofitInterface(FlowRestApi.class);
        addRetrofitInterface(VideoRestApi.class);
        addRetrofitInterface(MessageRestApi.class);
        addRetrofitInterface(VoteRestApi.class);
    }

    @Override
    public CacheManager createCacheManager(Application application) throws CacheCreationException {
        CacheManager cacheManager = new CacheManager();
        cacheManager.addPersister(new GsonRetrofitObjectPersisterFactory(application, getConverter(), getCacheFolder()));
        return cacheManager;
    }


    @Override
    public int getThreadCount() {
        return 2;
    }

    @Override
    protected Converter createConverter() {
        return new GsonConverter(GsonHelper.GSON);
    }

    public File getCacheFolder() {
        return null;
    }

    @Override
    protected String getServerUrl() {
        return Const.BASE_API_URL;
    }
}
