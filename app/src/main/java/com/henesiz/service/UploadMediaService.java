package com.henesiz.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.events.VideoUploadedEvent;
import com.henesiz.model.VideoInfo;
import com.henesiz.rest.CountingFileRequestBody;
import com.henesiz.rest.listener.AddVideoRequestListener;
import com.henesiz.rest.request.user.EditUserInfoRequest;
import com.henesiz.rest.request.video.AddVideoRequest;
import com.henesiz.rest.service.AppRetrofitSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Deni on 03.07.2015.
 */
public class UploadMediaService extends IntentService {
    private static final String TAG = "UploadMediaService";
    NotificationManager mNotifyManager;
    NotificationCompat.Builder mBuilder;
    private static boolean mIsUploading;

    public UploadMediaService() {
        super("UploadMediaService");
    }

    public static Intent newIntent(Context context, VideoInfo video) {
        Intent intent = new Intent(context, UploadMediaService.class);
        intent.putExtra("video", video);
        return intent;
    }

    private void updateProgress(int progress) {
        if (progress < 100) {
            mBuilder.setProgress(100, progress, false);
            mBuilder.setContentText("Progress: " + progress + "%");
            mNotifyManager.notify(1, mBuilder.build());
        } else {
            mNotifyManager.cancel(1);
        }
    }

    private Response uploadFiles(String videoPath, String imagePath)
            throws FileNotFoundException, IOException {
        Log.i(TAG, "start uploading:" + new Date());

        final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
        final MediaType MEDIA_TYPE_MP4 = MediaType.parse("video/mp4");
        final MediaType MEDIA_TYPE_TEXT = MediaType.parse("text/plain");

        final OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(500, TimeUnit.SECONDS);
        client.setWriteTimeout(500, TimeUnit.SECONDS);
        client.setReadTimeout(500, TimeUnit.SECONDS);
        MultipartBuilder multipartBuilder = new MultipartBuilder()
                .type(MultipartBuilder.FORM);

        if (!TextUtils.isEmpty(videoPath)) {
            File videoFile = new File(videoPath);
            if (videoFile.exists()) {
                multipartBuilder.addPart(
                        Headers.of("Content-Disposition", "form-data; name=\"file\"; filename=\"" + videoFile.getName() + "\""),
                        new CountingFileRequestBody(RequestBody.create(MEDIA_TYPE_MP4, videoFile), new CountingFileRequestBody.Listener() {
                            int mLastProgress = -1;

                            @Override
                            public void onRequestProgress(long bytesWritten, long contentLength) {
                                int progress = (int) ((bytesWritten / (float) contentLength) * 100);
                                if (progress > mLastProgress) {
                                    updateProgress(progress);
                                    Log.d(TAG, "uploaded:" + progress + "%");
                                    mLastProgress = progress;
                                }
                            }
                        })
                );
            } else {
                throw new FileNotFoundException(videoPath);
            }
        } else {
            throw new FileNotFoundException(videoPath);
        }

        if (!TextUtils.isEmpty(imagePath)) {
            File imgFile = new File(imagePath);
            multipartBuilder.addPart(
                    Headers.of("Content-Disposition", "form-data; name=\"thumb\"; filename=\"" + imgFile.getName() + "\""),
                    RequestBody.create(MEDIA_TYPE_PNG, imgFile));

        }

        Request request = new Request.Builder()
                .url(Const.MEDIA_URL + "/media/")
                .post(multipartBuilder.build())
                .build();

        Response response = client.newCall(request).execute();
        if (response != null && response.isSuccessful()) {
            Log.d("VUPL", "successful uploaded file");
        } else {
            Log.d("VUPL", "can't upload file, response " + (response != null ? response.message() : null));
        }
        updateProgress(100);

        return response;
    }

    private void updateInfo(final VideoInfo info, String videoUrl, String thumbUrl) {
        final SpiceManager spiceManager = new SpiceManager(AppRetrofitSpiceService.class);
        spiceManager.start(this);

        if (VideoType.AVATAR.equals(info.getVideoType()) && !TextUtils.isEmpty(thumbUrl)) {
            AppUser.get().setPhoto(thumbUrl);
            spiceManager.execute(new EditUserInfoRequest(AppUser.get()), null);
        }
        Log.d("VUPL", "started update info");
        spiceManager.execute(new AddVideoRequest(info, videoUrl, thumbUrl),
                new AddVideoRequestListener(info, new AddVideoRequestListener.OnAddedVideoListener() {
                    @Override
                    public void onAddedVideo(int videoId, String shortUrl) {
                        Log.d("VUPL", "info updated success");
                        EventBus.getDefault().postSticky(new VideoUploadedEvent(true, videoId, shortUrl, false));
                        Log.d("VUPL", "EventBus.getDefault().post(new VideoUploadedEvent());");
                    }
                }, true));

        Log.i(TAG, "end uploading:" + new Date());

    }

    public static boolean isRunning() {
        return mIsUploading;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mIsUploading = true;
        Log.d("VUPL", "mIsUploading = true;");
        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder = new NotificationCompat.Builder(this);
        mBuilder.setContentTitle("Video Upload")
                .setSmallIcon(R.drawable.logo);
        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
        mBuilder.setLargeIcon(bm);

        VideoInfo video = (VideoInfo) intent.getSerializableExtra("video");
        try {
            Response response = uploadFiles(video.getVideoPath(), video.getImagePath());
            if (response != null) {
                JSONObject answer = new JSONObject(response.body().string());
                String videoUrl = answer.getString("media");
                String thumbUrl = answer.getString("thumb");
                updateInfo(video, videoUrl, thumbUrl);
            }
        } catch (FileNotFoundException e) {
            int localId = video.getLocalId();
            getContentResolver().delete(ContentDescriptor.Videos.URI, "_id = " + video.getLocalId(), null);
            Log.d("VUPL", "getContentResolver().delete _id=" + localId);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            mIsUploading = false;
        }
        Log.d("VUPL", "mIsUploading = false;");
    }
}
