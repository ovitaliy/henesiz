package com.henesiz.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

import com.henesiz.R;
import com.henesiz.view.square.SquareRelativeLayout;

/**
 * Created by Владимир on 12.11.2014.
 */
public class BackgroundCutter {

    private Context mContext;
    private Resources mResources;
    private int mTopShift;
    private float mRadius;
    private int mWidth;
    private int mHeight;
    private int mBorderSize;
    private float mMultiplayer = 1;

    public BackgroundCutter(Context context) {
        mContext = context;
        mResources = mContext.getResources();
    }

    public int getTopShift() {
        return mTopShift;
    }

    public float getRadius() {
        return mRadius;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public int getBorderSize() {
        return mBorderSize;
    }

    public Bitmap getSmallCutBitmap(int background, int reqWidth, int reqHeight) {
        mMultiplayer = 1.4f;
        Bitmap result = getCutBackground(background, reqWidth, reqHeight, R.dimen.stream_displaying_top_shift);
        mMultiplayer = 1;
        return result;
    }

    public Bitmap getCutBackground(int background, int reqWidth, int reqHeight) {
        return getCutBackground(background, reqWidth, reqHeight, 0);
    }

    public Bitmap getBigCutBackground(int background, int reqWidth, int reqHeight) {
        mMultiplayer = 0.0f;
        Bitmap result = getCutBackground(background, reqWidth, reqHeight, 0);
        mMultiplayer = 1;
        return result;
    }

    /**
     * Get bitmap and cut circle from it.
     *
     * @param background The background to be cut
     * @param reqWidth   The width of the display
     * @param reqHeight  The height of the content
     * @return processed background
     */
    public Bitmap getCutBackground(int background, int reqWidth, int reqHeight, int shiftId) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;

        //get our image
        Bitmap bitmap = BitmapFactory.decodeResource(mResources, background, options);
        if (reqWidth == 0) {
            return bitmap;
        }

        //make image with necessary sizes
        bitmap = Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, false);
        if (shiftId == 0) {
            mTopShift = mResources.getDimensionPixelOffset(R.dimen.audio_circle_top_shift);
        } else {
            mTopShift = mResources.getDimensionPixelOffset(shiftId);
        }

        mWidth = bitmap.getWidth();
        mHeight = bitmap.getHeight();
        Rect rect = new Rect(0, 0, mWidth, mHeight);

        //result bitmap that we will use to draw
        Bitmap result = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

        final Paint paint = new Paint();
        final int color = mContext.getResources().getColor(R.color.green_main)/*0xffffffff*/;

        //get margin from resources for make circle with the same size as others
        int margin = (int) ((mResources.getDimensionPixelOffset(R.dimen.circle_margin) + mResources.getDimensionPixelOffset(R.dimen.mrg_normal)) * mMultiplayer);
        float center = mWidth / 2;
        mRadius = center - margin;

        //draw white circle at the background
        mBorderSize = mResources.getDimensionPixelSize(R.dimen.audio_image_border_size);
        Canvas bitmapCanvas = new Canvas(bitmap);
        paint.setColor(color);
        bitmapCanvas.drawCircle(mWidth / 2, (mHeight / 2) - mTopShift, mRadius, paint);

        //canvas where we will draw
        Canvas canvas = new Canvas(result);

        //init canvas
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);

        //draw our circle
        canvas.drawCircle(mWidth / 2, (mHeight / 2) - mTopShift, mRadius - mBorderSize, paint);

        //draw background
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return result;
    }

    public Bitmap cutBitmapColor(int colorId, int displayWidth, int displayHeight, SquareRelativeLayout boundFrame) {
        int color = mContext.getResources().getColor(colorId);
        Bitmap bitmap = Bitmap.createBitmap(displayWidth, displayHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);
        return cut(bitmap, displayWidth, displayHeight, boundFrame);
    }

    public Bitmap cutBitmapColor(int colorId, int displayWidth, SquareRelativeLayout boundFrame) {
        int color = mContext.getResources().getColor(colorId);
        Bitmap bitmap = Bitmap.createBitmap(displayWidth, displayWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(color);
        return cut(bitmap, displayWidth, boundFrame);
    }

    public Bitmap cutBitmap(int background, int displayWidth, int displayHeight, SquareRelativeLayout boundFrame) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        Bitmap bitmap = BitmapFactory.decodeResource(mResources, background, options);
        return cut(bitmap, displayWidth, displayHeight, boundFrame);
    }

    public Bitmap cut(Bitmap bitmap, int displayWidth, int displayHeight, SquareRelativeLayout boundFrame) {
        int top = boundFrame.getTop();
        int left = boundFrame.getLeft();
        int reqWidth = boundFrame.getRight() - boundFrame.getLeft();
        if (reqWidth == 0)
            return null;

        mBorderSize = mResources.getDimensionPixelSize(R.dimen.audio_image_border_size);

        bitmap = Bitmap.createScaledBitmap(bitmap, displayWidth, displayHeight, false);

        mWidth = bitmap.getWidth();
        mHeight = bitmap.getHeight();

        int radius = reqWidth / 2;


        final Paint paint = new Paint();
        final int color = mContext.getResources().getColor(R.color.green_main)/*0xffffffff*/;

        Canvas bitmapCanvas = new Canvas(bitmap);
        paint.setColor(color);
        bitmapCanvas.drawCircle(left + (mWidth - displayWidth) + radius, top + radius, radius, paint);

        Rect rect = new Rect(0, 0, mWidth, mHeight);

        //result bitmap that we will use to draw
        Bitmap result = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

        //canvas where we will draw
        Canvas canvas = new Canvas(result);

        //init canvas
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);

        //draw our circle
        canvas.drawCircle(left + (mWidth - displayWidth) + radius, top + radius, radius - mBorderSize, paint);

        //draw background
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return result;
    }

    public Bitmap cut(Bitmap bitmap, int displayWidth, SquareRelativeLayout boundFrame) {
        int top = 0;
        int left = boundFrame.getLeft();
        int reqWidth = boundFrame.getRight() - boundFrame.getLeft();
        if (reqWidth == 0)
            return null;

        mBorderSize = mResources.getDimensionPixelSize(R.dimen.audio_image_border_size);

        mWidth = bitmap.getWidth();
        mHeight = bitmap.getHeight();

        int radius = reqWidth / 2;

        final Paint paint = new Paint();
        final int color = mContext.getResources().getColor(R.color.green_main)/*0xffffffff*/;

        Canvas bitmapCanvas = new Canvas(bitmap);
        paint.setColor(color);
        bitmapCanvas.drawCircle(left + (mWidth - displayWidth) + radius, top + radius, radius, paint);

        Rect rect = new Rect(0, 0, mWidth, mHeight);

        //result bitmap that we will use to draw
        Bitmap result = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);

        //canvas where we will draw
        Canvas canvas = new Canvas(result);

        //init canvas
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);

        //draw our circle
        canvas.drawCircle(left + (mWidth - displayWidth) + radius, top + radius, radius - mBorderSize, paint);

        //draw background
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return result;
    }

    public static Bitmap cutRecordScreen(Context context, int color, int viewWidth, int viewHeight, int padding) {
        int borderSize = context.getResources().getDimensionPixelSize(R.dimen.audio_image_border_size);

        int radius = (Math.min(viewWidth - padding * 2, viewHeight - padding * 2)) / 2 - borderSize;

        Bitmap bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        Canvas bitmapCanvas = new Canvas(bitmap);

        bitmapCanvas.drawColor(color);

        //
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        paint = new Paint();
        paint.setStrokeWidth(borderSize);
        paint.setColor(context.getResources().getColor(R.color.green_main));
        paint.setStyle(Paint.Style.STROKE);
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        return bitmap;
    }


    public static Bitmap createCuttedBitmap(Context context, int color, int viewWidth, int viewHeight, int left, int top) {
        int borderSize = context.getResources().getDimensionPixelSize(R.dimen.audio_image_border_size);

        int radius = (Math.min(viewWidth, viewHeight)) / 2 - borderSize;

        Bitmap bitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.ARGB_8888);
        Canvas bitmapCanvas = new Canvas(bitmap);

        bitmapCanvas.drawColor(color);

        //
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        paint = new Paint();
        paint.setStrokeWidth(borderSize);
        paint.setColor(context.getResources().getColor(R.color.green_main));
        paint.setStyle(Paint.Style.STROKE);
        bitmapCanvas.drawCircle(viewWidth / 2, viewHeight / 2, radius, paint);

        return bitmap;
    }

}
