package com.henesiz.util;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Denis on 20.05.2015.
 */
public class BitmapLruCache extends LruCache<String, Bitmap> {

    public enum Size {
        SMALL, MEDIUM, LARGE
    }

    public static BitmapLruCache newInstance(Size size) {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        int maxSize = 0;
        switch (size) {
            case SMALL:
                maxSize = maxMemory / 8;
                break;
            case MEDIUM:
                maxSize = maxMemory / 6;
                break;
            case LARGE:
                maxSize = maxMemory / 4;
                break;
        }
        return newInstance(maxSize);
    }

    public static BitmapLruCache newInstance(int maxSize) {
        return new BitmapLruCache(maxSize);
    }

    public static BitmapLruCache newInstance() {
        return newInstance(Size.SMALL);
    }

    private BitmapLruCache(int maxSize) {
        super(maxSize);
    }

    @Override
    protected int sizeOf(String key, Bitmap bitmap) {
        return bitmap.getByteCount() / 1024;
    }

}
