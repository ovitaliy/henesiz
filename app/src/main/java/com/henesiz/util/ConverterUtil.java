package com.henesiz.util;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by ovitali on 07.04.2015.
 * Project is Magazines4Free
 */
public class ConverterUtil {

    public static float dpToPix(Context context, float value) {
        return dpToPix(context.getResources(), value);
    }

    public static float dpToPix(Resources resources, float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.getDisplayMetrics());
    }

    public static float pixToDp(Context context, float value) {
        return pixToDp(context.getResources(), value);
    }

    public static float pixToDp(Resources resources, float value) {
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return value / (metrics.densityDpi / 160f);
    }



}
