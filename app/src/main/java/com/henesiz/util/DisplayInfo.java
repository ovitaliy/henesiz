package com.henesiz.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.henesiz.R;

/**
 * Created by Владимир on 05.11.2014.
 */
public class DisplayInfo {
    private Activity mActivity;
    private Resources mResources;
    private DisplayMetrics mMetrics;

    public DisplayInfo(Activity activity) {
        mActivity = activity;
        mResources = mActivity.getResources();
        initDisplayMetrics();

    }

    private void initDisplayMetrics() {
        mMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(mMetrics);
    }

    public DisplayMetrics getMetrics() {
        return mMetrics;
    }

    public int getScreenHeight() {
        return mMetrics.heightPixels;
    }

    public int getScreenWidth() {
        return mMetrics.widthPixels;
    }

    public int getContentHeight() {
        return getScreenHeight() - getStatusBarHeight();
    }

    public int getCircleSize() {
        int margin = mResources.getDimensionPixelOffset(R.dimen.circle_margin) + mResources.getDimensionPixelOffset(R.dimen.mrg_normal);
        int radius = (getScreenWidth() / 2) - margin;
        return radius * 2;
    }

    public int getBigCircleSize() {
        int margin = (mResources.getDimensionPixelOffset(R.dimen.circle_margin) + mResources.getDimensionPixelOffset(R.dimen.mrg_normal));
        float multiplier = 0f;
        Log.i("imon", "big margin before: " + margin);
        margin *= multiplier;
        Log.i("imon", "big margin after: " + margin);
        int radius = (getScreenWidth() / 2) - margin;
        return radius * 2;
    }

    public int getSmallCircleSize() {
        int margin = mResources.getDimensionPixelOffset(R.dimen.circle_margin) + mResources.getDimensionPixelOffset(R.dimen.mrg_normal);
        float multiplier = 1.4f;
        margin *= multiplier;
        int radius = (getScreenWidth() / 2) - margin;
        return radius * 2;
    }


    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = mResources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = mResources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            Point size = new Point();
            display.getSize(size);
            return size.x;
        } else {
            return display.getWidth();
        }
    }

    public static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        if (android.os.Build.VERSION.SDK_INT >= 13) {
            Point size = new Point();
            display.getSize(size);
            return size.y;
        } else {
            return display.getHeight();
        }
    }
}
