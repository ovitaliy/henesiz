package com.henesiz.util;

import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by Denis on 06.05.2015.
 */
public class FfmpegHelper {
    public enum Type {
        VIDEO, IMAGE
    }

    public static class CmdBuilder {
        ArrayList<String> mInputs = new ArrayList<>();
        String mOutput;
        ArrayList<String> mFilters = new ArrayList<>();
        Type mType;
        boolean mApplyAudio;

        ArrayList<String> mAdditionalParams = new ArrayList<>();

        public CmdBuilder(Type type) {
            mType = type;
        }

        public CmdBuilder addInput(String input) {
            mInputs.add(input);
            return this;
        }

        public CmdBuilder addOutput(String output) {
            mOutput = output;
            return this;
        }

        public CmdBuilder addFilter(String filter) {
            mFilters.add(filter);
            return this;
        }

        public CmdBuilder applyAudio(boolean apply) {
            mApplyAudio = apply;
            return this;
        }

        public CmdBuilder setAdditionalParams(String params) {
            mAdditionalParams.add(params);
            return this;
        }

        public String build() {
            return build(true);
        }


        public String build(boolean changeCodec) {
            StringBuilder cmd = new StringBuilder();

            for (String input : mInputs) {
                cmd.append("-i ");
                cmd.append(input);
                cmd.append(" ");
            }

            cmd.append("-y ");

            if (mApplyAudio) {
                cmd.append("-map 0:0 -map 1:0 -shortest ");
            }

            int filterCount = mFilters.size();

//            if (filterCount > 0) {
//                cmd.append("-vf ");
//            }

            for (int i = 0; i < filterCount; i++) {
                String filter = mFilters.get(i);
                cmd.append(" -vf ");
                cmd.append(filter);
            }

            cmd.append(" ");

            if (changeCodec && mType.equals(Type.VIDEO)) {
                cmd.append("-vcodec mpeg4 -vb 20M ");
            }

            if (mAdditionalParams.size() > 0) {
                cmd.append(TextUtils.join(" ", mAdditionalParams));
                cmd.append(" ");
            }

            cmd.append("-strict -2 ");

            cmd.append(mOutput);

            String resultCommand = cmd.toString();

            while ((resultCommand.indexOf("  ")) != -1)
                resultCommand = resultCommand.replace("  ", " ");

            return resultCommand;
        }

    }

}