package com.henesiz.util;

import android.content.Context;

import com.henesiz.App;
import com.henesiz.Const;

import java.io.File;

/**
 * Created by ovitali on 23.12.2014.
 */
public class FilePathHelper implements Const {
    public static File getCacheDir() {
        Context context = App.getInstance();
        File directory = context.getExternalCacheDir();
        if (directory == null) {
            directory = context.getCacheDir();
        }
        directory = new File((directory.getAbsolutePath()), Const.DIR_BASE);
        if (!directory.exists())
            directory.mkdirs();

        return directory;
    }

    public static File getAudioTmpFile() {
        File file = new File(getCacheDir(), "audio_tmp." + AUDIO_EXTENTION);
        return file;
    }

    public static File getAudioStreamFile() {
        File file = new File(getCacheDir(), "audio_tmp_2.aac");
        return file;
    }

    public static File getVideoTmpFile() {
        File file = new File(getCacheDir(), "video_tmp." + VIDEO_EXTENTION);
        return file;
    }

    public static File getImageTmpFile() {
        File file = new File(getCacheDir(), "image_tmp." + IMAGE_EXTENTION);
        return file;
    }

    public static File getVideoTmpFile2() {
        File file = new File(getCacheDir(), "video_tmp_2." + VIDEO_EXTENTION);
        return file;
    }

    public static File getVideoTmpFile3() {
        File file = new File(getCacheDir(), "video_tmp_3." + VIDEO_EXTENTION);
        return file;
    }

    public static File getVideoTmpFile4() {
        File file = new File(getCacheDir(), "video_tmp_4." + VIDEO_EXTENTION);
        return file;
    }

    public static String getVideoFrameImagePath(int id) {
        return getVideoFrameImage(id).getAbsolutePath();
    }

    public static File getVideoFrameImage(int id) {
        File folder = getVideoFrameFolder();
        File file = new File(folder, String.format("frame%d." + IMAGE_EXTENTION, id));
        return file;
    }

    public static File getVideoPreviewImage() {
        File folder = getCacheDir();
        File file = new File(folder, "video_preview." + IMAGE_EXTENTION);
        return file;
    }

    public static File getVideoPreviewImageSmall() {
        File folder = getCacheDir();
        File file = new File(folder, "video_preview_small." + IMAGE_EXTENTION);
        return file;
    }

    public static File getVideoFilteredImagePreviewFolder() {
        File folder = new File(getCacheDir(), "filters");
        if (!folder.exists())
            folder.mkdirs();
        return folder;
    }

    public static File getFilteredImagePreview(String filterName) {
        File file = new File(getVideoFilteredImagePreviewFolder(), filterName + ".png");
        return file;
    }

    public static String getVideoPreviewImagePath() {
        return getVideoPreviewImage().getAbsolutePath();
    }

    public static String getVideoPreviewImageSmallPath() {
        return getVideoPreviewImageSmall().getAbsolutePath();
    }

    public static File getVideoFrameFolder() {
        File folder = new File(getCacheDir(), "frames");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getGrimaceTempFolder() {
        File folder = new File(getCacheDir(), "grimaces");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getVideoCacheDirectory() {
        File folder = new File(getCacheDir(), "videoCache");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getVideoTmpCacheDirectory() {
        File folder = new File(getCacheDir(), "videoTmpCache");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getUploadDirectory() {
        File folder = new File(getCacheDir(), "upload");
        if (!folder.exists()) {
            folder.mkdirs();
        }

        return folder;
    }

    public static File getAudioDirectory() {
        File folder = getCacheDir();
        File file = new File(folder, Const.DIR_AUDIO);
        return file;
    }
}
