package com.henesiz.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.Language;
import com.henesiz.model.City;
import com.henesiz.model.Country;
import com.henesiz.rest.ArgsMap;
import com.henesiz.rest.listener.CountriesRequestListener;
import com.henesiz.rest.model.CitiesListResponse;
import com.henesiz.rest.request.GetCountriesRequest;
import com.octo.android.robospice.SpiceManager;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Denis on 07.05.2015.
 */
public class LocationHelper {
    private static LinkedHashMap<Integer, Country> sCountries = new LinkedHashMap<>();
    private static LinkedHashMap<Integer, City> sCities = new LinkedHashMap<>();


    public static void init(final SpiceManager spiceManager) {
        new Thread() {
            @Override
            public void run() {
                loadCountries();
                if (sCountries.size() < 200) {
                    spiceManager.execute(new GetCountriesRequest(), new CountriesRequestListener());
                }
            }
        }.start();

    }

    public static void update(LinkedHashMap<Integer, Country> countries) {
        sCountries = countries;
    }

    private static void loadCountries() {
        ContentResolver contentResolver = App.getInstance().getContentResolver();
        String[] projection = new String[4];
        projection[0] = ContentDescriptor.Countries.Cols.ID;
        if (AppUser.get() != null) {
            Language lang = Language.getSystem();
            switch (lang) {
                case RUS:
                    projection[1] = ContentDescriptor.Countries.Cols.NAME_RUS;
                    break;
                case ENG:
                    projection[1] = ContentDescriptor.Countries.Cols.NAME_ENG;
                    break;
                case ESP:
                    projection[1] = ContentDescriptor.Countries.Cols.NAME_ESP;
                    break;
            }
            projection[2] = ContentDescriptor.Countries.Cols.PHONE_CODE;
            projection[3] = ContentDescriptor.Countries.Cols.PHONE_LENGHT;

            Cursor cursor = contentResolver.query(ContentDescriptor.Countries.URI,
                    projection,
                    null,
                    null,
                    null);

            try {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        Country country = Country.fromCursor(cursor);
                        sCountries.put(country.getId(), country);
                    } while (cursor.moveToNext());
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    public static Country getCountry(int countryId) {
        if (sCountries == null || sCountries.size() == 0) {
            loadCountries();
        }

        return sCountries.get(countryId);
    }

    public static List<Country> getCountriesList() {
        List<Country> list = new ArrayList<>(sCountries.size());
        if (sCountries == null || sCountries.size() == 0) {
            loadCountries();
        }
        for (Country country : sCountries.values()) {
            list.add(country);
        }
        return list;
    }

    public static City getCity(Context context, int cityId) {
        ContentResolver contentResolver = context.getContentResolver();
        if (sCities.get(cityId) == null) {
            String[] projection = new String[2];
            projection[0] = ContentDescriptor.Cities.Cols.ID;
            Language lang = Language.getSystem();
            switch (lang) {
                case RUS:
                    projection[1] = ContentDescriptor.Cities.Cols.NAME_RUS;
                    break;
                case ENG:
                    projection[1] = ContentDescriptor.Cities.Cols.NAME_ENG;
                    break;
                case ESP:
                    projection[1] = ContentDescriptor.Cities.Cols.NAME_ESP;
                    break;
            }

            Cursor cursor = contentResolver.query(ContentDescriptor.Cities.URI,
                    projection,
                    ContentDescriptor.Cities.Cols.ID + " = " + cityId,
                    null,
                    null);

            try {
                if (cursor != null && cursor.moveToFirst()) {
                    City city = City.fromCursor(cursor);
                    sCities.put(city.getId(), city);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        return sCities.get(cityId);
    }

    public static List<City> getCities(int countryId) {
        List<City> cities = new ArrayList<>();
        ContentResolver contentResolver = App.getInstance().getContentResolver();
        String[] projection = new String[2];
        projection[0] = ContentDescriptor.Cities.Cols.ID;
        Language lang = Language.getSystem();
        switch (lang) {
            case RUS:
                projection[1] = ContentDescriptor.Cities.Cols.NAME_RUS;
                break;
            case ENG:
                projection[1] = ContentDescriptor.Cities.Cols.NAME_ENG;
                break;
            case ESP:
                projection[1] = ContentDescriptor.Cities.Cols.NAME_ESP;
                break;
        }

        Cursor cursor = contentResolver.query(ContentDescriptor.Cities.URI,
                projection,
                ContentDescriptor.Cities.Cols.COUNTRY_ID + " = " + countryId,
                null,
                null);

        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    City city = City.fromCursor(cursor);
                    sCities.put(city.getId(), city);
                    cities.add(city);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return cities;
    }

    public static synchronized List<City> getCitiesFromServer(int countryId, String name) {
        ArgsMap argsMap = new ArgsMap(true);
        argsMap.put("id_country", countryId);

        if (!TextUtils.isEmpty(name)) {
            argsMap.put("lang", Language.getSystem().getRequestParam());
            argsMap.put("q", name);
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), new JSONObject(argsMap).toString());
        Request request = new Request.Builder()
                .url(Const.BASE_API_URL + "/database.getCities")
                .post(body)
                .build();

        OkHttpClient client = new OkHttpClient();
        List<City> cities = new ArrayList<>();
        try {
            Response response = client.newCall(request).execute();
            CitiesListResponse listResponse = GsonHelper.GSON.fromJson(response.body().string(), CitiesListResponse.class);
            if (listResponse != null) {
                List<City> list = listResponse.getCities();
                for (City city : list) {
                    city.setCountryId(countryId);
                }
                cities.addAll(listResponse.getCities());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cities;
    }
}
