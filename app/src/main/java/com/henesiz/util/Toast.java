package com.henesiz.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.View;

/**
 * Created by denisvasilenko on 04.02.16.
 */
public class Toast extends android.widget.Toast {
    static android.widget.Toast sToast;

    /**
     * Construct an empty Toast object.  You must call {@link #setView} before you
     * can call {@link #show}.
     *
     * @param context The context to use.  Usually your {@link Application}
     *                or {@link Activity} object.
     */
    public Toast(Context context) {
        super(context);
    }

    @SuppressLint("ShowToast")
    public static android.widget.Toast makeText(Context context, String text, int lenght) {
        if (sToast != null && sToast.getView().getWindowVisibility() == View.VISIBLE) {
            sToast.cancel();
        }
        sToast = android.widget.Toast.makeText(context,
                text, lenght);

        return sToast;
    }
}
