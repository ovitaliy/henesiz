package com.henesiz.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.ColorRes;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by ovitali on 10.04.2015.
 */
public class UiUtil {

    public static void setTextValue(View v, int viewId, int value) {
        String sValue = value > 0 ? String.valueOf(value) : "";
        setTextValue(v, viewId, sValue);
    }

    public static void setTextValue(View v, int viewId, String value) {
        TextView view = (TextView) v.findViewById(viewId);
        view.setText(value);
    }

    public static String getTextValue(View v, int viewId) {
        TextView view = (TextView) v.findViewById(viewId);
        return view.getText().toString().trim();
    }

    public static int getIntValue(View v, int viewId) {
        try {
            return Integer.parseInt(getTextValue(v, viewId));
        } catch (Exception ex) {
            return 0;
        }
    }

    public static void removeMeFromParent(View view) {
        ViewGroup parent = (ViewGroup) view.getParent();
        parent.removeView(view);
    }

    public static void hideKeyboard(Activity target) {
        if (target.getCurrentFocus() instanceof EditText) {
            hideKeyboard((EditText) target.getCurrentFocus());
        }
    }

    public static int getColor(Context context, @ColorRes int color) {
       /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return context.getColor(color);
        else*/
        return context.getResources().getColor(color);
    }

    public static void hideKeyboard(EditText target) {
        InputMethodManager imm = (InputMethodManager) target.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
        target.clearFocus();
    }

    public static void setBackgroundDrawable(View v, Drawable drawable) {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackgroundDrawable(drawable);
        } else {
            v.setBackground(drawable);
        }
    }

    public static void setBackgroundDrawable(View v, Bitmap bitmap) {
        setBackgroundDrawable(v, new BitmapDrawable(v.getResources(), bitmap));
    }

    public static void openLink(Context context, String link) {
        try {
            Uri uri = Uri.parse(link);
            context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void share(Context context, String title, String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(shareIntent, title));
    }


}
