package com.henesiz.util;

import android.database.Cursor;

import com.henesiz.App;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.model.UploadingMedia;
import com.henesiz.model.Video;
import com.henesiz.rest.listener.BaseRequestListener;
import com.henesiz.rest.request.video.VideoUploadRequest;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

import java.util.concurrent.Semaphore;

/**
 * Created by Deni on 29.03.2016.
 */
public final class VideoUploadHelper extends Thread {

    public static synchronized void uploadPendingVideos() {
        new VideoUploadHelper().start();
    }

    @Override
    public void run() {
        Cursor videoCursor = App.getInstance().getContentResolver().query(ContentDescriptor.Videos.URI,
                null,
                ContentDescriptor.Videos.Cols.UPLOADING_STATUS + " = " + Video.STATUS_UPLOAD_REQUIRED,
                null,
                ContentDescriptor.Videos.Cols.CREATED_AT + " ASC");

        if (videoCursor != null && videoCursor.moveToFirst()) {
            do {
                int id = videoCursor.getInt(videoCursor.getColumnIndex("_id"));

                VideoUploadRequest videoUploadRequest = new VideoUploadRequest(id);
                videoUploadRequest.setPriority(SpiceRequest.PRIORITY_LOW);
                App.getSpiceManager().execute(videoUploadRequest, String.valueOf(id), DurationInMillis.ALWAYS_EXPIRED, new BaseRequestListener<UploadingMedia>());
            } while (videoCursor.moveToNext());

            videoCursor.close();
        }
    }
}