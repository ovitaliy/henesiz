package com.henesiz.view;

import android.content.Context;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.enums.BalanceChangeType;
import com.henesiz.model.Balance;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 13.11.2014.
 */
public class BalanceItemView extends RelativeLayout {

    private Context mContext;
    private CircleImageView mImageView;
    private TextView mTitleTextView;
    private TextView mTextView;
    private TextView mCoinsView;
    private TextView mDateView;

    public BalanceItemView(Context context) {
        super(context);
        mContext = context;
        inflate(mContext, R.layout.item_balance, this);
        mImageView = (CircleImageView) findViewById(R.id.item_balance_image);
        mTitleTextView = (TextView) findViewById(R.id.item_balance_title);
        mTextView = (TextView) findViewById(R.id.item_balance_text);
        mCoinsView = (TextView) findViewById(R.id.balance_change_sum);
        mDateView = (TextView) findViewById(R.id.balance_change_date);
    }

    public void setData(Balance data) {
        String image = data.getObjectImg();
        if (TextUtils.isEmpty(image)) {
            image = AppUser.get().getImageLoaderPhoto();
        }
        ImageLoader.getInstance().displayImage(image, mImageView);

        BalanceChangeType type = data.getBalanceChangeType();
        String title;
        if (type != null) {
            title = type.getTitle();
        } else {
            title = "???";
        }
        mTitleTextView.setText(title);
        String sum = "";
        if (data.getChange() > 0) {
            sum += "+";
        } else {
            sum += "-";
        }
        sum += Math.abs(data.getChange()) + " " + getContext().getString(R.string.wow);
        mCoinsView.setText(sum);
        if (data != null) {
            mDateView.setText(Const.DAY_MONTH_YEAR_FORMAT.format(data.getCreatedAt()));
        }
    }
}
