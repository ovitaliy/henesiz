package com.henesiz.view;

import com.henesiz.enums.VideoType;
import com.henesiz.model.Category;

/**
 * Created by denisvasilenko on 05.10.15.
 */
public interface CategorizedFlowsView {
    VideoType getVideoType();

    void loadCategories(VideoType videoType);

    void openFlowList();

    Category getCurrentCategory();
}
