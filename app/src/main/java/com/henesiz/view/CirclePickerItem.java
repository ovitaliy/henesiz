package com.henesiz.view;

import android.graphics.drawable.Drawable;

/**
 * Indicates that view used as item in CirclePickerView
 */
public interface CirclePickerItem {
    int getId();

    Drawable getDrawable();

    String getTitle();
}