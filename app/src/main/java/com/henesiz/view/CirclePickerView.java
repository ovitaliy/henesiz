package com.henesiz.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.util.Utils;
import com.henesiz.view.square.SquareFrameLayout;

import java.util.ArrayList;

/**
 * Created by Denis on 05.03.2015.
 */
public class CirclePickerView extends SquareFrameLayout implements View.OnClickListener, TextWatcher {
    private TextView mLabel;
    private Context mContext;

    private int mImageSize;
    private int mCurrentSelectedElement;

    private Position mPosition = Position.SOUTH;

    private int mInitialAngle = mPosition.getAngle();

    private int mBorderColor;

    private SparseArray<CirclePickerItem> mElements = new SparseArray<>();
    private OnPickListener mListener;

    public CirclePickerView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public void setPosition(Position position) {
        mPosition = position;
        setInitialAngle(mPosition.getAngle());
    }

    public void setInitialAngle(int initialAngle) {
        mInitialAngle = initialAngle;
    }

    public CirclePickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public CirclePickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        init();
    }

    private void init() {
        mBorderColor = getResources().getColor(R.color.green_main);
        mLabel = new EditText(mContext);
        mLabel.setBackgroundResource(android.R.color.transparent);
        mLabel.setTextColor(mContext.getResources().getColor(R.color.green_main));
        mLabel.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        FrameLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mLabel.setLayoutParams(params);
        mLabel.setGravity(Gravity.CENTER);
        addView(mLabel);
        mLabel.setEnabled(false);
        mLabel.setHintTextColor(mContext.getResources().getColor(R.color.green_main));
    }

    public <T extends CirclePickerItem> void fill(final ArrayList<T> elements, int currentId, OnPickListener listener) {
        mListener = listener;
        mCurrentSelectedElement = currentId;

        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                while (getChildCount() > 1) {
                    removeViewAt(1);
                }

                mElements.clear();

                int count = elements.size();

                if (count == 2) {
                    count = 3;
                }

                if (count == 3) {//if 3 items should be triangle that begin from right side
                    setInitialAngle(30);
                }

                final int bigCircleBorder = getResources().getDimensionPixelSize(R.dimen.picker_border);
                final int itemBorderWidth = Utils.convertDpToPixel(9, getContext());

                int w = getWidth();
                if (w != 0) {

                    w = w - bigCircleBorder * 2;

                    ViewTreeObserver obs = getViewTreeObserver();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        obs.removeOnGlobalLayoutListener(this);
                    } else {
                        obs.removeGlobalOnLayoutListener(this);
                    }

                    //radius for smile positioning
                    int radius;
                    //resize smile bg, set size and radius according to orientation
                    View imageView = getChildAt(0);
                    FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) imageView.getLayoutParams();

                    flp.width = FrameLayout.LayoutParams.MATCH_PARENT;
                    flp.height = FrameLayout.LayoutParams.MATCH_PARENT;
                    mImageSize = w / 8;
                    radius = w * 3 / 8;
                    imageView.setLayoutParams(flp);

                    mLabel.setPadding(mImageSize * 2, 0, mImageSize * 2, 0);

                    //center X offset in smiles container
                    int x0 = w / 2 + bigCircleBorder;
                    //center Y offset in smiles container
                    int y0 = w / 2 + bigCircleBorder;
                    for (int i = 0; i < elements.size(); i++) {
                        CirclePickerItem element = elements.get(i);
                        double angle_offset = 360d / count;
                        double angle = (mInitialAngle + angle_offset * (i)) * Math.PI / 180;
                        //top/left offset of smile center (relative to container center)
                        double x = (double) Math.round(Math.cos(angle) * radius);
                        double y = (double) Math.round(Math.sin(angle) * radius);
                        CircleImageView elementImageView = new CircleImageView(mContext);
                        elementImageView.setBorderColor(mBorderColor);
                        elementImageView.setBorderSize(itemBorderWidth);
                        elementImageView.setOnClickListener(CirclePickerView.this);
                        elementImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        //load smile either from file or from resources
                        BitmapDrawable bitmapDrawable = (BitmapDrawable) element.getDrawable();
                        if (bitmapDrawable.getBitmap() != null) {
                            elementImageView.setImageBitmap(bitmapDrawable.getBitmap());
                        }

                        flp = new FrameLayout.LayoutParams(mImageSize, mImageSize);
                        flp.setMargins((int) (x0 + x - mImageSize / 2), (int) (y0 + y - mImageSize / 2), 0, 0);
                        addView(elementImageView, flp);
                        elementImageView.setId(element.getId());
                        mElements.put(element.getId(), element);
                    }

                    if (mCurrentSelectedElement >= 0) {
                        highlightElement(mCurrentSelectedElement);
                    } else {
                        mLabel.setText("");
                    }
                }
            }
        });
    }

    private CirclePickerItem getCurrentElement() {
        if (mCurrentSelectedElement >= 0) {
            return mElements.get(mCurrentSelectedElement);
        }
        return null;
    }

    private EditableCirclePickerItem getOtherOptionElement() {
        if (mCurrentSelectedElement >= 0) {
            CirclePickerItem item = mElements.get(mElements.size());
            if (item instanceof EditableCirclePickerItem) {
                return (EditableCirclePickerItem) item;
            }
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        highlightElement(id);
        if (mListener != null) {
            mListener.OnPick(mElements.get(id));
        }
    }

    private void highlightElement(int elementId) {
        ImageView smileView;
        FrameLayout.LayoutParams flp;

        CirclePickerItem element = mElements.get(elementId);
        if (element instanceof EditableCirclePickerItem && ((EditableCirclePickerItem) element).isOtherOption()) {
            mLabel.setHint(R.string.other_option);
            mLabel.setEnabled(true);
            mLabel.setText(element.getTitle());
            mLabel.requestFocus();
            mLabel.addTextChangedListener(this);
        } else {
            mLabel.removeTextChangedListener(this);
            mLabel.setEnabled(false);
            mLabel.setText(element.getTitle());
        }

        //shrink prev smile
        if (mCurrentSelectedElement >= 0) {
            smileView = (ImageView) findViewById(mCurrentSelectedElement);
            flp = (FrameLayout.LayoutParams) smileView.getLayoutParams();
            if (flp.width > mImageSize) {
                flp.width = mImageSize;
                flp.height = mImageSize;
                flp.leftMargin += mImageSize / 2;
                flp.topMargin += mImageSize / 2;
                smileView.setLayoutParams(flp);
            }
        }
        //
        smileView = (ImageView) findViewById(elementId);
        flp = (FrameLayout.LayoutParams) smileView.getLayoutParams();
        if (flp.width < mImageSize * 2) {
            flp.width = mImageSize * 2;
            flp.height = mImageSize * 2;
            flp.leftMargin -= mImageSize / 2;
            flp.topMargin -= mImageSize / 2;
            smileView.setLayoutParams(flp);
            smileView.bringToFront();
            mCurrentSelectedElement = elementId;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        EditableCirclePickerItem element = getOtherOptionElement();
        if (element != null) {
            element.setTitle(s.toString());
            if (mListener != null) {
                mListener.OnPick(element);
            }
        }
    }

    public interface OnPickListener {
        void OnPick(CirclePickerItem element);
    }

    public enum Position {
        NORTH {
            public int getAngle() {
                return 270;
            }
        }, NORTHEAST {
            public int getAngle() {
                return 315;
            }
        }, EAST {
            public int getAngle() {
                return 0;
            }
        }, SOUTHEAST {
            public int getAngle() {
                return 45;
            }
        }, SOUTH {
            public int getAngle() {
                return 90;
            }
        }, SOUTHWEST {
            public int getAngle() {
                return 135;
            }
        }, WEST {
            public int getAngle() {
                return 180;
            }
        }, NORTHWEST {
            public int getAngle() {
                return 225;
            }
        };

        public int getAngle() {
            return this.getAngle();
        }
    }
}