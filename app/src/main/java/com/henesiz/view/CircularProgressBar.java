package com.henesiz.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.henesiz.R;

/**
 * Created by ovitali on 09.01.2015.
 */
public class CircularProgressBar extends ProgressBar {

    private final RectF mCircleBounds = new RectF();
    private int STROKE_WIDTH = 0;
    private Paint mProgressColorPaint;

    public CircularProgressBar(Context context) {
        super(context);
    }

    public CircularProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircularProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        int prog = getProgress();
        float scale = getMax() > 0 ? (float) prog / getMax() * 360 : 0;
        canvas.drawArc(mCircleBounds, 90, scale, false, mProgressColorPaint);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        if (mProgressColorPaint == null) {
            STROKE_WIDTH = (int) getResources().getDimension(R.dimen.audio_image_border_size);

            mProgressColorPaint = new Paint();
            mProgressColorPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
            mProgressColorPaint.setColor(getResources().getColor(R.color.green_main));
            mProgressColorPaint.setAntiAlias(true);
            mProgressColorPaint.setStyle(Paint.Style.STROKE);
            mProgressColorPaint.setStrokeWidth(STROKE_WIDTH * 4);
        }

        final int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int min = Math.min(width, height);
        setMeasuredDimension(
                MeasureSpec.makeMeasureSpec(min, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(min, MeasureSpec.UNSPECIFIED)
        );

        mCircleBounds.set(STROKE_WIDTH * 2, STROKE_WIDTH * 2, min - STROKE_WIDTH * 2, min - STROKE_WIDTH * 2);
    }
}
