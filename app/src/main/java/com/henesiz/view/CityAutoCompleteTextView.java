package com.henesiz.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.activities.EditProfileActivity;
import com.henesiz.activities.RegistrationActivity;
import com.henesiz.model.City;
import com.henesiz.util.LocationHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovi on 12/23/15.
 */
public class CityAutoCompleteTextView extends AutoCompleteTextView {

    private CityAutoCompleteAdapter mAdapter;

    private City mSelectedCity;

    private int mCountryId;

    public CityAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public CityAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSaveEnabled(false);
        setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
        setThreshold(0);

        if (context instanceof RegistrationActivity || context instanceof EditProfileActivity) {
            setHint(R.string.choose_your_city);
        } else {
            setHint(R.string.choose_city);
        }

        mAdapter = new CityAutoCompleteAdapter();
        setAdapter(mAdapter);

        setOnItemSelectedListener(null);

        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    showInitialSuggestions();
                }
                return false;
            }
        });
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocused) {
                if (isFocused) {
                    showInitialSuggestions();
                }
            }
        });
    }

    void showInitialSuggestions() {
        if (TextUtils.isEmpty(getText().toString())) {
            performFiltering("", 0);
        }
    }

    public int getSelectedItemId() {
        return mSelectedCity != null ? mSelectedCity.getId() : -1;
    }

    public void setSelectedPosition(int position) {
        mSelectedCity = mAdapter.getItem(position);
        String cityName = mSelectedCity.getTitle();
        setText(cityName);
        setSelection(cityName.length());
    }

    public City getSelectedItem() {
        return mSelectedCity;
    }

    public void clear() {
        mSelectedCity = null;
        setText("");
        mAdapter.clear();
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    public CityAutoCompleteAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener l) {
        super.setOnItemSelectedListener(new WrappedOnItemSelectedListener(l));
    }

    public int getCountryId() {
        return mCountryId;
    }

    public void setCountryId(int countryId) {
        mCountryId = countryId;
    }

    private class WrappedOnItemSelectedListener implements AdapterView.OnItemSelectedListener {
        private AdapterView.OnItemSelectedListener mListener;

        public WrappedOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
            mListener = listener;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            setSelectedPosition(position);

            if (mListener != null)
                mListener.onItemSelected(parent, view, position, id);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            if (mListener != null)
                mListener.onNothingSelected(parent);
        }
    }

    public class CityAutoCompleteAdapter extends BaseAdapter implements Filterable {
        private List<City> mFilteredItems = new ArrayList<>();

        @Override
        public int getCount() {
            return mFilteredItems.size();
        }

        @Override
        public City getItem(int position) {
            return mFilteredItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mFilteredItems.get(position).getId();
        }

        public void clear() {
            if (mFilteredItems != null && mFilteredItems.size() > 0) {
                mFilteredItems.clear();
                notifyDataSetChanged();
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView;
            if (convertView != null) {
                textView = (TextView) convertView;
            } else {
                textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(R.layout.item_autocomplete_city, parent, false);
            }
            textView.setText(getItem(position).getTitle());
            return textView;
        }

        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();

                    if (!TextUtils.isEmpty(constraint) && constraint.toString().length() < 3) {
                        return filterResults;
                    }

                    String start = null;
                    if (!TextUtils.isEmpty(constraint)) {
                        start = constraint.toString().toLowerCase();
                    }

                    List<City> result = LocationHelper.getCitiesFromServer(mCountryId, start);

                    if (result != null && result.size() > 0) {
                        if (result.get(0).getCountryId() == mCountryId) {
                            filterResults.values = result;
                            filterResults.count = result.size();
                        }
                    }

                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        mFilteredItems = (List<City>) results.values;
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
        }
    }

}
