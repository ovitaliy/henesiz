package com.henesiz.view;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Comment;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Date;

/**
 * Created by Владимир on 26.11.2014.
 */
public class CommentView extends RelativeLayout implements View.OnClickListener {

    private CircleImageView mImageView;
    private TextView mNameTextView;
    private TextView mTextView;
    private TextView mTimeTextView;
    private Comment mData;

    private OnViewProfileListener mOnViewProfileListener;

    public CommentView(Context context) {
        super(context);
        inflate(context, R.layout.item_comment, this);
        mImageView = (CircleImageView) findViewById(R.id.author_image);
        mNameTextView = (TextView) findViewById(R.id.author_name);
        mTextView = (TextView) findViewById(R.id.message);
        mTimeTextView = (TextView) findViewById(R.id.time);

        mImageView.setOnClickListener(this);
        mNameTextView.setOnClickListener(this);
    }

    public void setData(Comment data) {
        mData = data;
        mNameTextView.setText(mData.getAuthor());
        mTextView.setText(mData.getText());
        long time = mData.getTime().getTime();
        Date date = new Date(time);
        mTimeTextView.setText(Const.HOUR_MINUTE_FORMAT.format(date));

        //setting image
        ImageLoader.getInstance().displayImage(mData.getAvatarUrl(), mImageView);

    }

    public void setOnViewProfileListener(OnViewProfileListener listener) {
        mOnViewProfileListener = listener;
    }

    @Override
    public void onClick(View v) {
        mOnViewProfileListener.onViewProfile(mData.getAuthorId());
    }
}
