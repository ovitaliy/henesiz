package com.henesiz.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.listener.SimpleTextListener;

/**
 * Created by ovitali on 24.09.2015.
 */
public class EditWithCounterView extends LinearLayout {

    public EditWithCounterView(Context context) {
        this(context, null);
    }


    TextView mNewMessageLength;
    EditText mNewMessageView;

    private int mMaxEditTextLength = 400;

    public EditWithCounterView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(VERTICAL);

        inflate(context, R.layout.layout_edit_with_counter, this);

        if (isInEditMode())
            return;

        mNewMessageLength = (TextView) findViewById(R.id.new_message_length_view);
        mNewMessageView = (EditText) findViewById(R.id.new_message_view);
        mNewMessageView.addTextChangedListener(new SimpleTextListener() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);

                setRemainedCharsCount();
            }
        });
        setRemainedCharsCount();

    }

    private void setRemainedCharsCount() {
        mNewMessageLength.setText(String.format("%d/%d", mNewMessageView.getText().length(), mMaxEditTextLength));
    }

    public void setMaxEditTextLength(int length) {
        mMaxEditTextLength = length;
    }

    public String getText() {
        return mNewMessageView.getText().toString();
    }

    public EditText getEditView() {
        return mNewMessageView;
    }

    public void setText(String text) {
        mNewMessageView.setText(text);
        setRemainedCharsCount();
    }

    public void setHint(String hint) {
        mNewMessageView.setHint(hint);
    }
}
