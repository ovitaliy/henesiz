package com.henesiz.view;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.util.ConverterUtil;
import com.henesiz.util.UiUtil;

/**
 * Created by ovitali on 17.02.2015.
 */
public class GreenProgressBar extends FrameLayout {
    private ProgressBar mProgressBar;
    private TextView mProgressTextView;

    public GreenProgressBar(Context context) {
        this(context, null);
    }

    public GreenProgressBar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GreenProgressBar(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mProgressBar = new ProgressBar(context);
        mProgressBar.setIndeterminate(true);
        mProgressBar.getIndeterminateDrawable().setColorFilter(UiUtil.getColor(getContext(), R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
        final int size = (int) ConverterUtil.dpToPix(getContext(), 70);
        addView(mProgressBar, size, size);
    }

    public void setProgress(int progress) {
        String text = progress + "%";
        if (mProgressTextView == null) {
            mProgressTextView = new TextView(getContext());
            mProgressTextView.setTextColor(UiUtil.getColor(getContext(), R.color.colorPrimary));
            mProgressTextView.setGravity(Gravity.CENTER);
            final int size = (int) ConverterUtil.dpToPix(getContext(), 70);
            addView(mProgressTextView, size, size);
        }

        mProgressTextView.setText(text);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        final int size = (int) ConverterUtil.dpToPix(getContext(), 70);
        super.onMeasure(
                MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(size, MeasureSpec.EXACTLY));
    }

}
