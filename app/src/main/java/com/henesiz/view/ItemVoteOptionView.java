package com.henesiz.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.henesiz.R;

/**
 * Created by denisvasilenko on 12.03.16.
 */
public class ItemVoteOptionView extends FrameLayout {
    TextView mTitleView;

    public ItemVoteOptionView(Context context) {
        super(context);
        init();
    }

    public ItemVoteOptionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ItemVoteOptionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_vote_option, this);
        mTitleView = (TextView) findViewById(R.id.title);
    }

    public void setTitle(String text) {
        mTitleView.setText(text);
    }
}
