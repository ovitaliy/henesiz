package com.henesiz.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.henesiz.R;

/**
 * Created by denisvasilenko on 12.03.16.
 */
public class ItemVoteQuestionView extends LinearLayout {
    Context mContext;
    TextView mQuestionView;
    CheckBox mCheckBox;
    TextView mStatusView;

    public ItemVoteQuestionView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public ItemVoteQuestionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public ItemVoteQuestionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        inflate(mContext, R.layout.item_vote_question, this);
        mQuestionView = (TextView) findViewById(R.id.question);
        mCheckBox = (CheckBox) findViewById(R.id.voted);
        mStatusView = (TextView) findViewById(R.id.status);
    }

    public void setQuestion(String question) {
        mQuestionView.setText(question);
    }

    public void setStatus(String status) {
        if (TextUtils.isEmpty(status)) {
            mStatusView.setVisibility(GONE);
        } else {
            mStatusView.setVisibility(VISIBLE);
            mStatusView.setText(status);
        }
    }

    public void setVoted(Boolean voted) {
        if (voted == null) {
            mCheckBox.setVisibility(GONE);
        } else {
            mCheckBox.setVisibility(VISIBLE);
            mCheckBox.setChecked(voted);
        }
    }
}
