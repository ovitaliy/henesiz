package com.henesiz.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.henesiz.R;

/**
 * Created by Deni on 03.02.2016.
 */
public class MenuView extends FrameLayout implements Checkable {
    boolean mChecked;
    TextView mLabelView;
    CheckableImageView mIconView;

    public MenuView(Context context) {
        super(context);
        init();
    }

    public MenuView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MenuView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.menu_item, this, true);
        mLabelView = (TextView) findViewById(R.id.menu_item_title);
        mIconView = (CheckableImageView) findViewById(R.id.menu_item_icon);
    }

    void updateState() {
        mIconView.setChecked(isChecked());
        int checkedColor = getResources().getColor(R.color.green_main);
        int uncheckedColor = getResources().getColor(R.color.black);
        mLabelView.setTextColor(isChecked() ? checkedColor : uncheckedColor);
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        updateState();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked());
    }

    private static final int[] CheckedStateSet = {
            android.R.attr.state_checked,
    };

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CheckedStateSet);
        }
        return drawableState;
    }

    @Override
    public boolean performClick() {
        toggle();
        return super.performClick();
    }
}
