package com.henesiz.view;

import com.henesiz.enums.VideoType;

/**
 * Created by denisvasilenko on 08.09.15.
 */
public interface NewVideoView {
    VideoType getVideoType();
}
