package com.henesiz.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by ovitali on 29.01.2015.
 */
public class TabView extends LinearLayout implements View.OnClickListener {

    private View mPreviosButton;
    private OnTabSelectListener mOnTabSelectListener;

    public TabView(Context context) {
        super(context, null);
    }

    public TabView(Context context, AttributeSet attrs) {
        super(context, attrs);

        setGravity(Gravity.CENTER_VERTICAL);
        setOrientation(HORIZONTAL);
    }

    public void setActive(int tab) {
        onClick(getChildAt(tab));
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);

        if (mPreviosButton == null) {
            mPreviosButton = child;
            mPreviosButton.setSelected(true);
        }

        child.setOnClickListener(this);
    }

    public void setOnTabSelectListener(OnTabSelectListener listener) {
        mOnTabSelectListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (!mPreviosButton.equals(v)) {
            v.setSelected(true);
            mPreviosButton.setSelected(false);
            mPreviosButton = v;
        }

        if (mOnTabSelectListener != null) {
            mOnTabSelectListener.onTabSelected(v, indexOfChild(v));
        }
    }

    public interface OnTabSelectListener {
        void onTabSelected(View view, int position);
    }
}
