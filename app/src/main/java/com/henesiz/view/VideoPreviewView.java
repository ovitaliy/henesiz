package com.henesiz.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.henesiz.R;
import com.henesiz.events.ProgressedImageLoadingListener;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Denis on 08.04.2015.
 */
public class VideoPreviewView extends FrameLayout {
    private CircleImageView mPreviewImage;
    private ImageButton mPlayButton;
    private GreenProgressBar mProgressBar;
    private boolean mHasPlayButton;

    public VideoPreviewView(Context context) {
        super(context);
        init();
    }

    public VideoPreviewView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VideoPreviewView, R.attr.hasPlayButton, 0);
        mHasPlayButton = a.getBoolean(0, false);
        a.recycle();
        init();
    }

    public VideoPreviewView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VideoPreviewView, R.attr.hasPlayButton, 0);
        mHasPlayButton = a.getBoolean(0, false);
        a.recycle();
        init();
    }

    private void init() {
        setClickable(false);
        mPreviewImage = new CircleImageView(getContext());
        LayoutParams previewParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        previewParams.gravity = Gravity.CENTER;
        mPreviewImage.setBorderSize(3);
        mPreviewImage.setBorderColor(getResources().getColor(R.color.green_main));
        mPreviewImage.setId(R.id.preview_image);
        addView(mPreviewImage, previewParams);

        mProgressBar = new GreenProgressBar(getContext());
        LayoutParams progressParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        progressParams.gravity = Gravity.CENTER;
        addView(mProgressBar, progressParams);

        if (mHasPlayButton) {
            int playButtonSize = getResources().getDimensionPixelSize(R.dimen.play_button_size);
            LayoutParams playParams = new LayoutParams(playButtonSize, playButtonSize);
            playParams.gravity = Gravity.CENTER;
            mPlayButton = new ImageButton(getContext());
            mPlayButton.setClickable(false);
            mPlayButton.setBackgroundResource(R.drawable.start_button);
            mPlayButton.setId(R.id.play_button);
            addView(mPlayButton, playParams);
        }
    }

    public void hide() {
        mPreviewImage.setImageDrawable(null);
        mProgressBar.setVisibility(GONE);
        mPreviewImage.setVisibility(GONE);
        if (mPlayButton != null) {
            mPlayButton.setVisibility(GONE);
        }
    }

    public void show(String imageUrl) {
        mPreviewImage.setImageDrawable(null);
        mPreviewImage.setVisibility(VISIBLE);
        if (mPlayButton != null) {
            mPlayButton.setVisibility(VISIBLE);
        }
        ImageLoader.getInstance().displayImage(imageUrl, mPreviewImage, new ProgressedImageLoadingListener(mProgressBar));
    }
}
