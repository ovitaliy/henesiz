package com.henesiz.view.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.R;
import com.humanet.audio.AudioTrack;
import com.henesiz.util.ImageCroper;
import com.henesiz.util.OnCacheUpdateListener;
import com.henesiz.view.CircleImageView;
import com.henesiz.view.CircleLayout;

import java.lang.ref.WeakReference;

/**
 * Created by Владимир on 03.12.2014.
 */
public class AudioItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTextView;
    private AudioTrack mData;
    private OnCacheUpdateListener mCacheListener;

    public AudioItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_audio, this);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);

        mImageView = (CircleImageView) findViewById(R.id.audio_item_image);
        mTextView = (TextView) findViewById(R.id.audio_item_title);

        int w = getW() - context.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border) * 2;
        mImageView.getLayoutParams().width = w;
        mImageView.getLayoutParams().height = w;

    }

    public void setOnCacheUpdateListener(OnCacheUpdateListener listener) {
        mCacheListener = listener;
    }

    public void setData(AudioTrack data) {
        mData = data;
        String title = mData.getTitle();
        if (title.lastIndexOf(".") > 0)
            title = title.substring(0, title.lastIndexOf("."));
        mTextView.setText(title);
        setImage();
    }

    public void setImage() {
//        Bitmap bitmap = getBaseImage(getContext());
        mImageView.setImageResource(R.drawable.icon_select_track);
    }

    private static WeakReference<Bitmap> sBitmapWeakReference;

    private synchronized static Bitmap getBaseImage(Context context) {
        Bitmap b = null;
        if (sBitmapWeakReference != null) {
            b = sBitmapWeakReference.get();
        }

        if (b == null) {
            b = BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_select_preview);
            b = Bitmap.createScaledBitmap(b, App.IMAGE_SMALL_WIDTH, App.IMAGE_SMALL_WIDTH, false);
            b = ImageCroper.getCircularBitmap(b, App.IMAGE_BORDER);
            sBitmapWeakReference = new WeakReference<>(b);
        }
        return b;
    }
}
