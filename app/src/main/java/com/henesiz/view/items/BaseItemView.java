package com.henesiz.view.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.model.SimpleListItem;
import com.henesiz.util.Utils;
import com.henesiz.view.CircleImageView;
import com.henesiz.view.CircleLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;

/**
 * Created by ovitali on 02.02.2015.
 */
public class BaseItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTextView;

    public BaseItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_audio, this);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);

        mImageView = (CircleImageView) findViewById(R.id.audio_item_image);
        mTextView = (TextView) findViewById(R.id.audio_item_title);

        mImageView.getLayoutParams().width = App.IMAGE_SMALL_WIDTH;
        mImageView.getLayoutParams().height = App.IMAGE_SMALL_WIDTH;
    }

    public void setData(SimpleListItem data) {
        if (data != null) {
            String title = data.getTitle();
            mTextView.setText(Utils.splitName(title));
            mTextView.invalidate();

            ImageLoader.getInstance().displayImage(data.getImage(), mImageView);
        }
    }

    //-------------
}
