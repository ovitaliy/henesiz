package com.henesiz.view.items;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.henesiz.R;
import com.henesiz.model.Frame;
import com.henesiz.util.BitmapDecoder;
import com.henesiz.util.ImageCroper;
import com.henesiz.util.OnCacheUpdateListener;
import com.henesiz.view.CircleImageView;
import com.henesiz.view.CircleLayout;

/**
 * Created by Владимир on 03.12.2014.
 */
public class FrameItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private Frame mData;
    private OnCacheUpdateListener mCacheListener;

    public FrameItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_frame, this);
        mImageView = (CircleImageView) findViewById(R.id.frame_item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));
    }

    public void setData(Frame data) {
        mData = data;
        setBitmap();
    }

    public void setOnCacheUpdateListener(OnCacheUpdateListener listener) {
        mCacheListener = listener;
    }

    private void setBitmap() {
        LoadImageTask task = new LoadImageTask(getContext(), mImageView, mData.getImagePath(), mCacheListener);
        task.executeOnExecutor(LoadImageTask.THREAD_POOL_EXECUTOR);
    }

    private static class LoadImageTask extends AsyncTask<Void, Void, Bitmap> {

        private CircleImageView mImageView;
        private String mImagePath;
        private Context mContext;

        OnCacheUpdateListener mCacheListener;

        private boolean fromCache = true;

        LoadImageTask(Context context, CircleImageView imageView, String imagePath, OnCacheUpdateListener cacheListener) {
            mImageView = imageView;
            mImagePath = imagePath;
            mContext = context;
            mCacheListener = cacheListener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            int w = getW() - mContext.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border) * 2;
            mImageView.getLayoutParams().width = w;
            mImageView.getLayoutParams().height = w;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            Bitmap bitmap = mCacheListener.getCachedBitmap(mImagePath);
            // int size = getW() - mContext.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border) * 4;
            if (bitmap == null) {
                fromCache = false;
                int camId = 0;
                int border = mContext.getResources().getDimensionPixelSize(R.dimen.stream_item_small_border);

                if (!TextUtils.isEmpty(mImagePath)) {
                    bitmap = BitmapDecoder.createSquareBitmap(mImagePath, 100, camId, false);
                }

                if (bitmap != null) {
                    bitmap = ImageCroper.getCircularBitmap(bitmap, border, mContext.getResources().getColor(R.color.green_main));
                }

            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (fromCache) {
                if (mImageView != null && bitmap != null && !isCancelled()) {
                    mImageView.setImageBitmapWithoutRounding(bitmap);
                    mCacheListener.onCacheUpdated(mImagePath, bitmap);
                }
            } else {
                mImageView.setImageBitmapWithoutRounding(bitmap);
            }
        }
    }
}
