package com.henesiz.view.items;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.view.CircleImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Inteza23 on 17.12.2014.
 */
public class RatingItemView extends LinearLayout {

    private TextView mNameView;
    private TextView mLocationView;
    private CircleImageView mAvatarView;
    private TextView mRatingView;


    public RatingItemView(Context context, boolean isMy) {
        super(context);

        if (isMy) {
            inflate(context, R.layout.item_rating_my, this);
        } else {
            inflate(context, R.layout.item_rating_user, this);
        }

        mAvatarView = (CircleImageView) findViewById(R.id.avatar);
        mNameView = (TextView) findViewById(R.id.name);
        mLocationView = (TextView) findViewById(R.id.location);
        mRatingView = (TextView) findViewById(R.id.rating);
    }

    public void setLocation(String location) {
        mLocationView.setText(Html.fromHtml(location));
    }

    public void setRating(String rating) {
        mRatingView.setText(rating);
    }

    public void setName(String name) {
        mNameView.setText(Html.fromHtml(name));
    }

    public void setAvatar(String photo) {
        mAvatarView.setImageDrawable(null);
        if (!TextUtils.isEmpty(photo)) {
            ImageLoader.getInstance().displayImage(photo, mAvatarView);
        }
    }
}
