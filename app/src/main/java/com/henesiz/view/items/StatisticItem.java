package com.henesiz.view.items;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.henesiz.App;
import com.henesiz.R;
import com.henesiz.view.CirclePickerItem;
import com.henesiz.view.EditableCirclePickerItem;

/**
 * Created by ovitali on 09.02.2015.
 */
public class StatisticItem implements EditableCirclePickerItem {
    private int mId;
    private String mTitle;
    private Drawable mDrawable;

    public StatisticItem(int id, CirclePickerItem item) {
        mTitle = item.getTitle();
        mId = id;
        mDrawable = item.getDrawable();
    }

    public StatisticItem(int id, String title) {
        mTitle = title;
        mId = id;
    }

    public StatisticItem(int id, String title, int drawableResId) {
        mTitle = title;
        mId = id;

        mDrawable = ContextCompat.getDrawable(App.getInstance(), drawableResId);
    }

    public int getId() {
        return mId;
    }

    public Drawable getDrawable() {
        if (mDrawable == null) {
            return ContextCompat.getDrawable(App.getInstance(), R.drawable.beach);
        }
        return mDrawable;
    }

    public String getTitle() {
        return mTitle;
    }

    @Override
    public boolean isOtherOption() {
        return false;
    }


    public void setTitle(String title) {
        mTitle = title;
    }

}
