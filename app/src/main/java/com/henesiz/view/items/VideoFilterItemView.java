package com.henesiz.view.items;

import android.content.Context;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.henesiz.R;
import com.henesiz.util.FilePathHelper;
import com.henesiz.util.OnCacheUpdateListener;
import com.henesiz.view.CircleImageView;
import com.henesiz.view.CircleLayout;
import com.humanet.filters.videofilter.IFilter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Владимир on 03.12.2014.
 */
public class VideoFilterItemView extends CircleLayout.ItemWrapper {

    private CircleImageView mImageView;
    private TextView mTitleView;
    private IFilter mData;

    public VideoFilterItemView(Context context) {
        super(context);
        inflate(context, R.layout.item_filter, this);

        setGravity(Gravity.CENTER_HORIZONTAL);
        setOrientation(LinearLayout.VERTICAL);

        mImageView = (CircleImageView) findViewById(R.id.filter_item_image);
        mImageView.setBackgroundResource(R.drawable.circle_bg);
        mImageView.setBorderSize(getResources().getDimensionPixelOffset(R.dimen.stream_item_small_border));

        mTitleView = (TextView) findViewById(R.id.filter_item_title);
    }


    public void setData(String image, IFilter data) {
        super.setData(data);
        mData = data;
        String title = data.getTitle();
        if (title.contains("Base")) {
            title = getContext().getString(R.string.video_recording_no_filter);
        }
        mTitleView.setText(title);

        String imagePath = "file://" + FilePathHelper.getFilteredImagePreview(data.getTitle()).getAbsolutePath();


        ImageLoader.getInstance().displayImage(imagePath,
                mImageView,
                DisplayImageOptions.createSimple());
    }
}
