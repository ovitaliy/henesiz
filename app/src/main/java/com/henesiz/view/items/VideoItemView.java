package com.henesiz.view.items;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.henesiz.App;
import com.henesiz.AppUser;
import com.henesiz.Const;
import com.henesiz.R;
import com.henesiz.db.ContentDescriptor;
import com.henesiz.enums.VideoType;
import com.henesiz.listener.OnUpdateVideoListener;
import com.henesiz.listener.OnViewProfileListener;
import com.henesiz.model.Video;
import com.henesiz.util.Utils;
import com.henesiz.view.CommentButton;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Владимир on 26.11.2014.
 */
public class VideoItemView extends RelativeLayout implements View.OnClickListener, Const {
    private ImageView mImageView;
    private TextView mTagsTextView;
    private TextView mUserNameTextView;
    private TextView mLocalityTextView;
    private ImageView mLikesTextView;
    private TextView mWatchesTextView;
    private TextView mAnswersTextView;
    private Button mVideoImageView;
    private ImageButton mStartButton;
    private CommentButton mCommentsButton;

    private Button mMoreButton;

    private TextView mCreatedTextView;

    private TextView mCrowdEarn;
    private TextView mCrowdCost;
    private TextView mCrowdMoncoin;

    private RelativeLayout mRelativeLayout;

    private Context mContext;
    private Video mData;
    private Video mVideoParent;
    private WeakReference<OnVideoChooseListener> mChooseListener;
    private WeakReference<OnCommentListener> mCommentListener;
    private WeakReference<OnMoreListener> mOnMoreListener;
    private WeakReference<OnViewProfileListener> mOnViewProfileListener;
    private WeakReference<OnUpdateVideoListener> mOnUpdateVideoListener;
    private VideoType mVideoType;

    private int mCurrentUserId;

    public VideoItemView(Context context, int currentUserId, VideoType videoType) {
        super(context);
        inflate(context, R.layout.item_video_list, this);
        mCurrentUserId = currentUserId;
        mContext = context;
        mVideoType = videoType;

        mImageView = (ImageView) findViewById(R.id.stream_item_image_view);
        mTagsTextView = (TextView) findViewById(R.id.stream_item_tags);
        mCrowdEarn = (TextView) findViewById(R.id.stream_item_crowd_earn);
        mCrowdCost = (TextView) findViewById(R.id.stream_item_crowd_cost);
        mCrowdMoncoin = (TextView) findViewById(R.id.stream_item_crowd_moncoin);
        mUserNameTextView = (TextView) findViewById(R.id.stream_item_user_name);
        mLocalityTextView = (TextView) findViewById(R.id.stream_item_locality);
        mLikesTextView = (ImageView) findViewById(R.id.stream_item_likes);
        mWatchesTextView = (TextView) findViewById(R.id.stream_item_watches);
        mAnswersTextView = (TextView) findViewById(R.id.stream_item_answers);
        mVideoImageView = (Button) findViewById(R.id.stream_item_image);
        mStartButton = (ImageButton) findViewById(R.id.stream_item_start_button);
        mCommentsButton = (CommentButton) findViewById(R.id.stream_item_comment);

        mCreatedTextView = (TextView) findViewById(R.id.stream_item_created);

        mMoreButton = (Button) findViewById(R.id.stream_item_more);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.stream_item_image_container);
        int w = App.WIDTH - App.MARGIN * 2;
        mRelativeLayout.getLayoutParams().width = w;
        mRelativeLayout.getLayoutParams().height = w;

        mStartButton.setOnClickListener(this);
        mVideoImageView.setOnClickListener(this);
        mCommentsButton.setOnClickListener(this);
        mMoreButton.setOnClickListener(this);
        mUserNameTextView.setOnClickListener(this);

        mLikesTextView.setOnClickListener(this);

        initBackgroundImage();
    }

    public void initBackgroundImage() {
        View videoContainer = findViewById(R.id.video_overflow);
        videoContainer.setBackgroundDrawable(new BitmapDrawable(getResources(), getHoledBackground(getContext())));
    }

    public void setData(Video data, Video videoParent) {
        mData = data;
        mVideoParent = videoParent;

        View videoView = mRelativeLayout.findViewById(R.id.stream_item_video_view);
        if (videoView != null) {
            mRelativeLayout.removeView(videoView);
        }

        //setting image
        mImageView.setImageDrawable(null);
        ImageLoader.getInstance().displayImage(mData.getThumbUrl(), mImageView);


        String userName = mData.getAuthorName();
        if (userName != null) {
            String name = Utils.splitName(userName);
            mUserNameTextView.setText(name);
        }

        mWatchesTextView.setText(String.valueOf(mData.getViews()));
        mAnswersTextView.setText(String.valueOf(mData.getReplays()));

        setMark(mData.getMyMark());

        if (!TextUtils.isEmpty(mData.getTitle())) {
            mTagsTextView.setText(mData.getTitle());
        } else {
            mTagsTextView.setText("");
        }

        Calendar calVideo = Calendar.getInstance();
        calVideo.setTimeInMillis(data.getCreatedAt());

        Calendar calCurrent = Calendar.getInstance();

        SimpleDateFormat format;
        if (calVideo.get(Calendar.DAY_OF_YEAR) < calCurrent.get(Calendar.DAY_OF_YEAR)) {
            format = Const.DAY_MONTH_YEAR_FORMAT;
        } else {
            format = Const.HOUR_MINUTE_FORMAT;
        }

        mCreatedTextView.setText(format.format(calVideo.getTime()));

        mCreatedTextView.setVisibility(VISIBLE);
        mCrowdEarn.setVisibility(View.GONE);
        mCrowdCost.setVisibility(View.GONE);
        mCrowdMoncoin.setVisibility(View.GONE);

        mVideoImageView.setEnabled(true);
        if (mVideoType != null) {
            mVideoImageView.setVisibility(View.VISIBLE);
            mVideoImageView.setBackground(ContextCompat.getDrawable(App.getInstance(), R.drawable.pay));

            switch (mVideoType) {
                case CROWD_ASK:
                    mCreatedTextView.setVisibility(GONE);
                    mCrowdEarn.setVisibility(View.VISIBLE);
                    mCrowdCost.setVisibility(View.VISIBLE);
                    mCrowdMoncoin.setVisibility(View.VISIBLE);
                    mCrowdEarn.setText(mData.getEarn() + "/");
                    mCrowdCost.setText(String.valueOf(mData.getCost()));

                    if (mData.getAuthorId() == AppUser.getUid()) {
                        mVideoImageView.setVisibility(GONE);
                    } else if (mData.getEarn() >= mData.getCost()) {
                        mVideoImageView.setVisibility(VISIBLE);
                        mVideoImageView.setBackground(ContextCompat.getDrawable(App.getInstance(), R.drawable.pay_disabled));
                        mVideoImageView.setEnabled(false);
                        mVideoImageView.setTextColor(Color.WHITE);
                    }

                    mVideoImageView.setText(String.valueOf(Const.CROWD_SINGLE_AMOUNT));
                    break;
                case CROWD_GIVE:
                    if (mData.getAuthorId() == AppUser.getUid()) {
                        mVideoImageView.setVisibility(GONE);
                        mCrowdCost.setVisibility(View.GONE);
                    } else {
                        mVideoImageView.setVisibility(VISIBLE);
                        mCrowdCost.setVisibility(View.VISIBLE);
                        mVideoImageView.setText(String.valueOf(0));
                    }
                    if (!mData.isAvailable()) {
                        mVideoImageView.setEnabled(false);
                        mVideoImageView.setBackground(ContextCompat.getDrawable(App.getInstance(), R.drawable.pay_disabled));
                        mVideoImageView.setTextColor(Color.WHITE);
                    }
                    break;
                default:
                    mVideoImageView.setVisibility(GONE);
            }
        } else {
            mVideoImageView.setVisibility(GONE);
        }

        mCommentsButton.setCount(mData.getCommentsCount());

        mImageView.setVisibility(View.VISIBLE);
        mStartButton.setVisibility(View.VISIBLE);

        String countryTitle = mData.getCountry();
        String cityTitle = mData.getCity();
        String locality = countryTitle;
        if (!TextUtils.isEmpty(countryTitle) && !TextUtils.isEmpty(cityTitle)) {
            locality += "<br>";
        }
        locality += cityTitle;
        mLocalityTextView.setText(Html.fromHtml(locality));
    }

    private void setMark(int mark) {
        int markRes;
        if (mark <= 0) {
            markRes = R.drawable.button_like_empty;
        } else {
            markRes = R.drawable.button_like_filled;
        }
        mLikesTextView.setImageResource(markRes);
    }

    private boolean isPayType() {
        if (mVideoType != null) {
            switch (mVideoType) {
                case CROWD_ASK:
                case CROWD_GIVE:
                    return true;
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.stream_item_start_button:
                if (mChooseListener != null) {
                    OnVideoChooseListener listener = mChooseListener.get();
                    if (listener != null) {
                        listener.onVideoChosen(mData);
                    }
                }
                break;
            case R.id.stream_item_user_name:
                if (mOnViewProfileListener != null) {
                    OnViewProfileListener listener = mOnViewProfileListener.get();
                    if (listener != null) {
                        listener.onViewProfile(mData.getAuthorId());
                    }
                }
                break;
            case R.id.stream_item_image:
                if (isPayType()) {
                    if (mOnUpdateVideoListener != null) {
                        OnUpdateVideoListener listener = mOnUpdateVideoListener.get();
                        if (listener != null) {
                            mVideoImageView.setEnabled(false);
                            mVideoImageView.setBackgroundResource(R.drawable.pay_disabled);
                            mVideoImageView.setTextColor(Color.WHITE);
                            listener.onVideoPay(mVideoType, mData.getVideoId());
                        }
                    }
                }
                break;
            case R.id.stream_item_comment:
                if (mCommentListener != null) {
                    OnCommentListener listener = mCommentListener.get();
                    if (listener != null) {
                        listener.onShowComment(mData);
                    }
                }
                break;
            case R.id.stream_item_more:
                if (mOnMoreListener != null) {
                    OnMoreListener listener = mOnMoreListener.get();
                    if (listener != null) {
                        listener.onMore(mData, mVideoParent);
                    }
                }
                break;
            case R.id.stream_item_likes:
                like();
                break;
        }
    }

    public void like() {

        int currentMark = mData.getMyMark();

        final int mark = currentMark <= 0 ? 1 : 0;

        int likes = mData.getLikes();
        if (mark == 1) {
            mData.setLikes(++likes);
        } else {
            mData.setLikes(--likes);
        }

        ContentValues contentValues = new ContentValues(2);
        contentValues.put(ContentDescriptor.Videos.Cols.LIKES, mData.getLikes());
        contentValues.put(ContentDescriptor.Videos.Cols.MY_MARK, mark);

        getContext().getContentResolver().update(
                ContentDescriptor.Videos.URI,
                contentValues,
                ContentDescriptor.Videos.Cols.ID + " = " + mData.getVideoId(),
                null);

        mData.setMyMark(mark);
        setMark(mark);

//        mLikesTextView.setText(String.valueOf(mData.getLikes()));

        if (mOnUpdateVideoListener != null) {
            OnUpdateVideoListener listener = mOnUpdateVideoListener.get();
            if (listener != null) {
                listener.onVideoMark(mData.getVideoId(), mark);
            }
        }
    }

    public void setOnVideoChooseListener(OnVideoChooseListener listener) {
        mChooseListener = new WeakReference<>(listener);
    }

    public void setOnCommentListener(OnCommentListener listener) {
        mCommentListener = new WeakReference<>(listener);
        ;
    }

    public void setOnMoreListener(OnMoreListener listener) {
        mOnMoreListener = new WeakReference<>(listener);
        ;
    }

    public void setOnViewProfileListener(OnViewProfileListener listener) {
        mOnViewProfileListener = new WeakReference<>(listener);
        ;
    }

    public void setOnPayVideoListener(OnUpdateVideoListener listener) {
        mOnUpdateVideoListener = new WeakReference<>(listener);
        ;
    }

    public interface OnVideoChooseListener {
        void onVideoChosen(Video video);
    }

    public interface OnCommentListener {
        void onComment(Video video, String comment);

        void onShowComment(Video video);
    }

    public interface OnMoreListener {
        void onMore(Video video, Video videoParent);

    }

    private static WeakReference<Bitmap> sBitmapWeakReference;

    private static Bitmap getHoledBackground(Context context) {
        Bitmap b = null;
        if (sBitmapWeakReference != null) {
            b = sBitmapWeakReference.get();
        }

        if (b == null) {
            Resources r = context.getResources();
            int w = App.WIDTH - App.MARGIN * 2;
            int color = r.getColor(R.color.white);

            // init background image
            Bitmap bitmap = Bitmap.createBitmap(w, w, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawColor(color);

            //crop hole
            int mBorderSize = r.getDimensionPixelSize(R.dimen.audio_image_border_size);

            w = bitmap.getHeight();

            int radius = w / 2;

            final Paint paint = new Paint();
            final int borderColor = r.getColor(R.color.green_main)/*0xffffffff*/;

            Canvas bitmapCanvas = new Canvas(bitmap);
            paint.setColor(borderColor);
            bitmapCanvas.drawCircle(radius, radius, radius, paint);

            Rect rect = new Rect(0, 0, w, w);

            //result bitmap that we will use to draw
            Bitmap result = Bitmap.createBitmap(w, w, Bitmap.Config.ARGB_4444);

            //canvas where we will draw
            canvas = new Canvas(result);

            //init canvas
            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);

            //draw our circle
            canvas.drawCircle(radius, radius, radius - mBorderSize, paint);

            //draw background
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.XOR));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            sBitmapWeakReference = new WeakReference<>(result);

            b = result;
        }

        return b;

    }


}
