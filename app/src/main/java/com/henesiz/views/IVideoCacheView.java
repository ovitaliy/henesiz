package com.henesiz.views;

import com.henesiz.model.Video;

import java.util.List;

/**
 * Created by ovi on 16.05.2016.
 */
public interface IVideoCacheView {

   void publishCachingProgress(float progress);
   void publishCachingComplete(boolean success);

}
