package com.henesiz.views;

import com.henesiz.model.Video;

import java.util.List;

/**
 * Created by ovi on 16.05.2016.
 */
public interface IVideoListView {
    void addVideos(List<Video> videoList);

    long getScreenId();

    long getLastVideoId();
}
